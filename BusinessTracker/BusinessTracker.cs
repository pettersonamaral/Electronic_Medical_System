﻿/*****************************************************************************************************
* FILE : 		BusinessTracker.cs
* PROJECT : 	SQ-II Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2018-Apr-01

* DESCRIPTION/Requirements: 

This class provides all functionality to BusinessTracker

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using Demographics;
using Billing;
using DBManager;
using System.Data;
using System.Reflection;
using Connection;
using Support;
namespace BusinessTracker
{
    public partial class BusinessTracker : IDemographics, IBilling
    {
        #region attributes
        private static volatile BusinessTracker instance;                       //Implementation Strategy - Multithreaded Singleton
        private static object syncRoot = new Object();                          //Implementation Strategy - Multithreaded Singleton
        private static ClientConnection hcnServer = ClientConnection.Instance;
        private static Logging logging = new Logging();


        #endregion


        #region Demographics ErrorCodes / Exception Errors
        // Patient Exceptions - 3500 - 3599
        public const int INVALIDPATIENTOBJECT   = -3500;
        public const int NULLADDRESSOBJECT      = -3501;
        public const int HOHANDADDRESSNULL      = -3502;
        public const int FAILTOINSERTNEWADDRESS = -3503;
        public const int FAILTOINSERTNEWPATIENT = -3504;

        // Patient Exceptions - 3600 - 3699
        public const int INVALIDADDRESSOBJECT   = -3600;

        // Appointment Exceptions - 3700 - 3799
        public const int INVALIDAPPOINTMENTBJECT = -3700;
        public const int NULLAPPOINTMENTOBJECT   = -3701;
        public const int INSERTAPPOINTMENTOBJECT = -3702;
        #endregion


        #region constants
        private const Int32 SUCCESS_GENERIC_RETURN = 0;


        #endregion


        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        private BusinessTracker() { }

        #endregion


        #region Instance
        // --- Starts Singleton Pattern
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        public static BusinessTracker Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new BusinessTracker();
                    }
                }

                return instance;
            }
        }
        #endregion


        /// <summary>
        /// Add an appointment. Returns 0 if it was Done OK.
        ///This method does not add Patient and Address objects.
        ///If there is a new Patient either with a new address or with HoH, the function should return an error "INVALIDPATIENTOBJECT"
        ///To add a new patient and a new address, to it by using the method AddPatient.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public int AddAppointment(Appointment obj)
        {
            //Method variables
            Int32 retCode           = -1;
            DateTime myDayAndTime   = DateTime.MinValue;
            Int32   myPatientID01   = 0, myPatientID02 = 0;

            try
            {
                #region initialization
                //If validation get a problem, return a error number (exit)
                if (obj.IsValid == false)                       { retCode = INVALIDAPPOINTMENTBJECT; return retCode; }
                if (obj.FirstPatient == null && obj == null)    { retCode = NULLAPPOINTMENTOBJECT; return retCode; }

                //Get attribuits velues
                myDayAndTime    = obj.GetDayAndTime();
                myPatientID01     = obj.FirstPatient.GetPatientID();


                //To do more tests
                if (obj.FirstPatient.PatientID < 1)     { retCode = INVALIDPATIENTOBJECT; return retCode; }

                if (obj.SecondPatient != null)
                {
                    if (obj.SecondPatient.PatientID < 1)
                    {
                        retCode = INVALIDPATIENTOBJECT; return retCode;
                    }
                    else { myPatientID02 = obj.SecondPatient.GetPatientID(); }
                }

                //The tests about the existence of the patient1 and patient2 will be happening in the DBManager library.


                #endregion

                #region Bussiness actions
                //----------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------
                if (DBSupport.InsertANewAppointment(myDayAndTime, myPatientID01, myPatientID02) != 1)
                {
                    retCode = INSERTAPPOINTMENTOBJECT;
                    return retCode;
                }
                else
                {
                    retCode = 0;
                    SendToLog(new Exception("Insert a new Appointment - [Successful].")); 
                }

                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Insert a new Appointment - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }


        //public int AddBill(Bill obj)
        //{
        //    throw new NotImplementedException();
        //}


        /// <summary>
        /// Add a patient. Returns 0 if it was Done OK.
        /// Add a patient always have to have an address related,
        /// either by the head of house or attached normally. 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public int AddPatient(Patient obj)
        {
            //Method variables
            Int32 retCode = SUCCESS_GENERIC_RETURN;
            Boolean isHoHFilled = false;
            Patient headOfHousePatient = null;

            //Patient variables
            Int32 patientID = 0;
            String HCN = "";
            String lName = " ";
            String fName = "";
            Char mInitial = ' ';
            DateTime dateOfBirth = DateTime.MinValue;
            String sex = "";
            String headOfHouse = "";

            //Address variables
            Int32 addressID = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";


            try
            {
                #region initialization
                //If validation get a problem, return a error number (exit)
                retCode = obj.ValidatePatient();
                if (retCode != 0)                           { retCode = INVALIDPATIENTOBJECT; return retCode; }                                                                                            
                if (obj.Address == null && obj == null)     { retCode = NULLADDRESSOBJECT; return retCode; }


                //Filling Patient attributes with data from the received obj
                patientID   = obj.PatientID;
                HCN         = obj.GethCN();
                lName       = obj.GetLname();
                fName       = obj.GetFname();
                mInitial    = obj.GetMinitial();
                dateOfBirth = obj.DateOfBirth;
                sex         = obj.GetSex();
                headOfHouse = obj.GetHeadOfHouse();               

                //Filling Address attributes with data from the received obj
                addressID   = obj.Address.GetAdressID();
                adressLine1 = obj.Address.GetAddressLineOne();
                adressLine2 = obj.Address.GetAddressLineTwo();
                city        = obj.Address.GetCity();
                province    = obj.Address.GetProvince();
                phoneNumber = obj.Address.GetPhoneNumber();

                //To do more tests
                if (headOfHouse != "")                  { isHoHFilled = true; }  //Check if HoH is filled
                if (addressID == 0) { addressID = DBSupport.GetNextAddressID(); }//If there is no addressID get the next AddressID available.
                #endregion


                #region Bussiness actions
                //----------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------
                //Check if HoH is NULL, then the obj have to have an Address, otherwise return an error and exit
                if ((isHoHFilled == false || headOfHouse == null))                    
                {
                    if ((obj.Address == null || adressLine1 == null || adressLine1 == "")) //Check If also address is null, if it is Exit
                    { 
                        retCode = HOHANDADDRESSNULL;
                        return retCode;
                    }
                    else
                    {
                        //Call Insert Address and then Patient
                        if (DBSupport.InsertANewAddress(adressLine1, adressLine2, city, province, phoneNumber, HCN) != 1)
                        {                                
                            retCode = FAILTOINSERTNEWADDRESS;
                            return retCode;
                        }
                        else if (DBSupport.InsertANewAPatient( HCN, lName, fName, mInitial, dateOfBirth, sex, headOfHouse, addressID) != 1)
                        {
                            retCode = FAILTOINSERTNEWPATIENT; 
                            return retCode;
                        }
                    }
                }
                else  //If HoH is filled, then check if there is a patient with that HoH number
                {
                    headOfHousePatient = FindPatient(headOfHouse);     //Looking for another patient base on this patient HoH ID 

                    if (headOfHousePatient == null || headOfHousePatient.ValidatePatient() != 0) //Check if myPatient01 is null or invalid
                    {
                        retCode = -3510;                        //If it get error exit because thare is not patient related with that Hoh
                        return retCode;
                    }
                    else  //Ignore any address of this object and add the same addressID of the headOfHousePatient
                    {
                        //Add the Patient, but uses the address of the headOfHousePatient
                        if (DBSupport.InsertANewAPatient(HCN, lName, fName, mInitial, dateOfBirth, sex, headOfHouse, headOfHousePatient.Address.GetAdressID()) != 1)
                        {
                            retCode = -3506; 
                            return retCode;
                        }
                    }
                }

                if (retCode == 0) { SendToLog(new Exception("Insert a new Patient - [Successful].")); } 

                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Insert a new Patient - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }


        //public int AddServiceList(int billID, ServiceList obj)
        //{
        //    throw new NotImplementedException();
        //}


        /// <summary>
        /// Find a address by address id. Returns either a full Address obj or null (if it was not fund).
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public Address FindAddress(int addressID)
        {
            //Method variables
            Address retCode = null;
            DataTable dt = new DataTable();

            //Address variables
            Int32 addressID2 = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";

            try
            {
                #region Address
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAddress(addressID);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    addressID2  = Convert.ToInt32(row[0]);
                    adressLine1 = row[1].ToString();
                    adressLine2 = row[2].ToString();
                    city        = row[3].ToString();
                    province    = row[4].ToString();
                    phoneNumber = row[5].ToString();
                }

                //Instantiate a new Patient object and fill it
                retCode = new Address();
                retCode.SetAdressID(addressID2);
                retCode.SetAdressLineOne(adressLine1);
                retCode.SetAdressLineTwo(adressLine2);
                retCode.SetCity(city);
                retCode.SetProvince(province);
                retCode.SetPhoneNumber(phoneNumber);
                #endregion

                //Check if the new data is valid
                if (retCode.ValidateAdress() != 0) { retCode = null; }
                if (retCode != null) { SendToLog(new Exception("FindAddress - [Successful].")); }
            }
            catch (Exception e)
            {
                SendToLog(new Exception("FindAddress - [Failure]."));
                throw e;
            }
            return retCode;
        }


        /// <summary>
        /// Find a Address by HCN number. Returns either a full Address obj or null (if it was not fund).
        /// </summary>
        /// <param name="hcnNumber"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public Address FindAddress(string hcnNumber)
        {
            //Method variables
            Address retCode = null;
            DataTable dt = new DataTable();

            //Address variables
            Int32 addressID2 = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";

            try
            {
                #region Address
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAddress(hcnNumber);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    addressID2  = Convert.ToInt32(row[0]);
                    adressLine1 = row[1].ToString();
                    adressLine2 = row[2].ToString();
                    city        = row[3].ToString();
                    province    = row[4].ToString();
                    phoneNumber = row[5].ToString();
                }

                //Instantiate a new Patient object and fill it
                retCode = new Address();
                retCode.SetAdressID(addressID2);
                retCode.SetAdressLineOne(adressLine1);
                retCode.SetAdressLineTwo(adressLine2);
                retCode.SetCity(city);
                retCode.SetProvince(province);
                retCode.SetPhoneNumber(phoneNumber);
                #endregion

                //Check if the new data is valid
                if (retCode.ValidateAdress() != 0) { retCode = null; }
                if (retCode != null) { SendToLog(new Exception("FindPatient - [Successful].")); }
            }
            catch (Exception e)
            {
                SendToLog(new Exception("FindPatient - [Failure]."));
                throw e;
            }
            return retCode;
        }


        /// <summary>
        /// Find a appointmentID by appointmentID. Returns either a full Appointment obj or null (if it was not fund).
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public Appointment FindAppointment(int appointmentID)
        {
            //Method variables
            Appointment retCode     = null;
            DataTable dt            = new DataTable();

            //Appointment variables
            Int32 myAppointmentID   = appointmentID;
            DateTime myDayAndTime   = DateTime.MinValue;

            //Patient variables
            Int32 patientID         = 0;
            String HCN              = "";
            String lName            = "";
            String fName            = "";
            Char mInitial           = ' ';
            DateTime dateOfBirth    = DateTime.MinValue;
            String sex              = "";
            String headOfHouse      = "";
            Int32 addressID         = 0;

            try
            {
                #region Appointment
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAppointment(appointmentID);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }
                else
                {
                    //Travese the table and grab the data
                    foreach (DataRow row in dt.Rows)
                    {
                        myAppointmentID = Convert.ToInt32(row[0]);
                        myDayAndTime = Convert.ToDateTime(row[1]);
                    }

                    //Instantiate a new Appointment object and fill it
                    retCode = new Appointment();
                    retCode.SetApointmentID(myAppointmentID);
                    retCode.SetDayAndTime(myDayAndTime);
                }
                #endregion


                #region Appoinment Line
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAnAppointmentLine(appointmentID);
                
                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count < 1) { retCode = null; return retCode; }
                else
                {
                    switch (dt.Rows.Count) 
                    {
                        case 1://If has just a row.                             
                            foreach (DataRow row in dt.Rows)
                            {
                                retCode.FirstPatient.SetPatientID(Convert.ToInt32(row["patientID"]));
                                retCode.SecondPatient = null; //Set up the SecondPatient as null
                            }
                            break;                        
                        case 2:
                            retCode.SecondPatient = new Patient();
                            retCode.FirstPatient.SetPatientID(Convert.ToInt32(dt.Rows[0][1]));
                            retCode.SecondPatient.SetPatientID(Convert.ToInt32(dt.Rows[1][1]));
                            break;
                        default:
                            retCode = null;
                            return retCode;
                    }
                }
                #endregion


                #region Patient #1

                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindPatient(retCode.FirstPatient.GetPatientID());

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }
                else
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        patientID = Convert.ToInt32(row[0]);
                        HCN = row[1].ToString();
                        lName = row[2].ToString();
                        fName = row[3].ToString();
                        mInitial = Convert.ToChar(row[4]);
                        dateOfBirth = Convert.ToDateTime(row[5]);
                        sex = row[6].ToString().Trim();
                        headOfHouse = row[7].ToString();
                        addressID = Convert.ToInt32(row[8]);
                    }

                    //Instantiate a new Patient object and fill it
                    retCode.FirstPatient.SetPatientID(patientID);
                    retCode.FirstPatient.SethCN(HCN);
                    retCode.FirstPatient.SetLname(lName);
                    retCode.FirstPatient.SetFname(fName);
                    retCode.FirstPatient.SetMinitial(mInitial);
                    retCode.FirstPatient.DateOfBirth = dateOfBirth;
                    retCode.FirstPatient.SetSex(sex);
                    retCode.FirstPatient.SetHeadOfHouse(headOfHouse);
                    retCode.FirstPatient.Address = FindAddress(addressID);
                }
                #endregion


                #region Patient #2
                //Check if the 
                if (retCode.SecondPatient != null)
                {
                    //Call the finder the receive a Datatable obj
                    dt = DBSupport.FindPatient(retCode.SecondPatient.GetPatientID());

                    //Check either if there is data in the datatable or return null
                    if (dt.Rows.Count != 1) { retCode = null; return retCode; }
                    else
                    {
                        //Travese the table and grab the data
                        foreach (DataRow row in dt.Rows)
                        {
                            patientID = Convert.ToInt32(row[0]);
                            HCN = row[1].ToString();
                            lName = row[2].ToString();
                            fName = row[3].ToString();
                            mInitial = Convert.ToChar(row[4]);
                            dateOfBirth = Convert.ToDateTime(row[5]);
                            sex = row[6].ToString().Trim();
                            headOfHouse = row[7].ToString();
                            addressID = Convert.ToInt32(row[8]);
                        }

                        //Instantiate a new Patient object and fill it
                        retCode.SecondPatient.SetPatientID(patientID);
                        retCode.SecondPatient.SethCN(HCN);
                        retCode.SecondPatient.SetLname(lName);
                        retCode.SecondPatient.SetFname(fName);
                        retCode.SecondPatient.SetMinitial(mInitial);
                        retCode.SecondPatient.DateOfBirth = dateOfBirth;
                        retCode.SecondPatient.SetSex(sex);
                        retCode.SecondPatient.SetHeadOfHouse(headOfHouse);
                        retCode.SecondPatient.Address = FindAddress(addressID);
                    }
                }
                #endregion

                //Check if the new data is valid
                if (retCode.ValidateAppointment() != 0) { retCode = null; }
                if (retCode != null) { SendToLog(new Exception("FindAppointment - [Successful].")); }
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Find an Appointment - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }

        /// <summary>
        /// Find an appointments. Returns a filled List<Appointment> if it was Done OK, or a null List<Appointment> if got any issue. 
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public List<Appointment> FindAppointments(int patientID)
        {
            //Method variables
            List<Appointment> retCode = new List<Appointment>();
            Appointment myAppointment = null;
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();


            //Appointment variables
            Int32 myAppointmentID = 0;
            DateTime myDayAndTime = DateTime.MinValue;

            //Patient variables
            Int32 myPatientID = 0;
            String HCN = "";
            String lName = "";
            String fName = "";
            Char mInitial = ' ';
            DateTime dateOfBirth = DateTime.MinValue;
            String sex = "";
            String headOfHouse = "";
            Int32 addressID = 0;

            try
            {
                #region Appointment
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAppointments(patientID);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count == 0) { retCode = null; return retCode; }
                else
                {
                    //Travese the table and grab the data
                    foreach (DataRow row in dt.Rows)
                    {
                        myAppointmentID = Convert.ToInt32(row[0]);
                        myDayAndTime = Convert.ToDateTime(row[1]);

                        //Instantiate a new Appointment object and fill it
                        myAppointment = new Appointment();
                        myAppointment.SetApointmentID(myAppointmentID);
                        myAppointment.SetDayAndTime(myDayAndTime);

                        #region Appoinment Line
                        //Call the finder the receive a Datatable obj
                        dt2 = DBSupport.FindAnAppointmentLine(myAppointment.GetAppointmentID());

                        //Check either if there is data in the datatable or return null
                        if (dt2.Rows.Count < 1) { retCode = null; return retCode; }
                        else
                        {
                            switch (dt2.Rows.Count)
                            {
                                case 1://If has just a row.                             
                                    foreach (DataRow row2 in dt2.Rows)
                                    {
                                        myAppointment.FirstPatient.SetPatientID(Convert.ToInt32(row2["patientID"]));
                                        myAppointment.SecondPatient = null; //Set up the SecondPatient as null
                                    }
                                    break;
                                case 2:
                                    myAppointment.SecondPatient = new Patient();
                                    myAppointment.FirstPatient.SetPatientID(Convert.ToInt32(dt2.Rows[0][1]));
                                    myAppointment.SecondPatient.SetPatientID(Convert.ToInt32(dt2.Rows[1][1]));
                                    break;
                                default:
                                    retCode = null;
                                    return retCode;
                            }
                        }
                        #endregion


                        #region Patient #1

                        //Call the finder the receive a Datatable obj
                        dt3 = DBSupport.FindPatient(myAppointment.FirstPatient.GetPatientID());

                        //Check either if there is data in the datatable or return null
                        if (dt3.Rows.Count != 1) { retCode = null; return retCode; }
                        else
                        {
                            foreach (DataRow row3 in dt3.Rows)
                            {
                                myPatientID = Convert.ToInt32(row3[0]);
                                HCN = row3[1].ToString();
                                lName = row3[2].ToString();
                                fName = row3[3].ToString();
                                mInitial = Convert.ToChar(row3[4]);
                                dateOfBirth = Convert.ToDateTime(row3[5]);
                                sex = row3[6].ToString().Trim();
                                headOfHouse = row3[7].ToString();
                                addressID = Convert.ToInt32(row3[8]);
                            }

                            //Instantiate a new Patient object and fill it
                            myAppointment.FirstPatient.SetPatientID(myPatientID);
                            myAppointment.FirstPatient.SethCN(HCN);
                            myAppointment.FirstPatient.SetLname(lName);
                            myAppointment.FirstPatient.SetFname(fName);
                            myAppointment.FirstPatient.SetMinitial(mInitial);
                            myAppointment.FirstPatient.DateOfBirth = dateOfBirth;
                            myAppointment.FirstPatient.SetSex(sex);
                            myAppointment.FirstPatient.SetHeadOfHouse(headOfHouse);
                            myAppointment.FirstPatient.Address = FindAddress(addressID);
                        }
                        #endregion


                        #region Patient #2
                        //Check if the 
                        if (myAppointment.SecondPatient != null)
                        {
                            //Call the finder the receive a Datatable obj
                            dt4 = DBSupport.FindPatient(myAppointment.SecondPatient.GetPatientID());

                            //Check either if there is data in the datatable or return null
                            if (dt4.Rows.Count != 1) { myAppointment = null; return retCode; }
                            else
                            {
                                //Travese the table and grab the data
                                foreach (DataRow row4 in dt4.Rows)
                                {
                                    myPatientID = Convert.ToInt32(row4[0]);
                                    HCN = row4[1].ToString();
                                    lName = row4[2].ToString();
                                    fName = row4[3].ToString();
                                    mInitial = Convert.ToChar(row4[4]);
                                    dateOfBirth = Convert.ToDateTime(row4[5]);
                                    sex = row4[6].ToString().Trim();
                                    headOfHouse = row4[7].ToString();
                                    addressID = Convert.ToInt32(row4[8]);
                                }

                                //Instantiate a new Patient object and fill it
                                myAppointment.SecondPatient.SetPatientID(myPatientID);
                                myAppointment.SecondPatient.SethCN(HCN);
                                myAppointment.SecondPatient.SetLname(lName);
                                myAppointment.SecondPatient.SetFname(fName);
                                myAppointment.SecondPatient.SetMinitial(mInitial);
                                myAppointment.SecondPatient.DateOfBirth = dateOfBirth;
                                myAppointment.SecondPatient.SetSex(sex);
                                myAppointment.SecondPatient.SetHeadOfHouse(headOfHouse);
                                myAppointment.SecondPatient.Address = FindAddress(addressID);
                            }
                        }
                        #endregion

                        //Check if the new data is valid
                        if (myAppointment.ValidateAppointment() != 0) { retCode = null; }
                        if (retCode != null)
                        {
                            retCode.Add(myAppointment);
                            SendToLog(new Exception("FindAppointment - [Successful]."));
                        }                        
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Find an Appointment - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }

        /// <summary>
        /// Find an appointments. Returns a filled List<Appointment> if it was Done OK, or a null List<Appointment> if got any issue. 
        /// </summary>
        /// <param name=begin, end></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public List<Appointment> FindAppointments(DateTime begin, DateTime end)
        {
            //Method variables
            List<Appointment> retCode = new List<Appointment>();
            Appointment myAppointment = null;
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();


            //Appointment variables
            Int32 myAppointmentID = 0;
            DateTime myDayAndTime = DateTime.MinValue;

            //Patient variables
            Int32 myPatientID = 0;
            String HCN = "";
            String lName = "";
            String fName = "";
            Char mInitial = ' ';
            DateTime dateOfBirth = DateTime.MinValue;
            String sex = "";
            String headOfHouse = "";
            Int32 addressID = 0;

            try
            {
                #region Appointment
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAppointment(begin, end);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count == 0) { retCode = null; return retCode; }
                else
                {
                    //Travese the table and grab the data
                    foreach (DataRow row in dt.Rows)
                    {
                        myAppointmentID = Convert.ToInt32(row[0]);
                        myDayAndTime = Convert.ToDateTime(row[1]);

                        //Instantiate a new Appointment object and fill it
                        myAppointment = new Appointment();
                        myAppointment.SetApointmentID(myAppointmentID);
                        myAppointment.SetDayAndTime(myDayAndTime);

                        #region Appoinment Line
                        //Call the finder the receive a Datatable obj
                        dt2 = DBSupport.FindAnAppointmentLine(myAppointment.GetAppointmentID());

                        //Check either if there is data in the datatable or return null
                        if (dt2.Rows.Count < 1) { retCode = null; return retCode; }
                        else
                        {
                            switch (dt2.Rows.Count)
                            {
                                case 1://If has just a row.                             
                                    foreach (DataRow row2 in dt2.Rows)
                                    {
                                        myAppointment.FirstPatient.SetPatientID(Convert.ToInt32(row2["patientID"]));
                                        myAppointment.SecondPatient = null; //Set up the SecondPatient as null
                                    }
                                    break;
                                case 2:
                                    myAppointment.SecondPatient = new Patient();
                                    myAppointment.FirstPatient.SetPatientID(Convert.ToInt32(dt2.Rows[0][1]));
                                    myAppointment.SecondPatient.SetPatientID(Convert.ToInt32(dt2.Rows[1][1]));
                                    break;
                                default:
                                    retCode = null;
                                    return retCode;
                            }
                        }
                        #endregion


                        #region Patient #1

                        //Call the finder the receive a Datatable obj
                        dt3 = DBSupport.FindPatient(myAppointment.FirstPatient.GetPatientID());

                        //Check either if there is data in the datatable or return null
                        if (dt3.Rows.Count != 1) { retCode = null; return retCode; }
                        else
                        {
                            foreach (DataRow row3 in dt3.Rows)
                            {
                                myPatientID = Convert.ToInt32(row3[0]);
                                HCN = row3[1].ToString();
                                lName = row3[2].ToString();
                                fName = row3[3].ToString();
                                mInitial = Convert.ToChar(row3[4]);
                                dateOfBirth = Convert.ToDateTime(row3[5]);
                                sex = row3[6].ToString().Trim();
                                headOfHouse = row3[7].ToString();
                                addressID = Convert.ToInt32(row3[8]);
                            }

                            //Instantiate a new Patient object and fill it
                            myAppointment.FirstPatient.SetPatientID(myPatientID);
                            myAppointment.FirstPatient.SethCN(HCN);
                            myAppointment.FirstPatient.SetLname(lName);
                            myAppointment.FirstPatient.SetFname(fName);
                            myAppointment.FirstPatient.SetMinitial(mInitial);
                            myAppointment.FirstPatient.DateOfBirth = dateOfBirth;
                            myAppointment.FirstPatient.SetSex(sex);
                            myAppointment.FirstPatient.SetHeadOfHouse(headOfHouse);
                            myAppointment.FirstPatient.Address = FindAddress(addressID);
                        }
                        #endregion


                        #region Patient #2
                        //Check if the 
                        if (myAppointment.SecondPatient != null)
                        {
                            //Call the finder the receive a Datatable obj
                            dt4 = DBSupport.FindPatient(myAppointment.SecondPatient.GetPatientID());

                            //Check either if there is data in the datatable or return null
                            if (dt4.Rows.Count != 1) { myAppointment = null; return retCode; }
                            else
                            {
                                //Travese the table and grab the data
                                foreach (DataRow row4 in dt4.Rows)
                                {
                                    myPatientID = Convert.ToInt32(row4[0]);
                                    HCN = row4[1].ToString();
                                    lName = row4[2].ToString();
                                    fName = row4[3].ToString();
                                    mInitial = Convert.ToChar(row4[4]);
                                    dateOfBirth = Convert.ToDateTime(row4[5]);
                                    sex = row4[6].ToString().Trim();
                                    headOfHouse = row4[7].ToString();
                                    addressID = Convert.ToInt32(row4[8]);
                                }

                                //Instantiate a new Patient object and fill it
                                myAppointment.SecondPatient.SetPatientID(myPatientID);
                                myAppointment.SecondPatient.SethCN(HCN);
                                myAppointment.SecondPatient.SetLname(lName);
                                myAppointment.SecondPatient.SetFname(fName);
                                myAppointment.SecondPatient.SetMinitial(mInitial);
                                myAppointment.SecondPatient.DateOfBirth = dateOfBirth;
                                myAppointment.SecondPatient.SetSex(sex);
                                myAppointment.SecondPatient.SetHeadOfHouse(headOfHouse);
                                myAppointment.SecondPatient.Address = FindAddress(addressID);
                            }
                        }
                        #endregion

                        //Check if the new data is valid
                        if (myAppointment.ValidateAppointment() != 0) { retCode = null; }
                        if (retCode != null)
                        {
                            retCode.Add(myAppointment);
                            SendToLog(new Exception("FindAppointment - [Successful]."));
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Find an Appointment - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }


        //public Bill FindBill(int patientID, int appointmentID)
        //{
        //    throw new NotImplementedException();
        //}


        /// <summary>
        /// Find a Patient by patientID. Returns either a full Patient obj or null (if it was not fund).
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public Patient FindPatient(int patientID)
        {
            //Method variables
            Patient retCode = null;
            DataTable dt = new DataTable();

            //Patient variables
            String HCN = "";
            String lName = "";
            String fName = "";
            Char mInitial = ' ';
            DateTime dateOfBirth = DateTime.MinValue;
            String sex = "";
            String headOfHouse = "";
            Int32 addressID = 0;

            //Address variables
            Int32 addressID2 = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";

            try
            {
                #region Patient
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindPatient(patientID);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    patientID   = Convert.ToInt32(row[0]);
                    HCN         = row[1].ToString();
                    lName       = row[2].ToString();
                    fName       = row[3].ToString();
                    mInitial    = Convert.ToChar(row[4]);
                    dateOfBirth = Convert.ToDateTime(row[5]);
                    sex         = row[6].ToString().Trim();
                    headOfHouse = row[7].ToString();
                    addressID   = Convert.ToInt32(row[8]);
                }

                //Instantiate a new Patient object and fill it
                retCode = new Patient();
                retCode.SetPatientID(patientID);
                retCode.SethCN(HCN);
                retCode.SetLname(lName);
                retCode.SetFname(fName);
                retCode.SetMinitial(mInitial);
                retCode.DateOfBirth = dateOfBirth;
                retCode.SetSex(sex);
                retCode.SetHeadOfHouse(headOfHouse);
                retCode.Address = FindAddress(addressID);
                #endregion


                #region Address
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAddress(retCode.Address.GetAdressID());

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    addressID2  = Convert.ToInt32(row[0]);
                    adressLine1 = row[1].ToString();
                    adressLine2 = row[2].ToString();
                    city        = row[3].ToString();
                    province    = row[4].ToString();
                    phoneNumber = row[5].ToString();
                }

                //Instantiate a new Patient object and fill it
                retCode.Address.SetAdressID(addressID2);
                retCode.Address.SetAdressLineOne(adressLine1);
                retCode.Address.SetAdressLineTwo(adressLine2);
                retCode.Address.SetCity(city);
                retCode.Address.SetProvince(province);
                retCode.Address.SetPhoneNumber(phoneNumber);
                #endregion

                //Check if the new data is valid
                if (retCode.ValidatePatient() != 0) { retCode = null; }
                if (retCode != null) { SendToLog(new Exception("FindPatient - [Successful].")); }
            }
            catch (Exception e)
            {
                SendToLog(new Exception("FindPatient - [Failure]."));
                throw e;
            }
            return retCode;
        }


        /// <summary>
        /// Find a Patient by HCN number. Returns either a full Patient obj or null (if it was not fund).
        /// </summary>
        /// <param name="hcnNumber"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public Patient FindPatient(string hcnNumber)
        {
            //Method variables
            Patient retCode = null; 
            DataTable dt = new DataTable();

            //Patient variables
            Int32 patientID     = 0;
            String HCN          = "";
            String lName        = "";
            String fName        = "";
            Char mInitial       = ' ';
            DateTime dateOfBirth= DateTime.MinValue;
            String sex          = "";
            String headOfHouse  = "";
            Int32 addressID     = 0;

            //Address variables
            Int32 addressID2    = 0;
            String adressLine1  = "";
            String adressLine2  = "";
            String city         = "";
            String province     = "";
            String phoneNumber  = "";

            try
            {
                #region Patient
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindPatient(hcnNumber);

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    patientID   = Convert.ToInt32(row[0]);
                    HCN         = row[1].ToString();
                    lName       = row[2].ToString();
                    fName       = row[3].ToString();
                    mInitial    = Convert.ToChar(row[4]);
                    dateOfBirth = Convert.ToDateTime(row[5]);
                    sex         = row[6].ToString().Trim();
                    headOfHouse = row[7].ToString();
                    addressID   = Convert.ToInt32(row[8]);
                }

                //Instantiate a new Patient object and fill it
                retCode = new Patient();
                retCode.SetPatientID(patientID);
                retCode.SethCN(HCN);
                retCode.SetLname(lName);
                retCode.SetFname(fName);
                retCode.SetMinitial(mInitial);
                retCode.DateOfBirth = dateOfBirth;
                retCode.SetSex(sex);
                retCode.SetHeadOfHouse(headOfHouse);
                retCode.Address = FindAddress(addressID);
                #endregion


                #region Address
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindAddress(retCode.Address.GetAdressID());

                //Check either if there is data in the datatable or return null
                if (dt.Rows.Count != 1) { retCode = null; return retCode;}

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    addressID2  = Convert.ToInt32(row[0]);
                    adressLine1 = row[1].ToString();
                    adressLine2 = row[2].ToString();
                    city        = row[3].ToString();
                    province    = row[4].ToString();
                    phoneNumber = row[5].ToString();
                }

                //Instantiate a new Patient object and fill it
                retCode.Address.SetAdressID(addressID2);
                retCode.Address.SetAdressLineOne(adressLine1);
                retCode.Address.SetAdressLineTwo(adressLine2);
                retCode.Address.SetCity(city);
                retCode.Address.SetProvince(province);
                retCode.Address.SetPhoneNumber(phoneNumber);
                #endregion


                //Check if the new data is valid
                if (retCode.ValidatePatient() != 0) { retCode = null; }
                if (retCode != null) { SendToLog(new Exception("FindPatient - [Successful].")); }
            }
            catch (Exception e)
            {
                SendToLog(new Exception("FindPatient - [Failure]."));
                throw e;
            }
            return retCode;
        }


        /// <summary>
        /// Find a patient. Returns List<Patient> if it was Done OK, and null if got an issue. 
        /// Find a by obj will use the attributes, patientID and hcn 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public List<Patient> FindPatient(Patient obj)
        {
            #region Variables
            //Method variables
            List<Patient> retCode = new List<Patient>(); 
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            Patient myPatient = new Patient();

            //Patient variables
            Int32 patientID = 0;
            String HCN = "";
            String lName = "";
            String fName = "";
            Char mInitial = ' ';
            DateTime dateOfBirth = DateTime.MinValue;
            String sex = "";
            String headOfHouse = "";
            Int32 addressID = 0;

            //Address variables
            Int32 addressID2 = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";
            #endregion
            try
            {
                //Call the finder the receive a Datatable obj
                dt = DBSupport.FindPatientByLastName(obj.GetLname());

                //Test if there are data
                if(dt.Rows.Count < 1)
                {
                    dt = DBSupport.FindPatient(obj.PatientID);
                    if (dt.Rows.Count != 1)
                    {
                        dt = DBSupport.FindPatient(obj.GethCN());
                        if (dt.Rows.Count != 1)
                        {
                            retCode = null; return retCode;
                        }
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    //Travese the table and grab the data
                    foreach (DataRow row in dt.Rows)
                    {
                        #region Patient


                        patientID = Convert.ToInt32(row[0]);
                        HCN = row[1].ToString();
                        lName = row[2].ToString();
                        fName = row[3].ToString();
                        mInitial = Convert.ToChar(row[4]);
                        dateOfBirth = Convert.ToDateTime(row[5]);
                        sex = row[6].ToString().Trim();
                        headOfHouse = row[7].ToString();
                        addressID = Convert.ToInt32(row[8]);
                        

                        //Instantiate a new Patient object and fill it
                        myPatient = new Patient();
                        myPatient.SetPatientID(patientID);
                        myPatient.SethCN(HCN);
                        myPatient.SetLname(lName);
                        myPatient.SetFname(fName);
                        myPatient.SetMinitial(mInitial);
                        myPatient.DateOfBirth = dateOfBirth;
                        myPatient.SetSex(sex);
                        myPatient.SetHeadOfHouse(headOfHouse);
                        myPatient.Address = FindAddress(addressID);
                        #endregion


                        #region Address
                        //Call the finder the receive a Datatable obj
                        dt2 = DBSupport.FindAddress(addressID);

                        //Check either if there is data in the datatable or return null
                        if (dt2.Rows.Count != 1) { retCode = null; return retCode; }

                        //Travese the table and grab the data
                        foreach (DataRow row2 in dt2.Rows)
                        {
                            addressID2 = Convert.ToInt32(row2[0]);
                            adressLine1 = row2[1].ToString();
                            adressLine2 = row2[2].ToString();
                            city = row2[3].ToString();
                            province = row2[4].ToString();
                            phoneNumber = row2[5].ToString();
                        }

                        //Instantiate a new Patient object and fill it
                        myPatient.Address.SetAdressID(addressID2);
                        myPatient.Address.SetAdressLineOne(adressLine1);
                        myPatient.Address.SetAdressLineTwo(adressLine2);
                        myPatient.Address.SetCity(city);
                        myPatient.Address.SetProvince(province);
                        myPatient.Address.SetPhoneNumber(phoneNumber);
                        #endregion


                        #region Add data into the List
                        //Check if the new data is valid
                        if (myPatient.ValidatePatient() != 0) { retCode = null; return retCode; }
                        retCode.Add(myPatient); //Add patient Obj at the patient list. 
                        #endregion
                    }
                }
                if (retCode != null) { SendToLog(new Exception("FindPatient - [Successful].")); }
            }
            catch (Exception e)
            {
                SendToLog(new Exception("FindPatient - [Failure]."));
                throw e;
            }
            return retCode;
        }


        /// <summary>
        /// Update an existing address. Returns 0 if it was Done OK.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public int UpdateAddress(Address obj)
        {
            //Method variables
            Int32 retCode = SUCCESS_GENERIC_RETURN;


            //Address variables
            Int32 addressID = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";

            try
            {
                #region initialization
                //If validation get a problem, return a error number (exit)
                retCode = obj.ValidateAdress();
                if (retCode != 0) { retCode = INVALIDADDRESSOBJECT; return retCode; }


                //Filling Address attributes with data from the received obj
                addressID = obj.GetAdressID();
                adressLine1 = obj.GetAddressLineOne();
                adressLine2 = obj.GetAddressLineTwo();
                city = obj.GetCity();
                province = obj.GetProvince();
                phoneNumber = obj.GetPhoneNumber();

                //To do more tests

                if (addressID == 0) { addressID = DBSupport.GetNextAddressID(); }//If there is no addressID get the next AddressID available.
                #endregion


                #region Bussiness actions
                //----------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------
                // The validation if this Address already exists will happening in the method DBSupport.UpdateAddress
                if (DBSupport.UpdateAddress(addressID, adressLine1, adressLine2, city, province, phoneNumber) != 1)
                {
                    retCode = INSERTAPPOINTMENTOBJECT;
                    return retCode;
                }

                if (retCode == 0) { SendToLog(new Exception("Insert a new Patient - [Successful].")); }

                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Insert a new Patient - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }


        /// <summary>
        /// Update an existing appointment. Returns 0 if it was Done OK.
        /// This method is limited to change existent fields  in an also, existent appointment (Date/time, patientID1, and patientID2)
        /// There is no implementation to delete the second patient from an existing appointment or turn an existing appointment empty.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public int UpdateAppointment(Appointment obj)
        {
            //Method variables
            Int32 retCode = SUCCESS_GENERIC_RETURN;
            Int32 myAppointmentID = 0;
            DateTime myDayAndTime = DateTime.MinValue;
            Patient myPatient01 = null, myPatient02 = null;

            try
            {
                #region initialization
                //If validation get a problem, return a error number (exit)
                retCode = obj.ValidateAppointment();
                if (retCode != 0) { retCode = INVALIDAPPOINTMENTBJECT; return retCode; }
                if (obj.FirstPatient == null && obj == null) { retCode = NULLAPPOINTMENTOBJECT; return retCode; }

                //Get attribuits velues
                myAppointmentID = obj.GetAppointmentID();
                myDayAndTime = obj.GetDayAndTime();
                myPatient01 = obj.FirstPatient;
                myPatient02 = obj.SecondPatient;

                //To do more tests
                if (obj.GetAppointmentID() < 1) { retCode = INVALIDAPPOINTMENTBJECT; return retCode; }
                if (obj.FirstPatient.PatientID < 1) { retCode = INVALIDPATIENTOBJECT; return retCode; }
                if (obj.SecondPatient != null)
                {
                    if (obj.FirstPatient.PatientID < 1) { retCode = INVALIDPATIENTOBJECT; return retCode; }
                }
                #endregion

                #region Bussiness actions
                //----------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------
                // The validation if this appointment already exists will happening in the method DBSupport.UpdateAppointment
                if (DBSupport.UpdateAppointment(myAppointmentID, myDayAndTime, myPatient01.PatientID, myPatient02.PatientID) != 1)
                {
                    retCode = INSERTAPPOINTMENTOBJECT;
                    return retCode;
                }


                if (retCode == 0) { SendToLog(new Exception("UpdateAppointment - [Successful].")); }
                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("UpdateAppointment - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }


        /// <summary>
        /// Update an existing patient. Returns 0 if it was Done OK.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public int UpdatePatient(Patient obj)
        {
            //Method variables
            Int32 retCode = SUCCESS_GENERIC_RETURN;
            Boolean isHoHFilled = false;
            Patient headOfHousePatient = null;

            //Patient variables
            Int32 patientID = 0;
            String HCN = "";
            String lName = " ";
            String fName = "";
            Char mInitial = ' ';
            DateTime dateOfBirth = DateTime.MinValue;
            String sex = "";
            String headOfHouse = "";

            //Address variables
            Int32 addressID = 0;
            String adressLine1 = "";
            String adressLine2 = "";
            String city = "";
            String province = "";
            String phoneNumber = "";


            try
            {
                #region initialization
                //If validation get a problem, return a error number (exit)
                retCode = obj.ValidatePatient();
                if (retCode != 0) { retCode = INVALIDPATIENTOBJECT; return retCode; }
                if (obj.Address == null && obj == null) { retCode = NULLADDRESSOBJECT; return retCode; }



                //Filling Patient attributes with data from the received obj
                patientID = obj.PatientID;
                HCN = obj.GethCN();
                lName = obj.GetLname();
                fName = obj.GetFname();
                mInitial = obj.GetMinitial();
                dateOfBirth = obj.DateOfBirth;
                sex = obj.GetSex();
                headOfHouse = obj.GetHeadOfHouse();

                //Filling Address attributes with data from the received obj
                addressID = obj.Address.GetAdressID();
                adressLine1 = obj.Address.GetAddressLineOne();
                adressLine2 = obj.Address.GetAddressLineTwo();
                city = obj.Address.GetCity();
                province = obj.Address.GetProvince();
                phoneNumber = obj.Address.GetPhoneNumber();

                //To do more tests
                if (headOfHouse != "") { isHoHFilled = true; }  //Check if HoH is filled
                if (addressID == 0) { addressID = DBSupport.GetNextAddressID(); }//If there is no addressID get the next AddressID available.
                #endregion


                #region Bussiness actions
                //----------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------
                //Check if HoH is NULL, then the obj have to have an Address, otherwise return an error and exit
                if ((isHoHFilled == false || headOfHouse == null))
                {
                    if ((obj.Address == null || adressLine1 == null || adressLine1 == "")) //Check If also address is null, if it is Exit
                    {
                        retCode = HOHANDADDRESSNULL;
                        return retCode;
                    }
                    else
                    {
                        //Call Insert Address and then Patient
                        if (DBSupport.UpdateAddress(addressID, adressLine1, adressLine2, city, province, phoneNumber) != 1)
                        {
                            retCode = FAILTOINSERTNEWADDRESS;
                            return retCode;
                        }
                        if (DBSupport.UpdatePatient(patientID, HCN, lName, fName, mInitial, dateOfBirth, sex, headOfHouse) != 1)
                        {
                            retCode = FAILTOINSERTNEWPATIENT;
                            return retCode;
                        }
                    }
                }
                else  //If HoH is filled, then check if there is a patient with that HoH number
                {
                    headOfHousePatient = FindPatient(headOfHouse);     //Looking for another patient base on this patient HoH ID 

                    if (headOfHousePatient == null || headOfHousePatient.ValidatePatient() != 0) //Check if myPatient01 is null or invalid
                    {
                        retCode = -3510;                        //If it get error exit because thare is not patient related with that Hoh
                        return retCode;
                    }
                    else  //No Ignore any address of this object and add the same addressID of the headOfHousePatient
                    {
                        //Call Insert Address and then Patient
                        if (DBSupport.UpdateAddress(addressID, adressLine1, adressLine2, city, province, phoneNumber) != 1)
                        {
                            retCode = FAILTOINSERTNEWADDRESS;
                            return retCode;
                        }
                        if (DBSupport.UpdatePatient(patientID, HCN, lName, fName, mInitial, dateOfBirth, sex, headOfHouse) != 1)
                        {
                            retCode = -3506;
                            return retCode;
                        }

                    }
                }

                if (retCode == 0) { SendToLog(new Exception("Update Patient - [Successful].")); }

                #endregion
            }
            catch (Exception e)
            {
                SendToLog(new Exception("Update Patient - [Failure]."));
                throw e;
            }
            finally
            {
            }
            return retCode;
        }


        /// <summary>
        /// This method will log an exception
        /// </summary>
        /// <param name="ex">The exception caught</param>
        /// <see cref="ServerMain.cs, Victor code."/>
        private static void SendToLog(Exception ex)
        {
            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, ex.Message); //Call the class
        }
    }
}
