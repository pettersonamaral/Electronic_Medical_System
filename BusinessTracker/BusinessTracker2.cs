﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billing;
using DBManager;
using Demographics;

namespace BusinessTracker
{
    public partial class BusinessTracker
    {

        #region Constants

        // Month numeration
        public enum monthEnum  { January, February, March, April, May, June, July, August, September, October, November, December };
        public enum ServiceStatus { UPRC, PAID, DECL, FHCV, CMOH }   //UPRC -> SHORT FOR UNPROCESSED


        #endregion Constants

        #region [PC TEMP CONTROL] Methods Under Construction

        public List<string> GenerateMonthlyBillingCSV(DateTime date)
        {
            throw new NotImplementedException();
        }     // TODO [REVIEW] Not necessary for

        public List<string> MonthlyReconcileCSV(DateTime date)
        {
            throw new NotImplementedException();
        }

        #endregion [PC TEMP CONTROL] Methods Under Construction

        #region Unit Tested [Happy Path]

        /// <summary>
        /// Process request of adding a new Bill by calling a DAL method
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int AddBill(Bill obj)
        {
            // Locals
            var retCode = 0;

            try
            {
                if(DBSupport.AddBill(obj) > 0) {  retCode = 0;}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return retCode;
        }
        
        /// <summary>
        /// Get the information needed to fill a Bill obj
        /// </summary>
        /// <param name="patientID"></param>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        public Bill FindBill(int patientID, int appointmentID)
        {
            // Locals
            var retBill = new Bill();
            var dtBill = new DataTable();
            var dtServiceList = new DataTable();

            // Bill properties temp variables
#pragma warning disable CS0219 // The variable 'theBillID' is assigned but its value is never used
            var theBillID = 0;
#pragma warning restore CS0219 // The variable 'theBillID' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'theFlagRecall' is assigned but its value is never used
            var theFlagRecall = 0;
#pragma warning restore CS0219 // The variable 'theFlagRecall' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'theStatusDone' is assigned but its value is never used
            var theStatusDone = false;
#pragma warning restore CS0219 // The variable 'theStatusDone' is assigned but its value is never used
            var thePatientID = 0;
            var thePatient = new Patient();
            var theAppointmentID = 0;
            var theAppointment = new Appointment();
            var theListServiceList = new List<ServiceList>();

            try
            {
                // Call DAL methods to Fill Data related to a Bill obj
                dtBill = DBSupport.FindBill(patientID, appointmentID);

                //Check if there is a line into this table
                if (dtBill.Rows.Count < 1) { return null; }


                //Travese the table and grab Bill table data
                foreach (DataRow row in dtBill.Rows)
                {
                    retBill.BillID = Convert.ToInt32(row[0]);
                    thePatientID = Convert.ToInt32(row[1]);
                    theAppointmentID = Convert.ToInt32(row[2]);
                    retBill.FlagRecall = Convert.ToInt32(row[3]);
                    retBill.StatusDone = Convert.ToBoolean(row[4]);
                }

                // Call DAL method to get ServiceList data to complement Bill obj
                dtServiceList = DBSupport.FindServiceList(retBill.BillID);

                //if (dtServiceList.Rows.Count < 1) { return null; }

                foreach (DataRow slRow in dtServiceList.Rows)
                {
                    var theServiceList = new ServiceList();
                    var serviceID = Convert.ToInt32(slRow[0]);
                    var theService = FindService(serviceID);
                    theServiceList.Quantity = Convert.ToInt32(slRow[2]);
                    theServiceList.Status = slRow.ToString();
                    theServiceList.Service = theService;

                    // Add a servicelist to the list of ServiceLists
                    theListServiceList.Add(theServiceList);
                }

                // Complete Bill obj properties
                thePatient = FindPatient(patientID);
                theAppointment = FindAppointment(appointmentID);
                retBill.Patient = thePatient;
                retBill.Appointment = theAppointment;
                retBill.Services = theListServiceList;
            }
            catch (Exception e)
            {
                throw e;
            }

            return retBill;
        }

        /// <summary>
        /// This method returns a Service object based on a provided serviceID. Returns null if fails to retrieve data
        /// </summary>
        /// <param name="serviceID"></param>
        /// <returns></returns>
        public Service FindService(int serviceID)
        {
            // Locals
            var retService = new Service();
            var dt = new DataTable();

            // Service variables
            var theServiceID = 0;                 // holds a unique identifier for each instantiated service object
            var feeCode = "";                // represents Ministry of Health service coding
            var effectiveDate = new DateTime();    // carries date when service became valid  (YYYYMMDD) 
            var serviceValue = 0.0;               // indicates the current service value

            try
            {
                // Call DAL method to fill datatable
                dt = DBSupport.FindService(serviceID);

                //Check if there is a line into this table
                if (dt.Rows.Count < 1) { return null; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    theServiceID = Convert.ToInt32(row[0]);
                    feeCode = row[1].ToString();
                    DateTime.TryParse(row[2].ToString(), out effectiveDate);
                    double.TryParse(row[3].ToString(), out serviceValue);
                }

                //Call a new address object with the new data
                retService = new Service(theServiceID, feeCode, effectiveDate, serviceValue);
            }
            catch (Exception e)
            {
                throw e;
            }
            return retService;
        }

        /// <summary>
        /// Returns the list of all available services in the database according to the MOH master table
        /// </summary>
        /// <param name="serviceCode"></param>
        /// <returns></returns>
        public List<Service> FindServices(string serviceCode = "")
        {
            // Locals
            var retServices = new List<Service>();
            var dt = new DataTable();

            try
            {
                // Call DAL method to fill datatable
                dt = DBSupport.FindServices(serviceCode);

                //Check if there is a line into this table
                if (dt.Rows.Count < 1) { return null; }

                //Travese the table and grab the data
                foreach (DataRow row in dt.Rows)
                {
                    // Creates a new Service obj
                    var newService = new Service();

                    // Fills Service obj parameters
                    newService.SetServiceID(Convert.ToInt32(row[0]));
                    newService.SetFeeCode(row[1].ToString());
                    DateTime.TryParse(row[2].ToString(), out var effectiveDate);
                    newService.EffectiveDate = effectiveDate;
                    double.TryParse(row[3].ToString(), out var serviceValue);
                    newService.SetServiceValue(serviceValue);

                    // Add newly created obj into the list
                    retServices.Add(newService);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return retServices;
        }

        /// <summary>
        /// Adds a ServiceLine to the database by calling a DAL method
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int AddServiceList(int billID, ServiceList obj)
        {
            // Locals
            var retCode = 0;

            try
            {
                retCode = DBSupport.AddServiceList(billID, obj);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return retCode;
        }

        /// <summary>
        /// Update Bill Information based on obj properties. If sucessfull, returns 4500 code, otherwhise returns -4511
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int UpdateBill(Bill obj)
        {
            // Locals
            var retCode = 0;

            try
            {
                // Update Bill table attributes // TEMPORATRY FIX TODO CHECK RETURN VALUE CONVENTIONS WITH TEAM... APPARENTLY DIFFERENT FROM EMS-I
                retCode = DBSupport.UpdateBill(obj) > 0 ? 0 : DBSupport.RETURN_BILLING_TRACKER_FAILURE;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return retCode;
        }

        /// <summary>
        /// Send request to DAL method to UPDATE ServiceLine info on Database. If sucessfull returns 4500 code, otherwise returns -4506
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int UpdateServiceList(int billID, ServiceList obj)
        {
            // Locals
            var retCode = 0;

            try
            {
                retCode = DBSupport.UpdateServiceList(billID, obj);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return retCode;

        }

        /// <summary>
        /// Generates a billing summary from the selected month
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string MonthlyBillingSummary(DateTime date)
        {
            //Locals
            var dt = new DataTable();
            var monthBillSummary = new StringBuilder();
            var sym = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;  // Aux to currency formatting
            var numMonth = date.Month;
            var numYear = date.Year;

            try
            {
                // Data retrieval to fill summary

                // Find Total Encounters Billed
                var totalEncountersBilled = DBSupport.FindNumberOfEncounters(date);

                // Find Total Billed Procedures (in Dollars)
                var totalBilledProcedures = DBSupport.FindTotalBilledProcedures(date);

                // Find Total Received (in Dollars)
                var receivedTotal = DBSupport.FindReceivedTotal(date);

                // Find Received Percentage ((RT/TBP) * 100)
                var receivedPercentage = ((receivedTotal / totalBilledProcedures) * 100);

                // Find Average Billing (RT/TEB in dollars)
                var averageBilling = (receivedTotal / totalEncountersBilled);

                // Find number of encounters to follow-up (Integer Sum of FHCV abd CMOH count)
                var numEncountersToFollowUp = DBSupport.FindNumOfEncountersToFollowUp(date);


                //// Builds Summary string
                monthBillSummary.AppendFormat("\n Monthly Billing Summary....... [  {0} - {1}   ]\n", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).ToUpper(), date.Year);
                monthBillSummary.AppendFormat(" Total Encounters Billed:...... [ # {0} ] \n", totalEncountersBilled.ToString("#,##0.00").PadLeft(9, ' '));
                monthBillSummary.AppendFormat(" Total Billed Procedures:...... [ {0}{1,10:#,##0.00} ] \n", sym, totalBilledProcedures.ToString("F"));
                monthBillSummary.AppendFormat(" Received Total:............... [ {0}{1,10:#,##0.00} ] \n", sym, receivedTotal.ToString("F"));
                monthBillSummary.AppendFormat(" Received Percentage:.......... [ % {0} ] \n", receivedPercentage.ToString("#,##0.00").PadLeft(9, ' '));
                monthBillSummary.AppendFormat(" Average Billing:.............. [ {0}{1,10:#,##0.00} ] \n", sym, averageBilling.ToString("F"));
                monthBillSummary.AppendFormat(" Num.Encounters to Follow-up... [ # {0} ] \n", numEncountersToFollowUp.ToString("#,##0.00").PadLeft(9, ' '));

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return monthBillSummary.ToString();
        }

        /// <summary>
        /// Find and fill bill obj based on billID
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        public Bill FindBill(int billID)
        {
            // Locals
            var retBill = new Bill();
            var dtBill = new DataTable();
            var dtServiceList = new DataTable();

            // Bill properties temp variables
#pragma warning disable CS0219 // The variable 'theBillID' is assigned but its value is never used
            var theBillID = 0;
#pragma warning restore CS0219 // The variable 'theBillID' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'theFlagRecall' is assigned but its value is never used
            var theFlagRecall = 0;
#pragma warning restore CS0219 // The variable 'theFlagRecall' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'theStatusDone' is assigned but its value is never used
            var theStatusDone = false;
#pragma warning restore CS0219 // The variable 'theStatusDone' is assigned but its value is never used
            var thePatientID = 0;
            var theAppointmentID = 0;
            var thePatient = new Patient();
            var theAppointment = new Appointment();
            var theListServiceList = new List<ServiceList>();

            try
            {
                // Call DAL methods to Fill Data related to a Bill obj
                dtBill = DBSupport.FindBill(billID);


                // Call DAL method to get ServiceList data to complement Bill obj
                dtServiceList = DBSupport.FindServiceList(billID);

                //Check if there is a line into this table
                if (dtBill.Rows.Count < 1) { return null; }
                if (dtServiceList.Rows.Count < 1) { return null; }

                //Travese the table and grab Bill table data
                foreach (DataRow row in dtBill.Rows)
                {
                    retBill.BillID = Convert.ToInt32(row[0]);
                    thePatientID = Convert.ToInt32(row[1]);
                    theAppointmentID = Convert.ToInt32(row[2]);
                    retBill.FlagRecall = Convert.ToInt32(row[3]);
                    retBill.StatusDone = Convert.ToBoolean(row[4]);
                }

                foreach (DataRow slRow in dtServiceList.Rows)
                {
                    var theServiceList = new ServiceList();
                    var serviceID = Convert.ToInt32(slRow[0]);
                    var theService = FindService(serviceID);
                    theServiceList.Service = theService;

                    // Add a servicelist to the list of ServiceLists
                    theListServiceList.Add(theServiceList);
                }

                // Add Patient and Appointment objs to the Bill Obj properties
                thePatient = FindPatient(thePatientID);
                theAppointment = FindAppointment(theAppointmentID);
                retBill.Patient = thePatient;
                retBill.Appointment = theAppointment;
            }
            catch (Exception e)
            {
                throw e;
            }

            return retBill;
        }

        /// <summary>
        /// Find and fill bill obj based on billID
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        public ServiceList FindServiceList(int billID, int serviceID)
        {
            // Locals
            var retServiceList = new ServiceList();
            var dtServiceList = new DataTable();

            try
            {
                // Call DAL methods to Fill Data related to a ServiceList obj
                dtServiceList = DBSupport.FindServiceList(billID, serviceID);

                //Check if there is a line into this table
                if (dtServiceList.Rows.Count < 1) { return null; }

                //Travese the table and grab ServiceList table data
                foreach (DataRow slRow in dtServiceList.Rows)
                {
                    serviceID = Convert.ToInt32(slRow[0]);
                    var theService = FindService(serviceID);
                    retServiceList.Service = theService;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return retServiceList;
        }

        #endregion Unit Tested [Happy Path]

    }
}
