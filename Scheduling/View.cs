﻿
/*****************************************************************************************************
* FILE : 		  View.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-09
* SECOND VERSION: 2017-Dec-08
* THIRD VERSION:  2018-Jan-05
* FINAL VERSION:  2018-Mar-28

* DESCRIPTION/Requirements: 

* This class/file contains all the methods for user interface. This class gives the user the ability to add a new patient and add
* all the necessary information about a patient. It also gives the user the ability to find a patient using HCN. It gives
* the user the ability to update patient with different options to choose from. It also helps the user to create a new appointment
* as well as update appointment if necessary. This is where the user will create a bill which later will get sent to 
* health canada.


* Refence:

*****************************************************************************************************/
using System;
using System.Collections.Generic;
using Interpreter;
using Demographics;
using Billing;
using System.Text.RegularExpressions;
using System.Threading;

namespace SchedullingUI
{
    /// <summary>
    /// <b>Description: </b>This class contains all the information about the main user interface.
    /// It contains the main menu with different options and the sub menu with other options which
    /// will give the user the flexibility to go ahead and customize patient information if need be.
    /// </summary>


    public class View
    {
        // Interfaces - Connection related
        private static ClientInterpreter myInterpreter = ClientInterpreter.Instance;


        // List of errors - 1100-1199
        public const int ERR_NOT_PROCESSED = -1101;

        // Date Limitations
        private readonly DateTime minDate = DateTime.Parse("2017-08-01");
        private readonly DateTime maxDate = DateTime.Parse("2017-12-31");

        // Printing constants
        private readonly string printLine = 
            "\n===================================================================\n";

        // Support variables
        private const int wait = 1200; // miliseconds

        // =======================================================================
        //                              CONSTRUCTOR
        // =======================================================================

        public View()
        {
            // Initialize if necessary
        }

        /// <summary>
        /// This is the readonly dictionary for patients sex
        /// </summary>
        private static readonly Dictionary<string, string> patientSex = new Dictionary<string, string>
        {
            {"FEMALE","F" },{"F","F"},{"MALE","M"},{"M","M"},
            {"INTERSEX","I" },{"I","I"},{"HERMAPHRODITE","H"},{"H","H"}
        };
        /// <summary>
        /// This is the readonly dictionary for provinces
        /// </summary>
        private static readonly Dictionary<string, string> Provinces = new Dictionary<string, string>
       {
           {"ONTARIO", "ON"},{"ON", "ON"},
            { "QUEBEC", "QC"},{"QC", "QC" },
            {"NOVA SCOTIA", "NS" },{"NS", "NS" },
            {"NEW BRUNSWICK", "NB" },{"NB", "NB" },
            {"MANITOBA", "MB"},{"MB","MB" },
            {"BRITISH COLUMBIA","BC" },{"BC","BC" },
            {"PRINCE EDWARD ISLAND", "PEI" },{"PEI","PEI" },
            {"SASKACHEWAN","SK" },{"SK","SK" },
            {"ALBERTA","AB" },{"AB","AB" },
            {"NEWFOUNDLAND AND LABRADOR","NL" },{"NL","NL" },
            {"NORTHWEST TERRITORIES","NT" },{"NT","NT"},
            {"YUKON","YT" },{"YT","YT"},
            {"NUNAVUT","NU" },{"NU","NU"}
       };
        //main menu
        /// <summary>
        /// This is the main menu for the system. After user runs the system, the
        /// user will be prompted to the first page which will then take the user 
        /// through different sub-menu's depending on what the user selects.
        /// </summary>
        /// <returns>This function does not return anything.</returns>
        public void MainMenu()
        {
            string header = "  Main Menu";
            while (true)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine(" Please select a option from the list:\n");
                Console.WriteLine("  [1] - Patient" + "\n  [2] - Schedule" + "\n  [3] - Billing"
                    + "\n  [4] - Exit");
                ConsoleKeyInfo selected = Console.ReadKey();
                if (selected.KeyChar == '1')
                {
                    PatientMenu(header);
                }
                else if (selected.KeyChar == '2')
                {
                    ScheduleMenu(header);
                }
                else if (selected.KeyChar == '3')
                {
                    BillingMenu(header);
                }
                else if (selected.KeyChar == '4')
                {
                    Console.Clear();
                    Console.WriteLine(header + printLine);
                    Console.WriteLine("  Exit the program?\n");
                    Console.WriteLine(" [Enter] - Confirm");
                    ConsoleKeyInfo userKey = Console.ReadKey();
                    if (userKey.Key == ConsoleKey.Enter)
                    {
                        myInterpreter.Disconnect();
                        break;
                    }
                }
            }
        }


        /// <summary>
        /// This method handles the patient menu where depending on the need of user
        /// it will provide the user the capability of adding a new patient, find a new
        /// patient and update the new patient.
        /// </summary>
        /// <returns>This function does not return anything.</returns>
        public void PatientMenu(string header = "")
        {
            header += " > Patient Menu";
            bool done = false;
            while(!done)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine("Please select a option from the list");
                Console.WriteLine("\n  [1] - Add Patient" + "\n  [2] - Find Patient" + "\n  [3] - Update Patient"
                    + "\n  [4] - Main Menu");
                ConsoleKeyInfo userKey = Console.ReadKey();
                if (userKey.KeyChar == '1')
                {
                    AddPatient(header);
                }
                else if (userKey.KeyChar == '2')
                {
                    FindPatient(header);
                }
                else if (userKey.KeyChar == '3')
                {
                    UpdatePatient(header);
                }
                else if (userKey.KeyChar == '4' || userKey.Key == ConsoleKey.Escape)
                {
                    done = true;
                }
                else
                {
                    Console.WriteLine("Invalid Key!");
                    Console.ReadKey();
                }
            }
        }

        /// <summary>
        /// This method handles the adding a patient to the database. It will ask
        /// the user to enter a series of information about the user in a order which
        /// will then get saved to the database.
        /// </summary>
        /// <returns>This function does not return anything.</returns>
        public void AddPatient(string header = "")
        {
            header += " > Add Patient";
            Patient tempPatient = new Patient();
            Console.Clear();
            Console.WriteLine(header + printLine);
            //Add patients first name
            AddPatientFName(tempPatient);
            //Add patients last name
            Console.WriteLine(header + printLine);
            AddPatientLName(tempPatient);
            //\Add patients middle initial if exists
            Console.WriteLine(header + printLine);
            AddPatientMInitial(tempPatient);
            //\Add patients date of birth which is a mandatory field
            Console.Clear();
            Console.WriteLine(header + printLine);
            AddPatientDofBirth(tempPatient);

            //https://msdn.microsoft.com/en-us/library/system.text.regularexpressions.regex(v=vs.110).aspx
            //\Add patients health card number - mandatory field without dashes
            Console.WriteLine(header + printLine);
            AddPatientHCN(tempPatient);
            //\Add patients gender - Mandatory field
            Console.WriteLine(header + printLine);
            AddPatientSex(tempPatient);

            //\ Add the health card number for the head of household if necessary.
            //\ The only time a user will have to enter the HCN for head of household is
            //\ if the patient is under 18 yr of age
            Console.Clear();
            Console.WriteLine(header + printLine);
            AddPatientHeadOfHouse(tempPatient);

            // ====================== ADDRESS ======================
            bool noAddress = true;
            int addressID = 0;

            if (tempPatient.GetHeadOfHouse() != "")
            {
                // try to find the head of house to get adderss ID
                Address addressFound = myInterpreter.FindAddress(tempPatient.GetHeadOfHouse());
                if (addressFound != null)
                {
                    if (addressFound.GetAdressID() != 0)
                    {
                        addressID = addressFound.GetAdressID();
                        noAddress = false;
                    }
                }
            }

            // no address detected! I have to create an address
            if (noAddress)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);

                Address tempAddress = new Address();
                //tempAddress.SetHCN(tempPatient.GethCN());
                AddPatientAddress(tempAddress);

                //\Enter City - mandatory field
                Console.WriteLine(header + printLine);
                AddPatientCity(tempAddress);
                //\ Enter Province - Mandatory field
                Console.WriteLine(header + printLine);
                AddPatientProvince(tempAddress);
                //\ Add phone number - mandatory field
                Console.WriteLine(header + printLine);
                AddPatientPhoneNumber(tempAddress);

                Console.Clear();
                Console.WriteLine(header + printLine);

                // at this point I have an address, so I have to add to the system
                Address addFound = myInterpreter.FindAddress(tempPatient.GethCN());
                if (addFound != null)
                {
                    //addressID = addFound.GetAdressID();
                    tempPatient.Address = addFound;
                }
            }

            

            Console.Clear();
            Console.WriteLine(header + printLine);

            int errorCode = myInterpreter.AddPatient(tempPatient);
            if (errorCode != Error.SUCCESS_GENERIC_RETURN)
            {
                Console.WriteLine("Could not add patient: " + Error.GetError(errorCode) +
                    "Press any key to continue...");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Patient successfully added to the system!");
                Thread.Sleep(wait);
            }
        }

        private static void AddPatientPhoneNumber(Address tempAddress)
        {
            while (true)
            {
                string numPhone = "";
                Console.WriteLine("Enter Phone Number. Please follow this format: 000-000-0000");
                numPhone = Console.ReadLine();
                Regex regex = new Regex(@"^\d{3}-\d{3}-\d{4}$");
                MatchCollection matches = regex.Matches(numPhone);
                if (numPhone == "")
                {
                    Console.WriteLine("Please enter a valid phone number");
                    continue;
                }
                if (matches.Count == 0)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                string[] justNumbers = numPhone.Split('-');
                string formattedNumber = numPhone;
                if (justNumbers.Length == 3)
                {
                    formattedNumber = "(" + justNumbers[0] + ")" + justNumbers[1] + "-" + justNumbers[2];
                }
                if (tempAddress.SetPhoneNumber(formattedNumber) != true)
                {
                    Console.WriteLine("Invalid input: setter refused");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientProvince(Address tempAddress)
        {
            while (true)
            {
                string province = "";
                Console.WriteLine("Enter Province");
                province = Console.ReadLine();
                if (province == "")
                {
                    Console.WriteLine("Field Cannot be empty");
                    continue;
                }
                else if (!Provinces.TryGetValue(province.ToUpper(), out province))
                {
                    Console.WriteLine("Invalid province");
                    continue;
                }
                else if (tempAddress.SetProvince(province) != true)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientAddress(Address tempAddress)
        {
            string addressLine = "";
            while (true)
            {
                Console.WriteLine("Enter address");
                addressLine = Console.ReadLine();
                if (addressLine == "")
                {
                    Console.WriteLine("Field Cannot be empty");
                    continue;
                }
                else if (tempAddress.SetAdressLineOne(addressLine) != true)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientCity(Address tempAddress)
        {
            while (true)
            {
                string city = "";
                Console.WriteLine("Enter city");
                city = Console.ReadLine();
                if (city == "")
                {
                    Console.WriteLine("Field Cannot be empty");
                    continue;
                }
                else if (tempAddress.SetCity(city) != true)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientHeadOfHouse(Patient tempPatient)
        {
            while (true)
            {
                string headOfHouse = "";
                Console.WriteLine("Enter the health card number for head of household." +
                    "Please follow this format: 0000000000AZ");
                headOfHouse = Console.ReadLine().ToUpper();
                if (headOfHouse == "")
                {
                    break;
                }
                else if (tempPatient.SetHeadOfHouse(headOfHouse) != true)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientSex(Patient tempPatient)
        {
            while (true)
            {
                string sex = "";
                Console.WriteLine("Gender: (F for Female, M for Male, I for Intersex" +
                    "and H for Hermaphrodite )");
                sex = Console.ReadLine();
                if (tempPatient.SetSex(sex) != true)
                {
                    Console.WriteLine("Please enter a valid gender");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientHCN(Patient tempPatient)
        {
            while (true)
            {
                string hCN = "";
                Console.WriteLine("Enter Health Card Number. Please follow this format: 0000000000AZ. " +
                    "Here Zero's are numbers and A to Z character");
                hCN = Console.ReadLine().ToUpper();
                if (tempPatient.SethCN(hCN) != true)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }

                if (hCN == "")
                {
                    Console.WriteLine("Field Cannot be empty");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientDofBirth(Patient tempPatient)
        {
            while (true)
            {
                string dOfBirth = "";
                Console.WriteLine("Enter Date of Birth. Please follow this format: DD-MM-YYYY");
                dOfBirth = Console.ReadLine();

                if (DateTime.TryParse(dOfBirth, out DateTime result) != true)
                {
                    Console.WriteLine("Please enter a valid date of birth");
                    continue;
                }
                else
                {
                    tempPatient.DateOfBirth = result;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientLName(Patient tempPatient)
        {
            while (true)
            {
                string lName = "";
                Console.WriteLine("Enter Last Name");
                lName = Console.ReadLine();
                if (tempPatient.SetLname(lName) != true)
                {
                    Console.WriteLine("Please enter a valid name");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientFName(Patient tempPatient)
        {
            while (true)
            {
                string fName = "";
                Console.WriteLine("Enter First Name");
                fName = Console.ReadLine();

                if (tempPatient.SetFname(fName) != true)
                {
                    Console.WriteLine("Please enter a valid name");
                    continue;
                }
                Console.Clear();
                break;
            }
        }

        private static void AddPatientMInitial(Patient tempPatient)
        {
            while (true)
            {
                Console.WriteLine("Enter Middle Initial");
                string input = Console.ReadLine();
                if (input.Length > 1)
                {
                    Console.WriteLine("Invalid");
                    continue;
                }
                else if (input.Length == 0)
                {
                    break;
                }
                else if (input.Length == 1)
                {
                    if (tempPatient.SetMinitial(input[0]) != true)
                    {
                        Console.WriteLine("Please enter a valid character");
                        continue;
                    }
                    Console.Clear();
                    break;
                }
            }
        }

        /// <summary>
        /// This method handles the find patient. In the event when the user selects the
        /// find patient option, this function will get called.
        /// </summary>
        /// /// <returns>This function does not return anything.</returns>
        public void FindPatient(string header = "")
        {
            header += " > Find Patient";
            Patient patient = FindOnePatient(header);
            if(patient != null)
            {
                //If the patient is found then it will print patient information one after
                //the other
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine("Here is the patient \n" + patient.ToString());//will print the result of the patient

                string findHCN = "";
                if(patient.GetHeadOfHouse() != "")
                {
                    findHCN = patient.GetHeadOfHouse();
                }
                else
                {
                    findHCN = patient.GethCN();
                }

                Address addr = myInterpreter.FindAddress(findHCN);

                Console.WriteLine("Address:\n {0} {1}", addr.GetAddressLineOne(), addr.GetAddressLineTwo());
                Console.WriteLine(" {0}, {1}", addr.GetCity(), addr.GetProvince());
                Console.WriteLine(" Phone: {0}", addr.GetPhoneNumber());
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
            }

            // do something with this patient found.
            // such as find more info about him or display other stuff
        }

        /// <summary>
        /// This method will focus on looking for one patient using last name
        /// </summary>
        /// <param name="header"></param>
        /// <returns>This function will return a patient of type Patient.</returns>
        private Patient FindOnePatient(string header = "")
        {
            header += " > Find One Patient";
            bool lastNameSet = false;
            bool tryToFindPatient = true;
            List<Patient> results = null;
            Patient findPatient = new Patient();

            while (tryToFindPatient)
            {
                // set last name
                while (!lastNameSet)
                {
                    Console.Clear();
                    Console.WriteLine(header + printLine);

                    Console.Write("Please enter patient's Last name: ");
                    lastNameSet = findPatient.SetLname(Console.ReadLine());
                    if (lastNameSet == false)
                    {
                        Console.WriteLine("Invalid input. Press any key to try again...");
                        Console.ReadKey();
                    }
                }

                // search
                results = myInterpreter.FindPatient(findPatient); // <<<<<<< TESTING THIS

                // check if found something
                if (results == null || results.Count == 0)
                {
                    bool validKey = false;
                    while (!validKey)
                    {
                        Console.Clear();
                        Console.WriteLine(header + printLine);
                        Console.WriteLine(" No patient found. Want to try again?");
                        Console.WriteLine("  [Enter] - Try again");
                        Console.WriteLine("  [Esc]   - Cancel");
                        ConsoleKeyInfo userKeySubMenu = Console.ReadKey();
                        if (userKeySubMenu.Key == ConsoleKey.Enter)
                        {
                            validKey = true;
                            lastNameSet = false;
                        }
                        else if (userKeySubMenu.Key == ConsoleKey.Escape)
                        {
                            validKey = true;
                            tryToFindPatient = false;
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            // Choose one patient only
            Patient found = null;
            if (results != null)
            {
                object o = SelectObjectFromList(ConvertList(results), header, "patient");

                found = o as Patient;
            }

            return found;
        }

        /// <summary>
        /// When the user selects update patient, then this function will get called.
        /// This method will also call the find patient in order to find the patient.
        /// Then the appropriate fields will get updated.
        /// </summary>
        /// <returns>This function does not return anything.</returns>
        public void UpdatePatient(string header = "")
        {
            header += " > Update Patient";
            Patient updatePatient = FindOnePatient(header);
            if(updatePatient != null)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                
                bool done = false;
                while (!done)
                {
                    Console.WriteLine("Which field would you like to Update for " + updatePatient.GetLname() + "?");

                    Console.WriteLine("");
                    Console.WriteLine(" [1] First Name.");
                    Console.WriteLine(" [2] Last Name.");
                    Console.WriteLine(" [3] Middle Initial.");
                    Console.WriteLine(" [4] Date Of Birth.");
                    Console.WriteLine(" [5] Health Card Number.");
                    Console.WriteLine(" [6] Sex.");
                    Console.WriteLine(" [7] Head Of Household.");
                    Console.WriteLine(" [8] Address.");
                    Console.WriteLine(" [9] City.");
                    Console.WriteLine(" [10] Province.");
                    Console.WriteLine(" [11] Phone Number.");
                    Console.WriteLine(" [12] Exit Menu.");

                    string input = Console.ReadLine();
                    int menuItem = 0;
                    try
                    {
                        menuItem = Convert.ToInt32(input);
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Invalid menu selection: " + input);
                        Console.ReadKey();
                        continue;
                    }

                    int error = 0;
                    switch (menuItem)
                    {
                        case 1:
                            AddPatientFName(updatePatient);
                            break;
                        case 2:
                            AddPatientLName(updatePatient);
                            break;
                        case 3:
                            AddPatientMInitial(updatePatient);
                            break;
                        case 4:
                            AddPatientDofBirth(updatePatient);
                            break;
                        case 5:
                            AddPatientHCN(updatePatient);
                            break;
                        case 6:
                            AddPatientSex(updatePatient);
                            break;
                        case 7:
                            AddPatientHeadOfHouse(updatePatient);
                            break;
                        case 8:
                            //if head of house is empty that means the patient has his/her own address which 
                            //needs to be updated
                            if (updatePatient.GetHeadOfHouse() == "")
                            {
                                // The patient has his/her own address
                                //Since we have it implemented as such where finding an address will be
                                //accomplished by using 
                                Address address = myInterpreter.FindAddress(updatePatient.GethCN());
                                if (address == null)
                                {
                                    Console.WriteLine("Error: Could not find address!");
                                    Console.ReadKey();
                                    continue;
                                }

                                AddPatientAddress(address);
                                error = myInterpreter.UpdateAddress(address);
                                if (error != Error.SUCCESS_GENERIC_RETURN)
                                {
                                    Console.WriteLine("Error updating address: " + Error.GetError(error));
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                // The patient uses head of house's address
                                Console.WriteLine("This patient uses head of household's address which cannot be updated here.");
                                Console.ReadKey();
                            }
                            break;
                        case 9:
                            if (updatePatient.GetHeadOfHouse() == "")
                            {
                                // The patient has his/her own address
                                Address address = myInterpreter.FindAddress(updatePatient.GethCN());
                                if (address == null)
                                {
                                    Console.WriteLine("Error: Could not find address!");
                                    Console.ReadKey();
                                    continue;
                                }

                                AddPatientCity(address);
                                error = myInterpreter.UpdateAddress(address);
                                if (error != Error.SUCCESS_GENERIC_RETURN)
                                {
                                    Console.WriteLine("Error updating address: " + Error.GetError(error));
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                // The patient uses head of house's address
                                Console.WriteLine("This patient uses head of household's city which cannot be updated here.");
                                Console.ReadKey();
                            }
                            break;
                        case 10:
                            if (updatePatient.GetHeadOfHouse() == "")
                            {
                                // The patient has his/her own address
                                Address address = myInterpreter.FindAddress(updatePatient.GethCN());
                                if (address == null)
                                {
                                    Console.WriteLine("Error: Could not find address!");
                                    Console.ReadKey();
                                    continue;
                                }

                                AddPatientProvince(address);
                                error = myInterpreter.UpdateAddress(address);
                                if (error != Error.SUCCESS_GENERIC_RETURN)
                                {
                                    Console.WriteLine("Error updating address: " + Error.GetError(error));
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                // The patient uses head of house's address
                                Console.WriteLine("This patient uses head of household's province which cannot be updated here.");
                                Console.ReadKey();
                            }
                            break;
                        case 11:
                            if (updatePatient.GetHeadOfHouse() == "")
                            {
                                // The patient has his/her own address
                                Address address = myInterpreter.FindAddress(updatePatient.GethCN());
                                if (address == null)
                                {
                                    Console.WriteLine("Error: Could not find address!");
                                    Console.ReadKey();
                                    continue;
                                }

                                AddPatientPhoneNumber(address);
                                error = myInterpreter.UpdateAddress(address);
                                if (error != Error.SUCCESS_GENERIC_RETURN)
                                {
                                    Console.WriteLine("Error updating address: " + Error.GetError(error));
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                // The patient uses head of house's address
                                Console.WriteLine("This patient uses head of household's phone number which cannot be updated here.");
                                Console.ReadKey();
                            }
                            break;
                        case 12:
                            done = true;
                            break;
                        default:
                            Console.WriteLine("Invalid menu selection: " + input);
                            Console.ReadKey();
                            break;
                    };

                    error = myInterpreter.UpdatePatient(updatePatient);
                    if (error != Error.SUCCESS_GENERIC_RETURN)
                    {
                        Console.WriteLine("Error updating patient: " + Error.GetError(error));
                        Console.ReadKey();
                    }
                }
            }

            //get a patient
            //promtp user asking what the user would like to do
            //
            //int errorcode = myInterpreter.UpdatePatient(updatePatient);
            //if the errorcode is not 0 then you display a message.
        }


        /// <summary>
        /// This method will handle the scheduling menu for the user. When the user
        /// wants to create/update/find new schedule, this function will get called.
        /// Based on what the user selects afterwords, other methods will get called.
        /// </summary>
        /// <returns>This function does not return anything.</returns>

        private void ScheduleMenu(string header = "")
        {
            // tools
            header += " > Schedulling Menu";
            bool exitScheduleMenu = false;
            //bool weekDay = false;
            List<Appointment> myResult;
            DateTime timeSlot = DateTime.MinValue;

            // main loop
            while (!exitScheduleMenu)
            {
                // Select time slot
                timeSlot = SelectTimeSlot(out myResult, out exitScheduleMenu, header);

                if (!exitScheduleMenu)
                {
                    exitScheduleMenu = InterpretTimeSlot(timeSlot, myResult, header);
                }
            }
        }


        /// <summary>
        /// This method will display the calendar when the user wants to create/find/update
        /// appointment. It will show different time slots to the user to select from.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="exit"></param>
        /// <param name="header"></param>
        /// <returns>This method returns SelectTimeSlot of type DateTime.</returns>
        private DateTime SelectTimeSlot(out List<Appointment> list, out bool exit, string header)
        {
            header += " > Select Time Slot";
            DateTime selected = minDate; // DateTime.Parse("2017-11-17");
            //DateTime timeSlot = DateTime.MinValue;
            List<Appointment> myResult = null;
            bool weekDay = false;
            bool selectAnotherDate = true;
            bool validKey = false;
            bool exitScheduleMenu = false;

            int[] timeSlots = { 9, 10, 11, 13, 14, 15 }; // 24 hours fourmat


            // Request the appointments of this day
            Console.WriteLine("Waiting server response...");
            // === REQUEST ACTION HERE ===
            myResult = myInterpreter.FindAppointments(selected, selected.AddDays(7));

            // Select time slot
            while (!validKey)
            {
                // opens calendar
                if (selectAnotherDate)
                {
                    // Select date 
                    selected = PrintCalendar(header + printLine + "  Please, use the arrows to select a date:\n",
                        selected, minDate, maxDate);
                    selectAnotherDate = false;
                    if (selected.DayOfWeek >= DayOfWeek.Monday && selected.DayOfWeek <= DayOfWeek.Friday)
                    { weekDay = true; }
                    else { weekDay = false; }

                    if (selected.Equals(DateTime.MinValue))
                    {
                        exitScheduleMenu = true;
                        exit = true;
                        break;
                    }
                }
                // Print header
                Console.Clear();
                Console.Write(header + printLine);
                Console.WriteLine(" Selected: " + selected.DayOfWeek.ToString() + ", " + selected.ToLongDateString() + "\n");
                Console.WriteLine(" Available Slots:");

                // Print slots
                for (int i = 0; i < timeSlots.Length; i++)
                {
                    if (i > 1 && !weekDay) { i = timeSlots.Length; break; }
                    Console.WriteLine("  [{0}] - {1:00}:00 to {2:00}:00 {3}", i + 1, timeSlots[i],
                        timeSlots[i] + 1, PrintAvailability(selected, timeSlots[i], myResult));
                }

                Console.WriteLine();
                // items to back
                Console.WriteLine(" [Backspace] - Back to Calendar Menu");
                Console.WriteLine(" [Esc]       - Back to Main Menu");

                // Get command from the user
                ConsoleKeyInfo keyPressed = Console.ReadKey();

                // Interpret key
                if (keyPressed.KeyChar == '1')
                {
                    selected = selected.AddHours(timeSlots[0]);
                    validKey = true;
                }
                else if (keyPressed.KeyChar == '2')
                {
                    selected = selected.AddHours(timeSlots[1]);
                    validKey = true;
                }
                else if (keyPressed.KeyChar == '3' && weekDay)
                {
                    selected = selected.AddHours(timeSlots[2]);
                    validKey = true;
                }
                else if (keyPressed.KeyChar == '4' && weekDay)
                {
                    selected = selected.AddHours(timeSlots[3]);
                    validKey = true;
                }
                else if (keyPressed.KeyChar == '5' && weekDay)
                {
                    selected = selected.AddHours(timeSlots[4]);
                    validKey = true;
                }
                else if (keyPressed.KeyChar == '6' && weekDay)
                {
                    selected = selected.AddHours(timeSlots[5]);
                    validKey = true;
                }
                else if (keyPressed.Key == ConsoleKey.Backspace)
                {
                    // select another date
                    selectAnotherDate = true;
                    validKey = false;
                }
                else if (keyPressed.Key == ConsoleKey.Escape)
                {
                    // back to main menu
                    exitScheduleMenu = true;
                    validKey = true;
                }

                if (selectAnotherDate)
                {
                    // restore selected date
                    selected = selected.Date;
                }

                // if the user wants to go back to main menu
                if (exitScheduleMenu) break;
            }


            // Return everything
            list = myResult;
            exit = exitScheduleMenu;
            return selected;
        }


        /// <summary>
        /// Assists ScheduleMenu() to print the status of the time slot after
        /// the user selects a date
        /// </summary>
        /// <param name="date">The date</param>
        /// <param name="hour">An hour from 0 to 23 representing a time slot</param>
        /// <param name="myList">The list of appointments to check</param>
        /// <returns>A string containing the availability for the especific time slot</returns>
        /// 
        private string PrintAvailability(DateTime date, int hour, List<Appointment> myList)
        {
            string retCode = "";
            if (myList == null)
            {
                myList = new List<Appointment>();
            }
            foreach (Appointment app in myList)
            {
                if (app.GetDayAndTime().Date == date.Date && app.GetDayAndTime().Hour == hour)
                {
                    retCode = "- Scheduled";
                    break;
                }
            }

            return retCode;
        }


        /// <summary>
        /// Assists ScheduleMenu() to provide more options when the user
        /// selects a time slot.
        /// </summary>
        /// <param name="timeSlot">The time slot selected</param>
        /// <param name="app">The list of appointments provided by the query</param>
        /// <returns>True if the process succeeded (user will go back to main menu);
        /// False if the user wants to go back to previous menu or the process failed</returns>
        /// 
        private bool InterpretTimeSlot(DateTime timeSlot, List<Appointment> app, string header = "")
        {
            header += " > Time Slot Menu";
            bool retCode = false;
            Appointment selected = null;
            Patient patient = null;
            Bill bill = null;

            // check if it exists
            foreach (Appointment a in app)
            {
                if (a.GetDayAndTime() == timeSlot)
                {
                    selected = a; // check if I can do this <<<<<<<<<<<<
                    break;
                }
            }

            bool invalidAction = true;
            while(invalidAction)
            {
                // Print header
                Console.Clear();
                Console.Write(header + printLine);
                Console.WriteLine(" Selected: {0}, {1}\n", timeSlot.DayOfWeek.ToString(), timeSlot.ToString());

                // if the appointment exists
                if (selected != null)
                {
                    patient = selected.FirstPatient;

                    // print appointment details
                    Console.WriteLine(" Patient: " + patient.GetLname() + ", " + patient.GetFname() +
                        " - " + patient.GethCN());
                    Console.WriteLine();

                    // Make a request to the server to check if there is a bill
                    bill = myInterpreter.FindBill(patient.PatientID, selected.GetAppointmentID());

                    if (bill != null)
                    {
                        PrintBillSummary(bill);
                    }


                    // for this appointment
                    if (bill == null)
                    {                    
                        Console.WriteLine("  [1] Reschedule Appointment");
                        Console.WriteLine("  [2] Create Bill\n");
                    }
                    else
                    {
                        if (!bill.StatusDone)
                        {
                            Console.WriteLine("  [1] Update Bill\n");
                        }
                    }
                }
                // if there is no appointment
                else
                {
                    // create an appointment
                    Console.WriteLine(" Time slot free! Create an appointment?\n");
                    Console.WriteLine(" [Enter]     - Yes");
                }

                Console.WriteLine(" [Backspace] - Back to Schedule Menu");
                Console.WriteLine(" [Esc]       - Back to Main Menu");

                // Interpret key
                ConsoleKeyInfo keyPressed = Console.ReadKey();
                if (keyPressed.Key == ConsoleKey.Backspace)
                {
                    invalidAction = false;
                }
                else if (keyPressed.Key == ConsoleKey.Escape)
                {
                    invalidAction = false;
                    retCode = true;
                }


                if (selected != null) // if the appointment exists
                {
                    if (keyPressed.KeyChar == '1')
                    {
                        if (bill == null) // no bills to this appointment
                        {
                            invalidAction = false;
                            retCode = RescheduleAppointment(selected, header);
                        }
                        else // the appointment has a bill
                        {
                            if (!bill.StatusDone)
                            {
                                invalidAction = false;
                                retCode = UpdateBillMenu(bill, header);
                            }
                        }
                    }
                    if (keyPressed.KeyChar == '2')
                    {
                        if (bill == null) // no bills to this appointment
                        {
                            invalidAction = false;
                            retCode = CreateBill(selected, header);
                        }
                    }
                }
                else // if there is no appointment
                {
                    if (keyPressed.Key == ConsoleKey.Enter)
                    {
                        invalidAction = false;
                        retCode = CreateAppointment(timeSlot, header);
                    }
                }

            }

            return retCode;
        }



        private void PrintBillSummary(Bill bill)
        {
            // query server to find serviceline and services
            List<ServiceList> listOfServices = bill.Services;

            Patient patient = myInterpreter.FindPatient(bill.Patient.GetPatientID());
            Appointment app = myInterpreter.FindAppointment(bill.Appointment.GetAppointmentID());

            if (patient != null && app != null)
            {
                Console.WriteLine($"  Bill ID:     {bill.GetBillID()}\n" +
                                  $"  Patient:     {PrintObject(patient)}\n" +
                                  $"  Appointment: {PrintObject(app)}\n" +
                                  $"  Services:");
                //                 "     A213 - 20-01-2005 - $ 25.00 - UNPROCESSED"
            }

            if (listOfServices.Count == 0)
            {
                Console.WriteLine("     * No services *");
            }
            foreach (ServiceList sl in listOfServices)
            {
                if (sl.GetQuantity() > 0)
                {
                    Service s = myInterpreter.FindService(sl.Service.GetServiceID());
                    Console.WriteLine($"     {s.GetFeeCode()} - {s.EffectiveDate.ToShortDateString()} - " +
                        $"{s.GetServiceValue().ToString("C")} - {sl.GetStatus()}");
                    //Console.WriteLine("  A213 - 20-01-2005 - $ 25.00 - UNPROCESSED");
                    //Console.WriteLine("  A314 - 20-01-2005 - $ 40.00 - UNPROCESSED");
                }
            }
            Console.Write($"  Flag Recall: ");
            if (bill.GetFlagRecall() == 0)
            {
                Console.WriteLine("No flag");
            }
            else if (app != null)
            {
                Console.WriteLine($"Return in {bill.GetFlagRecall()} weeks: " +
                    $"{app.GetDayAndTime().AddDays(7 * bill.GetFlagRecall()).ToShortDateString()}");
            }
            Console.WriteLine();
        }


        /// <summary>
        /// Assists the user to create a new appointment
        /// </summary>
        /// <param name="timeSlot">Selected Time Slot from previous menu</param>
        /// <returns>True if the process succeeded (user will go back to main menu);
        /// False if the user wants to go back to previous menu or the process failed</returns>
        /// <seealso cref="SelectTimeSlot(DateTime, List{Appointment})"/>
        /// 
        private bool CreateAppointment(DateTime timeSlot, string header = "")
        {
            header += " > Create Appointment";
            bool retCode = false;
            bool setterReturn = false;

            // Create appointment
            Appointment myAppointment = new Appointment();
            setterReturn = myAppointment.SetDayAndTime(timeSlot);

            if (setterReturn == false) // something went wrong with the setter
            {
                Console.Clear(); //
                Console.WriteLine("\n TimeSlot invalid. :( Please press any key to try again....");
                Console.ReadLine();
                retCode = false;
            }
            else // timeSlot set into Appointment!
            {
                // Find a patient
                Patient myPatient = FindOnePatient(header);

                if (myPatient == null) // couldn't find a patient
                {
                    Console.WriteLine("No patient found.");
                    retCode = false;
                }
                else // patient found
                {
                    myAppointment.FirstPatient.SetPatientID(myPatient.PatientID);
                    while(true)
                    {
                        PrintAppointmentSummary(myAppointment, header);
                        Console.WriteLine("\n Confirm?\n");
                        Console.WriteLine(" [Enter] - Confirm");
                        Console.WriteLine(" [Esc]   - Back to Previous Menu");

                        ConsoleKeyInfo userKey = Console.ReadKey();
                        if(userKey.Key == ConsoleKey.Enter)
                        {
                            int response = myInterpreter.AddAppointment(myAppointment);
                            if (response == Error.SUCCESS_GENERIC_RETURN)
                            {
                                retCode = true;
                                Console.WriteLine("\n Appointment created!");
                                Thread.Sleep(wait);
                                //Console.ReadKey();
                            }
                            else // rejected from server
                            {
                                Console.WriteLine(Error.GetError(response));
                                Console.WriteLine("\nAppointment COULD NOT be created! Press any key to try again...");
                                Console.ReadKey();
                            }
                            break;
                        }
                        else if(userKey.Key == ConsoleKey.Escape)
                        {
                            retCode = false;
                            break;
                        }
                    }
                }
            }
            return retCode;
        }


        /// <summary>
        /// Assists Schedulling menu chain to print the appointment summary. 
        /// </summary>
        /// <param name="app">The appointmet to be inserted into the DB</param>
        /// <param name="header">Schedulling Menu header</param>
        /// <seealso cref="CreateAppointment(DateTime, string)"/>
        /// 
        private void PrintAppointmentSummary(Appointment app, string header = "")
        {
            string myLine = "  ===============================================";
            header += " > Appointment Summary";
            Console.Clear();
            Console.WriteLine(header + printLine);
            Console.WriteLine(" Appointment summary");
            Console.WriteLine(myLine);
            Console.WriteLine("  TimeSlot: {0}, {1}", 
                app.GetDayAndTime().DayOfWeek.ToString(),
                app.GetDayAndTime().ToString());
            Patient patient = myInterpreter.FindPatient(app.FirstPatient.GetPatientID());

            Console.WriteLine($"  Patient: {PrintObject(patient)}");
            Console.Write(myLine);
        }


        /// <summary>
        /// This method handles the situation when the user wants the re-schedule an appointment.]
        /// </summary>
        /// <param name="current"></param>
        /// <param name="header"></param>
        /// <returns>This method will return the RescheduleAppointment of 
        /// type boolean.</returns>
        private bool RescheduleAppointment(Appointment current, string header)
        {
            header += " > Reschedule Appointment";
            bool success = false;
            bool skipProcess = false;
            bool dontExit = true;
            List<Appointment> myResults;

            while(!skipProcess)
            {
                Console.Clear();
                DateTime timeSlot = minDate;

                while (dontExit)
                {
                    timeSlot = SelectTimeSlot(out myResults, out dontExit, header);
                    if (dontExit)
                    {
                        skipProcess = true; // it means that I'll skip the whole process
                        break;
                    }

                    // check if timeSlot is available
                    List<Appointment> apps = myInterpreter.FindAppointments(timeSlot.Date, timeSlot.AddDays(7));
                    foreach(Appointment a in apps)
                    {
                        if (a.GetDayAndTime().Equals(timeSlot))
                        {
                            dontExit = true;
                            Console.Clear();
                            Console.WriteLine(header + printLine);
                            Console.WriteLine("Invalid time slot selected, please select another one");
                            Console.ReadKey();
                            break;
                        }
                    }
                }

                if (skipProcess)
                {
                    Console.WriteLine(" Cancelled by the user");
                    Thread.Sleep(wait);
                    break;
                }

                skipProcess = current.SetDayAndTime(timeSlot);
                if (!skipProcess) // setter failed for some reason
                {
                    Console.WriteLine("Failed to change current time slot...");
                    Console.ReadKey();
                    break;
                }
                else // Here I have the updated appointment
                {
                    bool validKey = false;
                    while (!validKey)
                    {
                        PrintAppointmentSummary(current, header);
                        Console.WriteLine("\n Confirm?\n");
                        Console.WriteLine(" [Enter] - Confirm");
                        Console.WriteLine(" [Esc]   - Back to Previous Menu");

                        ConsoleKeyInfo userKey = Console.ReadKey();
                        if (userKey.Key == ConsoleKey.Enter)
                        {
                            validKey = true;

                            int retFromServer = ERR_NOT_PROCESSED;
                            retFromServer = myInterpreter.UpdateAppointment(current); // <<<<<<<<<<<<<<<<<<<<<<<<
                            if (retFromServer == Error.SUCCESS_GENERIC_RETURN)
                            {
                                Console.WriteLine("Appointment successfully updated!");
                                Thread.Sleep(wait);
                                success = true;
                            }
                            else
                            {
                                Console.WriteLine("\n" + Error.GetError(retFromServer));
                                Console.WriteLine("Press any key to continue...");
                                Console.ReadKey();
                                success = false;
                            }
                        }
                        else if (userKey.Key == ConsoleKey.Escape)
                        {
                            validKey = true;
                            success = false;
                        }

                    }
                }
            }

            return success;
        }


        /// <summary>
        /// This method handles the billing menu. When the user wants to create a bill
        /// for the client, it will select this option from the main menu and this
        /// method will get called.
        /// </summary>
        /// <param name="app"></param>
        /// <returns>This method does not return anything.</returns>
        private void BillingMenu(string header = "")
        {
            bool exit = false;
            header += " > Billing Menu";
            // 1st loop
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine("  [1] Create Bill"); // ask the patient, then ask appointment
                Console.WriteLine("  [2] Find Bill"); // ask the patient >> give the option to update
                Console.WriteLine("  [3] Display Services");
                Console.WriteLine("  [4] Generate Monthly Billing File");
                Console.WriteLine("  [5] Simulate MoH Response to Monthly Billing");
                Console.WriteLine("  [6] Monthly Billing Summary");
                Console.WriteLine("\n [Esc] - Back to Main Menu");

                // get input
                ConsoleKeyInfo keyPressed = Console.ReadKey();
                if (keyPressed.KeyChar == '1') // 1. Create Bill
                {
                    exit = CreateBillMenu(header);
                }
                else if (keyPressed.KeyChar == '2') // 2. Find Bill
                {
                    exit = FindBillMenu(header);
                }
                else if (keyPressed.KeyChar == '3') // 3. Display Services
                {
                    exit = DisplayServices(header);
                }
                else if (keyPressed.KeyChar == '4') // 4. Generate Monthly Billing File
                {
                    exit = GenerateMonthlyBillingFileMenu(header);
                }
                else if (keyPressed.KeyChar == '5') // 5. Simulate MoH Response to Monthly Billing
                {
                    exit = ProcessMoHFile(header);
                }
                else if (keyPressed.KeyChar == '6') // 6. Monthly Billing Summary
                {
                    exit = MonthlyBillSummaryMenu(header);
                }
                else if (keyPressed.Key == ConsoleKey.Escape) // 3. Monthly Billing Summary
                {
                    exit = true;
                }
            }
        }


        /// <summary>
        /// Assists the user to create a bill when comming from Bill menu
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        private bool CreateBillMenu(string header = "")
        {
            string oldHeader = header;
            header += " > Create Bill";
            bool exit = false;

            Patient patient = FindOnePatient(header);

            if (patient != null)
            {
                List<Appointment> apps = myInterpreter.FindAppointments(patient.PatientID);

                object selected = SelectObjectFromList(ConvertList(apps), header, "appointment");

                Appointment appointment = selected as Appointment;

                if (appointment != null)
                {
                    exit = CreateBill(appointment, oldHeader);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine(header + printLine);

                    Console.WriteLine("  No Appointments");
                    Console.WriteLine("  Press any key to continue");
                    Console.ReadKey();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine(header + printLine);

                Console.WriteLine("  No Patient");
                Console.WriteLine("  Press any key to continue");
                Console.ReadKey();
            }

            return exit;
        }


        /// <summary>
        /// This method helps the user to create a new bill to the server.
        /// It adds a new object Bill to the list, then prompts the user
        /// to add services.
        /// </summary>
        /// <param name="appointment">An existing appointment</param>
        /// <param name="header">A header for the screen</param>
        /// <returns>TRUE if the server successfully billed the appointment
        ///            or FALSE otherwise</returns>
        private bool CreateBill(Appointment appointment, string header = "")
        {
            header += " > Create Bill";
            bool retCode = false;
            Console.Clear();

            // save some vars
            int patientID = appointment.FirstPatient.GetPatientID();
            int appID = appointment.GetAppointmentID();

            // create an instance of a bill
            Bill bill = new Bill();
            bill.Appointment = appointment;
            bill.Patient = appointment.FirstPatient;

            // try to save it into the DB
            int errCode = myInterpreter.AddBill(bill);

            if (errCode != 0)
            {
                // there is an error
                retCode = false;
                Console.WriteLine(Error.GetError(errCode) +
                    "\n Press any key to continue...");
                Console.ReadKey();
            }
            else 
            {
                // request billID to the server
                bill = myInterpreter.FindBill(patientID, appID);
                // now try to create the service list
                retCode = UpdateBillMenu(bill, header);
            }

            // get out properly
            if (retCode == true)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine("\n Success!");
                Thread.Sleep(wait);
            }

            return retCode;
        }


        /// <summary>
        /// This method helps the user to update an existing bill.
        /// It allows the user to make changes to the particular bill.
        /// The user can add and remove services as needed.
        /// </summary>
        /// <param name="bill">An existing bill</param>
        /// <param name="header">The header when applicable</param>
        /// <returns></returns>
        private bool UpdateBillMenu(Bill bill, string header = "")
        {
            header += " > Update Bill";
            bool retCode = false;

            if (bill.GetBillID() != 0)
            {
                // get list
                List<ServiceList> services = null;
                bool done = false;

                // here I'll do a while loop to add services,
                // then I will try to add them one by one to the server
                while (!done)
                {
                    services = bill.Services;
                    Console.Clear();
                    Console.WriteLine(header + printLine);

                    // print the bill
                    PrintBillSummary(bill);

                    if (!bill.StatusDone)
                    {
                        Console.WriteLine("\n [1] - Add Service");
                        if (services.Count > 0)
                        {
                            Console.WriteLine(" [2] - Delete Service");
                        }
                        Console.WriteLine(" [3] - Change Recall Flag"); // <<<<<<<<<<<<<<<<
                    }
                    Console.WriteLine(" [Esc] - Go back to main menu");

                    ConsoleKeyInfo userKey = Console.ReadKey();

                    if (userKey.Key == ConsoleKey.Escape)
                    {
                        done = true;
                    }
                    else if (userKey.KeyChar == '1')
                    {
                        if (!bill.StatusDone)
                        {
                            // add service
                            if (!AddServiceToBill(bill, header))
                            {
                                Console.WriteLine(" Failed to add service to this bill\n"+
                                    " Press any key to continue");
                                Console.ReadKey();
                            }
                        }
                    }
                    else if (userKey.KeyChar == '2')
                    {
                        if (!bill.StatusDone)
                        {
                            // delete service
                            ServiceList selected = SelectObjectFromList(ConvertList(services), header, "service") as ServiceList;
                            if (selected != null)
                            {
                                selected.SetQuantity(0);
                                int err = myInterpreter.UpdateServiceList(bill.GetBillID(), selected);
                                if (err != 0)
                                {
                                    Console.Clear();
                                    Console.WriteLine(header + printLine);
                                    Console.WriteLine(Error.GetError(err));
                                    Console.WriteLine(" Press any key to continue");
                                    Console.ReadKey();
                                }
                                else
                                {
                                    Console.Clear();
                                    Console.WriteLine(header + printLine);
                                    Console.WriteLine(" Success!");
                                    Thread.Sleep(wait);
                                }
                            }
                        }
                    }
                    else if (userKey.KeyChar == '3') // Change Recall Flag
                    {
                        if (!bill.StatusDone)
                        {
                            ChangeFlagReturn(bill, header);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Failed to load the bill");
                Console.WriteLine("Press any key to go back...");
                Console.ReadKey();
            }

            return retCode;
        }


        private void ChangeFlagReturn(Bill bill, string header)
        {
            header += " > Change Flag Return";
            bool done = false;
            bool cancel = false;
            int err = 0;

            while (!done)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);

                Console.WriteLine(
                    "  [1] - Recall in 1 week\n" +
                    "  [2] - Recall in 2 weeks\n" +
                    "  [3] - Recall in 3 weeks\n" +
                    "  [0] - No Recall\n");

                Console.WriteLine(" [Esc] - Go Back");

                ConsoleKeyInfo userKey = Console.ReadKey();

                if (userKey.Key == ConsoleKey.Escape)
                {
                    done = true;
                    cancel = true;
                }
                else if (userKey.KeyChar == '1')
                {
                    done = true;
                    bill.SetFlagRecall(1);
                }
                else if (userKey.KeyChar == '2')
                {
                    done = true;
                    bill.SetFlagRecall(2);
                }
                else if (userKey.KeyChar == '3')
                {
                    done = true;
                    bill.SetFlagRecall(3);
                }
                else if (userKey.KeyChar == '0')
                {
                    done = true;
                    bill.SetFlagRecall(0);
                }
            }

            if (!cancel)
            {
                err = myInterpreter.UpdateBill(bill);
                if (err != 0)
                {
                    Console.Clear();
                    Console.WriteLine(header + printLine);
                    Console.WriteLine(" " + Error.GetError(err));
                    Console.WriteLine(" Press any key to continue");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("\n Success");
                    Thread.Sleep(wait);
                }
            }
            else
            {
                Console.WriteLine("\n  Canceled by the user");
                Thread.Sleep(wait);
            }

        }


        private bool AddServiceToBill(Bill bill, string header)
        {
            header += " > Add Service";
            // check if the service is in the list (if yes, just change quantity, otherwise, add)
            bool retCode = false;

            Console.Clear();
            Console.WriteLine(header + printLine);

            Console.Write("  Please, type the service code or live it empty to find existing services in the system: ");
            string input = Console.ReadLine().ToUpper();

            List<Service> services = myInterpreter.FindServices(input);

            object selected = SelectObjectFromList(ConvertList(services), header, "service");

            Service newService = selected as Service;
            ServiceList existing = null;

            if (newService != null)
            {
                List<ServiceList> sls = bill.Services;
                foreach(ServiceList sl in sls)
                {
                    if (sl.Service.GetServiceID() == newService.GetServiceID())
                    {
                        existing = sl;
                        break;
                    }
                }

                if (existing != null)
                {
                    existing.SetQuantity(1);
                    int err = myInterpreter.UpdateServiceList(bill.GetBillID(), existing);
                    if (err != 0)
                    {
                        Console.Clear();
                        Console.WriteLine(header + printLine);
                        Console.WriteLine(Error.GetError(err));
                        Console.WriteLine(" Press any key to continue");
                        Console.ReadKey();
                    }
                    else
                    {
                        retCode = true;
                    }
                }
                else // doesn't exist
                {
                    ServiceList newItem = new ServiceList();
                    //newItem.SetBillID(bill.GetBillID());
                    newItem.Service.SetServiceID(newService.GetServiceID());
                    newItem.SetQuantity(1);
                    newItem.SetStatus(ServiceList.ServiceStatus.UPRC.ToString());

                    int err = myInterpreter.AddServiceList(bill.GetBillID(), newItem);
                    if (err != 0)
                    {
                        Console.Clear();
                        Console.WriteLine(header + printLine);
                        Console.WriteLine(Error.GetError(err));
                        Console.WriteLine(" Press any key to continue");
                        Console.ReadKey();
                    }
                    else
                    {
                        retCode = true;
                    }
                }
            }

            return retCode;
        }


        /// <summary>
        /// Displays the list of services available in the system
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        private bool DisplayServices(string header)
        {
            header += " > Display Services";
            bool exit = false;
            bool validKey = false;

            List<Service> services = myInterpreter.FindServices("");

            while(!validKey)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine(" List of services registered in the database:");

                if (services.Count == 0)
                {
                    Console.WriteLine("  No services registered in the database");
                    Console.WriteLine("  Press any key to continue");
                    Console.ReadKey();
                    validKey = true;
                }
                else
                {
                    foreach (Service s in services)
                    {
                        Console.WriteLine("  " + PrintObject(s));
                    }
                    Console.WriteLine();
                    Console.WriteLine(" [Esc] - Back to Previous Menu");
                    ConsoleKeyInfo userkey = Console.ReadKey();

                    if (userkey.Key == ConsoleKey.Escape)
                    {
                        validKey = true;
                    }
                }
            }


            return exit;
        }


        /// <summary>
        /// Assists the user to find bills
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        private bool FindBillMenu(string header = "")
        {
            bool retCode = false;
            header += " > Find Bill";
            Patient patient = FindOnePatient(header);
            List<Appointment> apps = new List<Appointment>();

            if (patient != null)
            {
                apps = myInterpreter.FindAppointments(patient.PatientID);
            }

            List<Bill> bills = new List<Bill>();

            Bill found = null;

            if (apps != null)
            {
                foreach (Appointment a in apps)
                {
                    found = myInterpreter.FindBill(a.FirstPatient.PatientID, a.GetAppointmentID());
                    if (found != null)
                    {
                        bills.Add(found);
                    }
                }
            }

            Bill selected = SelectObjectFromList(ConvertList(bills), header, "bill") as Bill;

            Console.Clear();
            Console.WriteLine(header + printLine);

            if (selected != null)
            {
                bool done = false;
                while (!done)
                {
                    Console.Clear();
                    Console.WriteLine(header + printLine);
                    PrintBillSummary(selected);

                    if (!selected.StatusDone)
                    {
                        Console.WriteLine(" [Enter] - Update Bill");
                    }

                    Console.WriteLine(" [Esc]   - Main Menu");

                    ConsoleKeyInfo userKey = Console.ReadKey();
                    if(userKey.Key == ConsoleKey.Escape)
                    {
                        done = true;
                        retCode = true;
                    }
                    else if (userKey.Key == ConsoleKey.Enter)
                    {
                        if (!selected.StatusDone)
                        {
                            done = true;
                            retCode = UpdateBillMenu(selected, header);
                        }
                    }
                }
            }
            else
            {
                if (patient == null)
                {
                    Console.WriteLine(" No patient selected");
                }
                else if (bills.Count == 0)
                {
                    Console.WriteLine(" No bills found");
                }
                else
                {
                    Console.WriteLine(" No bill selected");
                }
                Console.WriteLine("Press any key to go back");
                Console.ReadKey();
            }

            return retCode;
        }


        private bool GenerateMonthlyBillingFileMenu(string header)
        {
            header += " > Generate Billing File";
            bool exit = false;

            DateTime month = SelectMonth(header);

            if (!month.Equals(DateTime.MinValue))
            {
                List<string> entriesProcessed = myInterpreter.GenerateMonthlyBillingCSV(month);

                int page = 1;
                int itemsPerPage = 15;
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine($"  Updated (page {page})\n");

                for (int i = 0; i < entriesProcessed.Count; i++)
                {
                    Console.WriteLine($"  {entriesProcessed[i]}");
                    if ((i > 0) && (i % itemsPerPage) == 0)
                    {
                        page++;
                        Console.WriteLine("\n Continue...");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine(header + printLine);
                        Console.WriteLine($"  Updated (page {page})\n");
                    }
                }

                Console.WriteLine("\n  List Done!");
                Console.WriteLine("\n  Press any key to go back");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine($"  Canceled by the user");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }

            return exit;
        }


        private bool ProcessMoHFile(string header)
        {
            header += " > Process Ministry of Health File";
            bool exit = false;

            DateTime month = SelectMonth(header);

            if (!month.Equals(DateTime.MinValue))
            {
                List<string> entriesProcessed = myInterpreter.MonthlyReconcileCSV(month);

                int page = 1;
                int itemsPerPage = 15;
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine($"  Processed (page {page})\n");

                for (int i = 0; i < entriesProcessed.Count; i++)
                {
                    Console.WriteLine($"  {entriesProcessed[i]}");
                    if ((i > 0) && (i % itemsPerPage) == 0)
                    {
                        page++;
                        Console.WriteLine("\n Continue...");
                        Console.ReadKey();
                        Console.Clear();
                        Console.WriteLine(header + printLine);
                        Console.WriteLine($"  Processed (page {page})\n");
                    }
                }

                Console.WriteLine("\n  List Done!");
                Console.WriteLine("\n  Press any key to go back");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine($"  Canceled by the user");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }


            return exit;
        }


        /// <summary>
        /// Displays the billing summary for a selected month by the user.
        /// Returns true if the user wants to go back to main menu
        /// </summary>
        /// <param name="header">Optional header</param>
        /// <returns></returns>
        private bool MonthlyBillSummaryMenu(string header = "")
        {
            header += " > Monthly Bill Summary";
            bool backToMainMenu = false;
            bool validKey = false;

            DateTime monthSelected = SelectMonth(header);

            if (!monthSelected.Equals(DateTime.MinValue))
            {
                string summary = myInterpreter.MonthlyBillingSummary(monthSelected);

                while (!validKey)
                {
                    Console.Clear();
                    Console.WriteLine(header + printLine);


                    Console.WriteLine(summary);

                    Console.WriteLine("\n");
                    Console.WriteLine(" [Enter] - Back to previous Menu");
                    Console.WriteLine(" [Esc]   - Back Main Menu");

                    ConsoleKeyInfo key = Console.ReadKey();

                    if (key.Key == ConsoleKey.Enter)
                    {
                        validKey = true;
                    }
                    if (key.Key == ConsoleKey.Escape)
                    {
                        validKey = true;
                        backToMainMenu = true;
                    }
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine($"  Canceled by the user");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }


            return backToMainMenu;
        }


        // =======================================================================
        //                              GENETIC METHODS
        // =======================================================================



        private List<object> ConvertList(List<Patient> list)
        {
            List<object> result = new List<object>();
            foreach(Patient p in list)
            {
                result.Add(p);
            }
            return result;
        }

        private List<object> ConvertList(List<Bill> list)
        {
            List<object> result = new List<object>();
            foreach (Bill b in list)
            {
                result.Add(b);
            }
            return result;
        }

        private List<object> ConvertList(List<ServiceList> list)
        {
            List<object> result = new List<object>();
            foreach (ServiceList sl in list)
            {
                result.Add(sl);
            }
            return result;
        }

        private List<object> ConvertList(List<Service> list)
        {
            List<object> result = new List<object>();
            foreach (Service s in list)
            {
                result.Add(s);
            }
            return result;
        }

        private List<object> ConvertList(List<Appointment> list)
        {
            List<object> result = new List<object>();
            foreach (Appointment a in list)
            {
                result.Add(a);
            }
            return result;
        }


        /// <summary>
        /// Generic method that finds one object in a list of objects.
        /// If the user press Escape or list is empty, it will return NULL.
        /// </summary>
        /// <param name="list">A list of objects</param>
        /// <param name="header">Header</param>
        /// <param name="itemName"></param>
        /// <returns>The object selected from the user or NULL</returns>
        private object SelectObjectFromList(List<object> list, string header, string itemName)
        {
            header += " > Select One";
            object selected = null;
            bool select = false;

            if (list.Count == 0)
            {
                select = true;
            }
            if (list.Count == 1)
            {
                selected = list[0];
                select = true;
            }

            int page = 1;
            int itemsPerPage = 8;

            while (!select)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);
                Console.WriteLine($"  Please selecte one {itemName} from the list:");

                if (list.Count > itemsPerPage)
                {
                    Console.WriteLine("  * Use the arrows to change the pages");
                }

                Console.WriteLine();

                for (int i = 0; i < itemsPerPage; i++)
                {
                    int currentIndex = ((page - 1) * itemsPerPage) + i;
                    if (list.Count <= currentIndex)
                    {
                        break;
                    }

                    Console.WriteLine($"  [{i+1}] - {PrintObject(list[currentIndex])}");
                }

                Console.WriteLine();
                if (page > 1)
                {
                    Console.Write(" << ");
                }
                else
                {
                    Console.Write("    ");
                }

                if (list.Count > itemsPerPage)
                {
                    Console.Write($" Page {page} ");
                }

                if ((page * itemsPerPage) < list.Count)
                {
                    Console.Write(" >> ");
                }
                else
                {
                    Console.Write("    ");
                }

                if (list.Count > itemsPerPage)
                {
                    Console.WriteLine("\n");
                }
                else
                {
                    Console.WriteLine();
                }

                Console.WriteLine(" [Esc] - Cancel");

                // get key
                ConsoleKeyInfo userKey = Console.ReadKey();
                if (userKey.Key == ConsoleKey.Escape)
                {
                    break;
                }
                else if (userKey.Key == ConsoleKey.LeftArrow)
                {
                    if (page > 1) page--;
                }
                else if (userKey.Key == ConsoleKey.RightArrow)
                {
                    if ((page * itemsPerPage) < list.Count) page++;
                }
                else if (userKey.KeyChar >= '1' && userKey.KeyChar <= '9') // limitation related to previous loop
                {
                    // Select one
                    int intInput = Packager.GetInteger(userKey.KeyChar.ToString());
                    int recalIndex = ((page - 1) * itemsPerPage) + intInput - 1;
                    if (list.Count > recalIndex && recalIndex >= 0)
                    {
                        selected = list[recalIndex];
                        select = true;
                    }
                }

            }

            return selected;
        }


        /// <summary>
        /// Helps View layer to print informaton about especific
        /// objects. 
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private string PrintObject(object o)
        {
            string retCode = "";

            Patient pat = o as Patient;
            Service ser = o as Service;
            Bill bill = o as Bill;
            ServiceList sl = o as ServiceList;
            Appointment app = o as Appointment;

            if (pat != null)
            {
                retCode = $"{pat.GetFname()} {pat.GetLname()} - HCN: {pat.GethCN()}";
            }
            else if (ser != null)
            {
                retCode = $"{ser.GetFeeCode()} - {ser.EffectiveDate.ToShortDateString()} - " +
                    $"{ser.GetServiceValue().ToString("C")}";
            }
            else if (bill != null)
            {
                Appointment a = myInterpreter.FindAppointment(bill.Appointment.GetAppointmentID());
                retCode = $"{a.GetDayAndTime().ToShortDateString()} - ID: {bill.GetBillID()}"; 
            }
            else if (sl != null)
            {
                Service service = myInterpreter.FindService(sl.Service.GetServiceID());
                retCode = $"{service.GetFeeCode()} - {service.EffectiveDate.ToShortDateString()} - " +
                    $"{service.GetServiceValue().ToString("C")}";
            }
            else if (app != null)
            {
                retCode = $"{app.GetDayAndTime().DayOfWeek.ToString()}, {app.GetDayAndTime().ToString()}";
            }
            else
            {
                retCode = o.ToString();
            }

            return retCode;
        }


        // =======================================================================
        //                      TEST METHODS (DELETE IF NOT USING)
        // =======================================================================


            
        // =======================================================================
        //                            PRIVATE METHODS
        // =======================================================================

        /// <summary>
        /// This method will print the calendar and highlight a day.
        /// It allows the user to use the arrow keys to navigate and
        /// Enter key to select a day. This method returns the day selected
        /// by the user. This method supports min and max range.
        /// </summary>
        /// <param name="header">A custom header</param>
        /// <param name="date">The selected in the first run</param>
        /// <param name="minDate">Minimum day within a range</param>
        /// <param name="maxDate">Maximum day within a range</param>
        /// <returns>The day selected by the user or DateTime.MinValue</returns>
        /// 
        private DateTime PrintCalendar(string header, DateTime date, DateTime minDate, DateTime maxDate)
        {
            // Basic setup
            ConsoleColor highlight = ConsoleColor.Blue;

            // Check if range is messed up
            if (minDate > maxDate)
            {
                DateTime tmp = minDate;
                minDate = maxDate;
                maxDate = tmp;
            }

            // Check if it's out of range
            DateTime myDate = date;
            if (date < minDate || date > maxDate)
            {
                myDate = minDate;
                if (date > maxDate)
                {
                    myDate = maxDate;
                }
            }

            // Support variables
            ConsoleKeyInfo consoleKey = new ConsoleKeyInfo();
            DateTime monthRef = new DateTime(myDate.Year, myDate.Month, 1);
            int dayOfWeek = (int)monthRef.DayOfWeek;

            // Main loop
            while (true)
            {
                Console.Clear();
                // Print header
                Console.WriteLine(header);
                Console.WriteLine("\t\t     {0} {1}", GetMonthName(myDate.Month), myDate.Year);
                Console.WriteLine("Sun\tMon\tTue\tWed\tThu\tFri\tSat\n");

                // Recalculate month reference
                monthRef = DateTime.Parse(myDate.Year.ToString() + "-" + myDate.Month + "-01");
                dayOfWeek = (int)monthRef.DayOfWeek;

                // Align the first day of the week
                for (int i = 0; i < dayOfWeek; i++)
                {
                    Console.Write("\t");
                }

                // Print days
                for (int i = 1; i <= 31; i++)
                {
                    if (i > DateTime.DaysInMonth(monthRef.Year, monthRef.Month))
                    {
                        break;
                    }

                    if (i == myDate.Day)
                    {
                        Console.BackgroundColor = highlight;
                        Console.Write(i);
                        Console.ResetColor();
                        Console.Write("\t");
                    }
                    else
                    {
                        Console.Write(i + "\t");
                    }

                    dayOfWeek++;
                    if (dayOfWeek == 7)
                    {
                        dayOfWeek = 0;
                        Console.WriteLine("\n");
                    }
                }

                // Fix last line (to avoid having 4 lines after the calendar)
                if(dayOfWeek != 0) Console.WriteLine("\n");

                Console.WriteLine(" [Esc] - Cancel");

                // Read Key
                consoleKey = Console.ReadKey();
                if (consoleKey.Key == ConsoleKey.Enter)
                {
                    break;
                }
                else if (consoleKey.Key == ConsoleKey.Escape)
                {
                    myDate = DateTime.MinValue;
                    break;
                }
                else if (consoleKey.Key == ConsoleKey.LeftArrow)
                {
                    if (myDate.Subtract(TimeSpan.FromDays(1)) >= minDate)
                        myDate = myDate.Subtract(TimeSpan.FromDays(1));
                }
                else if (consoleKey.Key == ConsoleKey.RightArrow)
                {
                    if (myDate.AddDays(1) <= maxDate)
                        myDate = myDate.AddDays(1);
                }
                else if (consoleKey.Key == ConsoleKey.UpArrow)
                {
                    if (myDate.Subtract(TimeSpan.FromDays(7)) >= minDate)
                        myDate = myDate.Subtract(TimeSpan.FromDays(7));
                }
                else if (consoleKey.Key == ConsoleKey.DownArrow)
                {
                    if (myDate.AddDays(7) <= maxDate)
                        myDate = myDate.AddDays(7);
                }
            }
            return myDate;
        }



        /// <summary>
        /// Helps the user to select a month when necessary
        /// </summary>
        /// <param name="header">Optional header to be printed on screen</param>
        /// <returns>DateTime object representing the month selected</returns>
        private DateTime SelectMonth(string header = "")
        {
            header += " > Select Month";
            DateTime selectedMonth = minDate;
            bool selected = false;

            while(!selected)
            {
                Console.Clear();
                Console.WriteLine(header + printLine);

                Console.WriteLine(" Please select a month:");
                Console.WriteLine($" <<  {GetMonthName(selectedMonth.Month)}, {selectedMonth.Year}  >>");
                Console.WriteLine();
                Console.WriteLine(" [Enter] - Select Month");
                Console.WriteLine(" [Esc]   - Cancel");

                ConsoleKeyInfo keyInfo = Console.ReadKey();

                if (keyInfo.Key == ConsoleKey.LeftArrow)
                {
                    if (selectedMonth.Month > minDate.Month)
                    {
                        selectedMonth = selectedMonth.AddMonths(-1);
                    }
                }
                if (keyInfo.Key == ConsoleKey.RightArrow)
                {
                    if (selectedMonth.Month < maxDate.Month)
                    {
                        selectedMonth = selectedMonth.AddMonths(1);
                    }
                }
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    selected = true;
                }
                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    selected = true;
                    selectedMonth = DateTime.MinValue;
                }
            }

            return selectedMonth;
        }



        /// <summary>
        /// This method takes an integer representing a month (1 to 12)
        /// and returns its name. If the integer is invalid it will return
        /// "Unknown";
        /// </summary>
        /// <param name="month">Represents the month (1 to 12)</param>
        /// <returns>A string with the name of the month provided</returns>
        /// 
        private string GetMonthName(int month)
        {
            string retCode = "Unknown";
            switch (month)
            {
                case 1: retCode = "January"; break;
                case 2: retCode = "February"; break;
                case 3: retCode = "March"; break;
                case 4: retCode = "April"; break;
                case 5: retCode = "May"; break;
                case 6: retCode = "June"; break;
                case 7: retCode = "July"; break;
                case 8: retCode = "August"; break;
                case 9: retCode = "September"; break;
                case 10: retCode = "October"; break;
                case 11: retCode = "November"; break;
                case 12: retCode = "December"; break;
            }
            return retCode;
        }

    }
}
