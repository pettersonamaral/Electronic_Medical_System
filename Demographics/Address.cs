﻿/*****************************************************************************************************
* FILE : 		  Address.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Nov-09
* SECOND VERSION: 2017-Dec-08
* 

* DESCRIPTION/Requirements: 

* This class/file contains all the information about the address. It sets all the accessors and mutators
* for all the fields here which are being called from different part of this system. 
* It also validates all the fields in order to ensure that everything is in working order.

*****************************************************************************************************/

using System;

#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()

namespace Demographics
{
    /// <summary>
    /// <b>Description: </b>This class contains all the information about the address.
    /// It contains the necessary validation as well as functions that are being
    /// called from different class.
    /// </summary>
    public class Address : IEquatable<Address>
    {
        #region Constants

        //Error codes constants
        const int INVALID_ADDRESS_LINE_ONE = -3202;
        const int INVALID_ADDRESS_LINE_TWO = -3203;
        const int INVALID_CITY = -3204;
        const int INVALID_PROVINCE = -3205;
        const int INVALID_PHONE_NUMBER = -3206;

        //Declaration of the constants
        public const int maxLengthAddressID = 8;
        public const int maxLengthAdressLineOne = 60;
        public const int maxLengthAdressLineTwo = 60;
        public const int maxLengthCity = 60;
        public const int lengthProvince = 2;
        public const int lengthPhoneNumber = 13;
        
        //Constants for all Canadian provinces on 2017-Nov-14
        const string Ontario = "ON";
        const string Alberta = "AB";
        const string BritCol = "BC";
        const string Labrador = "NL";
        const string NewScot = "NS";
        const string PrinceEd = "PE";
        const string NewBrunswick = "NB";
        const string Quebec = "QC";
        const string Manitoba = "MB";
        const string Saskatchewan = "SK";
        const string Nunvut = "NU";
        const string NortwestTer = "NT";
        const string Yukon = "YT";

        #endregion

        #region Attributes

        //Variable declaration
        private int adressID;
        private string addressLineOne;
        private string addressLineTwo;
        private string city;
        private string province;
        private string phoneNumber;

        #endregion

        #region Constructors

        /**************************************************************************************************************
        FUNCTION : Address()
        DESCRIPTION :
            Constractor for address class

        PARAMETERS :<data-type> <name> : <purpose>
            N/A

        RETURN :<data-type> : <indicates>
            N/A
        ****************************************************************************************************************/
        public Address()
        {
            adressID = 0;
            addressLineOne = "";
            addressLineTwo = "";
            city = "";
            province = "";
            phoneNumber = "";
        }


        public Address(int newAddressID, string newAdressLineOne, string newAdressLineTwo, string newCity, string newProvince, string newPhoneNumber)
        {
            adressID = newAddressID;
            addressLineOne = newAdressLineOne;
            addressLineTwo = newAdressLineTwo;
            city = newCity;
            province = newProvince;
            phoneNumber = newPhoneNumber;
        }

        #endregion

        #region Setters and Getters

        /// <summary>
        /// Setting address ID
        /// </summary>
        /// <param name="newAdressID"></param>
        /// <returns>NOTA</returns>
        public bool SetAdressID(int newAdressID)
        {
            if (newAdressID > 0)
            {
                adressID = newAdressID;
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Setting the Address Line One 
        /// </summary>
        /// <param name="newAdressLineOne"></param>
        /// <returns>boolean value as a retCode</returns>
        public bool SetAdressLineOne(string newAdressLineOne)
        {
            bool retCode = false;
            if (newAdressLineOne.Length < maxLengthAdressLineOne && newAdressLineOne.Length > 1)
            {
                retCode = true;
            }
            if (retCode == true)
            {
                for (int i = 0; i < newAdressLineOne.Length; i++) //check every character
                {
                    char c = newAdressLineOne[i];
                    if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || (c >= 128 && c <= 154) || (c >= 160 && c <= 167)))//ascii ranges allowed
                    {
                        if (!(c == ' ' || c == '_' || c == '-' || c == '\'' || c == '.' || c == ';' || c == '&')) //additional allowed characters
                        {
                            retCode = false; //invalid character found - not valids
                            break;
                        }
                    }
                }
            }
            addressLineOne = newAdressLineOne;
            return retCode;
        }


        /// <summary>
        /// Setting the Address Line Two 
        /// </summary>
        /// <param name="newAdressLineTwo"></param>
        /// <returns>boolean value as a retCode</returns>
        public bool SetAdressLineTwo(string newAdressLineTwo)
        {
            bool retCode = false;
            if (newAdressLineTwo.Length < maxLengthAdressLineTwo )
            {
                retCode = true;
            }
            if ((retCode == true) && (newAdressLineTwo.Length > 1))
            {
                for (int i = 0; i < newAdressLineTwo.Length; i++) //check every character
                {
                    char c = newAdressLineTwo[i];
                    if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || (c >= 128 && c <= 154) || (c >= 160 && c <= 167)))//ascii ranges allowed
                    {
                        if (!(c == ' ' || c == '_' || c == '-' || c == '\'' || c == '.' || c == ';' || c == '&')) //additional allowed characters
                        {
                            retCode = false; //invalid character found - not valids
                            break;
                        }
                    }
                }
            }
            addressLineTwo = newAdressLineTwo;
            return retCode;
        }


        /// <summary>
        /// Setting the City 
        /// </summary>
        /// <param name="newCity"></param>
        /// <returns>boolean value as a retCode</returns>
        public bool SetCity(string newCity)
        {
            bool retCode = false;
            if (newCity.Length < maxLengthCity && newCity.Length > 1) //allow input between 1 and 60 characters only
            {
                retCode = true; ; //invalid character found - not valid
            }
            for (int i = 0; i < newCity.Length; i++)//check every character
            {
                if (retCode)
                {
                    char c = newCity[i];
                    if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= 128 && c <= 154) || (c >= 160 && c <= 167))) //allowed ascii ranges
                    {
                        if (!(c == ' ' || c == '-' || c == '\'' || c == '.')) //additonal allowed characters
                        {
                            retCode = false; //invalid character found - not valid
                            break;
                        }
                    }
                }
            }
            if (retCode)
            {
                city = newCity;
            }
            return retCode;
        }


        /// <summary>
        /// Setting the Province 
        /// </summary>
        /// <param name="newProvince"></param>
        /// <returns>boolean value as a retCode</returns>
        public bool SetProvince(string newProvince)
        {
            bool retCode = false;
            //check province 
            if ((newProvince == Ontario) || (newProvince == Alberta) || (newProvince == BritCol) ||
                (newProvince == Labrador) || (newProvince == NewScot) || (newProvince == PrinceEd) ||
                (newProvince == NewBrunswick) || (newProvince == Quebec) || (newProvince == Manitoba) ||
                (newProvince == Saskatchewan) || (newProvince == Nunvut) || (newProvince == NortwestTer) ||
                (newProvince == Yukon))
            {
                retCode = true;
                province = newProvince;
            }
            else
            {
                retCode = false;
            }
           
            return retCode;
        }

      
        /// <summary>
        /// Sets phone number if it's valid. This method checks the area code.
        /// It accepts the most common formats of input, and normalize it to
        /// xxx-xxx-xxxx
        /// </summary>
        /// <param name="newPhoneNumber">input from the user</param>
        /// <returns>boolean value as a retCode</returns>
        public bool SetPhoneNumber(string newPhoneNumber)
        {
            bool retCode = false;
            string areaCode = "";

            // Normalize format
            string formattedPhone = NormalizePhoneNumber(newPhoneNumber);

            //
            if (formattedPhone != "")
            {
                retCode = true;
                areaCode = formattedPhone.Substring(0, 3);
            }

            // Check area code
            if (retCode)
            {
                if ((areaCode == "403") || (areaCode == "587") ||
                   (areaCode == "780") || (areaCode == "825") ||
                   (areaCode == "236") || (areaCode == "250") ||
                   (areaCode == "604") || (areaCode == "778") ||
                   (areaCode == "204") || (areaCode == "431") ||
                   (areaCode == "506") || (areaCode == "709") ||
                   (areaCode == "902") || (areaCode == "782") ||
                   (areaCode == "226") || (areaCode == "249") ||
                   (areaCode == "289") || (areaCode == "343") ||
                   (areaCode == "365") || (areaCode == "416") ||
                   (areaCode == "437") || (areaCode == "519") ||
                   (areaCode == "613") || (areaCode == "647") ||
                   (areaCode == "705") || (areaCode == "807") ||
                   (areaCode == "905") || (areaCode == "418") ||
                   (areaCode == "438") || (areaCode == "450") ||
                   (areaCode == "514") || (areaCode == "579") ||
                   (areaCode == "581") || (areaCode == "819") ||
                   (areaCode == "873") || (areaCode == "306") ||
                   (areaCode == "639") || (areaCode == "867"))//only this area codes are avaluable in Canada
                {
                    phoneNumber = formattedPhone;
                    retCode = true;
                }
                else
                {
                    retCode = false;
                }
            }
            return retCode; 
        }


        /// <summary>
        /// Getting the address ID for address
        /// </summary>
        /// <returns>the address ID as int</returns>
        public int GetAdressID()
        {
            return adressID;
        }


        /// <summary>
        /// Getting the Address Line One for address
        /// </summary>
        /// <returns>the Address Line One as string</returns>
        public string GetAddressLineOne()
        {
            return addressLineOne;
        }
        

        /// <summary>
        /// Getting the Address Line Two for address
        /// </summary>
        /// <returns>the Address Line Two as string</returns>
        public string GetAddressLineTwo()
        {
            return addressLineTwo;
        }


        /// <summary>
        /// Getting the City for address
        /// </summary>
        /// <returns>the City as string</returns>
        public string GetCity()
        {
            return city;
        }


        /// <summary>
        /// Getting the Province for address
        /// </summary>
        /// <returns>the Province as string</returns>
        public string GetProvince()
        {
            return province;
        }


        /// <summary>
        /// Getting the Phone Number for address
        /// </summary>
        /// <returns>the Phone Number as string</returns>
        public string GetPhoneNumber()
        {
            return phoneNumber;
        }

        #endregion

        #region Validation Methods

        /// <summary>
        /// This function is checking if the address information is valid or not.
        /// It returns 0 if is valid
        /// </summary>
        /// <returns>Returns an integer value</returns>
        public int ValidateAdress()
        {
            int retCode = 0;
            if (GetAddressLineOne() == "")
            {
                retCode = INVALID_ADDRESS_LINE_ONE;
            }
            else if (GetCity() == "")
            {
                retCode = INVALID_CITY;
            }
            else if (GetProvince() == "")
            {
                retCode = INVALID_PROVINCE;
            }
            else if (GetPhoneNumber() == "")
            {
                retCode = INVALID_PHONE_NUMBER;
            }
            return retCode;
        }


        /// <summary>
        /// This function is checking if string is empty.
        /// </summary>
        /// <param name="stringToCheck">string to be checked</param>
        /// <returns>boolean value as a retCode</returns>
        public bool IsEmpty
        {
            get
            {
                bool retCode = false;
                if (addressLineOne == "" &&
                    addressLineTwo == "" &&
                    city == "" &&
                    province == "" &&
                    phoneNumber == "")
                {
                    retCode = true;
                }
                return retCode;
            }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Checks if two addresses are the same. Two addresses are the sama if city, province and two addrress lines are the same
        /// </summary>
        /// <param name="other"></param>
        /// <returns>bool as a resault</returns>
        public bool Equals(Address other)
        {
            if (other == null) return false;
            bool retCode = false;

            if (addressLineOne == other.addressLineOne &&
                addressLineTwo == other.addressLineTwo &&
                city == other.city &&
                province == other.province &&
                phoneNumber == other.phoneNumber)
            {
                retCode = true;
            }

            return retCode;
        }

        
        public static bool operator== (Address left, Address right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }


        public static bool operator!= (Address left, Address right)
        {
            return !(left == right);
        }


        /// <summary>
        /// Overridden ToString() that prints the address information
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string retcode = "";
            retcode += $"{GetAddressLineOne()}\n{GetAddressLineTwo()}\n";
            retcode += $"{GetCity()}, {GetProvince()}\n";
            retcode += $"Phone: {GetPhoneNumber()}";
            return retcode;
        }

        #endregion

        #region Supportive Methods

        /// <summary>
        /// This method normalizes the phone number. It checks if the input
        /// is a valid phone number and properly format it to the following
        /// ddd-ddd-dddd. If the number of digits is invalid or the input is
        /// not valid for some reason, it returns an empty string
        /// </summary>
        /// <param name="input">The input to be tested</param>
        /// <returns>A properly formatted phone number or an empty string</returns>
        /// 
        private string NormalizePhoneNumber(string input)
        {
            int phoneNumberSize = 12;
            string retCode = "";

            char[] charArray = input.Trim().ToCharArray();

            int maxSize = charArray.Length;
            if (charArray.Length <= phoneNumberSize) // check size
            {
                maxSize = phoneNumberSize;
            }

            bool valid = true;
            int resultIndex = 0;
            char[] myResult = new char[maxSize];

            for (int i = 0; i < charArray.Length; i++)
            {
                if (char.IsNumber(charArray[i]))
                {
                    myResult[resultIndex] = charArray[i];
                    resultIndex += 1;
                    if (resultIndex == 3 || resultIndex == 7) // insert '-'
                    {
                        myResult[resultIndex] = '-';
                        resultIndex += 1;
                    }
                }
                else if (charArray[i] != '(' && charArray[i] != ')' &&
                    charArray[i] != '-' && charArray[i] != ' ')
                {
                    valid = false;
                }
            }

            retCode = new string(myResult).Replace('\0', ' ').Trim();

            if (retCode.Length != phoneNumberSize)
            {
                valid = false;
            }

            if (!valid)
            {
                retCode = "";
            }


            return retCode;
        }

        #endregion
    }
}
