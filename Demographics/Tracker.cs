﻿/*****************************************************************************************************
* FILE : 		Tracker.cs
* PROJECT : 	SQ-I Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2017-Dec-16

* DESCRIPTION/Requirements: 

This class provides all functionality to Demographics Class library, this class connects to Patient, 
Address and Appointment class to make them functional.
This class allows to search, add and manipulate data within Demographics Class library.

*****************************************************************************************************/
/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Support;

namespace Demographics
{

    /// <summary>
    /// <b>Description: </b>This class provides all functionality to Demographics Class library, this class connects to Patient, Address and Appointment class to make them functional.
    /// This class allows to search, add and manipulate data within Demographics Class library.
    /// </summary>
    /// 
    public class Tracker : IDemographics
    {
        // Attributes - Lists
        private List<Address> addressList = new List<Address>();
        private List<Appointment> appointmentsList = new List<Appointment>();
        private List<Patient> patientList = new List<Patient>();



        // Interfaces
        private static FileIO fileIO;
        private static Logging logging;



        //Constants
        private const int SUCCESS_GENERIC_RETURN = 0;
        private const string DivDemographicTrackerFiles = "|";
        public static string[] FieldSeparator = { DivDemographicTrackerFiles };
        public enum DemoClasses { Address, Appointment, Patient };

        private const int ADDRESS_ALREADY_EXISTS = -3407;
        private const int ADD_ADDRESS_UNKNOWN_ERROR = -3408;
        private const int DEFAULT_DATE_AND_TIME = -3049;
        private const int PATIENT_DOES_NOT_HAVE_APPOINTMENT = -3410;
        private const int APPOINTMENT_ALREADY_EXISTS = -3411;
        private const int INVALID_PATIENT_OBJ_TO_UPDATE = -3412;
        private const int UNABLE_TO_UPDATE_PATIENT_IN_LIST = -3413;
        private const int UPDATE_PATIENT_UNKNOWN_ERROR = -3414;
        private const int UPDATE_ADDRESS_UNKNOWN_ERROR = -3415;
        private const int INVALID_ADDRESS_OBJ_TO_UPDATE = -3416;
        private const int UnableTo_UPDATE_ADDRESS_IN_LIST = -3417;
        private const int UPDATE_APPOINTMENT_UNKNOWN_ERROR = -3418;
        private const int INVALID_APPOINTMENT_OBJ_TO_UPDATE = -3419;
        private const int UnableTo_UPDATE_APPOINTMENT_IN_LIST = -3420;
        private const int INVALID_PATIENT_OBJ = -3421;
        private const int PATIENT_ALREADY_EXISTS = -3422;

        private const int GetIntegerFailFormat = -3441;
        private const int GetIntegerFailOverflow = -3442;
        private const int AddPatientFromFileAlreadyExists = -3443;
        private const int AddAddressFromFileAlreadyExists = -3444;
        private const int AddAppointmentFromFileAlreadyExists = -3445;
        private const int ReadPatientFileFailure = -3446;
        private const int ReadAddressFileFailure = -3447;
        private const int ReadAppointmentFileFailure = -3448;
        private const int WritePatientObjIntoFileFail = -3449;
        private const int WriteAddressObjIntoFileFail = -3450;
        private const int WriteAppointmentObjIntoFileFail = -3451;
        private const int PATIENT_LIST_ERROR = -3452;
        private const int UNABLE_TO_ADD_PATIENT = -3453;
        private const int ADDRESS_LIST_ERROR = -3454;
        private const int APPOINTMENT_LIST_ERROR = -3455;
        private const int UNABLE_TO_ADD_APPOINTMENT = -3456;





        private const int ReadFileToStringArrayFail = -3452;

        // =======================================================================
        //                              CONSTRUCTOR
        // =======================================================================



        ///  
        /// \CONSTRUCTOR 
        /// \details <b> Tracker CONSTRUCTOR in Demographics </b> 
        /// \param FileIO - <b>fileHandler</b>
        /// 
        /// \return NOTA - <b> NOTA </b>
        /// 
        /// \see <i>~Tracker()</i> 
        /// 
        public Tracker(FileIO fileHandler, Logging logHandler)
        {
            // Set interfaces
            fileIO = fileHandler;
            logging = logHandler;
        }

        // TEMPORARILY CREATED TO SUPPORT BILLING CLASS TRACKER

        public Tracker()
        {

        }


        // =======================================================================
        //                            PUBLIC METHODS
        // =======================================================================



        ///  
        /// \brief setter for PatientList 
        /// \details <b> set  PatientList, setter for PatientList </b> 
        /// \param List<Address> - <b> newPatientList </b> - new list
        /// 
        /// \return NOTA - <b> NOTA </b>
        /// 
        /// \see <i>Tracker(FileIO fileHandler, Logging logHandler)</i> 
        /// 
        public int SetPatientList(List<Patient> newPatientList)
        {
            return SUCCESS_GENERIC_RETURN;
        }


        ///  
        /// \brief setter for AdreessList 
        /// \details <b> set  AdreesstList, setter for AdreessList </b> 
        /// \param List<Address> - <b> newAdreessList </b> - new list
        /// 
        /// \return NOTA - <b> NOTA </b>
        /// 
        /// \see <i>Tracker(FileIO fileHandler, Logging logHandler)</i> 
        /// 
        public int SetAdreessList(List<Address> newAddressList)
        {
            return SUCCESS_GENERIC_RETURN;
        }


        ///  
        /// \brief setter for AppountmentList 
        /// \details <b> set  AppountmentList, setter for AppountmentList </b> 
        /// \param List<Appointment> - <b> newAppointmentList </b> - new list
        /// 
        /// \return NOTA - <b> NOTA </b>
        /// 
        /// \see <i>Tracker(FileIO fileHandler, Logging logHandler)</i> 
        /// 
        public int SetApointmentList(List<Appointment> newAppointmentList)
        {
            return SUCCESS_GENERIC_RETURN;
        }


        ///  
        /// \brief getter for PatientList 
        /// \details <b> return  patientList, getter for PatientList </b> 
        /// \param NOTA - <b> NOTA </b> 
        /// 
        /// \return List<Patient> - <b> patientList </b> -  PatientList
        /// 
        /// \see <i>Tracker(FileIO fileHandler, Logging logHandler)</i> 
        /// 
        public List<Patient> GetPatientList()
        {
            return patientList;
        }


        ///  
        /// \brief getter for AdreessList 
        /// \details <b> return  addressList, getter for AdreessList </b> 
        /// \param NOTA - <b> NOTA </b> 
        /// 
        /// \return List<Address> - <b> addressList </b> -  AddressList
        /// 
        /// \see <i>Tracker(FileIO fileHandler, Logging logHandler)</i> 
        /// 
        public List<Address> GetAdreessList()
        {
            return addressList;
        }


        ///  
        /// \brief getter for AppountmentList 
        /// \details <b> return  AppountmentList, getter for AppountmentList </b> 
        /// \param NOTA - <b> NOTA </b> 
        /// 
        /// \return List<Appointment> - <b> appointmentsList </b> -  AppountmentList
        /// 
        /// \see <i>Tracker(FileIO fileHandler, Logging logHandler)</i> 
        /// 
        public List<Appointment> GetAppointmentList()
        {
            return appointmentsList;
        }


        ///  
        /// \brief This function adds valid patient object to Patient list if it does nt exist 
        /// \details <b> This function gets new patient object, check if it is valid, if it is, function also checks if it does not exist yet. 
        /// If this object is not in list, function generate ID for this patient and add to list.</b> 
        /// \param Patient - <b> patient </b> - new Patient object  
        /// \return int - <b> retCode </b> - error/sucess status; if error returns negative velue related to particular method 
        /// 
        /// \see <i>patient.GetFname(),patient.GetLname(),patient.GethCN(),patient.GetMinitial(),patient.GetdateOfBirth(),patient.GetSex()patient.GetHeadOfHouse(),
        /// patient.GetAddressID(),GetAdreessList(),SetPatientList(List<Patient> newPatientList), List<Patient> GetPatientList()</i> 
        /// 
        public int AddPatient(Patient patient)
        {
            int retCode = SUCCESS_GENERIC_RETURN;
            // check if the address is valid
            if (patient.ValidatePatient() < 0)
            {
                retCode = INVALID_PATIENT_OBJ;
            }
            // check if this address already exists
            if (retCode == SUCCESS_GENERIC_RETURN)
            {
                if (FindPatient(patient.GethCN())==null)
                {
                    if (patient.PatientID == 0)
                    {
                        try
                        {
                            patientList.Sort();
                        }
                        catch(InvalidOperationException)
                        {
                            retCode = PATIENT_LIST_ERROR;
                        }
                        try
                        {
                            int lastIndex = 0;
                            if (patientList.Count > 0)
                            {
                                lastIndex = patientList.Count - 1;
                                patient.PatientID = patientList[lastIndex].GetPatientID() + 1;
                            }
                            else
                            {
                                patient.PatientID = 1;
                            }
                        }
                        catch (Exception e)
                        {
                            retCode = UNABLE_TO_ADD_PATIENT;
                        }
                    }
                    try
                    {
                        patientList.Add(patient);
                        AddPatientToFile(patient);
                    }
                    catch (Exception e)
                    {
                        retCode = PATIENT_LIST_ERROR;
                    }
                }
                else
                {
                    retCode = PATIENT_ALREADY_EXISTS;
                }
            }
            // return error code when applicable
            return retCode;
        }


        ///  
        /// \brief This function adds valid Address object to Address list if it does nt exist 
        /// \details <b> This function gets new Adress object, check if it is valid, if it is, function also checks if it does not exist yet. 
        /// If this object is not in list, function generate ID for this Adress and add to list.</b> 
        /// \param Adress - <b> adress </b> - new Adress object  
        /// \return int - <b> retCode </b> - error/sucess status; if error returns negative velue related to particular method 
        /// 
        /// \see <i>address.GetHCN(),address.GetAdressLineOne(),address.GetAdressLineTwo(),address.GetCity(),address.GetProvince(),address.GetPhoneNumber(),
        /// SetAdreessList(List<Address> newAddressList),List<Address> GetAdreessList()</i> 
        /// 
        public int AddAddress(Address address)
        {
            int retCode = ADD_ADDRESS_UNKNOWN_ERROR;
            if(address.ValidateAdress()==0)
            {
                retCode = SUCCESS_GENERIC_RETURN;
            }
            else
            {
                retCode = DEFAULT_DATE_AND_TIME;
            }           
            // check if this address already exists
            if (retCode == SUCCESS_GENERIC_RETURN)
            {
                if (FindAddress(address.GetHCN())==null)
                {
                    retCode = ADDRESS_ALREADY_EXISTS;
                }
            }
            // if not, create an ID for it
            if (address.GetAdressID() == SUCCESS_GENERIC_RETURN)
            {
                try
                {
                    addressList.Sort();
                }
                catch (InvalidOperationException)
                {
                    retCode = ADDRESS_LIST_ERROR;
                }
                try
                {
                    int lastIndex = 0;
                    if (patientList.Count > 0)
                    {
                        lastIndex = addressList.Count - 1;
                        address.SetAdressID(addressList[lastIndex].GetAdressID() + 1);
                    }
                    else
                    {
                        address.SetAdressID(1);
                    }
                }
                catch (Exception e)
                {
                    retCode = ADD_ADDRESS_UNKNOWN_ERROR;
                }
            }
            // insert it into the list
            if (retCode == SUCCESS_GENERIC_RETURN)
            {
                try
                {
                    addressList.Add(address);
                    AddAddressToFile(address);
                }
                catch (Exception e)
                {
                    retCode = ADDRESS_LIST_ERROR;
                }
            }
            // return error code when applicable
            return retCode;
        }


        ///  
        /// \brief This function adds valid Appointment object to Appointment list if it does nt exist 
        /// \details <b> This function gets new Adress object, check if it is valid, if it is, function also checks if it does not exist yet. 
        /// If this object is not in list, function generate ID for this Appointment and add to list.</b> 
        /// \param Appointment - <b> appointment </b> - new Appointment object  
        /// \return int - <b> retCode </b> - error/sucess status; if error returns negative velue related to particular method 
        /// 
        /// \see <i>appointment.ValidateAppointment(appointment),appointment.GetDayAndTime(),appointment.GetPatientID(),
        /// SetAppointmentList(List<Appointment> newAppointmentList),List<Appointment> GetAppointment()</i> 
        /// 
        public int AddAppointment(Appointment appointment)
        {
            int retCode = SUCCESS_GENERIC_RETURN;
            appointment.ValidateAppointment(appointment);
            // check if the address is valid
            if (appointment.GetDayAndTime() == DateTime.MinValue)
            {
                retCode = DEFAULT_DATE_AND_TIME;
            }
            if (appointment.GetPatientID() == SUCCESS_GENERIC_RETURN)
            {
                retCode = PATIENT_DOES_NOT_HAVE_APPOINTMENT;
            }
            
            // check if this address already exists
            if (retCode == SUCCESS_GENERIC_RETURN)
            {
                if (appointmentsList.Contains(appointment))
                {
                    retCode = APPOINTMENT_ALREADY_EXISTS;
                }
            }
            // if not, create an ID for it
            if (appointment.GetApointmentID() == SUCCESS_GENERIC_RETURN)
            {
                try
                {
                    patientList.Sort();
                }
                catch (InvalidOperationException)
                {
                    retCode = APPOINTMENT_LIST_ERROR;
                }
                try
                {
                    int lastIndex = 0;
                    if (appointmentsList.Count > 0)
                    {
                        lastIndex = appointmentsList.Count - 1;
                        appointment.SetApointmentID(appointmentsList[lastIndex].GetApointmentID() + 1);
                    }
                    else
                    {
                        appointment.SetApointmentID(1);
                    }
                }
                catch (Exception e)
                {
                    retCode = UNABLE_TO_ADD_APPOINTMENT;
                }

            }
            // insert it into the list
            if (retCode == SUCCESS_GENERIC_RETURN)
            {
                try
                {
                    appointmentsList.Add(appointment);
                    AddAppointmentToFile(appointment);
                }
                catch (Exception e)
                {
                    retCode = APPOINTMENT_LIST_ERROR;
                }
            }
            // return error code when applicable
            return retCode;
        }


        /// <summary>
        /// Search for appointments for paticular patient
        /// </summary>
        /// <param name="patiantIDToFind">ID op pationt who is supposed to have appointment</param>
        /// <returns>List of appointments belong to patient</returns>
        public List<Appointment> FindAppointments(int patiantIDToFind)
        {
            List<Appointment> AppointmentFound = new List<Appointment>();
            int numbOfElements = GetAppointmentList().Count;
            int found = 0;
            //check if list containts this ID
            if (patiantIDToFind > 0)//check if we have valid ID for search
            {
                try
                {
                    for (int i = 0; i < numbOfElements; i++)//check every character
                    {
                        if (appointmentsList[i].GetPatientID() == patiantIDToFind)
                        {
                            AppointmentFound.Add(appointmentsList[i]);
                            found = 1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Report an error...
                }
            }
            if(found==0)
            {
                AppointmentFound = null;
            }
            return AppointmentFound;
        }


        /// <summary>
        /// Search for appointments start with a particular day
        /// </summary>
        /// <param name="start">Starting date for search</param>
        /// <returns>List of appointments within a particular day</returns>
        public List<Appointment> FindAppointments(DateTime start)
        {
            Appointment appointment = new Appointment();
            List<Appointment> AppointmentFound = new List<Appointment>();
            int found = 0;
            int numbOfElements = GetAppointmentList().Count;
            //check if list containts this any appointments start from this day
            try
            {
                for (int i = 0; i < numbOfElements; i++)//check every character
                {
                    if (appointmentsList[i].GetDayAndTime().Date >= start.Date)
                    {
                        AppointmentFound.Add(appointmentsList[i]);
                        found = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                // Report an error...
            }
            if (found == 0)
            {
                AppointmentFound = null;
            }
            return AppointmentFound;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="hcnNumber">patient HCN number who this address belongs to</param>
        /// <returns>Address object, address belongs to patient</returns>
        public Address FindAddress(string hcnNumber)
        {
            Address address = new Address();
            Address AddressFound = new Address();
            if (address.SetHCN(hcnNumber) == true)
            {
                int numbOfElements = GetAdreessList().Count;
                //check if list containts this HCN
                try
                {
                    for (int i = 0; i < numbOfElements; i++)//check every character
                    {
                        if (addressList[i].GetHCN() == hcnNumber)
                        {
                            AddressFound = addressList[i];
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Report an error...
                }
            }
            else
            {
                AddressFound = null;
            }
            return AddressFound;
        }


        /// <summary>
        /// This function provides functionality to find patient in database by patient information
        /// </summary>
        /// <param name="Obj">Patient object to be found</param>
        /// <returns>List of patients found</returns>
        public List<Patient> FindPatient(Patient Obj)
        {
            List<Patient> patientsFound = new List<Patient>();
            int found = 0;
            if (Obj.GethCN() != "")
            {
                patientsFound.Add(FindPatient(Obj.GethCN()));
            }
            else
            {
                try
                {
                    foreach (Patient a in patientList) // for now I'm only checking the last name
                    {
                        if (Obj.GetLname().ToUpper() == a.GetLname().ToUpper())
                        {
                            patientsFound.Add(a);
                            found = 1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Report an error...
                }
            }
            if (found == 0)
            {
                patientsFound = null;
            }
            return patientsFound;
        }


        /// <summary>
        /// This function provides functionality to find patient in database by patient HCN number
        /// </summary>
        /// <param name="hcnNumber">patient HCN number</param>
        /// <returns>Patient Object</returns>
        public Patient FindPatient(string hcnNumber)
        {
            List<Patient> patientsFound = new List<Patient>();
            Patient patient = new Patient();
            int numbOfElements = GetPatientList().Count;
            int found = 0;
            if (patient.SethCN(hcnNumber)==true)
            {
                try
                {
                    for (int i = 0; i < numbOfElements; i++)//check every character
                    {
                        if (patientList[i].GethCN() == hcnNumber)
                        {
                            patient = patientList[i];
                            found = 1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Report an error...
                }
            }

            if(found==0)
            {
                patient = null;
            }
           
            return patient;
        }


        /// <summary>
        /// This function provides functionality to find patient in database by patient ID
        /// </summary>
        /// <param name="patientID">ID of pation to be found</param>
        /// <returns>Patient object</returns>
        public Patient FindPatient(int patientID)
        {
            List<Patient> patientsFound = new List<Patient>();
            Patient patient = new Patient();
            int numbOfElements = GetPatientList().Count;
            int found = 1;

            if (patientID>0)
            {
                try
                {
                    for (int i = 0; i < numbOfElements; i++)//check every character
                    {
                        if (patientList[i].GetPatientID() == patientID)
                        {
                            patient = patientList[i];
                            found = 1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Report an error...
                }
            }
            if (found == 0)
            {
                patient = null;
            }
            return patient;
        }




        public Appointment FindAppointment(int appointmentID)
        {
            List<Appointment> appointmentFound = new List<Appointment>();
            Appointment appointment = new Appointment();
            int numbOfElements = GetAppointmentList().Count;
            int found = 1;

            if (appointmentID > 0)
            {
                for (int i = 0; i < numbOfElements; i++)//check every character
                {
                    if (appointmentsList[i].GetApointmentID() == appointmentID)
                    {
                        appointment = appointmentsList[i];
                        found = 1;
                    }
                }
            }
            if (found == 0)
            {
                appointment = null;
            }
            return appointment;
        }


        /// <summary>
        /// This function provides functionality for updation pstient information in database
        /// </summary>
        /// <param name="obj">Patien obkect to be updated</param>
        /// <returns>int as a error status</returns>
        public int UpdatePatient(Patient obj)
        {
            int retCode = UPDATE_PATIENT_UNKNOWN_ERROR;
            Patient patientToUpdate = FindPatient(obj.GetPatientID());
            if (patientToUpdate==null)
            {
                retCode = INVALID_PATIENT_OBJ_TO_UPDATE;
            }
            else
            {
                try
                {
                    int index = patientList.IndexOf(patientToUpdate);
                    patientList.RemoveAt(index);
                    patientList.Insert(index, obj);
                    retCode = SUCCESS_GENERIC_RETURN;

                    //Write All Patient from list to file
                    if (WriteAllPatientObjsIntoFile() != SUCCESS_GENERIC_RETURN)
                    {
                        retCode = WritePatientObjIntoFileFail;
                    }
                }
                catch(ArgumentOutOfRangeException)
                {
                    retCode = UNABLE_TO_UPDATE_PATIENT_IN_LIST;
                }

            }
            return retCode;
        }


        /// <summary>
        /// This function provides functionality for updation address information in database
        /// </summary>
        /// <param name="obj">Address obkect to be updated</param>
        /// <returns></returns>
        public int UpdateAddress(Address obj)
        {
            int retCode = UPDATE_ADDRESS_UNKNOWN_ERROR;
            Address addressToUpdate = FindAddress(obj.GetHCN());
            if (addressToUpdate == null)
            {
                retCode = INVALID_ADDRESS_OBJ_TO_UPDATE;
            }
            else
            {
                try
                {
                    int index = addressList.IndexOf(addressToUpdate);
                    addressList.RemoveAt(index);
                    addressList.Insert(index, obj);
                    retCode = SUCCESS_GENERIC_RETURN;

                    //Write All Address from list to file
                    if (WriteAllAddressObjsIntoFile() != SUCCESS_GENERIC_RETURN)
                    {
                        retCode = WriteAddressObjIntoFileFail;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    retCode = UnableTo_UPDATE_ADDRESS_IN_LIST;
                }

            }
            return retCode;
        }


        /// <summary>
        /// This function provides functionality for appointment pstient information in database
        /// </summary>
        /// <param name="obj">Appointment obkect to be updated</param>
        /// <returns></returns>
        public int UpdateAppointment(Appointment obj)
        {
            int retCode = UPDATE_APPOINTMENT_UNKNOWN_ERROR;
            Appointment appointmrntToUpdate = FindAppointment(obj.GetApointmentID());
            if (appointmrntToUpdate == null)
            {
                retCode = INVALID_APPOINTMENT_OBJ_TO_UPDATE;
            }
            else
            {
                try
                {
                    int index = appointmentsList.IndexOf(appointmrntToUpdate);
                    appointmentsList.RemoveAt(index);
                    appointmentsList.Insert(index, obj);
                    retCode = SUCCESS_GENERIC_RETURN;

                    //Write All Address from list to file
                    if (WriteAllAppointmentObjsIntoFile() != SUCCESS_GENERIC_RETURN)
                    {
                        retCode = WriteAppointmentObjIntoFileFail;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    retCode = UnableTo_UPDATE_APPOINTMENT_IN_LIST;
                }
            }
            return retCode;
        }

        // ---------------------------------------------- 
        // ----------Demographic File I/O stuff---------- 
        // ---------------------------------------------- 

        /// <summary>
        /// Validation of Minitial for FileIO
        /// </summary>
        /// <param name="newMinitial"></param>
        /// <see cref="public bool SetMinitial(char newMinitial)" - Patient Obj/>
        /// <returns></returns>
        public bool CheckMinitial(string newMinitial)
        {
            bool setMadeSuccess = false;

            setMadeSuccess = Regex.IsMatch(newMinitial, @"^[a-zA-Z ]+$");

            return setMadeSuccess;
        }


        /// <summary>
        /// Check if exists an addressID item inside a list. 
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfAddressIDAlreadyExists(string addressID)
        {
            bool setMadeSuccess = false;

            Int32 id = GetInteger(addressID);

            for (int i = 0; i < addressList.Count; i++)
            {
                if (addressList[i].GetAdressID() == id)
                {
                    setMadeSuccess = true;
                    break;
                }
            }

            return setMadeSuccess;
        }


        /// <summary>
        /// Check if exists an HCN item inside a list. 
        /// </summary>
        /// <param name="hCN"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfAddressHcnAlreadyExists(string hCN)
        {
            bool setMadeSuccess = false;

            for (int i = 0; i < addressList.Count; i++)
            {
                if (addressList[i].GetHCN() == hCN)
                {
                    setMadeSuccess = true;
                    break;
                }
            }

            return setMadeSuccess;
        }


        /// <summary>
        /// Check if exists an PatientID item inside a list. 
        /// </summary>
        /// <param name="PatientID"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfPatientIDAlreadyExists(string patientID)
        {
            bool setMadeSuccess = false;

            Int32 id = GetInteger(patientID);

            for (int i = 0; i < patientList.Count; i++)
            {
                if (patientList[i].GetPatientID() == id)
                {
                    setMadeSuccess = true;
                    break;
                }
            }

            return setMadeSuccess;
        }


        /// <summary>
        /// Check if exists an HCN item inside a list. 
        /// </summary>
        /// <param name="hCN"></param>
        /// <returns></returns>
        public bool CheckIfPatientHcnAlreadyExists(string hCN)
        {
            bool setMadeSuccess = false;

            for (int i = 0; i < patientList.Count; i++)
            {
                if (patientList[i].GethCN() == hCN)
                {
                    setMadeSuccess = true;
                    break;
                }
            }

            return setMadeSuccess;
        }


        /// <summary>
        /// Check if exists an AppointmentID item inside a list. 
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfAppointmentIDAlreadyExists(string appointmentID)
        {
            bool setMadeSuccess = false;

            Int32 id = GetInteger(appointmentID);

            for (int i = 0; i < appointmentsList.Count; i++)
            {
                if (appointmentsList[i].GetApointmentID() == id)
                {
                    setMadeSuccess = true;
                    break;
                }
            }

            return setMadeSuccess;
        }


        /// <summary>
        /// Converts a string to an int
        /// </summary>
        /// <param name="number">A number in a string</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>
        /// <see cref="private static Patient ReadPatientFile()"> This method support the method ReadPatientFile.</see>
        /// <returns>The number or an error code</returns>
        public static int GetInteger(string number)
        {
            int errCode = SUCCESS_GENERIC_RETURN;

            try
            {
                errCode = Int32.Parse(number);
            }
            catch (FormatException) { errCode = GetIntegerFailFormat; }
            catch (OverflowException) { errCode = GetIntegerFailOverflow; }

            return errCode;
        }


        /// <summary>
        /// Tries to parse a string into a boolean
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>
        /// <see cref="private static Patient ReadPatientFile()"> This method support the method ReadPatientFile.</see>
        /// <returns></returns>
        public static char GetChar(string input)
        {
            char retCode = ' ';

            if (input.Length == 1)
            {
                retCode = input[0];
            }

            return retCode;
        }


        /// <summary>
        /// Tries to parse a string into a DateTime object
        /// </summary>
        /// <param name="dateAndTime">String representing a date and time</param>
        /// <see cref="public static DateTime GetDateTime(string dateAndTime)" on Packager.cs/>
        /// <returns>The proper value or DateTime.MinValue for an invalid string</returns>
        public static DateTime GetDateTime(string dateAndTime)
        {
            DateTime retCode = DateTime.MinValue;
            try
            {
                retCode = DateTime.Parse(dateAndTime);
            }
            catch (ArgumentNullException) { retCode = DateTime.MinValue; }
            catch (FormatException) { retCode = DateTime.MinValue; }
            catch (ArgumentException) { retCode = DateTime.MinValue; }

            return retCode;
        }


        /// <summary> 
        /// This method will start the reading process for all demographics file. 
        /// </summary> 
        /// <returns>bool - true if it was done well.</returns>
        public bool ReadDemographicsFiles()
        {
            bool setMadeSuccess = true;
            if (ReadAddressFile()       != SUCCESS_GENERIC_RETURN) { setMadeSuccess = false; }
            if (ReadPatientFile()       != SUCCESS_GENERIC_RETURN) { setMadeSuccess = false; }
            if (ReadAppointmentFile()   != SUCCESS_GENERIC_RETURN) { setMadeSuccess = false; }
            return setMadeSuccess;
        }


        /// <summary> 
        /// This method will start the wrinting process for all demographics file. 
        /// </summary> 
        /// <returns>bool - true if it was done well.</returns>
        public bool WriteDemographicsFiles()
        {
            bool setMadeSuccess = true;

            if (WriteAllAddressObjsIntoFile()       != SUCCESS_GENERIC_RETURN) { setMadeSuccess = false; }
            if (WriteAllAppointmentObjsIntoFile()   != SUCCESS_GENERIC_RETURN) { setMadeSuccess = false; }
            if (WriteAllPatientObjsIntoFile()       != SUCCESS_GENERIC_RETURN) { setMadeSuccess = false; }

            return setMadeSuccess;
        }


        /// <summary> 
        /// This method will add a patient on patient list.  
        /// </summary> 
        /// <returns>bool - true if it was done well.</returns>
        /// <see cref="public int AddPatient(Patient patient)"/>
        public int AddPatientFromFileToList(Patient patient)
        {
            int retCode = 0;
            int patientID = 0;

            // check if the address is valid
            if (patient.ValidatePatient() < 0) retCode = -1;
            // check if this address already exists
            if (retCode == 0)
            {
                if (patientList.Contains(patient))
                {
                    retCode = AddPatientFromFileAlreadyExists;
                }
            }
            // insert it into the list
            if (retCode == 0)
            {
                patientList.Add(patient);
            }
            // return error code when applicable
            return retCode;
        }


        /// <summary> 
        /// This method will add a patient into a patient file.  
        /// </summary> 
        /// <returns>bool - true if it was done well.</returns>
        /// <see cref="public int AddPatient(Patient patient)"/>
        public int AddPatientToFile(Patient patient)
        {
            int retCode = 0;

            // check if the address is valid
            if (patient.ValidatePatient() < 0) retCode = -1;

            // insert it into the File
            if (retCode == 0)
            {
                WriteOnePatientObjIntoFile(patient);
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddPatientToFile - Added a patient - [successful]");
            }
            // return error code when applicable
            return retCode;
        }


        /// <summary>
        /// This function adds valid Address object to Address list if it does nt exist 
        /// This function gets new Adress object, check if it is valid, if it is, function also checks if it does not exist yet. 
        /// </summary>
        /// <param name="address"></param>
        /// <see cref="public int AddAddress(Address address)"/>
        /// <returns>"0" - If was is done ok.</returns>
        public int AddAddressFromFileToList(Address address)
        {
            int retCode = -3408;
            if (address.ValidateAdress() == 0)
            {
                retCode = 0;
            }
            else
            {
                retCode = -3409;
            }
            // check if this address already exists
            //if (retCode == 0)
            //{
            //    if (addressList.Contains(address))
            //    {
            //        retCode = -3407;
            //    }
            //}
            // insert it into the list
            if (retCode == 0)
            {
                addressList.Add(address);
            }
            // return error code when applicable
            return retCode;
        }


        /// <summary>
        /// This function adds valid Address object to file  
        /// </summary>
        /// <param name="address"></param>
        /// <see cref="public int AddAddress(Address address)"/>
        /// <returns>"0" - If was is done ok.</returns>
        public int AddAddressToFile(Address address)
        {
            int retCode = -3408;
            if (address.ValidateAdress() == 0)
            {
                retCode = 0;
            }
            else
            {
                retCode = -3409;
            }
            // insert it into the File
            if (retCode == 0)
            {
                WriteOneAddressObjIntoFile(address);
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddAddressToFile - Added an address - [successful]");
            }
            // return error code when applicable
            return retCode;
        }


        /// <summary>
        /// This function adds a valid Appointment object to Appointment list if it does nt exist
        /// </summary>
        /// <param name="appointment"> obj Appointment</param>
        /// <see cref="public int AddAppointment(Appointment appointment)"/>
        /// <returns>"0" - If was is done ok.</returns>
        public int AddAppointmentFromFileToList(Appointment appointment)
        {
            int retCode = 0;
            appointment.ValidateAppointment(appointment);
            // check if the address is valid
            if (appointment.GetDayAndTime() == DateTime.MinValue)
            {
                retCode = -3409;
            }
            if (appointment.GetPatientID() == 0)
            {
                retCode = -3410;
            }

            // check if this address already exists
            if (retCode == 0)
            {
                if (appointmentsList.Contains(appointment))
                {
                    retCode = -3411;
                }
            }
            // insert it into the list
            if (retCode == 0)
            {
                appointmentsList.Add(appointment);
            }
            // return error code when applicable
            return retCode;
        }


        /// <summary>
        /// This function adds a valid Appointment object to file
        /// </summary>
        /// <param name="appointment"> obj Appointment</param>
        /// <see cref="public int AddAppointment(Appointment appointment)"/>
        /// <returns>"0" - If was is done ok.</returns>
        public int AddAppointmentToFile(Appointment appointment)
        {
            int retCode = 0;
            appointment.ValidateAppointment(appointment);
            // check if the address is valid
            if (appointment.GetDayAndTime() == DateTime.MinValue)
            {
                retCode = -3409;
            }
            if (appointment.GetPatientID() == 0)
            {
                retCode = -3410;
            }

            // insert it into the list
            if (retCode == 0)
            {
                WriteOneAppointmentObjIntoFile(appointment);
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddAppointmentToFile - Added an appointment - [successful]");
            }
            // return error code when applicable
            return retCode;
        }


        // =======================================================================
        //                          Private Methods
        // =======================================================================

        /// <summary>
        /// All Address ID and HCN (Both together) has to have a match with just one patient. 
        /// If it doesn't match, create a new patient with that HCN + AddresID. 
        /// Also, if only one of them match, update the other one. 
        /// </summary>
        private void MatchAddresIdAndPatientID()
        {
            bool matchPatientAddressId = false;
            bool matchPatientHCN = false;
            Int32 addressId = 0, patientAddressId = 0;
            string addressHCN = "", patientHCN = "";

            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - [Started]");

            for (Int32 i = 0; i < addressList.Count; i++)
            {
                matchPatientAddressId = false;
                matchPatientHCN = false;

                addressId = addressList[i].GetAdressID();
                addressHCN = addressList[i].GetHCN();

                for (Int32 j = 0; j < patientList.Count; j++)
                {
                    patientAddressId = patientList[i].GetAddressID();
                    patientHCN = patientList[i].GethCN();

                    if (addressId == patientAddressId) { matchPatientAddressId = true; }

                    if (addressHCN == patientHCN) { matchPatientHCN = true; }

                    // both matchPatientAddressId and matchPatientHCN have matched just jump tho next patient
                    if ((matchPatientAddressId == true) && (matchPatientHCN == true)) { break; }

                    //Either matchPatientAddressId or matchPatientHCN have matched
                    else
                    {
                        if (!matchPatientAddressId == true) //Updating Patient address ID base on Address ID
                        {
                            Patient myPatient = new Patient();
                            myPatient = FindPatient(patientHCN);
                            myPatient.SetAddressID(addressId);
                            if (UpdatePatient(myPatient) == SUCCESS_GENERIC_RETURN)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - Update Patient with a new addressID - [successful]");
                            }
                            else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - Update Patient with a new addressID - [fail]"); }

                        }
                        if (!matchPatientHCN == true) //Updating Patient HCN base on Address HCN
                        {
                            Patient myPatient = new Patient();
                            myPatient = FindPatient(patientHCN);
                            myPatient.SethCN(addressHCN);
                            if (UpdatePatient(myPatient) == SUCCESS_GENERIC_RETURN)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - Update Patient with a new HCN - [successful]");
                            }
                            else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - Update Patient with a new HCN - [fail]"); }
                        }
                    }
                }
                //After finish if it have no match we must creat a new patient. 
                //if ((matchPatientAddressId == false) && (matchPatientHCN == false))
                //{
                //    Patient myPatient = new Patient();
                //    myPatient = CreateAnUnknonPatientObj(patientList.Count + 1, addressId, addressHCN);
                //    if (AddPatient(myPatient) == SUCCESS_GENERIC_RETURN)
                //    {
                //        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - Added a new Patient with new ID, HCN and AddressID - [successful]");
                //    }
                //    else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - Added a new Patient with new ID, HCN and AddressID - [fail]"); }
                //}
            }

            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "MatchAddresIdAndPatientID - [Finished]");
        }


        /// <summary>
        /// Create a line into the db Patient file. 
        /// </summary>
        /// <param name="obj">The object Patient</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private static Int32 WriteOnePatientObjIntoFile(Patient obj)
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string txtPatient = "", txtPatientID = "";

            try
            {
                // Fill the info
                txtPatient += obj.PatientID.ToString() + DivDemographicTrackerFiles;
                txtPatientID = txtPatient;

                txtPatient += obj.GethCN() + DivDemographicTrackerFiles;
                txtPatient += obj.GetLname() + DivDemographicTrackerFiles;
                txtPatient += obj.GetFname() + DivDemographicTrackerFiles;
                txtPatient += obj.GetMinitial().ToString() + DivDemographicTrackerFiles;
                txtPatient += obj.GetdateOfBirth() + DivDemographicTrackerFiles;
                txtPatient += obj.GetSex() + DivDemographicTrackerFiles;
                txtPatient += obj.GetHeadOfHouse() + DivDemographicTrackerFiles;
                txtPatient += obj.AddressID.ToString() + "\n";

                if (fileIO.WriteFullPatiantFileAppend(txtPatient) == true)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the PatientID-Obj |" + txtPatientID );
                }                
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a Patient object into a file - [failure]");
                setMadeSuccess = WritePatientObjIntoFileFail;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Create a line into the db Address file. 
        /// </summary>
        /// <param name="obj">The object Address</param>
        /// <see cref="public static string PackAddress(Address obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private static Int32 WriteOneAddressObjIntoFile(Address obj)
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string txtAddress = "", txtAddressID = "";

            try
            {
                // Fill the info
                txtAddress += obj.GetAdressID().ToString() + DivDemographicTrackerFiles;
                txtAddressID = txtAddress;

                txtAddress += obj.GetHCN() + DivDemographicTrackerFiles;
                txtAddress += obj.GetAdressLineOne() + DivDemographicTrackerFiles;
                txtAddress += obj.GetAdressLineTwo() + DivDemographicTrackerFiles;
                txtAddress += obj.GetCity() + DivDemographicTrackerFiles;
                txtAddress += obj.GetProvince() + DivDemographicTrackerFiles;
                txtAddress += obj.GetPhoneNumber() + "\n";

                if (fileIO.WriteFullAddressFileAppend(txtAddress) == true)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the AddressID-Obj |" + txtAddressID );
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse an Address object into a file - [failure]");
                setMadeSuccess = WriteAddressObjIntoFileFail;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Create a line into the db Appointment file. 
        /// </summary>
        /// <param name="obj">The object Appointment</param>
        /// <see cref="public static string PackAppointment(Appointment obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private static Int32 WriteOneAppointmentObjIntoFile(Appointment obj)
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string txtAppointment = "", txtAppointmentID = "";

            try
            {
                // Fill the info
                txtAppointment += obj.GetApointmentID().ToString() + DivDemographicTrackerFiles;
                txtAppointmentID = txtAppointment;

                txtAppointment += obj.GetDayAndTime().ToString() + DivDemographicTrackerFiles;
                txtAppointment += obj.GetPatientID().ToString() + DivDemographicTrackerFiles;
                txtAppointment += obj.GetSecondPatientID().ToString() + "\n";


                if (fileIO.WriteFullAppointmentFileAppend(txtAppointment) == true)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the AppointmentID-Obj |" + txtAppointmentID);
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse an Appointment object into a file - [failure]");
                setMadeSuccess = WriteAppointmentObjIntoFileFail;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Write all Patient Objs into the db Patient file. 
        /// </summary>
        /// <param name="obj">The object Patient</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private Int32 WriteAllPatientObjsIntoFile()
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string txtPatient = "", txtPatientID = "";
            bool firstInteraction = true;

            try
            {
                foreach (var obj in patientList) // Loop through List with Patient
                {
                    // Fill the info
                    txtPatient = "";
                    txtPatient += obj.PatientID.ToString() + DivDemographicTrackerFiles;
                    txtPatientID = txtPatient;

                    txtPatient += obj.GethCN() + DivDemographicTrackerFiles;
                    txtPatient += obj.GetLname() + DivDemographicTrackerFiles;
                    txtPatient += obj.GetFname() + DivDemographicTrackerFiles;
                    txtPatient += obj.GetMinitial().ToString() + DivDemographicTrackerFiles;
                    txtPatient += obj.GetdateOfBirth() + DivDemographicTrackerFiles;
                    txtPatient += obj.GetSex() + DivDemographicTrackerFiles;
                    txtPatient += obj.GetHeadOfHouse() + DivDemographicTrackerFiles;
                    txtPatient += obj.AddressID.ToString() + "\n";

                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullPatiantFileOverwrite(txtPatient) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (Overwrite) the PatientID-Obj |" + txtPatientID );
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullPatiantFileAppend(txtPatient) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the PatientID-Obj |" + txtPatientID );
                        }
                    }                  
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a Patient object into a file - [failure]");
                setMadeSuccess = WritePatientObjIntoFileFail;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Write all Address Objs into the db Patient file. 
        /// </summary>
        /// <param name="obj">The object Address</param>
        /// <see cref="public static string PackAddress(Address obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private Int32 WriteAllAddressObjsIntoFile()
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string txtAddress = "", txtAddressID = "";
            bool firstInteraction = true;

            try
            {
                foreach (var obj in addressList) // Loop through List with Patient
                {
                    // Fill the info
                    txtAddress = "";
                    txtAddress += obj.GetAdressID().ToString() + DivDemographicTrackerFiles;
                    txtAddressID = txtAddress;

                    txtAddress += obj.GetHCN() + DivDemographicTrackerFiles;
                    txtAddress += obj.GetAdressLineOne() + DivDemographicTrackerFiles;
                    txtAddress += obj.GetAdressLineTwo() + DivDemographicTrackerFiles;
                    txtAddress += obj.GetCity() + DivDemographicTrackerFiles;
                    txtAddress += obj.GetProvince() + DivDemographicTrackerFiles;
                    txtAddress += obj.GetPhoneNumber() + "\n";

                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullAddressFileOverwrite(txtAddress) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (Overwrite) the AddressID-Obj |" + txtAddressID + "|");
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullAddressFileAppend(txtAddress) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the AddressID-Obj |" + txtAddressID + "|");
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse an Address object into a file - [failure]");
                setMadeSuccess = WriteAddressObjIntoFileFail;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Write all Appointment Objs into the db Patient file. 
        /// </summary>
        /// <param name="obj">The object Appointment</param>
        /// <see cref="public static string PackAppointment(Appointment obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private Int32 WriteAllAppointmentObjsIntoFile()
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string txtAppointment = "", txtAppointmentID = "";
            bool firstInteraction = true;

            try
            {
                foreach (var obj in appointmentsList) // Loop through List with Patient
                {
                    // Fill the info
                    txtAppointment = "";
                    txtAppointment += obj.GetApointmentID().ToString() + DivDemographicTrackerFiles;
                    txtAppointmentID = txtAppointment;

                    txtAppointment += obj.GetDayAndTime().ToString() + DivDemographicTrackerFiles;
                    txtAppointment += obj.GetPatientID().ToString() + DivDemographicTrackerFiles;
                    txtAppointment += obj.GetSecondPatientID().ToString() + "\n";

                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullAppointmentFileOverwrite(txtAppointment) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (overwrite) the AppointmentID-Obj |" + txtAppointmentID + "|");

                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullAppointmentFileAppend(txtAppointment) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the AppointmentID-Obj |" + txtAppointmentID + "|");
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse an Appointment object into a file - [failure]");
                setMadeSuccess = WriteAppointmentObjIntoFileFail;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Creates a Patient object by parsing a string
        /// </summary>
        /// <param name="message">Patients information</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        private Int32 ReadPatientFile()
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            Patient myPatientObj = null;
            string txtPatient = "";
            string[] fileLines = null;
            string[] fieldsSplitted = null;

            // Read the Appointment array of strings
            if (ReadFileToStringArray(DemoClasses.Patient, ref fileLines) == SUCCESS_GENERIC_RETURN
                && fileLines != null) 
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadPatientFile - read patient file - successful");
                try
                {
                    for (Int32 i = 0; i < fileLines.Length; i++)
                    {
                        myPatientObj = null;
                        txtPatient = fileLines[i];
                        fieldsSplitted = txtPatient.Split(FieldSeparator, StringSplitOptions.None);
                        if ((fieldsSplitted.Length == 9) && (fileLines[i] != "")) // ObjTag + PatientTag + 9 fields + ""
                        {
                            // new Patient()
                            myPatientObj = new Patient();

                            // patientID
                            int z = GetInteger(fieldsSplitted[0]);
                            if (z != SUCCESS_GENERIC_RETURN) { myPatientObj.PatientID = z; }
                            else {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the ID criteria");
                                continue; }

                            // patientID check - if addressID already exists skip this registre
                            if (CheckIfPatientIDAlreadyExists(fieldsSplitted[0])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it patientID already exists");
                                continue; }

                            // HCN
                            if (!myPatientObj.SethCN(fieldsSplitted[1])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the HCN criteria");
                                continue; }

                            // HCN check - if HCN already exists skip this registre
                            if (CheckIfPatientHcnAlreadyExists(fieldsSplitted[1])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it HCN already exists");
                                continue; }

                            // lastName
                            if (!myPatientObj.SetLname(fieldsSplitted[2])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the Last Name criteria");
                                continue;  }

                            // firstName
                            if (!myPatientObj.SetFname(fieldsSplitted[3])){
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the First Name criteria");
                                continue;}

                            // mInitials
                            if (CheckMinitial(fieldsSplitted[4]))
                            { myPatientObj.SetMinitial(GetChar(fieldsSplitted[4])); }
                            else {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the Initials criteria");
                                continue; }

                            // dateOfBirth
                            if (!myPatientObj.SetdateOfBirth(fieldsSplitted[5])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the dateOfBirth criteria");
                                continue; }

                            // sex
                            if (!myPatientObj.SetSex(fieldsSplitted[6])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the Sex criteria");
                                continue;}

                            // headOfHouse
                            if (!myPatientObj.SetHeadOfHouse(fieldsSplitted[7])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the headOfHouse criteria");
                                continue; }

                            // addressID
                            z= GetInteger(fieldsSplitted[8]);
                            if (z != SUCCESS_GENERIC_RETURN) { myPatientObj.AddressID = z; }
                            else {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the AddressID criteria");
                                continue; }


                            if (AddPatientFromFileToList(myPatientObj) == SUCCESS_GENERIC_RETURN)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddPatientFromFileToList - Added the patient obj, " + i + "/" + (fileLines.Length - 1) + " - [successful]");
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddPatientFromFileToList - Added the patient obj " + i + "/" + (fileLines.Length - 1) + " - [fail]");
                            }
                        }
                        else
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Patient line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the amount of fields criteria");
                        }
                    }

                    //Call the check integrati between Patient and Address
                    MatchAddresIdAndPatientID();
                }
                catch (Exception)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create an Patient Object - [failure]");
                    setMadeSuccess = ReadPatientFileFailure;
                }
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Creates a Address object by parsing a string
        /// </summary>
        /// <param name="message">Addresss information</param>
        /// <see cref="public static string PackAddress(Address obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private Int32 ReadAddressFile()
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            Address myAddressObj = null;
            string txtAddress = "";
            string[] fileLines = null;
            string[] fieldsSplitted = null;

            if (ReadFileToStringArray(DemoClasses.Address,ref fileLines) == SUCCESS_GENERIC_RETURN) // Read the Appointment array of strings
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadAddressFileLines - Splitting lines to fields");
                try
                {
                    for (Int32 i = 0; i < fileLines.Length; i++)
                    {
                        txtAddress = fileLines[i];
                        fieldsSplitted = txtAddress.Split(FieldSeparator, StringSplitOptions.None);
                        if ((fieldsSplitted.Length == 7) && (fileLines[i] != "")) // ObjTag + AddressTag + 7 fields + ""
                        {
                            myAddressObj = new Address();

                            // addressID
                            if (!myAddressObj.SetAdressID(GetInteger(fieldsSplitted[0]))) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the addressID criteria");
                                continue; }


                            // HCN
                            if (!myAddressObj.SetHCN(fieldsSplitted[1])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the HCN criteria");
                                continue; }

                            // HCN check - if HCN already exists skip this registre
                            if (CheckIfAddressHcnAlreadyExists(fieldsSplitted[1])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it HCN already exists");
                                continue; }

                            // addressLineOne
                            if (!myAddressObj.SetAdressLineOne(fieldsSplitted[2])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the addressLineOne criteria");
                                continue; }

                            // addressLineTwo
                            if (!myAddressObj.SetAdressLineTwo(fieldsSplitted[3])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the addressLineTwo criteria");
                                continue; }

                            // city
                            if (!myAddressObj.SetCity(fieldsSplitted[4])){
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the city criteria");
                                continue; }

                            // province
                            if (!myAddressObj.SetProvince(fieldsSplitted[5])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the province criteria");
                                continue; }

                            // phone
                            if(!myAddressObj.SetPhoneNumber(fieldsSplitted[6])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address line - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the phone criteria");
                                continue; }                            


                            if (AddAddressFromFileToList(myAddressObj) == SUCCESS_GENERIC_RETURN)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddAddressFromFileToList - Added the address obj " + i + "/" + (fileLines.Length - 1) + " - [successful]");
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddAddressFromFileToList - Added the address obj " + i + "/" + (fileLines.Length - 1) + " - [fail]");
                            }
                        }
                        else
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Address File Lines - Adding address, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the amount of fields criteria");
                        }
                    }
                }
                catch (Exception)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create an Address Object - [failure]");
                    setMadeSuccess = ReadAddressFileFailure;
                }
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Creates a Appointment object by parsing a string from file
        /// </summary>
        /// <param name="message">Appointment information</param>
        /// <see cref="public static string PackAppointment(Appointment obj)"> This method was used as a reference.</see>/>
        /// <returns>int, "0" - If was is done ok.</returns>
        private Int32 ReadAppointmentFile()
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            Appointment myAppointmentObj = null;
            string txtAddress = "";
            string[] fileLines = null;
            string[] fieldsSplitted = null;
 
            if (ReadFileToStringArray(DemoClasses.Appointment,ref fileLines) == SUCCESS_GENERIC_RETURN) // Read the Appointment array of strings
            {
                try
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadAppointmentFileLines - Splitting lines to fields");
                    for (Int32 i = 0; i < fileLines.Length; i++)
                    {
                        txtAddress = fileLines[i];
                        fieldsSplitted = txtAddress.Split(FieldSeparator, StringSplitOptions.None);
                        if ((fieldsSplitted.Length == 4) && (fileLines[i] != "")) // ObjTag + AddressTag + 9 fields + ""
                        {
                            myAppointmentObj = new Appointment();

                            // appointmentID
                            if(!myAppointmentObj.SetApointmentID(GetInteger(fieldsSplitted[0]))) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the appointmentID criteria");
                                continue; }

                            // appointmentID check - if appointmentID already exists skip this registre
                            if (CheckIfAppointmentIDAlreadyExists(fieldsSplitted[0])) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it appointmentID already exists");
                                continue; }

                            // dateTime
                            if (!myAppointmentObj.SetDayAndTime(GetDateTime(fieldsSplitted[1]))) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the dateTime criteria");
                                continue; }

                            // patientID
                            if (!myAppointmentObj.SetPatientID(GetInteger(fieldsSplitted[2]))) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the patientID criteria");
                                continue; }

                            // patientID check - if patientID doesnt exist skip this registre
                            //if (!CheckIfPatientIDAlreadyExists(fieldsSplitted[0])) {
                            //    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it patientID already exists");
                            //    continue; }

                            // secondPatientID
                            if (!myAppointmentObj.SetSecondPatientID(GetInteger(fieldsSplitted[3]))) {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the secondPatientID criteria");
                                continue; }


                            if (AddAppointmentFromFileToList(myAppointmentObj) == SUCCESS_GENERIC_RETURN)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddAppointmentFromFileToList - Added the appointment obj, " + i + "/" + (fileLines.Length - 1) + "  - [successful]");
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddAppointmentFromFileToList - Added the appointment obj " + i + "/" + (fileLines.Length - 1) + " - [fail]");
                            }
                        }
                        else
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Appointment line - Adding Appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the amount of fields criteria");
                        }
                    }
                }
                catch (Exception)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse an Appointment line object to fields, and to create an appointment object - [failure]");
                    setMadeSuccess = ReadAddressFileFailure;
                }
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Receive a command "{ Address, Appointment, Patient }" as the first argument, and will return by ref on the second element an array of string with the content of the file passed on the first argument.   
        /// </summary>
        /// <param name="fileName"> enum that must be { Address, Appointment, Patient }.</param>
        /// <param name="getFileLines"> A ref to array of strings must be passed here.</param>
        /// /// <see cref="public static string PackAddress(Address obj)"> This method was used as a reference.</see>/>
        /// <returns>int, "0" - If was is done ok.</returns>        
        private int ReadFileToStringArray(DemoClasses fileName, ref string [] getFileLines)
        {
            Int32 setMadeSuccess = SUCCESS_GENERIC_RETURN; //Return variable, "0" means successful. 
            string strFromFileDatabase = "";

            try
            {
                switch (fileName)
                {
                    case DemoClasses.Address:
                        {
                            if (fileIO.ReadFullAddressFile(ref strFromFileDatabase) == true) // Read the Address File
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadAddressFile - read address file - [successful]");

                                getFileLines = strFromFileDatabase.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadAddressFile - Parse a line from file to array of strings - [successful]");
                            }
                            break;
                        }
                    case DemoClasses.Appointment:
                        {
                            if (fileIO.ReadFullAppointmentFile(ref strFromFileDatabase) == true) // Read the Address File
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadAppointmentFile - read appointment file - [successful]");

                                getFileLines = strFromFileDatabase.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadAppointmentFile - Parse a line from file to array of strings - [successful]");
                            }
                            break;
                        }
                    case DemoClasses.Patient:
                        {
                            if (fileIO.ReadFullPatiantFile(ref strFromFileDatabase) == true) // Read the Address File
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadPatientFile - read patient file - [successful]");

                                getFileLines = strFromFileDatabase.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadPatientFile - Parse a line from file to array of strings - [successful]");
                            }
                            break;
                        }
                    default:
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFileToStringArray - Invalid input parameter");
                            setMadeSuccess = ReadFileToStringArrayFail;
                            break;
                        }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFileToStringArray - An Exception was reached when tried to parse a line from file to array of strings - [failure]");
                setMadeSuccess = ReadAddressFileFailure;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Add a new pacientient during load process when the system is reading file. 
        /// </summary>
        /// <param name="idPatient"></param>
        /// <param name="idAddress"></param>
        /// <param name="hCN"></param>
        /// <returns></returns>
        private Patient CreateAnUnknonPatientObj(int idPatient, int idAddress, string hCN = "")
        {
            int myPatientId = idPatient;
            string myHCN = hCN;

            if (myHCN == "") { myHCN = patientList.Count.ToString("0000000000") + "ZZ"; } //Create a new HCN

            string  fName = "Unknown",
                    lName = "PatientLastName",
                    patientHCN = myHCN,
                    birthDay = "11-11-1111",
                    sex = "H",
                    HoH = myHCN;

            var myPatient = new Patient();

            myPatient.PatientID = myPatientId;
            myPatient.SetFname(fName);
            myPatient.SetLname(lName);
            myPatient.SethCN(patientHCN);
            myPatient.SetdateOfBirth(birthDay);
            myPatient.SetSex(sex);
            myPatient.SetHeadOfHouse(HoH);
            myPatient.SetAddressID(idAddress);

            return myPatient;
        }


        /// <summary>
        /// Add a new adress during load process when the system is reading file. 
        /// </summary>
        /// <param name="idPatient"></param>
        /// <param name="idAddress"></param>
        /// <param name="hCN"></param>
        /// <returns></returns>
        private Address CreateAnUnknonAddressObj(int idPatient, int idAddress, string hCN = "")
        {
            int myPatientId = idPatient;
            string myHCN = hCN;

            if (myHCN == "") { myHCN = patientList.Count.ToString() + "ZZ"; } //Create a new HCN

            string HCN = myHCN,
                address1 = "Unknown Address1",
                address2 = "Unknown Address2 ",
                city = "Neverland",
                province = "ON",
                phoneNumber = "519-000-0000"
                ;

            var myAddress = new Address();

            myAddress.SetHCN(HCN);
            myAddress.SetAdressLineOne(address1);
            myAddress.SetAdressLineTwo(address2);
            myAddress.SetCity(city);
            myAddress.SetProvince(province);
            myAddress.SetPhoneNumber(phoneNumber);

            return myAddress;
        }
    }
}
*/