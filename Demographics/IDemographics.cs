﻿using System;
using System.Collections.Generic;

namespace Demographics
{
    public interface IDemographics
    {
        // =============== ADD ================
        int AddPatient(Patient obj);
        int AddAppointment(Appointment obj);


        // =============== FIND ===============
        Patient FindPatient(int patientID);
        Patient FindPatient(string hcnNumber);
        List<Patient> FindPatient(Patient obj);

        Address FindAddress(int addressID);
        Address FindAddress(string hcnNumber);

        Appointment FindAppointment(int appointmentID);
        List<Appointment> FindAppointments(int patientID);
        List<Appointment> FindAppointments(DateTime begin, DateTime end);


        // ============== UPDATE ==============
        int UpdatePatient(Patient obj);
        int UpdateAddress(Address obj);
        int UpdateAppointment(Appointment obj);
    }
}
