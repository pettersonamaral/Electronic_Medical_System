﻿/*****************************************************************************************************
* FILE : 		  Patient.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Nov-09
* SECOND VERSION: 2017-Dec-08
* 

* DESCRIPTION/Requirements: 

* This class/file contains all the information about a patient. For an example, patients first name, last name, date of
* birth, address etc. The accessors and mutators are created here. These functions are being called from other functions
* where necessary. It also does all the necessary validation in order to ensure that everything is in working order.

* Refence:

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()

namespace Demographics
{
    /// <summary>
    /// <b>Description: </b>This class contains all the information about the patient.
    /// It contains the necessary validation as well as functions that are being
    /// called from different class.
    /// </summary>
    public class Patient : IEquatable<Patient>
    {
        #region Constants and Attributes

        //declaring constants for maximum lengths
        const int MaxFnameLength = 60;
        const int MaxLnameLength = 60;

        //member variable declaration
        private int patientID = 0;
        private string fName= "";
        private string lName = "";
        private string hCN = "";
        private char mInitial = ' ';
        private DateTime dateOfBirth = DateTime.MinValue;
        private string sex = "";
        private string headOfHouse = "";
        //private int addressID = 0;
        Address address = new Address();

        /// <summary>
        /// This is the readonly dictionary for patients sex
        /// </summary>
        private static readonly Dictionary<string, string> patientSex = new Dictionary<string, string>
        {
            {"FEMALE","F" },{"F","F"},{"MALE","M"},{"M","M"},
            {"INTERSEX","I" },{"I","I"},{"HERMAPHRODITE","H"},{"H","H"}
        };

        #endregion

        #region Properties

        public int PatientID { get => patientID; set => patientID = value; }
        public Address Address { get => address; set => address = value; }
        public DateTime DateOfBirth { get => dateOfBirth; set => dateOfBirth = value; }

        #endregion

        #region Constructor

        //default constructor
        /// <summary>
        /// This is the default constructor for patient class.
        /// </summary>
        /// <returns>This function does not return anything.</returns>
        public Patient()
        {

        }

        #endregion

        #region Setters and Getters

        /// <summary>
        /// This method sets the patient ID to the patient object
        /// </summary>
        /// <param name="newPatientID"></param>
        /// <returns>This function does not return anything.</returns>
        public void SetPatientID(int newPatientID)
        {
            patientID = newPatientID;
        }

        /// <summary>
        /// This method gets the patients ID when the method is being called
        /// </summary>
        /// <returns>This method returns a type int </returns>
        public int GetPatientID()
        {
            return patientID;
        }

        /// <summary>
        /// This method will set the first name for the patient
        /// </summary>
        /// <param name="newFname"></param>
        /// <returns>Boolean value</returns>
        public bool SetFname(string newFname)
        {
            if(newFname == null)
            {
                return false;
            }
            if (newFname == "")
            {
                return false;
            }
            if(newFname.Length>MaxFnameLength)
            {
                return false;
            }
            if (IsLetter(newFname) != true)
            {
                return false;
            }
            fName = newFname;
            return true;
        }


        /// <summary>
        /// Getting the first name for patient
        /// </summary>
        /// <returns>the first name as string</returns>
        public string GetFname()
        {
            return fName;
        }

        /// <summary>
        /// Setting the last name for patient
        /// </summary>
        /// <param name="newlName"></param>
        /// <returns>Boolean value</returns>
        public bool SetLname(string newLname)
        {
            if(newLname == null)
            {
                return false;
            }
            if (newLname == "")
            {
                return false;
            }
            if(newLname.Length>MaxLnameLength)
            {
                return false;
            }
            if (IsLetter(newLname) != true)
            {
                return false;
            }
            lName = newLname;
            return true;
        }
        /// <summary>
        /// Getting last name for patient
        /// </summary>
        /// <returns>Last name as string</returns>
        public string GetLname()
        {
            return lName;
        }

        /// <summary>
        /// Setting the new health card number for patient
        /// </summary>
        /// <param name="newhCN"></param>
        /// <returns>A boolean value</returns>
       
        public bool SethCN(string newhCN)
        {
            if(newhCN == null)
            {
                return false;
            }
            if (newhCN == "")
            {
                return false;
            }
            if(HcnCheck(newhCN) != true)
            {
                return false;
            }
            hCN = newhCN;
            return true;
        }

        /// <summary>
        /// Get the health card number
        /// </summary>
        /// <returns>Returns the health card number</returns>
        public string GethCN()
        {
            return hCN;
        }

        /// <summary>
        /// Setting the middle initial for patient if there is any
        /// </summary>
        /// <param name="newMinitial"></param>
        /// <returns>A boolean value</returns>
        public bool SetMinitial(char newMinitial)
        {
            if(char.IsLetter(newMinitial) != true)
            {
                return false;
            }
            mInitial = newMinitial;
            return true;
        }

        /// <summary>
        /// Get the middle initial for patient
        /// </summary>
        /// <returns>The middle initial for patient</returns>
        public char GetMinitial()
        {
            return mInitial;
        }

        /// <summary>
        /// Setting the gender/sex of the patient
        /// </summary>
        /// <param name="newSex"></param>
        /// <returns>A boolean value</returns>
        public bool SetSex(string newSex)
        {
            if(newSex == null)
            {
                return false;
            }
            if (newSex == "")
            {
                return false;
            }
            if (!patientSex.TryGetValue(newSex.ToUpper(), out newSex))
            {
                return false;
            }
            sex = newSex;
            return true;
        }

        /// <summary>
        /// Get the gender/sex of the patient
        /// </summary>
        /// <returns>The sex or gender of the patient</returns>
        public string GetSex()
        {
            return sex;
        }

        /// <summary>
        /// Setting the head of the household in the event of a minor or under aged patient
        /// </summary>
        /// <param name="newHeadOfHouse"></param>
        /// <returns>A boolean value</returns>
        public bool SetHeadOfHouse(string newHeadOfHouse)
        {
            if(newHeadOfHouse == null)
            {
                return false;
            }
            if(newHeadOfHouse == "")
            {
                return true;
            }
            if (HcnCheck(newHeadOfHouse) != true)
            {
                return false;
            }
            headOfHouse = newHeadOfHouse;
            return true;
        }

        /// <summary>
        /// Get the head of the household
        /// </summary>
        /// <returns>The head of the household</returns>
        public string GetHeadOfHouse()
        {
            return headOfHouse;
        }

        #endregion

        #region Supportive Methods

        /// <summary>
        /// This function will check if there is any character that is not a letter
        /// in a string
        /// </summary>
        /// <param name="line"></param>
        /// <returns>It returns a boolean value</returns>
        //https://msdn.microsoft.com/en-us/library/zff1at55(v=vs.110).aspx
        private bool IsLetter(string line)
        {
            if (line.Length == 0)
            {
                return false;
            }
            for (int i = 0; i < line.Length; i++)
            {
                if (Char.IsLetter(line[i]) != true)
                {
                    if (line[i] != '-')
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// This function checks if the health card number is correct
        /// </summary>
        /// <param name="healthCardNumber"></param>
        /// <returns>A boolean value</returns>
        /// Source:  //https://msdn.microsoft.com/en-us/library/system.text.regularexpressions.regex(v=vs.110).aspx
        private bool HcnCheck(string healthCardNumber)
        {
            Regex regex = new Regex(@"^\d{4}\d{3}\d{3}[A-Za-z]{2}$");
            MatchCollection matches = regex.Matches(healthCardNumber);
            if (matches.Count == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// This function is checking if the patient information is valid or not.
        /// It return 0 if is valid, or an error code otherwise
        /// </summary>
        /// <returns>Returns an integer value</returns>
        public int ValidatePatient() // Error range: 3100-3199
        {
            int retCode = 0;
            if (GetFname() == "")
            {
                retCode = -3101;
            }
            else if (GetLname() == "")
            {
                retCode = -3102;
            }
            else if (GethCN() == "")
            {
                retCode = -3103;
            }
            else if (dateOfBirth == DateTime.MinValue)
            {
                retCode = -3105;
            }
            else if (GetSex() == "")
            {
                retCode = -3106;
            }
            else if(Address.ValidateAdress() != 0)
            {
                retCode = -3107;
            }
            return retCode;
        }

        /// <summary>
        /// This method will check if the string is empty
        /// </summary>
        /// <param name="stringToCheck"></param>
        /// <returns>This method returns a value of type boolean</returns>
        public bool IsEmpty(string stringToCheck)
        {
            bool retCode = false;
            if (stringToCheck == "")
            {
                retCode = true;
            }
            return retCode;
        }

        #endregion

        #region Overridden Methods

        /// <summary>
        /// Defines the Equals operator for this class
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Patient other)
        {
            if (other == null) return false;
            bool retCode = false;

            if (fName == other.fName &&
                lName == other.lName &&
                hCN == other.hCN &&
                mInitial == other.mInitial &&
                dateOfBirth.Equals(other.dateOfBirth) &&
                Address.Equals(other.Address))
            {
                retCode = true;
            }

            return retCode;
        }


        public static bool operator ==(Patient left, Patient right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }


        public static bool operator !=(Patient left, Patient right)
        {
            return !(left == right);
        }



        /// <summary>
        /// This method will return the patient information in a string format.
        /// </summary>
        /// <returns> This method will return a string</returns>
        public override string ToString()
        {
            return $"{fName} {lName} - {hCN}";
        }


        /// <summary>
        /// This method will return the patient information in a string format.
        /// </summary>
        /// <returns> This method will return a string</returns>
        public string ToString(bool fullInfo)
        {
            string Result = "";

            if (fullInfo)
            {
                Result += "First Name: " + this.fName + "\n";
                Result += "Last Name: " + this.lName + "\n";
                Result += "Middle Initial: " + this.mInitial + "\n";
                Result += "Date of Birth: " + this.dateOfBirth.ToShortDateString() + "\n";
                Result += "Health Card Number: " + this.hCN + "\n";
                Result += "Gender: " + this.sex + "\n";
                Result += "Head of House: " + this.headOfHouse + "\n";
                Result += "Address:\n" + address.ToString() + "\n";
            }
            else
            {
                Result = ToString();
            }
            return Result;
        }

        #endregion
    }
}

