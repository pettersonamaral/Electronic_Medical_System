﻿/*****************************************************************************************************
* FILE : 		  Appointment.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Nov-09
* SECOND VERSION: 2017-Dec-08
* 

* DESCRIPTION/Requirements: 

* This class/file contains all the information about appointment data members. It does all the necessary validations
* in order to ensure that everything is in order.

* Refence:

*****************************************************************************************************/

using System;

#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()

namespace Demographics
{
    /// <summary>
    /// <b>Description: </b>This class contains all the information about the appointment.
    /// It contains the necessary validation as well as functions that are being
    /// called from different class.
    /// </summary>
    public class Appointment : IEquatable<Appointment>
    {
        #region Constants and Attributes

        public const int lenthOfAppointmentID = 8;
        public const int lenthOfpatientID = 8;
        public const int lenthOfsecondPatientID = 8;

        private int appointmentID;
        private DateTime dayAndTime;
        private const Int32 SUCCESS_GENERIC_RETURN = 0;

        #endregion

        #region Properties

        public Patient FirstPatient { get; set; }
        public Patient SecondPatient { get; set; }

        public bool IsValid
        {
            get
            {
                bool retCode = false;

                if (SetDayAndTime(dayAndTime) && FirstPatient != null &&
                    FirstPatient.ValidatePatient() == 0)
                {
                    retCode = true;
                }

                return retCode;
            }
        }

        #endregion

        #region Constructor

        ///  
        /// \brief [constructor] 
        /// \details <b> Constructor for Apointment object </b> 
        /// \param NOTA - <b> NOTA </b> 
        /// \return NOTA - <b> NOTA </b> 
        /// 
        public Appointment()
        {
            appointmentID = 0;
            dayAndTime = DateTime.MinValue;
            FirstPatient = new Patient();
            SecondPatient = null;
        }

        #endregion

        #region Setters and Getters

        /// <summary>
        /// Setting the Appountment ID 
        /// </summary>
        /// <param name="newAppointmentID"></param>
        /// <returns>NOTA</returns>
        public bool SetApointmentID(int newAppointmentID)
        {
            if(newAppointmentID>0)
            {
                appointmentID = newAppointmentID;
                return true;
            }
            return false;
        }
      

        /// <summary>
        /// Setting theDay And time  
        /// </summary>
        /// <param name="newdayAndTime"></param>
        /// <returns>boolean value as a retCode</returns>
        public bool SetDayAndTime(DateTime newdayAndTime)
        {
            bool retCode = false;
            if(newdayAndTime.DayOfWeek>= DayOfWeek.Monday && newdayAndTime.DayOfWeek <= DayOfWeek.Friday)
            {
               if(( newdayAndTime.Hour>= 9 && newdayAndTime.Hour <=11)||(newdayAndTime.Hour >= 13 && newdayAndTime.Hour <= 15))
                {
                    if(newdayAndTime.Minute==0)
                    {
                        retCode = true;
                    }
                }
            }
            else
            {
                if (newdayAndTime.Hour >= 9 && newdayAndTime.Hour <= 11)
                {
                    if (newdayAndTime.Minute == 0)
                    {
                        retCode = true;
                    }
                }
            }
            if (retCode == true)
            {
                dayAndTime = newdayAndTime;
            }
            return retCode;
        }


        /// <summary>
        /// Getting the Appointment ID for appointment
        /// </summary>
        /// <returns>the Appointment ID as int</returns>
        public int GetAppointmentID()
        {
            return appointmentID;
        }


        /// <summary>
        /// Getting Day And Time for appointment
        /// </summary>
        /// <returns>the dayAndTime as DateTime</returns>
        public DateTime GetDayAndTime()
        {
            return dayAndTime;
        }


        /// <summary>
        /// This function is checking if the appointment information is valid or not.
        /// </summary>
        /// <returns>Returns an integer value</returns>
        public Int32 ValidateAppointment()
        {
            Int32 retCode           = SUCCESS_GENERIC_RETURN;
            Int32 myAppointmentID   = 0;
            DateTime myDayAndTime   = DateTime.MinValue;
            Patient myPatient01     = null, myPatient02 = null;

            try
            {
                //Get attribuits velues
                myAppointmentID = GetAppointmentID();
                myDayAndTime    = GetDayAndTime();
                myPatient01     = FirstPatient;
                myPatient02     = SecondPatient;

                //check if the object is complete
                if ((myDayAndTime == DateTime.MinValue) ||
                    (myDayAndTime == null))                     { retCode = -3409; }        //If validation got a problem, return a negative number
                else if (myAppointmentID <= 0)                  { retCode = -3410; }        //If validation got a problem, return a negative number
                else if (myPatient01.ValidatePatient() != 0)    { retCode = -3110; }        //If validation got a problem, return a negative number
                else if ((myPatient02 != null) &&                                           //If existe a second patient, check it
                    (myPatient01.ValidatePatient() != 0))       { retCode = -3120; }        //If validation got a problem, return a negative number
            }
            catch
            {
                throw new NotImplementedException();
            }
            finally {   }
            return retCode;
        }

        #endregion

        #region Operators

        /// <summary>
        /// Equals operator overridden to support comparisson
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Appointment other)
        {
            if (other == null) { return false; }
            bool retCode = false;

            if (dayAndTime.Equals(other.dayAndTime) &&
                FirstPatient.Equals(other.FirstPatient))
            {
                if (SecondPatient != null && other.SecondPatient != null)
                {
                    retCode = SecondPatient.Equals(other.SecondPatient);
                }
                else if(SecondPatient == null && other.SecondPatient == null)
                {
                    retCode = true;
                }
            }

            return retCode;
        }


        public static bool operator ==(Appointment left, Appointment right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }


        public static bool operator !=(Appointment left, Appointment right)
        {
            return !(left == right);
        }


        #endregion

        public override string ToString()
        {
            return dayAndTime.ToString();
        }
    }
}

