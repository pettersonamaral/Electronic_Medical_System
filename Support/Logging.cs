﻿/*****************************************************************************************************
* FILE : 		Logging.cs
* PROJECT : 	SQ-I Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2017-Nov-09

* DESCRIPTION/Requirements: 
The purpose and goal of any entry made in the logging file is to help recreate (and trace) the sequence 
of events that occurred and lead to a certain point in processing a customer reported error perhaps
3.2.4.2.1	The logging filename will be in the format: ems.YYYY-MM-DD.log (where YYYY-MM-DD is the day that the logging file was created)
3.2.4.2.2	Each logging event will be contained in the following fields:
    Event Timestamp in the format YYYY-MM-DD hh:mm:ss, eg. "2010-09-01 07:02:00"
    [ClassName.MethodName] logging the event – eg. "[Patient.Validate]"
    Event details

    An example might be:
    2017-09-01 07:02:00 [Patient.Validate] Patient - Clarke,Sean (HCN 333 333 333) – INVALID

Example to call this class:
    using Support;
    using System.Reflection;

    private static Logging  logging = new Logging(); //Creating a new obj
    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Patient - Clarke,Sean (HCN 333 333 333) - INVALID"); //Call the class




* Refence:
https://gist.github.com/heiswayi/69ef5413c0f28b3a58d964447c275058
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;


namespace Support
{
    /// <summary>
    /// <b>Description:</b>Class Logging - This class must manage the Log stuff and write property text into the log file.
    /// </summary>
    public class Logging
    {
        // =======================================================================
        //                            PRIVATE METHODS
        // =======================================================================
        //Variable

        private const string folderName = @".\Log";   //Path to keep the files 


        private string datetimeFormatLogLine;   //Keep format log line information
        private string datetimeFormatFileName;  //Keep format log line information
        private string fileName;                //Keep the log file name
        private string filePath;                //Keep the path of the log files
        private string logHeader;               //Keep the information of the very first line of all log files 
        private bool   logClassHealthy;         //Keep the information if the log class is healthy


        /// <summary>
        /// Supported log level
        /// </summary>
        //[Flags]
        private enum LogLevel
        {
            TRACE,
            INFO,
            DEBUG,
            WARNING,
            ERROR,
            FATAL,
            SETPROGRAM
        }

        /// <summary>
        /// Format a log message based on log level
        /// </summary>
        /// <param name="level"></param>
        /// <param name="classAndMethodName">Class and Method name that did call the log class message</param>
        /// <param name="text"></param>
        private void WriteFormattedLog(LogLevel level, Type classAndMethodName, string text)
        {
            string pretext = "";
            switch (level)
            {
                case LogLevel.TRACE: pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] " + " [TRACE]   "; break;
                case LogLevel.INFO: pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] " + " [INFO]    "; break;
                case LogLevel.DEBUG: pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] " + " [DEBUG]   "; break;
                case LogLevel.WARNING: pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] " + " [WARNING] "; break;
                case LogLevel.ERROR: pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] " + " [ERROR]   "; break;
                case LogLevel.FATAL: pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] " + " [FATAL]   "; break;
                case LogLevel.SETPROGRAM:pretext = DateTime.Now.ToString(datetimeFormatLogLine) + " [" + classAndMethodName + "] "; break; //eg. 2017-09-01 07:02:00 [Patient.Validate] Patient - Clarke,Sean (HCN 333 333 333) – INVALID
                default: pretext = ""; break;
            }

            //Call a function to verify if the Log still healthy 
            CheckIfLogClassIsHealthy(pretext + text);
        }

        /// <summary>
        /// Verify if the Class Log class is healthy and then call Write File. 
        /// </summary>
        /// <param name="text">Log message</param>
        private void CheckIfLogClassIsHealthy(string text)
        {
            if (logClassHealthy == true)
            {
                if (!CheckIfFileExistsAndCreateIt() == true)
                {
                    logClassHealthy = false;   //If CheckIfFileExistsAndCreateIt method doesn't return false then call the method WriteLine.
                }
                if (!WriteLine(text) == true)
                {
                    logClassHealthy = false;   //If WriteLine method doesn't return false then the Log Class still healthy.
                }
            }
        }


        /// <summary>
        /// This class verify if the file already exists, if it doesn't exist a new one will be created.
        /// </summary>
        /// <returns>True if was fine.</returns>
        private bool CheckIfFileExistsAndCreateIt()
        {
            bool setMadeSuccess = true;
            fileName = GetFileName();            // Load path + file name + file extension 
            logHeader = GetLogHeader();          // Load header line to log file

            try
            {
                if (File.Exists(filePath))
                {
                    //Check if have a file with the same name of the directory, if exist rename it.
                    File.Move(folderName, folderName + ".file");                                 
                }

                if (!Directory.Exists(filePath))
                {
                    //Create a folder if it doesn't exist  
                    Directory.CreateDirectory(filePath);                                   
                }

                if (Directory.Exists(fileName))
                {
                    //Check if have a directory with the same name of the file, if exist rename it.
                    Directory.Move(fileName, fileName + ".dir");                            
                }

                if (!File.Exists(fileName))
                {
                    //Create a file if it doesn't exist 
                    WriteLine(DateTime.Now.ToString(datetimeFormatLogLine) + " " + logHeader);  
                }
                
            }

            catch (Exception e)
            {
                //throw new Exception(String.Format("The Log Class generate an error type {0}", e.Message));
                Console.WriteLine("\nThe " + MethodBase.GetCurrentMethod().DeclaringType + " Class generates an error: \n-> {0}", e.Message);
                Console.WriteLine("\n     >>> The EMS system will be running without log support events.<<< \n");
                setMadeSuccess = false;
                logClassHealthy = false;                
            }
            return setMadeSuccess;
        }

        /// <summary>
        /// Write into a log file a line of formatted log message.
        /// </summary>
        /// <param name="text">Formatted text the will into the log message</param>
        /// <returns>True if was fine.</returns>
        private bool WriteLine(string text)
        {
            bool setMadeSuccess = true;
            try
            {
                using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Unicode))                 // Write into a log file a line of formatted log message.
                {
                    if (text != "")
                    {
                        writer.WriteLine(text);
                    }
                }
            }

            catch (Exception e)
            {
                //throw new Exception(String.Format("The Log Class generate an error type {0}", e.Message));
                Console.WriteLine("\nThe " + MethodBase.GetCurrentMethod().DeclaringType + " Class generates an error: \n-> {0}", e.Message);
                Console.WriteLine("\n     >>> The EMS system will be running without log support events.<<< \n");
                setMadeSuccess = false;
                logClassHealthy = false;
            }
            return setMadeSuccess;
        }




        // =======================================================================
        //                            PUBLIC METHODS
        // =======================================================================

        /// <summary>
        /// Constructor Logging - Initialize a new instance of object class.
        /// Initialize attributes.
        /// </summary>
        public Logging()
        {
            datetimeFormatLogLine = "yyyy-MM-dd HH:mm:ss"; // Load date format to log lines 
            datetimeFormatFileName = "yyyy-MM-dd";         // Load date format to log file name
            filePath = @".\log\";                          // Load path of data files
            logClassHealthy = true;                        // Saying that the Log is healthy
        }

        /// <summary>
        /// This method retuns a string with the actual filename that must be used. 
        /// </summary>
        /// <returns>string - fileName information</returns> 
        public string GetFileName()
        {
            string plainText = "";
            plainText = filePath + "ems." + DateTime.Now.ToString(datetimeFormatFileName) + ".log";                      // Load path + file name + file extension 
            return plainText;
        }

        /// <summary>
        /// This method retuns a string with the actual header to a new log file.  
        /// </summary>
        /// <returns>string - Header information</returns> 
        public string GetLogHeader()
        {
            string plainText = "";
            plainText = "ems." + DateTime.Now.ToString(datetimeFormatFileName) + ".log" + " file is created.";                      // Load path + file name + file extension 
            return plainText;
        }

        /// <summary>
        /// This method returns the path to write logs. 
        /// </summary>
        /// <returns>string - Path information</returns> 
        public string GetLogPath()
        {
            string plainText = "";
            plainText = folderName;
            return plainText;
        }

        /// <summary>
        /// Log a SET Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void SetProgram(Type classAndMethodName, string details)
        {
            WriteFormattedLog(LogLevel.SETPROGRAM, classAndMethodName, details);
        }

        /// <summary>
        /// Log a Debug Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void Debug(Type classAndMethodName, string text)
        {
            WriteFormattedLog(LogLevel.DEBUG, classAndMethodName, text);
        }

        /// <summary>
        /// Log a Error Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void Error(Type classAndMethodName, string text)
        {
            WriteFormattedLog(LogLevel.ERROR, classAndMethodName, text);
        }

        /// <summary>
        /// Log a Fatal Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void Fatal(Type classAndMethodName, string text)
        {
            WriteFormattedLog(LogLevel.FATAL, classAndMethodName, text);
        }

        /// <summary>
        /// Log a Info Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void Info(Type classAndMethodName, string text)
        {
            WriteFormattedLog(LogLevel.INFO, classAndMethodName, text);
        }

        /// <summary>
        /// Log a Trace Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void Trace(Type classAndMethodName, string text)
        {
            WriteFormattedLog(LogLevel.TRACE, classAndMethodName, text);
        }

        /// <summary>
        /// Log a Warning Program message
        /// </summary>
        /// <param name="classAndMethodName">[ClassName.MethodName], eg. "[Patient.Validate]"</param>
        /// <param name="details">Event details(text of this event)</param>
        /// <returns>void()</returns> 
        public void Warning(Type classAndMethodName, string text)
        {
            WriteFormattedLog(LogLevel.WARNING, classAndMethodName, text);
        }
    }
}
