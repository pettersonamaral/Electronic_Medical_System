﻿/*****************************************************************************************************
* FILE : 		  FileIO.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Nov-09
* SECOND VERSION: 2017-Dec-08
* 

* DESCRIPTION/Requirements: 

* This class/file contains the necessary methods to open (for reading and or writing) and close the Database file.
* This class will also contain methods to read a database record and to write a database record. The class is designed
* in such a way that uring the invali or incomplete status of a file, it will return error by notifying the user.
* the file it will 

* Refence:

*****************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace Support
{
    /// <summary>
    /// <b>Description:</b>FileIO Class - This class will control the IO from and to data files. 
    /// </summary>
    public class FileIO
    {
        // Attributes - Support related
        private static Logging logging = new Logging();

        // =======================================================================
        //                            PRIVATE METHODS
        // =======================================================================      
        
        //Constant
        private const string folderName = @".\DBase";   //Path to keep the files 

        //Variable
        private string fileName;                        //Keep the log file name


        /// <summary>
        /// FileManager - Call the function to get the file names, and check if the directory exists.
        /// </summary>
        /// <param name="source"> Text that will go into the file.</param>
        /// <param name="destination"> Destination file.</param>
        /// <param name="append"> Either append or everwrite the files.</param>
        /// <returns>true - If was is done ok</returns>
        private bool FileManager(string source, string destination, bool append)
        {
            bool setMadeSuccess = false;

            if (MakeFileName(destination) == false) { return setMadeSuccess; }   // Call the function MakeFileName if fail, return fail.


            if (CheckIfDirectoryExistsAndCreateIt() == true)                // Call the function CheckIfDirectoryExistsAndCreateIt if pass, then call WriteFile
            {
                if (WriteFile(source, append) == true) { setMadeSuccess = true; }
            }

            return setMadeSuccess;
        }


        /// <summary>
        /// FileManager - Call the function to get the file names, and check if the directory exists.
        /// </summary>
        /// <param name="source"> Text that will go into the file.</param>
        /// <param name="destination"> Destination file.</param>
        /// <param name="append"> Either append or everwrite the files.</param>
        /// <returns>true - If was is done ok</returns>
        private bool FileManager(string source, ref string destination)
        {
            bool setMadeSuccess = false;


            if (MakeFileName(source) == false) { return setMadeSuccess; } // If MakeFileName fail, return fail.

            //Call a function to verify if the directory exists, then call ReadFile
            if (CheckIfDirectoryExistsAndCreateIt() == true)
            {
                if (ReadFile(fileName, ref destination) == true) {setMadeSuccess = true; }
            }  

            return setMadeSuccess;
        }


        /// <summary>
        /// Dictionary of files. Where the system stores an index string key for each full path filename.
        /// </summary>
        // source https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/how-to-initialize-a-dictionary-with-a-collection-initializer
        private static Dictionary<string, string> myFilesDic = new Dictionary<string,string>()
        {
            { "PATIENT",        folderName + "\\" + "patient"    + "_db" + ".txt"},
            { "ADDRESS",        folderName + "\\" + "address"    + "_db" + ".txt"},
            { "APPOINTMENT",    folderName + "\\" + "appointment"+ "_db" + ".txt"},
            { "BILL",           folderName + "\\" + "bill"       + "_db" + ".txt"},
            { "SERVICELINE",    folderName + "\\" + "serviceline"+ "_db" + ".txt"},
            { "SERVICE",        folderName + "\\" + "service"    + "_db" + ".txt"},
            { "RESPONSE",       folderName + "\\" + "response"   + "_db" + ".txt"},
            { "SERVICESMASTER", folderName + "\\" + "servicesmaster"+ "_db" + ".txt"},
            { "MONTHLYBILLING", folderName + "\\" + "monthlybilling"+ "_db" + ".txt"},
        };

        /// <summary>
        /// Format a file name
        /// </summary>
        /// <param name="level">File Name Index </param>
        /// <param name="text">Log message</param>
        /// <returns>true - If was is done ok</returns>
        private bool MakeFileName(string filename)
        {
            bool setMadeSuccess = true;
            
            if (myFilesDic.TryGetValue(filename, out string pretext))
            {
                //Update the FileIO Class "fileName" with "pretext" content. 
                fileName = pretext;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// This class verify if the diretory already exists, if it doesn't exist a new one will be created.
        /// </summary>
        /// <returns>true - If it was valid</returns>
        private bool CheckIfDirectoryExistsAndCreateIt()
        {
            bool setMadeSuccess = true;

            try
            {
                if (File.Exists(folderName)) File.Move(folderName, folderName + ".file");                      //If exists a file with the directory name, rename it.
                if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);                      //Create a folder if it doesn't exist  
            }

            catch (Exception e)
            {
                Console.WriteLine("\nThe " + MethodBase.GetCurrentMethod().DeclaringType + " Class generates an error: \n-> {0}", e.Message);
                Console.WriteLine("\n     >>> Problem to handle directory stuff.<<< \n");
                setMadeSuccess = false;
            }
            return setMadeSuccess;
        }


        /// <summary>
        /// Write a file based on a string input 
        /// </summary>
        /// <param name="text">Formatted text to put into a file</param>
        /// <param name="append"> Either false/true if you want to append on file.</param>
        /// <returns>true - If was is done ok</returns>
        private bool WriteFile(string text, bool append)
        {
            bool setMadeSuccess = true;
            try
            {
                if (append == true)
                {
                    using (StreamWriter writer = new StreamWriter(fileName, append, Encoding.Unicode))                        // Write into a file a line of formatted log message.
                    {
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Begin writing. " + fileName.Length + "bytes will be written in the file \"" + fileName + "\"."); //Call the class
                        if (text != "") { writer.Write(text); }
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "End writing. " + fileName.Length + "bytes were written in the file \"" + fileName + "\".");//Call the class
                    }
                }
                else
                {
                    using (StreamWriter writer = new StreamWriter(fileName, append, Encoding.Unicode))                        // Write into a file a line of formatted log message.
                    {
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Begin writing. " + fileName.Length + "bytes will be written in the file \"" + fileName + "\"."); //Call the class
                        if (text != "") { writer.Write(text); }
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "End writing. " + fileName.Length + "bytes were written in the file \"" + fileName + "\".");//Call the class
                    }
                }
            }
            catch (Exception e)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "WriteFile function failure to read the file " + fileName + ". " + e.Message); //Call the class
                Console.WriteLine("\nThe " + MethodBase.GetCurrentMethod().DeclaringType + " Class generates an error: \n-> {0}", e.Message);
                setMadeSuccess = false;
            }            
            return setMadeSuccess;
        }


        /// <summary>
        /// Read a file based on a string input and returns an empty string ("") passed as a ref to this function.
        /// </summary>
        /// <param name="fileName">The file where the data come from.</param>
        /// <param name="destination">Ref. of a string that will receive the data.</param>
        /// <returns>true - If was is done ok</returns>
        private bool ReadFile(string fileName, ref string destination)
        {
            bool setMadeSuccess = true;
 
            try
            {
                using (StreamReader read = new StreamReader(fileName) )                 // Read into a log file a line of formatted log message.
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Begin reading. " + fileName.Length + "bytes will be read from the file \"" + fileName + "\"."); //Call the class
                    destination = read.ReadToEnd();
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "End reading. " + fileName.Length + "bytes will be read from the file \"" + fileName + "\"."); //Call the class
                }
            }

            catch (Exception e)
            {
                //throw new Exception(String.Format("The Log Class generate an error type {0}", e.Message));
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFile function failure to read the file " + fileName + ". " + e.Message); //Call the class
                Console.WriteLine("\nThe " + MethodBase.GetCurrentMethod().DeclaringType + " Class generates an error: \n-> {0}", e.Message);                
                setMadeSuccess = false;
            }

            return setMadeSuccess;
        }



        // =======================================================================
        //                            PUBLIC METHODS
        // =======================================================================

        /// <summary>
        /// Constructor FileIO - Initialize a new instance of object class.
        /// Initialize attributes.
        /// </summary>
        public FileIO()
        {
            fileName = "";                     // Load date format to log lines 
        }


        //----Write ADDRESS
        /// <summary>
        /// Write something into ADDRESS file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullAddressFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "ADDRESS", true) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into ADDRESS file without append flagged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullAddressFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "ADDRESS", false) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Write PATIENT
        /// <summary>
        /// Write something into PATIENT file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullPatiantFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "PATIENT", true) == true) { setMadeSuccess = true; } 

            return setMadeSuccess; 
        }

        /// <summary>
        /// Write something into PATIENT file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullPatiantFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "PATIENT", false) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Write APPOINTMENT
        /// <summary>
        /// Write something into APPOINTMENT file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullAppointmentFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "APPOINTMENT", true) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into APPOINTMENT file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullAppointmentFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "APPOINTMENT", false) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Write BILL
        /// <summary>
        /// Write something into BILL file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullBillFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "BILL", true) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into BILL file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        /// <returns>true - If was is done ok</returns>
        public bool WriteFullBillFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "BILL", false) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Write SERVICESMASTERFILE
        /// <summary>
        /// Write something into SERVICELINE file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullServicesMasterFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "SERVICESMASTERFILE", true) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into SERVICELINE file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullServicesMasterFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "SERVICESMASTER", false) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Write SERVICELINE
        /// <summary>
        /// Write something into SERVICELINE file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullServicelineFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "SERVICELINE", true) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into SERVICELINE file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullServicelineFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "SERVICELINE", false) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Write SERVICE
        /// <summary>
        /// Write something into SERVICE file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullServiceFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "SERVICE", true) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into SERVICE file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullServiceFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "SERVICE", false) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Write RESPONSE
        /// <summary>
        /// Write something into SERVICE file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullResponseFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "RESPONSE", true) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into SERVICE file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullResponseFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "RESPONSE", false) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Write MONTHLYBILLING
        /// <summary>
        /// Write something into MONTHLYBILLING file with append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullMonthlybillingFileAppend(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "MONTHLYBILLING", true) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }

        /// <summary>
        /// Write something into MONTHLYBILLING file without append flaged
        /// </summary>
        /// <param name="buffer">Text details that will go into the log file</param>
        ///  <returns>true - If was is done ok</returns>
        public bool WriteFullMonthlybillingFileOverwrite(string buffer)
        {
            bool setMadeSuccess = false;

            if (FileManager(buffer, "MONTHLYBILLING", false) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Read ADDRESS
        /// <summary>
        /// Read the whole into ADDRESS
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullAddressFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("ADDRESS", ref destination) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Read PATIENT
        /// <summary>
        /// Read the whole into PATIENT
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullPatiantFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("PATIENT", ref destination) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Read APPOINTMENT
        /// <summary>
        /// Read the whole into APPOINTMENT
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullAppointmentFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("APPOINTMENT", ref destination) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Read BILL
        /// <summary>
        /// Read the whole into BILL
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullBillFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("BILL", ref destination) == true) { setMadeSuccess = true; } 

            return setMadeSuccess;
        }


        //----Read SERVICESMASTERFILE
        /// <summary>
        /// Read the whole into SERVICELINE
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullServicesMasterFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("SERVICESMASTER", ref destination) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Read RESPONSE
        /// <summary>
        /// Read the whole into SERVICE
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullResponseFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("RESPONSE", ref destination) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Read SERVICELINE
        /// <summary>
        /// Read the whole into SERVICELINE
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullServicelineFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("SERVICELINE", ref destination) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Read SERVICE
        /// <summary>
        /// Read the whole into SERVICE
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullServiceFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("SERVICE", ref destination) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----Read MONTHLYBILLING
        /// <summary>
        /// Read the whole into MONTHLYBILLING
        /// </summary>
        /// <param name="destination"> Pass the file path as reference. 
        ///  <returns>true - If was is done ok</returns>
        public bool ReadFullMonthlybillingFile(ref string destination)
        {
            bool setMadeSuccess = false;

            if (FileManager("MONTHLYBILLING", ref destination) == true) { setMadeSuccess = true; }

            return setMadeSuccess;
        }


        //----SUPPORT
        /// <summary>
        /// Read a File and load it on a string reference.
        /// </summary>
        /// <param name="void"> There is no parameter
        ///  <returns>true - If was is done ok</returns>
        public string[] GetFileNames()
        {
            string[] fileNames = new string[myFilesDic.Count];


            for (int i = myFilesDic.Count -1; i >= 0; i--)
            {
                var item = myFilesDic.ElementAt(i);
                fileNames[i] = item.Value;
            }
            return fileNames;
        }

        /// <summary>
        /// This class verify if the files already exist, if them doesn't exist, new files will be created.
        /// </summary>
        /// <returns>true - If it was valid</returns>
        public bool CheckIfFilesExistsAndCreateIt()
        {
            bool setMadeSuccess = true;
            string[] filesNames = GetFileNames();

            try
            {
                CheckIfDirectoryExistsAndCreateIt();
                for (int i = 0; i < filesNames.Length; i++)
                {
                    if (Directory.Exists(filesNames[i])) Directory.Move(filesNames[i], filesNames[i] + ".dir");   //If exists a diretory with the file name, rename it.
                    if (!File.Exists(filesNames[i]))
                    {
                        var myfile = File.Create(filesNames[i]);                                                  //If file doesn't exists, create it.                    
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "CheckIfFilesExistsAndCreateIt - Created the new empty file " + filesNames[i] + "- [successful]" );
                        myfile.Close();                                                                           //Close file. 
                    }                                  
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("\nThe " + MethodBase.GetCurrentMethod().DeclaringType + " Class generates an error: \n-> {0}", e.Message);
                Console.WriteLine("\n     >>> Problem to handle check if files exist.<<< \n");
                setMadeSuccess = false;
            }
            return setMadeSuccess;
        }
    }
}
