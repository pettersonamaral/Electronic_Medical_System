﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connection;

namespace HCNServer
{
    /// <summary>
    /// This class contains information to check if HCN is valid or not and returns status back to connection
    /// </summary>
    static public class HVC
    {
        //list of valid HCNs with codes
        static List<string> hcnList = new List<string>();
        //a list of respond codes
        const string valid = "VALID";
        const string vcode = "VCODE";
        const string punko = "PUNKO";


        /// <summary>
        /// This method checks if HCN is valid or not and if check code matches it
        /// </summary>
        /// <param name="hcn"></param>
        /// <param name="postalCode"></param>
        /// <returns>Validation respond status</returns>
        static public string CheckHCN(string hcn, string postalCode)
        {
            //assume HCN is not valid
            string retCode = punko;
            string chckCode = "";
            //get check code
            for (int i=0; i<2;i++)
            {
                chckCode = chckCode + postalCode[i];
            }
            int lstSize = hcnList.Count();
            int found = 0;
            //go throught list to compare HCNs
            for(int i=0; i< lstSize; i++)
            {
                string currHcnAndCode = hcnList[i];
                string currHcn = "";
                //hCN
                for (int j=0;j<11;j++)
                {
                    currHcn = currHcn + currHcnAndCode[j];
                }
                string currCode = "";
                //Check code
                currCode = currCode + currHcnAndCode[13];
                currCode = currCode + currHcnAndCode[14];
                //if HCN matches
                if(hcn== currHcn)
                {
                    if(chckCode == currCode)
                    {
                        retCode = valid;
                    }
                    else
                    {
                        retCode = vcode;
                    }
                    found = 1;
                }
            }
            if(found==0)
            {
                retCode = punko;
            }
            return retCode;
        }


        /// <summary>
        /// This method adds a list of valid HCNs with check code to the list
        /// </summary>
        public static void AddHCN()
        {
            hcnList.Add("123456789AAN2");
            hcnList.Add("11111111BBP8");
            hcnList.Add("0987654321ZZM7");
            hcnList.Add("2143658709KLV4");
            hcnList.Add("3412785690GHN2");
            hcnList.Add("0912873465BNN4");
            hcnList.Add("6789054321VCK4");
            hcnList.Add("0192837465CDF5");
            hcnList.Add("1122334455BVN2");
            hcnList.Add("0099887766BRH5");
            hcnList.Add("6847964534HBM6");
            hcnList.Add("9705338725SSF4");
            hcnList.Add("123466789AAN7");
            hcnList.Add("123456789BVN2");
            hcnList.Add("123456789MMJ8");
            hcnList.Add("123456789BAR4");
        }



        public static void OnMessageReceived(object source, ClientEventArgs args)
        {
            string[] messages = args.Message.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            string response = "Not Processed";

            if (messages.Length == 2)
            {
                response = CheckHCN(messages[0], messages[1]);
            }
            else
            {
                response = "Invalid arguments. Expecting HCN;POSTAL_CODE. E.G: 1234567890AA;N2P0C0";
            }
            ServerConnection.Instance.WriteMessage(args.Client, response);
        }
    }
}
