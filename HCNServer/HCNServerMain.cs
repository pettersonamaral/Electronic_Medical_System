﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Connection;

namespace HCNServer
{
    class HCNServerMain
    {
        private static ServerConnection myConnection = ServerConnection.Instance;

        static void Main(string[] args)
        {
            Console.Title = "HCN Server - Ver. 0.1a";
            HVC.AddHCN();
            // Parse Arguments
            ParseArgs(args, out int port);

            // Subscribe to events
            myConnection.MessageReceived += HVC.OnMessageReceived;

            // Start server
            myConnection.Start(port);

            // Keep server open
            Console.ReadLine();

            // Close Everything
            myConnection.Shutdown();
        }

        /// <summary>
        /// this method parses agruments recived from an application to get HCN and Postal code
        /// </summary>
        /// <param name="args"></param>
        /// <param name="port"></param>
        static void ParseArgs(string[] args, out int port)
        {
            port = ServerConnection.HCN_PORT;

            for(int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-p")
                {
                    Int32.TryParse(args[i + 1], out port);
                }
            }

        }
    }
}
