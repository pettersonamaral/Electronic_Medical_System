﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Connection;


namespace Server
{
    public static class View
    {
        #region Event Handlers

        public static void OnServerStarted(object source, EventArgs args)
        {
            Console.WriteLine("IPv4: " + ServerConnection.Instance.IP);
            Console.WriteLine("Port: " + ServerConnection.Instance.Port.ToString());
            Console.WriteLine("Server setup complete!");
        }

        public static void OnClientConnected(object source, ClientEventArgs args)
        {
            Console.WriteLine("Client connected!");
        }

        public static void OnClientDisconnected(object source, ClientEventArgs args)
        {
            Console.WriteLine("Client disconnected.");
        }

        public static void OnMessageReceived(object source, ClientEventArgs args)
        {
            Console.WriteLine("Message Received: " + args.Message);
        }

        #endregion

        #region 

        /// <summary>
        /// This method will wait the user to type a valid command.
        /// If the user types "exit" or "quit" the method returns
        /// </summary>
        public static void RunViewLayer()
        {
            string localCommand = "";
            while (true)
            {
                localCommand = Console.ReadLine();
                localCommand = localCommand.ToLower();
                if (localCommand == "exit" || localCommand == "quit")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid command. Type \'exit\' or \'quit\' to terminate the server.");
                }
            }
        }

        #endregion
    }
}
