﻿/*****************************************************************************************************
* FILE : 		  ServerMain.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-09
* SECOND VERSION: 2017-Dec-08
* FINAL VERSION:  2018-Jan-04

* DESCRIPTION/Requirements: 

* This file contains the server code that helps establish the connection between the server and client.

* Refence:

*****************************************************************************************************/


/// \mainpage Project: ELECTRONIC MEDICAL SYSTEM (EMS)
///
///\section intro_sec About The Project
///The purpose of this project is to develop a very well designed and secure Electronic Medical System (EMS) 
///prototype for Omnicorp Corporation where the medical system will be used by a medical clinic in Ontario.
///The main idea behind this project is to create a medical system that will be simple, flawless, secure and 
///be able to manage patients’in an ordinary fashion where the user will be able to register patients, book 
///appointments and create billing information when needed.  
///
/// The EMS prototype is expected to have a very nice console interface, as well as a flexible and 
///well-designed back end.It also should be very user-friendly, so that the user doesn’t have to go through a 
///hard time to figure out the system.Nevertheless, a thorough training will be provided on how the system 
///should perform and how to troubleshoot if it ever becomes necessary. 
///
/// The justification behind the EMS project is to provide the family clinic a secure and fast tool to book 
/// appointments, enter new patients, and generate reports when needed.The clinic observed by Mr.Thornton 
/// currently does everything manually which is very time-consuming and arguably unsafe. Another goal of 
/// this project is to provide a scalable/transferable system, so as clinics expand, the system can grow 
/// up with them.Moreover, if the EMS prototype runs successfully (flawlessly as they mention), 
/// Omnicorp plans to implement the system to other clinics
///
/// <p font-size="20px"><b>Authors:</b> Humaira Siddiqa, Victor Lauria, Anastasiia Korneva, Petterson Amaral, Paulo Casado </p>
///\subsection prog Program: INFO2180:  SOFTWARE QUALITY I
///\subsection date Date: December 15th, 2017
///

using System;
using Support;
using Connection;
using System.Net;
using DBManager;
using Billing;
using Demographics;
using Interpreter;
using System.Reflection;

namespace Server
{
    /// <summary>
    /// <b>Description: </b>This is the core of the server application. It contains
    /// the file I/O login, demographics tracker, billing tracker, server interpreter and server
    /// connection as part of the application.  When loaded, the Server main will load all the 
    /// entries from the database, load all the tracker information and will start to accept the client 
    /// requests.
    /// </summary>
    static class ServerMain
    {
        // Constants
        private static readonly int HCN_CONNECT_MAX_TRIES = 5;

        // Attributes - Support related
        private static FileIO   fileIO  = new FileIO();
        private static Logging  logging = new Logging();

        // Attributes - Connection related
        private static ServerInterpreter interpreter = ServerInterpreter.Instance;
        private static ServerConnection myServer = ServerConnection.Instance;
        private static ClientConnection hcnServer = ClientConnection.Instance;
        static DBConnection dBConnection = DBConnection.Instance;


        // ======================= MAIN =======================
        static void Main(string[] args)
        {
            Console.Title = "EMS Server - Ver. 2.0a";

            // Check arguments
            ParseArguments(args, out int port, out IPAddress hcnAddress, out int hcnPort);

            // Initialize Database
            fileIO.CheckIfFilesExistsAndCreateIt(); //Check if files exist. Create it if doesn't exist.

            try
            {
                dBConnection.Connect();
                Int32 i = DBSupport.CountPatients(); //Pet - Just a temporary way to test the connection and a simple select
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            // Subscribe Interpreter to the connection
            myServer.MessageReceived += interpreter.OnMessageReceived;

            // Subscribe View to the connection
            myServer.ServerStarted += View.OnServerStarted;
            myServer.ClientConnected += View.OnClientConnected;
            myServer.ClientDisconnected += View.OnClientDisconnected;

            // Connect to HCN Server
            int tries = 0;
            Console.WriteLine("Connecting to HCN Server...");
            while(!hcnServer.IsConnected && tries < HCN_CONNECT_MAX_TRIES)
            {
                tries++;
                try
                {
                    hcnServer.Connect(hcnAddress, hcnPort);
                    Console.WriteLine("Connected to HCN Server!");
                }
                catch (Exception)
                {
                    // The server is trying to connect to the HCN server few times
                    // before giving up
                }
            }

            if (tries >= HCN_CONNECT_MAX_TRIES)
            {
                // Failed to connect to the HCN server
                HandleException(new Exception("Failed to connect to HCN Server."));
                Console.ReadKey();
            }
            else
            {
                // Start server connection
                myServer.Start(port);

                // Run server until receive 'exit' or 'quit' command
                View.RunViewLayer();
                
                // Close everything
                myServer.Shutdown();
                hcnServer.Disconnect();
            }
        }


        /// <summary>
        /// This method parses the arguments when the server starts. Any invalid argument
        /// will not set the variable and a default value will be used.
        /// The possible arguments are:
        /// -p      : sets the port of the server
        /// -hip    : sets the HCN server IP address
        /// -hp     : sets the HCN server port
        /// E.G.    : Server.exe -p 8000 -hip 127.0.0.1 -hp 8001
        /// </summary>
        /// <param name="args">Argument list</param>
        /// <param name="port">Output the port</param>
        /// <param name="hcnAddress">Output the HCN address</param>
        /// <param name="hcnPort">Output  the HCN port</param>
        /// 
        private static void ParseArguments(string[] args, out int port, out IPAddress hcnAddress, out int hcnPort)
        {
            port = ServerConnection.PORT;
            hcnAddress = IPAddress.Parse("127.0.0.1");
            hcnPort = ServerConnection.HCN_PORT;

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-p")
                {
                    Int32.TryParse(args[i + 1], out port);
                }

                else if (args[i] == "-hip")
                {
                    IPAddress.TryParse(args[i + 1], out hcnAddress);
                }

                else if (args[i] == "-hp")
                {
                    Int32.TryParse(args[i + 1], out hcnPort);
                }
            }
        }


        /// <summary>
        /// This method will log an exception
        /// </summary>
        /// <param name="x">The exception caught</param>
        /// 
        private static void HandleException(Exception x)
        {
            Console.WriteLine(x.Message);
            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, x.Message); //Call the class
        }
    }
}

