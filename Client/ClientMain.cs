﻿
/*****************************************************************************************************
* FILE : 		  ClientMain.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-28
* SECOND VERSION: 2017-Dec-08
* THIRD VERSION:  2018-Jan-04
* FINAL VERSION:  2018-Mar-28

* DESCRIPTION/Requirements: 

* The file contains the main function for the client class.
* 
* Refence: 

*****************************************************************************************************/
using System;
using System.Net;
using Connection;
using SchedullingUI;

namespace Client
{
    /// <summary>
    /// <b>Description: </b>ClientMain is the core of the client application. It instantiates the ClientConnection, 
    /// ClientInterpreter, and the View layer. This application is the only applications that the
    /// user in going to interact with. The client application connects to an existing Server 
    /// application that is part of the same network.
    /// </summary>
    static class ClientMain
    {
        // Interfaces
        private static View userView = new View();

        // ======================= MAIN =======================
        static void Main(string[] args)
        {
            Console.Title = "EMS Client - Ver. 1.0";

            ConnectLoop();

            // Test Humairas code
            userView.MainMenu();

            ClientConnection.Instance.Disconnect();
        }


        // This has to be moved to the view or something
        private static void ConnectLoop()
        {
            try
            {
                ClientConnection.Instance.Connect(IPAddress.Parse("127.0.0.1"), ServerConnection.PORT);
            }
            catch (Exception)
            {
                // If fails to connect to a local server, do the next loop
            }

            while (!ClientConnection.Instance.IsConnected)
            {
                try
                {
                    Console.WriteLine("Please, type the server address: ");
                    string addr = Console.ReadLine();
                    if (addr != "") ClientConnection.Instance.Connect(IPAddress.Parse(addr), ServerConnection.PORT);
                }
                catch (FormatException)
                {
                    // Ignore errors for now
                }
            }
        }
    }
}
