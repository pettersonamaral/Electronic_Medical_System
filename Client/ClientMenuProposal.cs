﻿/*****************************************************************************************************
* FILE : 		Program.cs
* PROJECT : 	SQ-I Term Project 
* PROGRAMMER : 	PETTERSON AMARAL
* FIRST VERSION:2017-Oct-28

* DESCRIPTION/Requirements: 

Simply Menu interface to see if you like it guys. 

* Refence:

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using System.Linq;

namespace Client
{
   public  class ClientMenuProposal
    {
        //-> Declaration of the constants
        public const int kIterations = 100;                         //Interaction default initial value
        public const int kIterationsMin = 1;                        //Interaction default minimum value
        public const int kIterationsMax = 1000000000;               //Interaction default maximum value
        public const int kIterationsTimes = 6;                      //Interaction times default initial value
        public const int kSearchs = 9;                              //Searchs default initial value
        public const int kSearchsMin = 1;                           //Searchs default initial value
        public const int kRandomStringsMin = 1;                     //Random string default length 
        public const int kRandomStringsMax = 1000;                  //Random string default length 
        public const int kRandomStrings = 15;                       //Random string default length 


        //-> Struct declaration
        struct summary
        {
            public int key;
            public string actionType;
            public string howLongTakeIt;
            public string collectionType;
        }


        //-> Declaration of the collections
        static ArrayList al = new ArrayList();


        //-> Variable declaration
        static Int32 iterations = kIterations;                         //how much data will be used in each data structure collection
        static Int32 iterationsTimes = kIterationsTimes;               //how many times this action will happen
        static Int32 searchsTimes = kSearchs;                          //how many times this action will happen
        static Int32 randomStrLength = kRandomStrings;                 //legth of the random strings to do a test

        static List<summary> SummaryList = new List<summary>();        //Instanciate a new General Statistics Collection object
        private static Random random = new Random();                   //Instanciate a new random string creator object
        private static string[] arrayRetrive = new string[iterations]; //Instanciate a new array of strings object to do a retrive at collections
        private static string[] arrayLoadData = new string[iterations];//Instanciate a new array of random strings to load the collections
        private static string[] arraySearch = new string[searchsTimes];//Instanciate a new array of strings object to do a retrive at collections





        // =======================================================================
        //                            PUBLIC METHODS
        // =======================================================================
        /**************************************************************************************************************
        FUNCTION : HeaderMenu()
        DESCRIPTION :
            Run a Header to keep that lastest variables updated on all menus. 

        PARAMETERS :<data-type> <name> : <purpose>
            string buffer : validation according to with regex expression

        RETURN :<data-type> : <indicates>
            bool result: the value of the user input if it is ok.
        ****************************************************************************************************************/
        public static void HeaderMenu()
        {
            Console.Clear();
            Console.WriteLine("               WMP_AS03_TestMeasure - PROG2120-WINDOWS AND MOBILE PROGRAMMING");
            Console.WriteLine("-------------------------------------------------------------------------------------------");
            Console.WriteLine("Current settings ");
            Console.WriteLine(" -->Iterations      |{0,10:D}| - how much data will be used in each collection ", iterations);
            Console.WriteLine(" -->IterationsTimes |{0,10:D}| - how many times this action will happen", iterationsTimes);
            Console.WriteLine(" -->searches        |{0,10:D}| - how many searches will happen", searchsTimes);
            Console.WriteLine(" -->length Rand. Str|{0,10:D}| - how the length of the random strings", randomStrLength);
            Console.WriteLine("-------------------------------------------------------------------------------------------");
        }



        /**************************************************************************************************************
        FUNCTION : MainMenu()
        DESCRIPTION :
            Print a menu on screen and return a user choice. 

        PARAMETERS :<data-type> <name> : <purpose>
            void

        RETURN :<data-type> : <indicates>
            bool result: the value of the user input if it is ok.

            Source: https://stackoverflow.com/questions/20365214/a-simple-menu-in-a-console-application
                    http://www.dreamincode.net/forums/topic/365540-Console-Menu-with-Arrowkeys/
                    https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-numeric-format-strings
                    https://docs.microsoft.com/en-us/dotnet/api/system.consolekeyinfo?view=netframework-4.7.1
        ****************************************************************************************************************/
        public static bool MainMenu()
        {
            bool setMadeSuccess = true;
            string InvalidKeyMsg = "";
            ConsoleKeyInfo key;
            do
            {
                //Print out MainMenu
                HeaderMenu();
                Console.WriteLine("||Test Manager C# data Collections|| - MAINMENU");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("[ 1 ] Run Statistics boards");
                Console.WriteLine("[ 2 ] Change current settings");
                Console.WriteLine();
                Console.WriteLine("[ 0 ] Exit");
                Console.WriteLine();
                Console.WriteLine("Ps.: Choose a number from the menu above.");
                Console.WriteLine();
                Console.WriteLine(InvalidKeyMsg);


                key = Console.ReadKey();

                if (key.Key == ConsoleKey.NumPad1 || key.Key == ConsoleKey.D1)
                {
                    if (Statisticsboards() == true)
                    {
                        InvalidKeyMsg = "Fail to set up a new value into iterations times.\nPlease, go back to menu choice.";
                        setMadeSuccess = true;
                    }
                    else
                    {
                        InvalidKeyMsg = "";
                        setMadeSuccess = false;
                    }
                    Console.Clear();
                }
                else if (key.Key == ConsoleKey.NumPad2 || key.Key == ConsoleKey.D2)
                {
                    if (SetupVariables() == false)          // Call a subMenu function SetupVariables
                    {
                        InvalidKeyMsg = "Fail to setup test variables";
                        setMadeSuccess = false;
                    }
                    else
                    {
                        InvalidKeyMsg = "";
                        setMadeSuccess = true;
                    }
                    Console.Clear();
                }
                else if (key.Key == ConsoleKey.NumPad0 || key.Key == ConsoleKey.D0)
                {
                    InvalidKeyMsg = "";
                    return setMadeSuccess = true;
                }
                else
                {
                    InvalidKeyMsg = key.Key.ToString() + " -> Invalid Menu input value. \nPlease, go back to menu choice.";
                    setMadeSuccess = false;
                    Console.Clear();
                }
            } while (key.Key != ConsoleKey.NumPad0 || key.Key == ConsoleKey.D0);
            return setMadeSuccess;
        }



        /**************************************************************************************************************
        FUNCTION : SetupVariables(string buffer)
        DESCRIPTION :
            Validation if the string value is numeric and length

        PARAMETERS :<data-type> <name> : <purpose>
            string buffer : validation according to with regex expression

        RETURN :<data-type> : <indicates>
            bool result: the value of the user input if it is ok.

        Source: https://stackoverflow.com/questions/29970244/how-to-validate-a-phone-number
        ****************************************************************************************************************/
        public static bool SetupVariables()
        {
            bool setMadeSuccess = false;
            string InvalidKeyMsg = "", NewIterationString = "", NewIterationsTimesString = "", NewSearchsString = "", NewRandonStringLegth = "";
            ConsoleKeyInfo key;
            do
            {
                //Print out MainMenu
                HeaderMenu();
                Console.WriteLine("||Test Manager C# data Collections|| - MainMenu > SETUP ENVIRONMENT");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("[ 1 ] Change amount of iterations");
                Console.WriteLine("[ 2 ] Change amount of iterations times");
                Console.WriteLine("[ 3 ] Change amount of searchs");
                Console.WriteLine("[ 4 ] Change length of random strings");
                Console.WriteLine();
                Console.WriteLine("[ 0 ] Go Back");
                Console.WriteLine();
                Console.WriteLine("Ps.: Choose a number from the menu above.");
                Console.WriteLine();
                Console.WriteLine(InvalidKeyMsg);

                key = Console.ReadKey();


                if (key.Key == ConsoleKey.NumPad1 || key.Key == ConsoleKey.D1) //[ 1 ] Change amount of iterations
                {
                    Console.WriteLine(" Enter a new value to iterations. (from {1} to {0})", kIterationsMax, searchsTimes);
                    NewIterationString = Console.ReadLine();


                    if (ChangeVariableValue(NewIterationString, ref iterations, searchsTimes, kIterationsMax) == false)
                    {
                        InvalidKeyMsg = "Fail to set up a new value into iterations times.\nPlease, go back to menu choice.";
                        setMadeSuccess = false;
                    }
                    else                                                                                                // Things to do if the new value is ok. 
                    {
                        InvalidKeyMsg = "";                                                                             // Clean message fail status.                                                   
                        setMadeSuccess = true;
                    }
                    Console.Clear();
                }
                else if (key.Key == ConsoleKey.NumPad2 || key.Key == ConsoleKey.D2)//[ 2 ] Change amount of iterations times
                {
                    Console.WriteLine(" Enter a new value to iterations times. (from {1} to {0})", kIterationsMax, kIterationsMin);
                    NewIterationsTimesString = Console.ReadLine();


                    if (ChangeVariableValue(NewIterationsTimesString, ref iterationsTimes, kIterationsMin, kIterationsMax) == false)
                    {
                        InvalidKeyMsg = "Fail to set up a new value into iterations times.\nPlease, go back to menu choice.";
                        setMadeSuccess = false;
                    }
                    else
                    {
                        InvalidKeyMsg = "";                                                                                // Clean message fail status. 
                        setMadeSuccess = true;
                    }
                    Console.Clear();
                }
                else if (key.Key == ConsoleKey.NumPad3 || key.Key == ConsoleKey.D3)//[ 3 ] Change amount of searchs
                {
                    Console.WriteLine(" Enter a new value to how many searchs. (from {1} to {0})", iterations, kSearchsMin);
                    NewSearchsString = Console.ReadLine();


                    if (ChangeVariableValue(NewSearchsString, ref searchsTimes, kSearchsMin, iterations) == false)
                    {
                        InvalidKeyMsg = "Fail to set up a new value into iterations times.\nPlease, go back to menu choice.";
                        setMadeSuccess = false;
                    }
                    else
                    {


                        InvalidKeyMsg = "";                                                                             // Clean message fail status. 
                        setMadeSuccess = true;
                    }
                    Console.Clear();
                }
                else if (key.Key == ConsoleKey.NumPad4 || key.Key == ConsoleKey.D4)//[ 4 ] Change length of random strings
                {
                    Console.WriteLine(" Enter a new value to how long the random strings have to be. (from 1 to {0})", kRandomStringsMax);
                    NewRandonStringLegth = Console.ReadLine();


                    if (ChangeVariableValue(NewRandonStringLegth, ref randomStrLength, kRandomStringsMin, kRandomStringsMax) == false)
                    {
                        InvalidKeyMsg = "Fail to set up a new value into iterations times.\nPlease, go back to menu choice.";
                        setMadeSuccess = false;
                    }
                    else
                    {

                        InvalidKeyMsg = "";                                                                             // Clean message fail status. 
                        setMadeSuccess = true;
                    }
                    Console.Clear();
                }
                else if (key.Key == ConsoleKey.NumPad0 || key.Key == ConsoleKey.D0)
                {
                    InvalidKeyMsg = "";
                    setMadeSuccess = true;
                    Console.Clear();
                }
                else
                {
                    InvalidKeyMsg = key.Key.ToString() + " -> Invalid Menu input value. \nPlease, go back to menu choice.";
                    Console.Clear();
                    setMadeSuccess = false;
                }
            } while (key.Key != ConsoleKey.NumPad0 || key.Key == ConsoleKey.D0);
            return setMadeSuccess;
        }



        /**************************************************************************************************************
        FUNCTION : Statisticsboards()
        DESCRIPTION :
            Run the statistics of the system after call some methods. 

        PARAMETERS :<data-type> <name> : <purpose>
            string buffer : validation according to with regex expression

        RETURN :<data-type> : <indicates>
            bool result: the value of the user input if it is ok.
        ****************************************************************************************************************/
        public static bool Statisticsboards()
        {
            bool setMadeSuccess = false;
            int i = 0, j = 0;
            Console.Clear();

            HeaderMenu();
            Console.WriteLine("MainMenu > STATISTICS BOARD");
            Console.WriteLine();
            Console.WriteLine("||Test Manager C# data Collections||");

            Console.WriteLine("Search                           Retrieve");

            //Adding sample data
            SummaryList.Add(new summary() { key = 1, actionType = "Retrieve", howLongTakeIt = "454545454", collectionType = "Dictionary" });
            SummaryList.Add(new summary() { key = 2, actionType = "Retrieve", howLongTakeIt = "656565656", collectionType = "Dictionary" });
            SummaryList.Add(new summary() { key = 3, actionType = "Search", howLongTakeIt = "878787878", collectionType = "Dictionary" });
            SummaryList.Add(new summary() { key = 4, actionType = "Search", howLongTakeIt = "252252525", collectionType = "Dictionary" });


            // Splitting a value from a list in two columns
            while ((i < SummaryList.Count) && (j < SummaryList.Count))
            {
                if (SummaryList[i].actionType == "Search" && SummaryList[j].actionType == "Retrieve")
                {
                    Console.Write(" -> {0,-10} {1,15}  |", SummaryList[i].collectionType, SummaryList[i].howLongTakeIt);
                    Console.Write(" -> {0,-10} {1,15}   \n", SummaryList[j].collectionType, SummaryList[j].howLongTakeIt);
                    i++;
                    j++;
                }
                else
                {
                    if (SummaryList[i].actionType != "Search")
                    {
                        i++;
                    }
                    if (SummaryList[j].actionType != "Retrieve")
                    {
                        j++;
                    }
                }
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();

            return setMadeSuccess;
        }



        /**************************************************************************************************************
        FUNCTION : ChangeVariableValue(string stringNewValue, ref int updateThisInt, int minEdge, int maxEdge)
        DESCRIPTION :
           Validation if the string value is numeric and length 

        PARAMETERS :<data-type> <name> : <purpose>
           string stringNewValue,   string to be validated according to with regex expression
           ref int updateThisInt,   here is the value that this funcition will change.
           int minEdge,             value max to bounders test
           int maxEdge,             value max to bounders test          

        RETURN :<data-type> : <indicates>
           bool setMadeSuccess : True if the string match the Regex expression.

           Source: https://stackoverflow.com/questions/29970244/how-to-validate-a-phone-number
        ****************************************************************************************************************/
        public static bool ChangeVariableValue(string stringNewValue, ref int updateThisInt, int minEdge, int maxEdge)
        {
            bool setMadeSuccess = false;
            Int32 NewIterationsTimesInt = updateThisInt;

            if (RegexMenuInputValidation(stringNewValue) == true)
            {
                try
                {
                    NewIterationsTimesInt = Int32.Parse(stringNewValue);                                //Try to do a parse from string to int 
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);                                                       // Write a error message if I get an exception
                    return setMadeSuccess = false;
                }

                if (TestValueRange(minEdge, maxEdge, NewIterationsTimesInt) == true)                    // check the boundary
                {
                    updateThisInt = NewIterationsTimesInt;                                              //Update global Variable
                    setMadeSuccess = true;
                }
                else setMadeSuccess = false;                                                            //Error message if boundary test fail 
            }
            else setMadeSuccess = false;

            return setMadeSuccess;
        }



        /**************************************************************************************************************
         FUNCTION : RegexUtilitiesManuInputValidade(string buffer)
         DESCRIPTION :
            Validation if the string value is numeric and length

         PARAMETERS :<data-type> <name> : <purpose>
            string buffer : validation according to with regex expression

         RETURN :<data-type> : <indicates>
            bool setMadeSuccess : True if the string match the Regex expression.

            Source: https://stackoverflow.com/questions/29970244/how-to-validate-a-phone-number
                    https://stackoverflow.com/questions/3968049/regex-allow-a-string-to-only-contain-numbers-0-9-and-limit-length-to-45
         ****************************************************************************************************************/
        public static bool RegexMenuInputValidation(string buffer)
        {
            bool setMadeSuccess = false;

            if (Regex.Match(buffer, pattern: @"^\d{1,10}$").Success) //^means at start,     \d means the same of [0-9],     {1,10} means from 1 to 10 length
            {
                setMadeSuccess = true;
            }
            return setMadeSuccess;
        }



        /**************************************************************************************************************
        FUNCTION : LoadArrayOfStrings()
        DESCRIPTION :
           Load the array(arrayLoadData) with specific string size to test the collections

        PARAMETERS :<data-type> <name> : <purpose>
           void

        RETURN :<data-type> : <indicates>
           void
        ****************************************************************************************************************/
        public static void LoadArrayOfStrings()
        {
            Array.Clear(arrayLoadData, 0, arrayLoadData.Length);//Make sure that arrayRetrieve is clear before start to load it
            for (int i = 0; i < iterations; i++)
            {
                arrayLoadData[i] = RandonStringGenerator(randomStrLength);
            }
        }



        /**************************************************************************************************************
        FUNCTION : LoadArrayOfSearch()
        DESCRIPTION :
           Load the array of searchs with random string from arrayLoadData

        PARAMETERS :<data-type> <name> : <purpose>
           void

        RETURN :<data-type> : <indicates>
           void
        ****************************************************************************************************************/
        public static void LoadArrayOfSearch()
        {
            for (int i = 0; i < searchsTimes; i++)
            {
                arraySearch[i] = arrayLoadData[random.Next(0, iterations)];
            }
        }



        /**************************************************************************************************************
        FUNCTION : TestValueRange(Int64 min, Int64 max, Int64 theValue)
        DESCRIPTION :
           Validation if a numeric value is between a known range.

        PARAMETERS :<data-type> <name> : <purpose>
           Int64 min, minimum value of the range
           Int64 max, maximum value of the range
           Int64 theValue, Value that have to be compared

        RETURN :<data-type> : <indicates>
           bool setMadeSuccess : True if the value is in the range. 
        ****************************************************************************************************************/
        public static bool TestValueRange(Int64 min, Int64 max, Int64 theValue)
        {
            bool setMadeSuccess = false;

            if ((theValue >= min) &&
                  (theValue <= max))
            {
                setMadeSuccess = true;
            }
            return setMadeSuccess;
        }



        /**************************************************************************************************************
        FUNCTION : RandonStringGenerator(Int32 length)
        DESCRIPTION :
            Create a random string based on a length argment.

        PARAMETERS :<data-type> <name> : <purpose>
            Int32 length: a int value with the size of the string that will be created

        RETURN :<data-type> : <indicates>
            string : a randon string with the leng received. 
            source: https://stackoverflow.com/questions/1344221/how-can-i-generate-random-alphanumeric-strings-in-c
        ****************************************************************************************************************/
        public static string RandonStringGenerator(Int32 length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
