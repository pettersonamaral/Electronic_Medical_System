﻿
/*****************************************************************************************************
* FILE : 		  Packager.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-28
* SECOND VERSION: 2017-Dec-08
* FINAL VERSION:  2018-Jan-04

* DESCRIPTION/Requirements: 

* The Packager is a supportive class used pack objects into strings and strings back into objects. Packager is used 
* only by ClientInterpreter and ServerInterpreter.
* The supported objects that can be packed and/or unpacked are:
*                                       - Patient
*                                       - Address
*                                       - Appointment
*                                       - Bill
*                                       - ServiceLine
*                                       - Service
* 
* Refence: https://github.com/AbleOpus/NetworkingSamples

*****************************************************************************************************/
using System;
using System.Collections.Generic;
using Demographics;
using Billing;

namespace Interpreter
{
    /// <summary>
    /// <b>Description: </b>The Packager is a supportive class used pack objects into 
    /// strings and strings back into objects. Packager is used only by
    /// ClientInterpreter and ServerInterpreter.
    /// The supported objects that can be packed and/or unpacked are:
    /// Patient, Address, Appointment, Bill, ServiceLine, and Service
    /// </summary>
    public static class Packager
    {
        #region Constants
        private const int MIN_OBJECTS_IN_APPOINTMENT = 3;
        private const int MAX_OBJECTS_IN_APPOINTMENT = 5;
        #endregion

        #region Tags

        // Constants
        private const string div = ";";
        private const string TagObject = "OBJECT" + div;
        private const string TagPatient = "Patient" + div;
        private const string TagAddress = "Address" + div;
        private const string TagAppointment = "Appointment" + div;
        private const string TagBill = "Bill" + div;
        private const string TagServiceList = "ServiceLine" + div;
        private const string TagService = "Service" + div;

        #endregion

        #region ErrorCodes

        // Exceptions - 2500 - 2599
        public const int Unprocessed = -2501;
        public const int Format = -2502;
        public const int Overflow = -2503;
        public const int Argument = -2504;

        #endregion

        #region Separators

        // Separators
        private static string[] ObjectSeparator = { TagObject };
        private static string[] FieldSeparator = { div };

        #endregion

        #region Properties
        public static string Div
        { get => div; }
        #endregion

        // =======================================================================
        //                              DEMOGRAPHICS
        // =======================================================================

        #region Patient

        /// <summary>
        /// Creates a string version of a Patient object
        /// </summary>
        /// <seealso cref="UnpackPatient(string)"/>
        /// <param name="obj">The object Patient</param>
        /// <returns>A string version of a Patient</returns>
        public static string PackPatient(Patient obj)
        {
            if (obj == null) { obj = new Patient(); }

            string pack = TagObject;
            pack += TagPatient;

            // Fill the info
            pack += obj.PatientID.ToString() + div;
            pack += obj.GethCN() + div;
            pack += obj.GetLname() + div;
            pack += obj.GetFname() + div;
            pack += obj.GetMinitial() + div;
            pack += obj.DateOfBirth.ToString() + div;
            pack += obj.GetSex() + div;
            pack += obj.GetHeadOfHouse() + div;

            pack += PackAddress(obj.Address);

            return pack;
        }


        /// <summary>
        /// Creates a Patient object by parsing a string
        /// </summary>
        /// <seealso cref="PackPatient(Patient)"/>
        /// <param name="message">Patients information</param>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        public static Patient UnpackPatient(string message)
        {
            Patient obj = null;

            string[] objs = SplitObjects(message);

            if (objs.Length == 2)
            {
                obj = new Patient();

                string[] fields = objs[0].Split(FieldSeparator, StringSplitOptions.None);

                if (fields.Length == 11) // ObjTag + PatientTag + 8 fields + ""
                {
                    if ((fields[1] + div) == TagPatient)
                    {
                        // patientID
                        obj.PatientID = GetInteger(fields[2]);

                        // HCN
                        obj.SethCN(fields[3]);

                        // lastName
                        obj.SetLname(fields[4]);

                        // firstName
                        obj.SetFname(fields[5]);

                        // mInitials
                        obj.SetMinitial(GetChar(fields[6]));

                        // dateOfBirth
                        obj.DateOfBirth = GetDateTime(fields[7]);

                        // sex
                        obj.SetSex(fields[8]);

                        // headOfHouse
                        obj.SetHeadOfHouse(fields[9]);
                    }
                }

                // address
                obj.Address = UnpackAddress(objs[1]);
            }


            return obj;
        }

        #endregion

        #region Address

        /// <summary>
        /// Creates a string version of an Address object
        /// </summary>
        /// <seealso cref="UnpackAddress(string)"/>
        /// <param name="obj">The object Address</param>
        /// <returns>A string version of a Address</returns>
        public static string PackAddress(Address obj)
        {
            if (obj == null) { obj = new Address(); }

            string pack = TagObject;
            pack += TagAddress;

            // Fill the info
            pack += obj.GetAdressID().ToString() + div;
            pack += obj.GetAddressLineOne() + div;
            pack += obj.GetAddressLineTwo() + div;
            pack += obj.GetCity() + div;
            pack += obj.GetProvince() + div;
            pack += obj.GetPhoneNumber() + div;

            return pack;
        }


        /// <summary>
        /// Creates an Address object by parsing a string
        /// </summary>
        /// <seealso cref="PackAddress(Address)"/>
        /// <param name="message">Address information</param>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        public static Address UnpackAddress(string message)
        {
            Address obj = null;
            string[] parsed = message.Split(FieldSeparator, StringSplitOptions.None);

            if (parsed.Length == 9) // ObjTag + AddressTag + 6 fields + ""
            {
                if ((parsed[1] + div) == TagAddress)
                {
                    obj = new Address();
                    // addressID
                    obj.SetAdressID(GetInteger(parsed[2]));

                    // addressLineOne
                    obj.SetAdressLineOne(parsed[3]);

                    // addressLineTwo
                    obj.SetAdressLineTwo(parsed[4]);

                    // city
                    obj.SetCity(parsed[5]);

                    // province
                    obj.SetProvince(parsed[6]);

                    // phone
                    obj.SetPhoneNumber(parsed[7]);
                }
            }

            return obj;
        }

        #endregion

        #region Appointment

        /// <summary>
        ///  Creates a string version of an Appointment object
        /// </summary>
        /// <seealso cref="UnpackAppointment(string)"/>
        /// <param name="obj">The object Appointment</param>
        /// <returns>A string version of a Appointment</returns>
        public static string PackAppointment(Appointment obj)
        {
            if (obj == null) { obj = new Appointment(); }

            string pack = TagObject;
            pack += TagAppointment;

            // Fill the info
            pack += obj.GetAppointmentID().ToString() + div;
            pack += obj.GetDayAndTime().ToString() + div;

            pack += PackPatient(obj.FirstPatient);
            if (obj.SecondPatient != null)
            {
                pack += PackPatient(obj.SecondPatient);
            }

            return pack;
        }


        /// <summary>
        /// Creates an Appointment object by parsing a string
        /// </summary>
        /// <seealso cref="PackAppointment(Appointment)"/>
        /// <param name="message">Appointment information</param>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        public static Appointment UnpackAppointment(string message)
        {
            Appointment obj = null;
            string[] objs = SplitObjects(message);

            if (objs.Length >= MIN_OBJECTS_IN_APPOINTMENT)
            {
                obj = new Appointment();

                string[] fields = objs[0].Split(FieldSeparator, StringSplitOptions.None);

                if (fields.Length == 5) // ObjTag + AppointmentTag + 2 fields + ""
                {
                    if ((fields[1] + div) == TagAppointment)
                    {
                        // appointmentID
                        obj.SetApointmentID(GetInteger(fields[2]));

                        // dateTime
                        obj.SetDayAndTime(GetDateTime(fields[3]));
                    }
                }

                obj.FirstPatient = UnpackPatient(objs[1] + objs[2]);

                if (objs.Length == MAX_OBJECTS_IN_APPOINTMENT)
                {
                    obj.SecondPatient = UnpackPatient(objs[3] + objs[4]);
                }
            }

            return obj;
        }

        #endregion

        #region Lists

        public static string PackListOfPatients(List<Patient> list)
        {
            string output = "";
            foreach (Patient p in list)
            {
                output += PackPatient(p);
            }
            return output;
        }

        public static List<Patient> UnpackListOfPatients(string message)
        {
            List<Patient> retCode = new List<Patient>();

            string[] objs = SplitObjects(message);

            for (int i = 0; i < objs.Length; i += 2)
            {
                string parameter = objs[i] + objs[i + 1];
                Patient patient = UnpackPatient(parameter);
                if (patient != null)
                {
                    retCode.Add(patient);
                }
            }

            return retCode;
        }

        public static string PackListOfAppointments(List<Appointment> list)
        {
            string output = "";
            foreach (Appointment a in list)
            {
                output += PackAppointment(a);
            }
            return output;
        }

        public static List<Appointment> UnpackListOfAppointments(string message)
        {
            List<Appointment> retCode = new List<Appointment>();

            string[] objs = SplitAppointmentObjects(message);

            foreach (string obj in objs)
            {
                Appointment app = UnpackAppointment(obj);
                if (app != null) { retCode.Add(app); }
            }

            return retCode;
        }

        #endregion

        // =======================================================================
        //                                BILLING
        // =======================================================================

        #region Bill

        /// <summary>
        /// Creates a string version of a Bill object
        /// </summary>
        /// <seealso cref="UnpackBill(string)"/>
        /// <param name="obj">The object Bill</param>
        /// <returns>A string version of a Bill</returns>
        public static string PackBill(Bill obj)
        {
            if (obj == null) { obj = new Bill(); }

            string pack = TagObject;
            pack += TagBill;

            // Fill the info
            pack += obj.GetBillID().ToString() + div;
            pack += obj.GetFlagRecall().ToString() + div;
            pack += obj.StatusDone.ToString() + div;

            pack += PackPatient(obj.Patient);
            pack += PackAppointment(obj.Appointment);

            pack += PackListOfServiceLine(obj.Services);

            return pack;
        }


        /// <summary>
        /// Creates a Bill object by parsing a string
        /// </summary>
        /// <seealso cref="PackBill(Bill)"/>
        /// <param name="message">Bill information</param>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        public static Bill UnpackBill(string message)
        {
            Bill obj = null;

            string[] objs = SplitObjects(message);
            int expectedLength = 6;

            if (objs.Length >= expectedLength)
            {
                obj = new Bill();

                string[] fields = SplitFields(objs[0]);

                if (fields.Length == 6) // ObjTag + BillTag + 3 fields + ""
                {
                    if ((fields[1] + div) == TagBill)
                    {
                        // billID
                        obj.SetBillID(GetInteger(fields[2]));

                        // flagRecall
                        obj.SetFlagRecall(GetInteger(fields[3]));

                        // statusDone
                        obj.StatusDone = GetBoolean(fields[4]);
                    }
                }

                obj.Patient = UnpackPatient(objs[1] + objs[2]);

                // Find Appointment
                string appointment = objs[3];
                int servicesIndex = 4;

                if (SplitFields(objs[4])[1] + div == TagPatient)
                {
                    servicesIndex += 2;
                    appointment += objs[4] + objs[5];
                    if (objs.Length > expectedLength && SplitFields(objs[6])[1] + div == TagPatient)
                    {
                        servicesIndex += 2;
                        appointment += objs[6] + objs[7];
                    }
                }

                obj.Appointment = UnpackAppointment(appointment);

                if (objs.Length > servicesIndex)
                {
                    for (int i = servicesIndex; i < objs.Length; i += 2)
                    {
                        obj.Services.Add(UnpackServiceList(objs[i] + objs[i + 1]));
                    }
                }
            }

            return obj;
        }

        #endregion

        #region ServiceList

        /// <summary>
        /// Creates a string version of a ServiceList object
        /// </summary>
        /// <seealso cref="UnpackServiceList(string)"/>
        /// <param name="obj">The object ServiceList</param>
        /// <returns>A string version of a ServiceList</returns>
        public static string PackServiceList(ServiceList obj)
        {
            if (obj == null) { obj = new ServiceList(); }

            string pack = TagObject;
            pack += TagServiceList;

            // Fill the info
            pack += obj.GetQuantity().ToString() + div;
            pack += obj.GetStatus() + div;

            pack += PackService(obj.Service);

            return pack;
        }


        /// <summary>
        /// Creates a ServiceList object by parsing a string
        /// </summary>
        /// <seealso cref="PackServiceList(ServiceList)"/>
        /// <param name="message">ServiceList information</param>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        public static ServiceList UnpackServiceList(string message)
        {
            ServiceList obj = null;
            string[] objs = SplitObjects(message);

            if (objs.Length == 2)
            {
                obj = new ServiceList();

                string[] fields = objs[0].Split(FieldSeparator, StringSplitOptions.None);

                if (fields.Length == 5) // ObjTag + ServiceListTag + 2 fields + ""
                {
                    if ((fields[1] + div) == TagServiceList)
                    {
                        // quantity
                        obj.SetQuantity(GetInteger(fields[2]));

                        // status
                        obj.SetStatus(fields[3]);
                    }
                }

                obj.Service = UnpackService(objs[1]);
            }


            return obj;
        }

        #endregion

        #region Service

        /// <summary>
        /// Creates a string version of a Service object
        /// </summary>
        /// <seealso cref="UnpackService(string)"/>
        /// <param name="obj">The object Service</param>
        /// <returns>A string version of a Service</returns>
        public static string PackService(Service obj)
        {
            if (obj == null) { obj = new Service(); }

            string pack = TagObject;
            pack += TagService;

            // Fill the info
            pack += obj.GetServiceID().ToString() + div;
            pack += obj.GetFeeCode() + div;
            pack += obj.EffectiveDate.ToString() + div;
            pack += obj.GetServiceValue().ToString() + div;

            return pack;
        }


        /// <summary>
        /// Creates a Service object by parsing a string
        /// </summary>
        /// <seealso cref="PackService(Service)"/>
        /// <param name="message">Service information</param>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        public static Service UnpackService(string message)
        {
            Service obj = null;
            string[] fields = message.Split(FieldSeparator, StringSplitOptions.None);

            if (fields.Length == 7) // ObjTag + ServiceTag + 3 fields + ""
            {
                if ((fields[1] + div) == TagService)
                {
                    // new Service()
                    obj = new Service();
                    // serviceID
                    obj.SetServiceID(GetInteger(fields[2]));
                    // fee code
                    obj.SetFeeCode(fields[3]);
                    // effective date
                    obj.EffectiveDate = GetDateTime(fields[4]);
                    // value
                    obj.SetServiceValue(GetDouble(fields[5]));
                }
            }

            return obj;
        }

        #endregion

        #region Lists

        public static string PackListOfBills(List<Bill> list)
        {
            string output = "";
            foreach (Bill b in list)
            {
                output += PackBill(b);
            }
            return output;
        }

        public static List<Bill> UnpackListOfBills(string message)
        {
            List<Bill> retCode = new List<Bill>();

            string[] objs = SplitBillObjects(message);

            foreach (string obj in objs)
            {
                Bill bill = UnpackBill(obj);
                if (bill != null) { retCode.Add(bill); }
            }

            return retCode;
        }

        public static string PackListOfServiceLine(List<ServiceList> list)
        {
            string output = "";
            foreach (ServiceList sl in list)
            {
                output += PackServiceList(sl);
            }
            return output;
        }

        public static List<ServiceList> UnpackListOfServiceLine(string message)
        {
            List<ServiceList> retCode = new List<ServiceList>();

            string[] objs = SplitObjects(message);

            for (int i = 0; i < objs.Length; i += 2)
            {
                ServiceList slist = UnpackServiceList(objs[i] + objs[i + 1]);
                if (slist != null) { retCode.Add(slist); }
            }

            return retCode;
        }

        public static string PackListOfServices(List<Service> list)
        {
            string output = "";
            foreach (Service s in list)
            {
                output += PackService(s);
            }
            return output;
        }

        public static List<Service> UnpackListOfServices(string message)
        {
            List<Service> retCode = new List<Service>();

            string[] objs = SplitObjects(message);

            foreach (string obj in objs)
            {
                Service service = UnpackService(obj);
                if (service != null) { retCode.Add(service); }
            }

            return retCode;
        }

        #endregion

        // =======================================================================
        //                               OTHER TOOLS
        // =======================================================================

        #region Other Tools

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string[] SplitFields(string message)
        {
            return message.Split(FieldSeparator, StringSplitOptions.None);
        }


        /// <summary>
        /// This method assists the interfaces to split objects in the same string into
        /// an array of strings. Each string in the array will contain one of the objects
        /// </summary>
        /// <param name="message">An string with more than one object</param>
        /// <returns></returns>
        private static string[] SplitObjects(string message)
        {
            // expected input: OBJECT;Bill;;;;OBJECT;Bill;;;;OBJECT;Bill;;;;OBJECT;Bill;;;;
            string[] retCode = message.Split(ObjectSeparator, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < retCode.Length; i++)
            {
                retCode[i] = TagObject + retCode[i];
            }

            return retCode;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string[] SplitBillObjects(string message)
        {
            // expected input: OBJECT;Bill;;;;OBJECT;Bill;;;;OBJECT;Bill;;;;OBJECT;Bill;;;;
            string divisor = TagObject + TagBill;

            string[] retCode = message.Split(new string[] { divisor }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < retCode.Length; i++)
            {
                retCode[i] = divisor + retCode[i];
            }

            return retCode;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string[] SplitAppointmentObjects(string message)
        {
            // expected input: OBJECT;Bill;;;;OBJECT;Bill;;;;OBJECT;Bill;;;;OBJECT;Bill;;;;
            string divisor = TagObject + TagAppointment;

            string[] retCode = message.Split(new string[] { divisor }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < retCode.Length; i++)
            {
                retCode[i] = divisor + retCode[i];
            }

            return retCode;
        }


        /// <summary>
        /// Converts a string to an int
        /// </summary>
        /// <param name="number">A number in a string</param>
        /// <returns>The number or an error code</returns>
        public static int GetInteger(string number)
        {
            int errCode;

            try
            {
                errCode = Int32.Parse(number);
            }
            catch (FormatException) { errCode = Format; }
            catch (OverflowException) { errCode = Overflow; }

            return errCode;
        }


        /// <summary>
        /// Tries to parse a string into a double
        /// </summary>
        /// <param name="number">A number in a string</param>
        /// <returns>The number or an error code</returns>
        public static double GetDouble(string number)
        {
            double retCode;

            try
            {
                retCode = Double.Parse(number);
            }
            catch (ArgumentNullException) { retCode = Argument; }
            catch (FormatException) { retCode = Format; }
            catch (OverflowException) { retCode = Overflow; }

            return retCode;
        }


        /// <summary>
        /// Tries to parse a string into a DateTime object
        /// </summary>
        /// <param name="dateAndTime">String representing a date and time</param>
        /// <returns>The proper value or DateTime.MinValue for an invalid string</returns>
        public static DateTime GetDateTime(string dateAndTime)
        {
            DateTime retCode;
            try
            {
                retCode = DateTime.Parse(dateAndTime);
            }
            catch (ArgumentNullException) { retCode = DateTime.MinValue; }
            catch (FormatException) { retCode = DateTime.MinValue; }
            catch (ArgumentException) { retCode = DateTime.MinValue; }

            return retCode;
        }


        /// <summary>
        /// Tries to parse a string into a boolean. Change this later
        /// </summary>
        /// <param name="boolean">String representing </param>
        /// <returns>the boolean or false by default</returns>
        public static bool GetBoolean(string boolean)
        {
            bool retCode = false;

            try
            {
                retCode = Boolean.Parse(boolean);
            }
            catch (ArgumentNullException) { retCode = false; }
            catch (FormatException) { retCode = false; }

            return retCode;
        }


        /// <summary>
        /// Tries to parse a string into a boolean
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static char GetChar(string input)
        {
            char retCode = ' ';

            if (input.Length == 1)
            {
                retCode = input[0];
            }

            return retCode;
        }


        /// <summary>
        /// Tries to parse a string into a list of string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> GetList(string input)
        {
            List<string> result = new List<string>();

            string[] split = input.Split(FieldSeparator, StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in split)
            {
                result.Add(s);
            }

            return result;
        }

        #endregion

    }
}
