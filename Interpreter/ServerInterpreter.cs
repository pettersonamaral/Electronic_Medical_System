﻿
/*****************************************************************************************************
* FILE : 		  ServerInterpreter.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-28
* SECOND VERSION: 2017-Nov-08
* FINAL VERSION:  2018-Jan-05

* DESCRIPTION/Requirements: 

* This The ServerInterpreter is a supportive class used to make the interface between a client application that 
* contains a ClientInterpreter and a server application that instantiates a ServerConnection. ServerInterpreter 
* receives requests from the ServerConnection and performs the appropriate action within the server application 
* through the Tracker classes of Demographics and Billing libraries.
* Refence:

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using Demographics;
using Billing;
using Connection;

namespace Interpreter
{
    /// <summary>
    /// <b>Description: </b>The ServerInterpreter is a supportive class used to make the interface
    /// between a client application that contains a ClientInterpreter and
    /// a server application that instantiates a ServerConnection.
    /// ServerInterpreter receives requests from the ServerConnection and performs the 
    /// appropriate action within the server application through the Tracker classes
    /// of Demographics and Billing libraries.
    /// </summary>
    public sealed class ServerInterpreter
    {
        #region References

        // Singleton Related
        private static ServerInterpreter instance;
        private static readonly object padlock = new object();

        // Trackers
        private IDemographics demoTracker = BusinessTracker.BusinessTracker.Instance;
        private IBilling billTracker = BusinessTracker.BusinessTracker.Instance;

        #endregion

        #region Actions

        private static string div = Packager.Div;
        private static string[] FieldSeparator = { Packager.Div };

        #region List of actions - Demographics
        // List of actions - Demographics
        public static readonly string AddPatient = "AddPatient" + div;
        public static readonly string AddAppointment = "AddAppointment" + div;
        public static readonly string FindPatientStr = "FindPatientStr" + div;
        public static readonly string FindPatientObj = "FindPatientObj" + div;
        public static readonly string FindPatientInt = "FindPatientInt" + div;
        public static readonly string FindAddressInt = "FindAddressInt" + div;
        public static readonly string FindAddressStr = "FindAddressStr" + div;
        public static readonly string FindAppointment = "FindAppointment" + div;
        public static readonly string FindAppointmentsInt = "FindAppointmentsInt" + div;
        public static readonly string FindAppointmentsDate = "FindAppointmentsDate" + div;
        public static readonly string UpdatePatient = "UpdatePatient" + div;
        public static readonly string UpdateAddress = "UpdateAddress" + div;
        public static readonly string UpdateAppointment = "UpdateAppointment" + div;

        #endregion

        #region List of actions - Bill
        // List of actions - Bill
        public static readonly string AddBill = "AddBill" + div;
        public static readonly string AddServiceList = "AddServiceList" + div;
        public static readonly string FindBill = "FindBill" + div;
        public static readonly string FindServices = "FindServices" + div;
        public static readonly string FindService = "FindService" + div;
        public static readonly string UpdateBill = "UpdateBill" + div;
        public static readonly string UpdateServiceList = "UpdateServiceList" + div;
        public static readonly string MonthlyBillingSummary = "MonthlyBillingSummary" + div;
        public static readonly string MonthlyBillingCSV = "MonthlyBillingCSV" + div;
        public static readonly string MonthlyReconcileCSV = "MonthlyReconcileCSV" + div;

        #endregion

        // Separators
        private static string[] ActionSeparator = {
            // Demographics
            AddPatient, AddAppointment, FindPatientStr, FindPatientObj, FindPatientInt, FindAddressInt, FindAddressStr,
            FindAppointment, FindAppointmentsInt, FindAppointmentsDate, UpdatePatient, UpdateAddress, UpdateAppointment,
            // Billing 
            AddBill, AddServiceList, FindBill, FindServices, FindService, UpdateBill, UpdateServiceList,
            MonthlyBillingSummary, MonthlyBillingCSV, MonthlyReconcileCSV };

        #endregion

        #region Exceptions and Error Codes

        // Result Exceptions
        public const string NullResponse = "null";
        public const string NoArgument = "NoArg";
        public const string ExceptionTag = "Exception";

        // List of errors - 2400 - 2499
        public const int EmptyRequest = -2401;
        public const int InvalidAction = -2402;
        public const int InvalidArgument = -2403;
        public const int NotImplemented = -2404;

        #endregion

        #region Constructor

        // =======================================================================
        //                              CONSTRUCTOR
        // =======================================================================

        /// <summary>
        /// ServerInterpreter constructor. This is the only constructor for this class.
        /// ServerInterpreter requires to be connected to both trackers from Demographics
        /// library and Billing Library to work properly.
        /// </summary>
        private ServerInterpreter()
        { }

        #endregion

        #region Properties

        /// <summary>
        /// This property is the interface to the instance of this class.
        /// Whenever the user wants to use this class, he or she has to call 
        /// the Instance property. This assures that there's only one instance 
        /// of ServerInterpreter in the application.
        /// </summary>
        public static ServerInterpreter Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ServerInterpreter();
                    }
                    return instance;
                }
            }
        }

        #endregion

        #region Public Methods

        // =======================================================================
        //                            PUBLIC METHODS
        // =======================================================================

        /// <summary>
        /// This method interprets the message from the ServerConnection
        /// and provides an answer.
        /// </summary>
        /// <param name="message">The message received from the client</param>
        /// <returns>An answer related to the message request</returns>
        public string InterpretMessage(string message)
        {
            string answer = "ERROR";

            try
            {
                if (message.Length == 0)
                {
                    answer = EmptyRequest.ToString();
                }
                else
                {
                    ParseRequest(message, out string action, out string argument);

                    // check if the argument still empty
                    if (argument == "")
                    {
                        answer = InvalidArgument.ToString();
                    }

                    // Check which action is being requested
                    // ================== DEMOGRAPHICS ==================
                    else if (action == AddPatient)
                    {
                        Patient obj = Packager.UnpackPatient(argument);
                        if (obj == null) { answer = InvalidArgument.ToString(); }
                        else { answer = demoTracker.AddPatient(obj).ToString(); }
                    }

                    else if (action == AddAppointment)
                    {
                        Appointment obj = Packager.UnpackAppointment(argument);
                        if (obj == null) { answer = InvalidArgument.ToString(); }
                        else { answer = demoTracker.AddAppointment(obj).ToString(); }
                    }

                    else if (action == FindPatientStr)
                    {
                        Patient obj = demoTracker.FindPatient(argument);
                        if (obj == null) { answer = NullResponse; }
                        else { answer = Packager.PackPatient(obj); }
                    }

                    else if (action == FindPatientObj)
                    {
                        Patient obj = Packager.UnpackPatient(argument);
                        if (obj == null)
                        {
                            answer = InvalidArgument.ToString();
                        }
                        else // argument is a valid patient
                        {
                            List<Patient> foundList = demoTracker.FindPatient(obj);
                            if (foundList == null || foundList.Count == 0)
                            {
                                answer = NullResponse;
                            }
                            else
                            {
                                answer = "";
                                foreach (Patient patient in foundList)
                                {
                                    answer += Packager.PackPatient(patient);
                                }
                            }
                        }
                    }

                    else if (action == FindPatientInt)
                    {
                        Patient result = demoTracker.FindPatient(Packager.GetInteger(argument));
                        if (result == null) { answer = NullResponse; }
                        else { answer = Packager.PackPatient(result); }
                    }

                    else if (action == FindAddressStr)
                    {
                        Address result = demoTracker.FindAddress(argument);
                        if (result == null) { answer = NullResponse; }
                        else { answer = Packager.PackAddress(result); }
                    }

                    else if (action == FindAppointment)
                    {
                        int appID = Packager.GetInteger(argument);
                        Appointment obj = demoTracker.FindAppointment(appID);
                        if (obj == null) { answer = NullResponse; }
                        else { answer = Packager.PackAppointment(obj); }
                    }

                    else if (action == FindAppointmentsInt)
                    {
                        List<Appointment> foundList = demoTracker.FindAppointments(Packager.GetInteger(argument));
                        if (foundList == null || foundList.Count == 0)
                        {
                            answer = NullResponse;
                        }
                        else
                        {
                            answer = "";
                            foreach (Appointment app in foundList)
                            {
                                answer += Packager.PackAppointment(app);
                            }
                        }
                    }

                    else if (action == FindAppointmentsDate)
                    {
                        string[] timeList = argument.Split(FieldSeparator, StringSplitOptions.RemoveEmptyEntries);

                        if ((timeList.Length == 2) &&
                            (DateTime.TryParse(timeList[0], out DateTime start)) &&
                            (DateTime.TryParse(timeList[1], out DateTime end)))
                        {
                            List<Appointment> foundList = demoTracker.FindAppointments(start, end);
                            if (foundList == null || foundList.Count == 0)
                            {
                                answer = NullResponse;
                            }
                            else
                            {
                                answer = "";
                                foreach (Appointment app in foundList)
                                {
                                    answer += Packager.PackAppointment(app);
                                }
                            }
                        }
                        else
                        {
                            answer = InvalidArgument.ToString();
                        }
                    }

                    else if (action == UpdatePatient)
                    {
                        Patient obj = Packager.UnpackPatient(argument);
                        answer = demoTracker.UpdatePatient(obj).ToString();
                    }

                    else if (action == UpdateAddress)
                    {
                        Address obj = Packager.UnpackAddress(argument);
                        answer = demoTracker.UpdateAddress(obj).ToString();
                    }

                    else if (action == UpdateAppointment)
                    {
                        Appointment obj = Packager.UnpackAppointment(argument);
                        answer = demoTracker.UpdateAppointment(obj).ToString();
                    }

                    // ===================== BILLING ====================
                    else if (action == AddBill)
                    {
                        Bill obj = Packager.UnpackBill(argument);
                        if (obj == null) { answer = InvalidArgument.ToString(); }
                        else { answer = billTracker.AddBill(obj).ToString(); }
                    }

                    else if (action == AddServiceList)
                    {
                        int billId = Packager.GetInteger(SplitArguments(argument, 0));
                        ServiceList obj = Packager.UnpackServiceList(SplitArguments(argument, 1));
                        if (obj == null) { answer = InvalidArgument.ToString(); }
                        else { answer = billTracker.AddServiceList(billId, obj).ToString(); }

                    }

                    else if (action == FindBill)
                    {
                        string[] integers = argument.Split(FieldSeparator, StringSplitOptions.RemoveEmptyEntries);
                        if (integers.Length != 2)
                        {
                            answer = InvalidArgument.ToString();
                        }
                        else
                        {
                            int argPatientID = Packager.GetInteger(integers[0]);
                            int argAppID = Packager.GetInteger(integers[1]);
                            Bill obj = billTracker.FindBill(argPatientID, argAppID);
                            if (obj == null || obj.GetBillID() == 0)
                            {
                                answer = NullResponse;
                            }
                            else
                            {
                                answer = Packager.PackBill(obj);
                            }
                        }
                    }

                    else if (action == FindServices)
                    {
                        if (argument == NoArgument) { argument = ""; }
                        List<Service> services = billTracker.FindServices(argument);
                        answer = "";
                        foreach (Service s in services)
                        {
                            answer += Packager.PackService(s);
                        }
                        if (answer == "")
                        {
                            answer = NullResponse;
                        }
                    }

                    else if (action == FindService)
                    {
                        Service obj = billTracker.FindService(Packager.GetInteger(argument));
                        answer = Packager.PackService(obj);
                        if (answer == "") answer = NullResponse;
                    }

                    else if (action == UpdateBill)
                    {
                        Bill obj = Packager.UnpackBill(argument);
                        answer = billTracker.UpdateBill(obj).ToString();
                        if (answer == "") answer = NotImplemented.ToString();
                    }

                    else if (action == UpdateServiceList)
                    {
                        int billId = Packager.GetInteger(SplitArguments(argument, 0));
                        ServiceList obj = Packager.UnpackServiceList(SplitArguments(argument, 1));
                        answer = billTracker.UpdateServiceList(billId, obj).ToString();
                    }

                    else if (action == MonthlyBillingSummary)
                    {
                        DateTime obj = Packager.GetDateTime(argument);
                        answer = billTracker.MonthlyBillingSummary(obj);
                        if (answer == "") answer = NullResponse;
                    }

                    else if (action == MonthlyBillingCSV)
                    {
                        DateTime obj = Packager.GetDateTime(argument);
                        List<string> result = billTracker.GenerateMonthlyBillingCSV(obj);
                        answer = "";
                        foreach (string s in result)
                        {
                            answer += s + div;
                        }
                        if (answer == "") answer = NullResponse;
                    }

                    else if (action == MonthlyReconcileCSV)
                    {
                        DateTime obj = Packager.GetDateTime(argument);
                        List<string> list = billTracker.MonthlyReconcileCSV(obj);
                        answer = "";
                        foreach (string s in list)
                        {
                            answer += s + div;
                        }
                        if (answer == "") answer = NullResponse;
                    }

                    else
                    {
                        answer = InvalidAction.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                answer = ExceptionTag + div + e.Message;
            }
            return answer;
        }

        #endregion

        #region Interpreter Tools

        /// <summary>
        /// Parses the raw request and split the message received in action and arguments
        /// </summary>
        /// <param name="message">Raw message received</param>
        /// <param name="action">Output action</param>
        /// <param name="arguments">Output arguments</param>
        private void ParseRequest(string message, out string action, out string arguments)
        {
            // Find Action
            string[] actions = message.Split(FieldSeparator, StringSplitOptions.RemoveEmptyEntries);
            action = "";
            if (actions.Length > 0)
            {
                action = actions[0] + div;
            }

            // Find Arguments
            string[] args = message.Split(ActionSeparator, StringSplitOptions.RemoveEmptyEntries);
            arguments = "";
            if (args.Length > 0)
            {
                arguments = args[0];
            }
        }


        /// <summary>
        /// Returns the argument 0 or 1 from the raw string
        /// </summary>
        /// <param name="rawArgument"></param>
        /// <param name="argumentIndex"></param>
        /// <returns></returns>
        private string SplitArguments(string rawArgument, int argumentIndex)
        {
            string retStr = "";
            if (argumentIndex == 0)
            {
                retStr = rawArgument.Split(FieldSeparator, StringSplitOptions.RemoveEmptyEntries)[0];
            }
            if (argumentIndex == 1)
            {
                int splitIndex = rawArgument.IndexOf(div) + 1;
                retStr = rawArgument.Substring(splitIndex, rawArgument.Length - splitIndex);
            }
            return retStr;
        }

        #endregion

        #region Event Handlers

        public void OnClientConnected(object source, ClientEventArgs args)
        {
            // May be used in the future
        }

        public void OnClientDisconnected(object source, ClientEventArgs args)
        {
            // May be used in the future
        }

        public void OnMessageReceived(object source, ClientEventArgs args)
        {
            string response = InterpretMessage(args.Message);
            ServerConnection.Instance.WriteMessage(args.Client, response);
        }

        #endregion
    }
}
