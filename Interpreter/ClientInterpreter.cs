﻿
/*****************************************************************************************************
* FILE : 		  ClientInterprefer.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-28
* SECOND VERSION: 2017-Dec-08
* THIRD VERSION:  2018-Jan-04
* FINAL VERSION:  2018-Mar-28

* DESCRIPTION/Requirements: 

* The ClientInterpreter is a supportive class used to make the interface between a client application that 
* contains a ClientInterpreter and a server application that instantiates a ServerConnection. ClientInterpreter 
* have the knowledge of all possible actions that the user can perform and assists the View layer to get the 
* appropriate response from the server. The format of message sent to the server is a string started with the especific 
* action as the first parameter. All parameters are divided by ';' Basic structure of a message generated to 
* ClientConnection: action;args; 
* E.G.: AddAppointment;OBJECT;Appointment;appointmentID;datetime;patientID;patientID;
* 
* Refence: 

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using Demographics;
using Billing;
using Connection;
using System.Net;

namespace Interpreter
{
    /// <summary>
    /// <b>Description: </b>The ClientInterpreter is a supportive class used to make the interface
    /// between a client application that contains a ClientInterpreter and
    /// a server application that instantiates a ServerConnection.
    /// ClientInterpreter have the knowledge of all possible actions that the
    /// user can perform and assists the View layer to get the appropriate
    /// response from the server.
    /// The format of message sent to the server is a string started with the
    /// especific action as the first parameter. All parameters are divided by ';'
    /// Basic structure of a message generated to ClientConnection: action;args;
    /// E.G.: AddAppointment;OBJECT;Appointment;appointmentID;datetime;patientID;patientID;
    /// </summary>
    /// 
    public sealed class ClientInterpreter : IDemographics, IBilling
    {
        #region Attributes

        // Thread-safe Singleton
        private static ClientInterpreter instance;
        private static readonly object padlock = new object();

        // Interfaces - Connection
        private static ClientConnection myConnection = ClientConnection.Instance;

        // Separators
        private static string div = Packager.Div;

        #endregion

        #region Instance

        /// <summary>
        /// This property is the interface to the instance of this class.
        /// Whenever the user wants to use this class, he or she has to call 
        /// the Instance property. This assures that there's only one instance 
        /// of ClientInterpreter in the application.
        /// </summary>
        public static ClientInterpreter Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ClientInterpreter();
                    }
                    return instance;
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// ClientInterpreters constructor that needs ClientConnection
        /// to work. This constructor is mandatory
        /// </summary>
        /// <param name="connectionHandler">Client connection handler</param>
        ClientInterpreter()
        {

        }

        #endregion

        #region Connection Control

        public bool Connect(IPAddress addr, int port)
        {
            try
            {
                if (!myConnection.IsConnected)
                {
                    myConnection.Connect(addr, port);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return myConnection.IsConnected;
        }


        /// <summary>
        /// This method helps the view layer to disconnect from the server
        /// </summary>
        public void Disconnect()
        {
            myConnection.Disconnect();
        }

        #endregion

        // =======================================================================
        //                              DEMOGRAPHICS
        // =======================================================================

        #region Demographics - Add

        /// <summary>
        /// Sends AddPatient request to the server
        /// </summary>
        /// <param name="obj">Filled Patient</param>
        /// <returns>Error code from the server</returns>
        public int AddPatient(Patient obj)
        {
            string message = ServerInterpreter.AddPatient + Packager.PackPatient(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }


        /// <summary>
        /// Sends AddAppointment request to the server
        /// </summary>
        /// <param name="obj">Filled Appointment</param>
        /// <returns>Error code from the server</returns>
        public int AddAppointment(Appointment obj)
        {
            string message = ServerInterpreter.AddAppointment + Packager.PackAppointment(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }

        #endregion

        #region Demographics - Find

        /// <summary>
        /// Sends FindPatient request to the server
        /// </summary>
        /// <param name="hcnNumber">HCN of the patient</param>
        /// <returns>A Patient if it finds one, or NULL</returns>
        public Patient FindPatient(string hcnNumber)
        {
            string message = ServerInterpreter.FindPatientStr + hcnNumber;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.UnpackPatient(answer);
        }


        /// <summary>
        /// Sends FindPatient request to the server
        /// </summary>
        /// <param name="obj">Patient partially filled</param>
        /// <returns>List of objects found on server side</returns>
        public List<Patient> FindPatient(Patient obj)
        {
            List<Patient> retCode = null;
            string message = ServerInterpreter.FindPatientObj + Packager.PackPatient(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }

            if (answer != ServerInterpreter.NullResponse)
            {
                retCode = Packager.UnpackListOfPatients(answer);
            }

            return retCode;
        }


        /// <summary>
        /// Sends FindPatient request to the server
        /// </summary>
        /// <param name="patientID">Patients ID</param>
        /// <returns>A Patient if it finds one, or NULL</returns>
        public Patient FindPatient(int patientID)
        {
            string message = ServerInterpreter.FindPatientInt + patientID;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.UnpackPatient(answer);
        }


        /// <summary>
        /// Sends FindAddress request to the server
        /// </summary>
        /// <param name="hcnNumber">HCN of the patient</param>
        /// <returns>An Address if it finds one, or NULL</returns>
        public Address FindAddress(string hcnNumber)
        {
            string message = ServerInterpreter.FindAddressStr + hcnNumber;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.UnpackAddress(answer);
        }


        /// <summary>
        /// Sends FindAddress request to the server
        /// </summary>
        /// <param name="addressID">Address ID</param>
        /// <returns>An Address if it finds one, or NULL</returns>
        public Address FindAddress(int addressID)
        {
            string message = ServerInterpreter.FindAddressInt + addressID;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.UnpackAddress(answer);
        }


        /// <summary>
        /// Sends FindAppointment request to the server
        /// </summary>
        /// <param name="appointmentID">Appointment ID</param>
        /// <returns>An Appointment if it finds one, or NULL</returns>
        public Appointment FindAppointment(int appointmentID)
        {
            string message = ServerInterpreter.FindAppointment + appointmentID.ToString();
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.UnpackAppointment(answer);
        }


        /// <summary>
        /// Sends FindAppointments request to the server
        /// </summary>
        /// <param name="patientID">The Patient ID</param>
        /// <returns>List of objects found on server side</returns>
        public List<Appointment> FindAppointments(int patientID)
        {
            List<Appointment> retCode = null;

            string message = ServerInterpreter.FindAppointmentsInt + patientID;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }

            if (answer != ServerInterpreter.NullResponse)
            {
                retCode = Packager.UnpackListOfAppointments(answer);
            }
            return retCode;
        }


        /// <summary>
        /// Sends FindAppointments request to the server
        /// </summary>
        /// <param name="begin">Date to start looking for</param>
        /// <returns>List of objects found on server side</returns>
        public List<Appointment> FindAppointments(DateTime begin, DateTime end)
        {
            List<Appointment> retCode = null;

            string message = ServerInterpreter.FindAppointmentsDate + begin.ToString() + div + end.ToString();
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }


            if (answer != ServerInterpreter.NullResponse)
            {
                retCode = Packager.UnpackListOfAppointments(answer);
            }
            return retCode;
        }

        #endregion

        #region Demographics - Update

        /// <summary>
        /// Sends UpdatePatient request to the server
        /// </summary>
        /// <param name="obj">Updated Patient</param>
        /// <returns>Error code from the server</returns>
        public int UpdatePatient(Patient obj)
        {
            string message = ServerInterpreter.UpdatePatient + Packager.PackPatient(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }


        /// <summary>
        /// Sends UpdateAddress request to the server
        /// </summary>
        /// <param name="obj">Updated Address</param>
        /// <returns>Error code from the server</returns>
        public int UpdateAddress(Address obj)
        {
            string message = ServerInterpreter.UpdateAddress + Packager.PackAddress(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }


        /// <summary>
        /// Sends UpdateAppointment request to the server
        /// </summary>
        /// <param name="obj">Updated Appointment</param>
        /// <returns>Error code from the server</returns>
        public int UpdateAppointment(Appointment obj)
        {
            string message = ServerInterpreter.UpdateAppointment + Packager.PackAppointment(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }

        #endregion

        // =======================================================================
        //                                BILLING
        // =======================================================================
        
        #region Billing - Add

        /// <summary>
        /// Sends AddBill request to the server
        /// </summary>
        /// <param name="obj">Filled Bill</param>
        /// <returns>Error code from the server</returns>
        public int AddBill(Bill obj)
        {
            string message = ServerInterpreter.AddBill + Packager.PackBill(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }


        /// <summary>
        /// Sends AddBill request to the server
        /// </summary>
        /// <param name="obj">Filled Bill</param>
        /// <returns>Error code from the server</returns>
        public int AddServiceList(int billID, ServiceList obj)
        {
            string message = ServerInterpreter.AddServiceList + Packager.PackServiceList(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }

        #endregion

        #region Billing - Find

        /// <summary>
        /// Sends FindBill request to the server
        /// </summary>
        /// <param name="patientID">The Patient ID</param>
        /// <param name="appointmentID">The Appointment ID</param>
        /// <returns>A Bill if it finds one, or NULL</returns>
        public Bill FindBill(int patientID, int appointmentID)
        {
            string message = ServerInterpreter.FindBill + patientID + div + appointmentID;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }

            Bill retObject = null;
            if (answer != ServerInterpreter.NullResponse)
            {
                retObject = Packager.UnpackBill(answer);
            }

            return retObject;
        }


        /// <summary>
        /// Sends FindServices request to the server
        /// </summary>
        /// <param name="serviceCode"></param>
        /// <returns>List of objects found on server side</returns>
        public List<Service> FindServices(string serviceCode = "")
        {
            List<Service> retCode = null;

            if (serviceCode == "") { serviceCode = ServerInterpreter.NoArgument; }
            string message = ServerInterpreter.FindServices + serviceCode;
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }

            if (answer != ServerInterpreter.NullResponse)
            {
                retCode = Packager.UnpackListOfServices(answer);
            }
            return retCode;
        }


        /// <summary>
        /// Sends FindService request to the server
        /// </summary>
        /// <param name="serviceID"></param>
        /// <returns>A Service or null</returns>
        public Service FindService(int serviceID)
        {
            string message = ServerInterpreter.FindService + serviceID.ToString();
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.UnpackService(answer);
        }

        #endregion

        #region Billing - Update

        /// <summary>
        /// Sends UpdateBill request to the server
        /// </summary>
        /// <param name="obj">An existing Bill</param>
        /// <returns>The error code from the server</returns>
        public int UpdateBill(Bill obj)
        {
            string message = ServerInterpreter.UpdateBill + Packager.PackBill(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }


        /// <summary>
        /// Sends UpdateServiceList request to the server
        /// </summary>
        /// <param name="obj">An existing ServiceList</param>
        /// <returns>The error code from the server</returns>
        public int UpdateServiceList(int billID, ServiceList obj)
        {
            string message = ServerInterpreter.UpdateServiceList + Packager.PackServiceList(obj);
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            return Packager.GetInteger(answer);
        }

        #endregion

        #region Billing - Especial

        /// <summary>
        /// Sends MonthlyBillingSummary request to the server
        /// </summary>
        /// <param name="date">Year and Month reference</param>
        /// <returns>The summary according to requirements</returns>
        public string MonthlyBillingSummary(DateTime date)
        {
            string message = ServerInterpreter.MonthlyBillingSummary + date.ToString();
            return myConnection.SendRequest(message);
        }


        /// <summary>
        /// Sends MonthlyBillingCSV request to the server
        /// </summary>
        /// <param name="date">Year and Month reference</param>
        /// <returns>List of entries from the file</returns>
        public List<string> GenerateMonthlyBillingCSV(DateTime date)
        {
            string message = ServerInterpreter.MonthlyBillingCSV + date.ToString();
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            if (answer == ServerInterpreter.NullResponse) { answer = ""; }
            return Packager.GetList(answer);
        }


        /// <summary>
        /// Sends MonthlyReconcileCSV request to the server
        /// </summary>
        /// <param name="date">Year and Month reference</param>
        /// <returns>Error code from the server</returns>
        public List<string> MonthlyReconcileCSV(DateTime date)
        {
            string message = ServerInterpreter.MonthlyReconcileCSV + date.ToString();
            string answer = myConnection.SendRequest(message);
            if (CheckException(answer) != null) { throw CheckException(answer); }
            if (answer == ServerInterpreter.NullResponse) { answer = ""; }
            return Packager.GetList(answer);
        }

        #endregion

        // =======================================================================
        //                                TOOLS
        // =======================================================================

        #region Interpreter Tools

        /// <summary>
        /// Assists ClientInterpreter to handle exceptions comming
        /// from the server
        /// </summary>
        /// <param name="message">The answer from the server</param>
        /// <returns></returns>
        private Exception CheckException(string message)
        {
            Exception retEx = null;
            if (message.Length > ServerInterpreter.ExceptionTag.Length)
            {
                string exc = message.Substring(0, ServerInterpreter.ExceptionTag.Length);
                if (exc == ServerInterpreter.ExceptionTag)
                {
                    retEx = new Exception(message.Substring(ServerInterpreter.ExceptionTag.Length + 1));
                }
            }
            return retEx;
        }

        #endregion
    }
}
