﻿
/*****************************************************************************************************
* FILE : 		  ServerConnections.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-28
* SECOND VERSION: 2017-Dec-08
* THIRD VERSION:  2018-Jan-04
* FINAL VERSION:  2018-Mar-01

* DESCRIPTION/Requirements: 

* The ServerConnection is a supportive class used to make the interface between a client application that contains 
* a ClientInterpreter and a server application that instantiates a ServerConnection. ServerConnection receives 
* requests from the ClientConnection and redirect it to the ServerInterpreter. ServerConnection also sends back 
* the response from the server to the ClientConnection through a socket connection.
* 
* Singleton Reference: http://csharpindepth.com/Articles/General/Singleton.aspx
* Encryption Reference: https://www.codeproject.com/Articles/5719/Simple-encrypting-and-decrypting-data-in-C
* Events Reference: https://www.youtube.com/watch?v=jQgwEsJISy0
* TCP Reference: http://www.mikeadev.net/2012/07/multi-threaded-tcp-server-in-csharp/

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

#pragma warning disable CS0618 // Ignore Dns.GetHostByName being obsolete

namespace Connection
{
    /// <summary>
    /// <b>Description: </b>The ClientEventArgs is a supportive class that helps to send information
    /// related to the events raised during a server session. It carries a TcpClient and a message.
    /// </summary>
    public class ClientEventArgs : EventArgs
    {
        public TcpClient Client { get; set; }
        public string Message { get; set; }

        public ClientEventArgs(TcpClient client)
        {
            Client = client;
            Message = "";
        }

        public ClientEventArgs(TcpClient client, string message)
        {
            Client = client;
            Message = message;
        }
    }


    /// <summary>
    /// <b>Description: </b>The ServerConnection is a supportive class used to make the interface
    /// between a client application that contains a ClientInterpreter and
    /// a server application that instantiates a ServerConnection.
    /// ServerConnection receives requests from the ClientConnection and redirect it
    /// to the ServerInterpreter. ServerConnection also sends back the response from the
    /// server to the ClientConnection through a TCP connection.
    /// The communication within the application is done by events.
    /// </summary>
    public sealed class ServerConnection
    {
        #region Constants
        public static readonly string EXIT_CODE = "exit";
        public static readonly int PORT = 8000;
        public static readonly int HCN_PORT = 8001;
        #endregion

        #region Attributes

        // Thread-safe Singleton
        private static ServerConnection instance;
        private static readonly object padlock = new object();

        // Event Handler
        public event EventHandler ServerStarted;
        public event EventHandler<ClientEventArgs> ClientConnected;
        public event EventHandler<ClientEventArgs> MessageReceived;
        public event EventHandler<ClientEventArgs> ClientDisconnected;
        public event EventHandler<ClientEventArgs> ConnectionError;

        // Connection related
        private bool started = false;
        private TcpListener listener;
        private readonly List<TcpClient> clients;
        private int port = PORT;

        #endregion

        #region Constructor

        /// <summary>
        /// A private constructor that prevents this class to be instantiated more than once 
        /// following the singleton design pattern.
        /// </summary>
        ServerConnection()
        {
            clients = new List<TcpClient>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// This property is the interface to the instance of this class.
        /// Whenever the user wants to use this class, he or she has to call 
        /// the Instance property. This assures that there's only one instance 
        /// of ServerConnection in the application.
        /// </summary>
        public static ServerConnection Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ServerConnection();
                    }
                    return instance;
                }
            }
        }


        public int Port
        {
            get => port;
            set
            {
                if (value > 0 && !started)
                {
                    port = value;
                }
            }
        }


        public string IP
        {
            get
            {
                string addr = "";
                foreach (IPAddress ip in Dns.GetHostByName(Dns.GetHostName()).AddressList)
                {
                    addr = ip.ToString();
                }
                return addr;
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// This method assists the ServerStarted event.
        /// </summary>
        /// <seealso cref="ServerStarted"/>
        private void OnServerStarted()
        {
            if (ServerStarted != null)
            {
                ServerStarted(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// This method assists the ClientConnected event.
        /// </summary>
        /// <seealso cref="ClientConnected"/>
        private void OnClientConnected(TcpClient client)
        {
            if (ClientConnected != null)
            {
                ClientConnected(this, new ClientEventArgs(client));
            }
        }

        /// <summary>
        /// This method assists the ClientDisconnected event.
        /// </summary>
        /// <seealso cref="ClientDisconnected"/>
        private void OnClientDisconnected(TcpClient client)
        {
            if (ClientDisconnected != null)
            {
                ClientDisconnected(this, new ClientEventArgs(client));
            }
        }

        /// <summary>
        /// This method assists the MessageReceived event.
        /// </summary>
        /// <seealso cref="MessageReceived"/>
        private void OnMessageReceived(TcpClient client, string message)
        {
            if (message == null) { message = ""; }
            if (MessageReceived != null)
            {
                MessageReceived(this, new ClientEventArgs(client, message));
            }
        }

        private void OnConnectionError(TcpClient client, string message)
        {
            if (message == null) { message = ""; }
            if (ConnectionError != null)
            {
                ConnectionError(this, new ClientEventArgs(client, message));
            }
        }

        #endregion

        #region Workflow

        /// <summary>
        /// This method starts the ServerConenction and creates a 
        /// thread to receive new connections.
        /// </summary>
        /// <param name="port">port number</param>
        public void Start(int port)
        {
            if (!started)
            {
                Port = port;
                listener = new TcpListener(IPAddress.Any, Port);
                listener.Start();
                started = true;
                Thread t = new Thread(new ThreadStart(StartAcceptingClients));
                t.Start();
            }
        }

        /// <summary>
        /// This method runs on a separate thread created by Start(). It 
        /// waits for a new client connection. After a new connection starts, 
        /// this method creates a new thread for each client connected.
        /// </summary>
        /// <seealso cref="Start"/>
        private void StartAcceptingClients()
        {
            OnServerStarted();
            while (started)
            {
                try
                {
                    TcpClient client = listener.AcceptTcpClient();
                    clients.Add(client);
                    OnClientConnected(client);
                    Thread t = new Thread(new ParameterizedThreadStart(HandleClient));
                    t.Start(client);
                }
                catch (SocketException)
                {
                    // if the listener gets disconnected
                }
            }
        }

        /// <summary>
        /// This method runs on a separate thread to handle a client 
        /// after the connection has been stablished by StartAcceptingClients().
        /// After that, it will wait for new messages and raise the event 
        /// MessageReceived when it receives a new message.
        /// </summary>
        /// <param name="obj">TcpClient object</param>
        /// <seealso cref="StartAcceptingClients"/>
        /// <seealso cref="MessageReceived"/>
        /// <seealso cref="MessageReceived"/>
        private void HandleClient(object obj)
        {
            TcpClient client = (TcpClient)obj;

            bool clientConnected = true;
            string data;

            while (clientConnected)
            {
                try
                {
                    data = ReadMessage(client);

                    if (data == EXIT_CODE)
                    {
                        clientConnected = false;
                        continue;
                    }
                    else
                    {
                        OnMessageReceived(client, data);
                    }
                }
                catch (IOException)
                {
                    clientConnected = false;
                    continue;
                }
                catch (Exception ex)
                {
                    // We got an intruder
                    OnConnectionError(client, ex.Message);
                    clientConnected = false;
                }
            }

            // Disconnect client
            DisconnectClient(client);
        }

        /// <summary>
        /// This method receives a new message and decrypt it to be readable.
        /// It returns an unencrypted message.
        /// </summary>
        /// <param name="client">client connection</param>
        /// <returns>A string with the message received.</returns>
        /// <exception cref="IOException">If there's any problem with the communication</exception>
        /// <exception cref="InvalidOperationException">If the client is suspicious</exception>
        /// 
        private string ReadMessage(TcpClient client)
        {
            StreamReader sr = new StreamReader(client.GetStream(), Encoding.ASCII);
            string encrypted = "";
            string decrypted = "";

            try
            {
                encrypted = sr.ReadLine();
            }
            catch (IOException ioe)
            {
                throw ioe;
            }

            // decrypt
            try
            {
                decrypted = Encrypter.Decrypt(encrypted);
            }
            catch (Exception)
            {
                // this happens if the client doesn't know that the connection is encrypted (nasty clients)
                throw new InvalidOperationException("Nasty client detected!");
            }

            return decrypted;
        }

        /// <summary>
        /// This method writes a message to the client. It encrypts the message 
        /// before send it.
        /// </summary>
        /// <param name="client">The client</param>
        /// <param name="message">The message to send</param>
        /// <exception cref="IOException">If there's any problem with the communication</exception>
        public void WriteMessage(TcpClient client, string message)
        {
            StreamWriter sw = new StreamWriter(client.GetStream(), Encoding.ASCII);

            // encrypt
            string encrypted = Encrypter.Encrypt(message);
            try
            {
                sw.WriteLine(encrypted);
                sw.Flush();
            }
            catch (IOException ioe)
            {
                throw ioe;
            }

        }

        /// <summary>
        /// This method disconnects a client properly.
        /// </summary>
        /// <param name="client">The client to be disconnected</param>
        private void DisconnectClient(TcpClient client)
        {
            if (clients.Contains(client))
            {
                OnClientDisconnected(client);
                client.Close();
                clients.Remove(client);
            }
        }

        /// <summary>
        /// Disconnects all the clients currently connected and 
        /// closes the listener as well.
        /// </summary>
        public void Shutdown()
        {
            lock (padlock)
            {
                started = false;
                while (clients.Count > 0)
                {
                    DisconnectClient(clients[0]);
                }
            }
            listener.Stop();
        }

        #endregion
    }
}
