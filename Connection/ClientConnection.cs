﻿
/*****************************************************************************************************
* FILE : 		  ClientConnection.cs
* PROJECT : 	  SQ-I Term Project 
* PROGRAMMER : 	  ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:  2017-Oct-28
* SECOND VERSION: 2017-Dec-08
* THIRD VERSION:  2018-Jan-04
* FINAL VERSION:  2018-Mar-01

* DESCRIPTION/Requirements: 

* The ClientConnection is a supportive class used to handle a connection between a client application that 
* contains a ClientInterpreter and server application that instantiates a ServerConnection. ClientConnection 
* makes the interface between the ClientInterpreter and sends a request to ServerConnection through a socket connection.
* 
* Singleton Reference: http://csharpindepth.com/Articles/General/Singleton.aspx
* Encryption Reference: https://www.codeproject.com/Articles/5719/Simple-encrypting-and-decrypting-data-in-C
* TCP Reference: http://www.mikeadev.net/2012/07/multi-threaded-tcp-server-in-csharp/

*****************************************************************************************************/

using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Connection
{
    /// <summary>
    /// <b>Description: </b>The ClientConnection is a supportive class used to handle a connection 
    /// between a client application that contains a ClientInterpreter and
    /// a server application that instantiates a ServerConnection.
    /// ClientConnection makes the interface between the ClientInterpreter and
    /// sends a request to ServerConnection through a TCP connection.
    /// </summary>
    public sealed class ClientConnection
    {
        #region Attributes

        // Thread-safe Singleton
        private static ClientConnection instance;
        private static readonly object padlock = new object();

        // Connection related
        private TcpClient client;
        private StreamReader sr;
        private StreamWriter sw;

        #endregion

        #region Constructor

        /// <summary>
        /// A private constructor that prevents this class to be instantiated more than once 
        /// following the singleton design pattern.
        /// </summary>
        ClientConnection()
        {
            client = new TcpClient();
        }

        #endregion

        #region Properties

        /// <summary>
        /// This property returns the connection state.
        /// True if the client is connected, false otherwise.
        /// </summary>
        public bool IsConnected { get { bool ret = client.Connected; return ret; } }

        /// <summary>
        /// This property is the interface to the instance of this class.
        /// Whenever the user wants to use this class, he or she has to call 
        /// the Instance property. This assures that there's only one instance 
        /// of ClientConnection in the application.
        /// </summary>
        public static ClientConnection Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ClientConnection();
                    }
                    return instance;
                }
            }
        }

        #endregion

        #region Workflow

        /// <summary>
        /// This method tries to connect to a server on the IP address.
        /// </summary>
        /// <param name="ip">IP address of the server</param>
        /// <exception cref="InvalidOperationException">If connect fails</exception>
        public void Connect(IPAddress ip, int port)
        {
            if (!client.Connected)
            {
                IAsyncResult ar = client.BeginConnect(ip, port, null, null);
                System.Threading.WaitHandle wh = ar.AsyncWaitHandle;

                try
                {
                    //client.Connect(ip, port);
                    if (!ar.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1), false))
                    {
                        client.Close();
                        client = new TcpClient();
                        throw new TimeoutException();
                    }

                    sw = new StreamWriter(client.GetStream(), Encoding.ASCII);
                    sr = new StreamReader(client.GetStream(), Encoding.ASCII);
                    sw.AutoFlush = true;
                }
                catch (SocketException)
                {
                    throw new InvalidOperationException("Failed to connect");
                }
                catch(Exception)
                {
                    throw;
                }
                finally
                {
                    wh.Close();
                }
            }
        }

        /// <summary>
        /// Sends a request to the server and waits for a response.
        /// It returns the response from the server.
        /// </summary>
        /// <param name="request">The request to be send</param>
        /// <returns>The response from the server</returns>
        /// <exception cref="InvalidOperationException">If the clients disconnects from the server</exception>
        public string SendRequest(string request)
        {
            string response = "";

            try
            {
                string encrypted = Encrypter.Encrypt(request);
                sw.WriteLine(encrypted);

                if (request == ServerConnection.EXIT_CODE)
                {
                    Disconnect();
                }
                else
                {
                    encrypted = sr.ReadLine();
                    response = Encrypter.Decrypt(encrypted);
                }
            }
            catch (IOException)
            {
                throw
                    new InvalidOperationException("Failed to send request to the server. Check the connection.");
            }

            return response;
        }

        /// <summary>
        /// This method disconnects the client from the server.
        /// </summary>
        public void Disconnect()
        {
            if (client.Connected)
            {
                client.Close();
                sr = null;
                sw = null;
            }
        }

        #endregion
    }
}
