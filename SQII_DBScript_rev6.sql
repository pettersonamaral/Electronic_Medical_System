-- //********************************************************************************************************************************* 
-- // FILE          : SQII_DBScript_rev6.sql
-- // PROJECT       : SQ-II Term Project  
-- // PROGRAMMER    : ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
-- // VERSION DATE  : 2018-APR-19
-- // DESCRIPTION   : The file store the script to create the databese "SQII_EMS"
-- //**********************************************************************************************************************************

USE [master]
DECLARE @kill varchar(8000) = '';  
SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';'  
FROM sys.dm_exec_sessions
WHERE database_id  = db_id('SQII_EMS')

EXEC(@kill);
GO


DROP DATABASE IF EXISTS [SQII_EMS]
GO

/****** Object:  Database [SQII_EMS]    Script Date: 2018-04-27 2:21:54 PM ******/
CREATE DATABASE [SQII_EMS]
GO
ALTER DATABASE [SQII_EMS] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SQII_EMS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SQII_EMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SQII_EMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SQII_EMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SQII_EMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SQII_EMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [SQII_EMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SQII_EMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SQII_EMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SQII_EMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SQII_EMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SQII_EMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SQII_EMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SQII_EMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SQII_EMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SQII_EMS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SQII_EMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SQII_EMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SQII_EMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SQII_EMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SQII_EMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SQII_EMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SQII_EMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SQII_EMS] SET RECOVERY FULL 
GO
ALTER DATABASE [SQII_EMS] SET  MULTI_USER 
GO
ALTER DATABASE [SQII_EMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SQII_EMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SQII_EMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SQII_EMS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SQII_EMS] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SQII_EMS', N'ON'
GO
ALTER DATABASE [SQII_EMS] SET QUERY_STORE = OFF
GO
USE [SQII_EMS]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [SQII_EMS]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[addressID] [int] IDENTITY(1,1) NOT NULL,
	[addressLine1] [nvarchar](60) NOT NULL,
	[addressLine2] [nvarchar](60) NULL,
	[city] [nvarchar](60) NOT NULL,
	[province] [nchar](2) NOT NULL,
	[numPhone] [nchar](13) NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[addressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Appointment]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appointment](
	[appointmentID] [int] IDENTITY(1,1) NOT NULL,
	[dateAndTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Appointment] PRIMARY KEY CLUSTERED 
(
	[appointmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentLine]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppointmentLine](
	[appointmentID] [int] NOT NULL,
	[patientID] [int] NOT NULL,
 CONSTRAINT [PK_AppointmentLine] PRIMARY KEY CLUSTERED 
(
	[appointmentID] ASC,
	[patientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[billID] [int] IDENTITY(1,1) NOT NULL,
	[patientID] [int] NOT NULL,
	[appointmentID] [int] NOT NULL,
	[flagRecall] [int] NOT NULL,
	[statusDone] [bit] NOT NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[billID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[patientID] [int] IDENTITY(1,1) NOT NULL,
	[HCN] [nchar](12) NOT NULL,
	[lastName] [nvarchar](60) NOT NULL,
	[firstName] [nvarchar](60) NOT NULL,
	[mInitial] [nchar](1) NULL,
	[dateBirth] [datetime] NOT NULL,
	[gender] [nchar](10) NOT NULL,
	[headOfHouse] [nchar](12) NULL,
	[addressID] [int] NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[patientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Service]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Service](
	[serviceID] [int] IDENTITY(1,1) NOT NULL,
	[feeCode] [nchar](4) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[value] [float] NOT NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[serviceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServiceLine]    Script Date: 2018-04-27 2:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceLine](
	[serviceID] [int] NOT NULL,
	[billID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[status] [nchar](4) NOT NULL,
 CONSTRAINT [PK_ServiceLine] PRIMARY KEY CLUSTERED 
(
	[serviceID] ASC,
	[billID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (1, N'5858 Eget Road', N'Unit 1 ', N'Puerto Montt', N'ON', N'(403)289-4961')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (2, N'171-1190 Erat St.', N'Unit 1 ', N'Kingussie', N'ON', N'(780)327-7577')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (3, N'Ap #783-383 Praesent Av.', N'Unit 1 ', N'Armo', N'ON', N'(236)685-3944')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (4, N'1625 Pretium Street', N'Unit 1 ', N'Clearwater Municipal District', N'ON', N'(204)487-0506')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (5, N'P.O. Box 124, 8233 Ligula Avenue', N'Unit 1 ', N'Sainte-Marie-sur-Semois', N'ON', N'(403)718-1331')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (6, N'973-5445 Luctus Rd.', N'Unit 1 ', N'Pallavaram', N'ON', N'(780)745-9645')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (7, N'P.O. Box 153, 1777 Iaculis Av.', N'Unit 1 ', N'Overland Park', N'ON', N'(236)496-0049')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (8, N'Ap #881-7188 Magna Street', N'Unit 1 ', N'Bois-de-Villers', N'ON', N'(204)213-3676')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (9, N'7536 Interdum Rd.', N'Unit 1 ', N'Sambalpur', N'ON', N'(403)740-7008')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (10, N'Ap #287-935 Vitae, Rd.', N'Unit 1 ', N'San Giovanni Suergiu', N'ON', N'(780)766-3363')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (11, N'6440 Eu Rd.', N' Unit 2 ', N'Asbestos', N'ON', N'(236)760-0117')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (12, N'690-1172 Auctor St.', N' Unit 2 ', N'Taupo', N'ON', N'(204)914-8337')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (13, N'P.O. Box 787, 9040 Ligula. St.', N' Unit 2 ', N'Dreux', N'ON', N'(403)453-6291')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (14, N'P.O. Box 694, 881 Vel, Street', N' Unit 2 ', N'Kelowna', N'ON', N'(780)212-9919')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (15, N'223-2627 Ut, St.', N' Unit 2 ', N'Penhold', N'ON', N'(236)273-2072')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (16, N'P.O. Box 512, 884 Tortor Road', N' Unit 2 ', N'Oklahoma City', N'ON', N'(204)238-5610')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (17, N'P.O. Box 435, 9375 Vehicula. Rd.', N' Unit 2 ', N'Tampa', N'ON', N'(403)891-7206')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (18, N'Ap #885-4561 Dictum. Rd.', N' Unit 2 ', N'Jette', N'ON', N'(780)916-0684')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (19, N'P.O. Box 891, 766 Fermentum Av.', N' Unit 2 ', N'Lichtaart', N'ON', N'(236)186-9681')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (20, N'7270 Mollis. Ave', N' Unit 2 ', N'Pietracatella', N'ON', N'(204)534-9006')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (21, N'P.O. Box 159, 3631 Nonummy. Road', N' Unit 3 ', N'King Township', N'ON', N'(403)439-9951')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (22, N'3800 Purus St.', N' Unit 3 ', N'Dorval', N'ON', N'(780)135-8064')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (23, N'Ap #560-156 Ac Ave', N' Unit 3 ', N'Firozabad', N'ON', N'(236)652-9426')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (24, N'7982 Massa Avenue', N' Unit 3 ', N'Ajax', N'ON', N'(204)206-3807')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (25, N'Ap #501-502 Natoque St.', N' Unit 3 ', N'Deschambault', N'ON', N'(403)195-7000')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (26, N'Ap #828-5407 Mauris. Road', N' Unit 3 ', N'Bendigo', N'ON', N'(780)187-7346')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (27, N'5544 Nulla St.', N' Unit 3 ', N'Tavistock', N'ON', N'(236)844-2562')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (28, N'762-3713 A Avenue', N' Unit 3 ', N'Stockholm', N'ON', N'(204)687-2042')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (29, N'7980 Sed Avenue', N' Unit 3 ', N'Paine', N'ON', N'(403)745-2837')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (30, N'Ap #908-2089 Etiam Road', N' Unit 3 ', N'Irricana', N'ON', N'(780)942-0599')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (31, N'P.O. Box 799, 1658 Duis St.', N'Unit 4 ', N'Sundrie', N'ON', N'(236)736-5973')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (32, N'6080 Integer St.', N'Unit 4 ', N'College', N'ON', N'(204)711-1704')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (33, N'435-9102 Ipsum Street', N'Unit 4 ', N'Wolfenbttel', N'ON', N'(403)819-7004')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (34, N'P.O. Box 474, 3984 Fames Ave', N'Unit 4 ', N'Kahramanmara', N'ON', N'(780)201-2718')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (35, N'6725 Euismod Rd.', N'Unit 4 ', N'Aurangabad', N'ON', N'(236)531-4887')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (36, N'P.O. Box 780, 2696 Accumsan St.', N'Unit 4 ', N'Whitehorse', N'ON', N'(204)943-2685')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (37, N'907-8055 Quam Av.', N'Unit 4 ', N'Reyhanlı', N'ON', N'(403)295-3538')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (38, N'Ap #420-7984 In Ave', N'Unit 4 ', N'Hinckley', N'ON', N'(780)501-2734')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (39, N'976-7063 Eget, Street', N'Unit 4 ', N'Cochrane', N'ON', N'(236)139-4086')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (40, N'4698 Pretium Av.', N'Unit 4 ', N'Bouffioulx', N'ON', N'(204)946-9335')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (41, N'8802 Ornare, Road', N'Unit 5', N'Stirling', N'ON', N'(403)259-3528')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (42, N'Ap #252-6321 Nec Ave', N'Unit 5', N'Plauen', N'ON', N'(780)128-0897')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (43, N'916-9105 Neque Avenue', N'Unit 5', N'Lacombe County', N'ON', N'(236)316-1921')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (44, N'P.O. Box 910, 9410 Ut Av.', N'Unit 5', N'Alexandria', N'ON', N'(204)272-8638')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (45, N'235-4437 Pede. Road', N'Unit 5', N'Campitello di Fassa', N'ON', N'(403)917-4162')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (46, N'P.O. Box 606, 1512 Non, Street', N'Unit 5', N'Chambave', N'ON', N'(780)761-7656')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (47, N'Ap #589-7156 Donec St.', N'Unit 5', N'Rathenow', N'ON', N'(236)445-9209')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (48, N'4627 Lacus. Rd.', N'Unit 5', N'Campos dos Goytacazes', N'ON', N'(204)712-0787')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (49, N'Ap #500-7597 Convallis Ave', N'Unit 5', N'Lloydminster', N'ON', N'(403)787-5204')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (50, N'Ap #125-8932 Risus. Av.', N'Unit 5', N'Follina', N'ON', N'(780)814-0206')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (51, N'P.O. Box 770, 401 Nunc St.', N'Unit 1 ', N'Sevilla', N'ON', N'(236)530-1649')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (52, N'5521 Donec Ave', N'Unit 1 ', N'Saint-Luc', N'ON', N'(204)110-6962')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (53, N'5206 Placerat Rd.', N'Unit 1 ', N'Lille', N'ON', N'(403)829-8482')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (54, N'P.O. Box 341, 4341 Cras Avenue', N'Unit 1 ', N'Edmundston', N'ON', N'(780)402-0707')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (55, N'Ap #828-8983 Semper Rd.', N'Unit 1 ', N'Clarksville', N'ON', N'(236)913-1859')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (56, N'508-5489 Eros Ave', N'Unit 1 ', N'Oostkerke', N'ON', N'(204)634-6076')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (57, N'855-7182 Tempus St.', N'Unit 1 ', N'Duque de Caxias', N'ON', N'(403)599-6963')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (58, N'Ap #307-9552 Vestibulum Ave', N'Unit 1 ', N'Lithgow', N'ON', N'(780)528-5297')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (59, N'366 Magna. Road', N'Unit 1 ', N'Broken Arrow', N'ON', N'(236)825-5651')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (60, N'9017 Cras Av.', N'Unit 1 ', N'Macklin', N'ON', N'(204)143-4311')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (61, N'P.O. Box 871, 1586 Mauris Ave', N' Unit 2 ', N'San Damiano al Colle', N'ON', N'(403)823-8059')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (62, N'6305 A St.', N' Unit 2 ', N'Sant''Eusanio Forconese', N'ON', N'(780)528-9968')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (63, N'Ap #357-3252 Est Avenue', N' Unit 2 ', N'Hove', N'ON', N'(236)779-6329')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (64, N'4420 Hendrerit Rd.', N' Unit 2 ', N'VTM', N'ON', N'(204)512-3387')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (65, N'9825 Id Avenue', N' Unit 2 ', N'Casper', N'ON', N'(403)751-6653')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (66, N'7628 Vitae, St.', N' Unit 2 ', N'Arvier', N'ON', N'(780)636-4787')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (67, N'1881 Eu Road', N' Unit 2 ', N'Torgny', N'ON', N'(236)269-8469')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (68, N'695-4017 Sed Rd.', N' Unit 2 ', N'Ongole', N'ON', N'(204)329-8432')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (69, N'P.O. Box 412, 454 Ornare. Av.', N' Unit 2 ', N'Gravelbourg', N'ON', N'(403)519-4482')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (70, N'1710 Pede. Av.', N' Unit 2 ', N'Monticelli d''Ongina', N'ON', N'(780)291-5265')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (71, N'Ap #259-7958 Sed Avenue', N' Unit 3 ', N'Lo Barnechea', N'ON', N'(236)711-9139')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (72, N'963-827 Lacus. Rd.', N' Unit 3 ', N'Stornaway', N'ON', N'(204)513-0037')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (73, N'P.O. Box 100, 5937 Lobortis. Ave', N' Unit 3 ', N'Wardha', N'ON', N'(403)275-7719')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (74, N'Ap #351-7905 Sed Av.', N' Unit 3 ', N'Zonhoven', N'ON', N'(780)769-1769')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (75, N'156-5349 Nullam Rd.', N' Unit 3 ', N'Dundee', N'ON', N'(236)481-8591')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (76, N'826-7616 Facilisi. Avenue', N' Unit 3 ', N'Kukatpalle', N'ON', N'(204)122-7468')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (77, N'5067 At St.', N' Unit 3 ', N'Maiduguri', N'ON', N'(403)614-2671')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (78, N'Ap #179-9087 Ante, Avenue', N' Unit 3 ', N'Hudiksvall', N'ON', N'(780)746-7161')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (79, N'P.O. Box 218, 8454 Faucibus Rd.', N' Unit 3 ', N'Bremerhaven', N'ON', N'(236)448-7293')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (80, N'1636 Aenean St.', N' Unit 3 ', N'Bodmin', N'ON', N'(204)406-3612')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (81, N'1491 Est, Ave', N'Unit 4 ', N'Vance', N'ON', N'(403)764-3245')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (82, N'Ap #192-5716 Neque Avenue', N'Unit 4 ', N'Houtain-le-Val', N'ON', N'(780)221-8499')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (83, N'836-7957 Donec Av.', N'Unit 4 ', N'Kurnool', N'ON', N'(236)485-7303')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (84, N'Ap #808-2192 Est Road', N'Unit 4 ', N'Glendon', N'ON', N'(204)600-7668')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (85, N'Ap #927-3392 Orci Av.', N'Unit 4 ', N'Birmingham', N'ON', N'(403)602-8982')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (86, N'Ap #764-9031 Congue Road', N'Unit 4 ', N'Kenosha', N'ON', N'(780)790-2523')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (87, N'709-3342 Neque Rd.', N'Unit 4 ', N'Warisoulx', N'ON', N'(236)888-8134')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (88, N'3348 Rutrum. St.', N'Unit 4 ', N'Montignoso', N'ON', N'(204)824-3546')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (89, N'Ap #966-8643 Et Street', N'Unit 4 ', N'Thon', N'ON', N'(403)758-9622')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (90, N'1807 Eget, Rd.', N'Unit 4 ', N'Wolfsberg', N'ON', N'(780)663-0513')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (91, N'P.O. Box 362, 2818 Erat. Av.', N'Unit 5', N'Anapolis', N'ON', N'(236)735-2923')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (92, N'Ap #320-8516 Facilisis Road', N'Unit 5', N'Rio Hurtado', N'ON', N'(204)508-4586')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (93, N'7540 Ac Rd.', N'Unit 5', N'Mosciano Sant''Angelo', N'ON', N'(403)449-3527')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (94, N'3532 Nullam Street', N'Unit 5', N'Helkijn', N'ON', N'(780)772-2826')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (95, N'5735 Curabitur Rd.', N'Unit 5', N'Ravenstein', N'ON', N'(236)554-4587')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (96, N'9265 Diam. St.', N'Unit 5', N'Qualicum Beach', N'ON', N'(204)654-0636')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (97, N'Ap #254-5548 Lacinia Rd.', N'Unit 5', N'Lions Bay', N'ON', N'(403)106-8760')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (98, N'6874 Egestas Road', N'Unit 5', N'Verdun', N'ON', N'(780)165-2013')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (99, N'5626 Ultrices Ave', N'Unit 5', N'Billings', N'ON', N'(236)721-7265')
GO
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (100, N'7063 Ornare, Rd.', N'Unit 5', N'Silverton', N'ON', N'(204)869-9986')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (101, N'299 Doon Drive', N'', N'Kitchener', N'ON', N'519-555-6644 ')
INSERT [dbo].[Address] ([addressID], [addressLine1], [addressLine2], [city], [province], [numPhone]) VALUES (102, N'1701 Warp Dr.', N'', N'Guelph', N'ON', N'519-555-8686 ')
SET IDENTITY_INSERT [dbo].[Address] OFF
SET IDENTITY_INSERT [dbo].[Appointment] ON 

INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (1, CAST(N'2019-04-04T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (2, CAST(N'2018-05-04T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (3, CAST(N'2017-06-24T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (4, CAST(N'2017-08-17T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (5, CAST(N'2017-05-05T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (6, CAST(N'2018-04-18T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (7, CAST(N'2018-03-30T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (8, CAST(N'2017-06-30T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (9, CAST(N'2018-10-20T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (10, CAST(N'2017-07-05T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (11, CAST(N'2018-01-27T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (12, CAST(N'2018-07-31T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (13, CAST(N'2018-12-25T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (14, CAST(N'2018-07-31T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (15, CAST(N'2019-02-09T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (16, CAST(N'2017-09-20T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (17, CAST(N'2017-07-31T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (18, CAST(N'2017-09-01T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (19, CAST(N'2017-08-24T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (20, CAST(N'2018-08-20T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (21, CAST(N'2017-09-26T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (22, CAST(N'2017-09-05T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (23, CAST(N'2018-07-19T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (24, CAST(N'2018-08-12T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (25, CAST(N'2018-02-28T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (26, CAST(N'2018-12-01T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (27, CAST(N'2017-11-23T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (28, CAST(N'2019-02-14T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (29, CAST(N'2018-11-15T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (30, CAST(N'2018-10-21T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (31, CAST(N'2018-07-14T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (32, CAST(N'2017-10-31T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (33, CAST(N'2017-12-18T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (34, CAST(N'2017-06-01T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (35, CAST(N'2018-03-14T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (36, CAST(N'2019-01-03T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (37, CAST(N'2017-08-17T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (38, CAST(N'2018-12-17T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (39, CAST(N'2018-11-14T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (40, CAST(N'2018-11-23T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (41, CAST(N'2018-12-04T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (42, CAST(N'2018-12-03T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (43, CAST(N'2017-09-04T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (44, CAST(N'2017-11-30T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (45, CAST(N'2017-07-08T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (46, CAST(N'2018-02-05T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (47, CAST(N'2018-01-26T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (48, CAST(N'2017-09-25T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (49, CAST(N'2019-04-19T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (50, CAST(N'2018-04-11T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (51, CAST(N'2017-05-18T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (52, CAST(N'2018-01-12T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (53, CAST(N'2017-08-28T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (54, CAST(N'2019-03-31T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (55, CAST(N'2018-02-07T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (56, CAST(N'2017-08-02T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (57, CAST(N'2017-10-13T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (58, CAST(N'2017-11-12T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (59, CAST(N'2017-08-11T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (60, CAST(N'2017-05-25T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (61, CAST(N'2018-03-10T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (62, CAST(N'2017-12-04T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (63, CAST(N'2018-09-20T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (64, CAST(N'2017-08-26T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (65, CAST(N'2018-03-16T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (66, CAST(N'2018-11-05T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (67, CAST(N'2018-10-02T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (68, CAST(N'2018-04-08T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (69, CAST(N'2017-08-31T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (70, CAST(N'2018-10-22T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (71, CAST(N'2018-07-26T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (72, CAST(N'2018-07-10T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (73, CAST(N'2017-05-02T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (74, CAST(N'2018-11-23T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (75, CAST(N'2019-04-15T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (76, CAST(N'2018-09-06T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (77, CAST(N'2017-10-06T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (78, CAST(N'2017-04-24T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (79, CAST(N'2018-05-19T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (80, CAST(N'2018-05-05T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (81, CAST(N'2018-09-26T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (82, CAST(N'2018-08-28T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (83, CAST(N'2017-08-31T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (84, CAST(N'2018-11-15T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (85, CAST(N'2017-06-28T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (86, CAST(N'2018-08-07T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (87, CAST(N'2017-10-07T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (88, CAST(N'2017-06-12T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (89, CAST(N'2018-03-09T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (90, CAST(N'2017-05-08T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (91, CAST(N'2018-08-16T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (92, CAST(N'2017-04-26T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (93, CAST(N'2017-10-09T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (94, CAST(N'2018-08-10T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (95, CAST(N'2017-12-19T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (96, CAST(N'2017-12-05T10:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (97, CAST(N'2017-09-03T11:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (98, CAST(N'2019-02-11T12:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (99, CAST(N'2017-12-04T13:00:00.000' AS DateTime))
GO
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (100, CAST(N'2017-10-15T14:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (101, CAST(N'2018-04-26T15:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (102, CAST(N'2018-04-26T13:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (103, CAST(N'2017-12-04T15:00:00.000' AS DateTime))
INSERT [dbo].[Appointment] ([appointmentID], [dateAndTime]) VALUES (104, CAST(N'2017-12-15T11:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Appointment] OFF
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (1, 1)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (2, 2)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (3, 3)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (4, 4)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (5, 5)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (6, 6)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (7, 7)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (8, 8)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (9, 9)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (10, 10)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (11, 11)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (12, 12)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (13, 13)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (14, 14)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (15, 15)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (16, 16)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (17, 17)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (18, 18)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (19, 19)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (20, 20)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (21, 21)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (22, 22)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (23, 23)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (24, 24)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (25, 25)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (26, 26)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (27, 27)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (28, 28)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (29, 29)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (30, 30)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (31, 31)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (32, 32)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (33, 33)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (34, 34)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (35, 35)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (36, 36)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (37, 37)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (38, 38)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (39, 39)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (40, 40)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (41, 41)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (42, 42)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (43, 43)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (44, 44)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (45, 45)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (46, 46)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (47, 47)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (48, 48)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (49, 49)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (50, 50)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (51, 51)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (52, 52)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (53, 53)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (54, 54)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (55, 55)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (56, 56)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (57, 57)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (58, 58)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (59, 59)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (60, 60)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (61, 61)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (62, 62)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (63, 63)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (64, 64)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (65, 65)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (66, 66)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (67, 67)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (68, 68)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (69, 69)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (70, 70)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (71, 71)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (72, 72)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (73, 73)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (74, 74)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (75, 75)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (76, 76)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (77, 77)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (78, 78)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (79, 79)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (80, 80)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (81, 81)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (82, 82)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (83, 83)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (84, 84)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (85, 85)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (86, 86)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (87, 87)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (88, 88)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (89, 89)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (90, 90)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (91, 91)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (92, 92)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (93, 93)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (94, 94)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (95, 95)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (96, 96)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (97, 97)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (98, 98)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (99, 99)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (100, 100)
GO
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (101, 101)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (102, 101)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (103, 102)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (104, 102)
INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) VALUES (104, 103)
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (1, 1, 1, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (2, 2, 2, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (3, 3, 3, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (4, 4, 4, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (5, 5, 5, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (6, 6, 6, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (7, 7, 7, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (8, 8, 8, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (9, 9, 9, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (10, 10, 10, 1, 1)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (11, 11, 11, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (12, 12, 12, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (13, 13, 13, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (14, 14, 14, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (15, 15, 15, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (16, 16, 16, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (17, 17, 17, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (18, 18, 18, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (19, 19, 19, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (20, 20, 20, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (21, 21, 21, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (22, 22, 22, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (23, 23, 23, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (24, 24, 24, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (25, 25, 25, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (26, 26, 26, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (27, 27, 27, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (28, 28, 28, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (29, 29, 29, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (30, 30, 30, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (31, 31, 31, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (32, 32, 32, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (34, 34, 34, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (35, 35, 35, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (36, 36, 36, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (37, 37, 37, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (38, 38, 38, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (39, 39, 39, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (40, 40, 40, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (41, 41, 41, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (42, 42, 42, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (43, 43, 43, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (44, 44, 44, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (45, 45, 45, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (46, 46, 46, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (47, 47, 47, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (48, 48, 48, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (49, 49, 49, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (50, 50, 50, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (51, 51, 51, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (52, 52, 52, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (53, 53, 53, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (54, 54, 54, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (55, 55, 55, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (56, 56, 56, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (57, 57, 57, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (58, 58, 58, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (59, 59, 59, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (60, 60, 60, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (61, 61, 61, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (63, 63, 63, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (64, 64, 64, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (65, 65, 65, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (66, 66, 66, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (67, 67, 67, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (68, 68, 68, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (69, 69, 69, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (70, 70, 70, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (71, 71, 71, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (72, 72, 72, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (73, 73, 73, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (74, 74, 74, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (75, 75, 75, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (76, 76, 76, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (77, 77, 77, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (78, 78, 78, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (79, 79, 79, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (80, 80, 80, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (81, 81, 81, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (82, 82, 82, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (83, 83, 83, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (84, 84, 84, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (85, 85, 85, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (86, 86, 86, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (87, 87, 87, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (88, 88, 88, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (89, 89, 89, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (90, 90, 90, 2, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (91, 91, 91, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (92, 92, 92, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (93, 93, 93, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (94, 94, 94, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (97, 97, 97, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (98, 98, 98, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (100, 100, 100, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (101, 2, 505, 1, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (102, 101, 101, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (103, 101, 101, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (104, 101, 102, 0, 0)
GO
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (112, 102, 103, 3, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (113, 102, 104, 0, 0)
INSERT [dbo].[Bill] ([billID], [patientID], [appointmentID], [flagRecall], [statusDone]) VALUES (114, 103, 104, 0, 0)
SET IDENTITY_INSERT [dbo].[Bill] OFF
SET IDENTITY_INSERT [dbo].[Patient] ON 

INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (1, N'7876259275ZW', N'Landry', N'Yen', N'W', CAST(N'1950-05-30T00:00:00.000' AS DateTime), N'M         ', N'            ', 1)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (2, N'6822576213MZ', N'Randolph', N'Hope', N'E', CAST(N'1951-04-02T00:00:00.000' AS DateTime), N'M         ', N'            ', 2)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (3, N'6512519888RU', N'Frank', N'Gary', N'Q', CAST(N'1952-12-21T00:00:00.000' AS DateTime), N'M         ', N'            ', 3)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (4, N'4289284256RX', N'Blair', N'Perry', N'D', CAST(N'1953-10-09T00:00:00.000' AS DateTime), N'M         ', N'            ', 4)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (5, N'9887874384TU', N'Hatfield', N'Driscoll', N'C', CAST(N'1954-08-05T00:00:00.000' AS DateTime), N'M         ', N'            ', 5)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (6, N'6744559967VC', N'Villarreal', N'Azalia', N'X', CAST(N'1955-01-11T00:00:00.000' AS DateTime), N'M         ', N'            ', 6)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (7, N'9336115168YB', N'Ayers', N'Jason', N'L', CAST(N'1956-04-25T00:00:00.000' AS DateTime), N'M         ', N'            ', 7)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (8, N'9853542893QB', N'Pollard', N'Ryan', N'D', CAST(N'1957-10-29T00:00:00.000' AS DateTime), N'M         ', N'            ', 8)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (9, N'9667856976PE', N'Kinney', N'Aurelia', N'I', CAST(N'1958-02-08T00:00:00.000' AS DateTime), N'M         ', N'            ', 9)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (10, N'1663437992TE', N'Alston', N'Baxter', N'A', CAST(N'1959-10-24T00:00:00.000' AS DateTime), N'M         ', N'            ', 10)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (11, N'9174527719NB', N'Mccray', N'Cody', N'I', CAST(N'1960-04-03T00:00:00.000' AS DateTime), N'F         ', N'            ', 11)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (12, N'2217758589EH', N'Alvarado', N'Dorothy', N'V', CAST(N'1961-11-25T00:00:00.000' AS DateTime), N'F         ', N'            ', 12)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (13, N'7999519219CF', N'Gallegos', N'Kelsie', N'I', CAST(N'1962-10-27T00:00:00.000' AS DateTime), N'F         ', N'            ', 13)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (14, N'8315832228NK', N'May', N'Oren', N'J', CAST(N'1963-06-07T00:00:00.000' AS DateTime), N'F         ', N'            ', 14)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (15, N'4524596679PE', N'Solis', N'Venus', N'O', CAST(N'1964-09-05T00:00:00.000' AS DateTime), N'F         ', N'            ', 15)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (16, N'3155329796ZZ', N'Sutton', N'Arthur', N'Y', CAST(N'1965-01-16T00:00:00.000' AS DateTime), N'F         ', N'            ', 16)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (17, N'6398723418WR', N'Jenkins', N'Alfonso', N'N', CAST(N'1966-09-11T00:00:00.000' AS DateTime), N'F         ', N'            ', 17)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (18, N'5288158751QC', N'Giles', N'Amir', N'L', CAST(N'1967-04-23T00:00:00.000' AS DateTime), N'F         ', N'            ', 18)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (19, N'8442399284WQ', N'Whitfield', N'Linda', N'F', CAST(N'1968-07-30T00:00:00.000' AS DateTime), N'F         ', N'            ', 19)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (20, N'5578846521NR', N'Poole', N'Cooper', N'E', CAST(N'1969-08-31T00:00:00.000' AS DateTime), N'F         ', N'            ', 20)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (21, N'6334119731FF', N'Workman', N'Holly', N'J', CAST(N'1970-09-10T00:00:00.000' AS DateTime), N'M         ', N'            ', 21)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (22, N'5446398854YR', N'Farley', N'Yuri', N'C', CAST(N'1971-08-17T00:00:00.000' AS DateTime), N'M         ', N'            ', 22)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (23, N'1855383791QI', N'Barron', N'Amy', N'Q', CAST(N'1972-06-24T00:00:00.000' AS DateTime), N'M         ', N'            ', 23)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (24, N'4563841614TB', N'Cherry', N'Judah', N'N', CAST(N'1973-02-13T00:00:00.000' AS DateTime), N'M         ', N'            ', 24)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (25, N'6689492844UC', N'Hendricks', N'Raymond', N'A', CAST(N'1974-06-28T00:00:00.000' AS DateTime), N'M         ', N'            ', 25)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (26, N'6453437732PH', N'Norton', N'Bo', N'Z', CAST(N'1975-01-14T00:00:00.000' AS DateTime), N'M         ', N'            ', 26)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (27, N'8984985618BO', N'Giles', N'Hillary', N'K', CAST(N'1976-12-11T00:00:00.000' AS DateTime), N'M         ', N'            ', 27)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (28, N'6878296223RF', N'Willis', N'Alan', N'R', CAST(N'1977-02-12T00:00:00.000' AS DateTime), N'M         ', N'            ', 28)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (29, N'7787431879VN', N'Bartlett', N'Zahir', N'X', CAST(N'1978-09-20T00:00:00.000' AS DateTime), N'M         ', N'            ', 29)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (30, N'4264456231FF', N'Hayden', N'Xaviera', N'K', CAST(N'1979-03-22T00:00:00.000' AS DateTime), N'M         ', N'            ', 30)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (31, N'5314326211WN', N'Mcbride', N'Nash', N'J', CAST(N'1980-08-28T00:00:00.000' AS DateTime), N'F         ', N'            ', 31)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (32, N'6669731515BS', N'Hoover', N'Kevin', N'H', CAST(N'1981-02-22T00:00:00.000' AS DateTime), N'F         ', N'            ', 32)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (33, N'9174239832JX', N'Higgins', N'Gemma', N'C', CAST(N'1982-05-10T00:00:00.000' AS DateTime), N'F         ', N'            ', 33)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (34, N'3445855742ZS', N'Frederick', N'Ian', N'N', CAST(N'1983-05-04T00:00:00.000' AS DateTime), N'F         ', N'            ', 34)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (35, N'4725246173GZ', N'Douglas', N'Jesse', N'P', CAST(N'1984-04-20T00:00:00.000' AS DateTime), N'F         ', N'            ', 35)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (36, N'3779492734JV', N'Bradshaw', N'Herrod', N'P', CAST(N'1985-06-06T00:00:00.000' AS DateTime), N'F         ', N'            ', 36)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (37, N'1327437472EQ', N'House', N'Gisela', N'U', CAST(N'1986-11-12T00:00:00.000' AS DateTime), N'F         ', N'            ', 37)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (38, N'5926929562PV', N'Delacruz', N'Cruz', N'F', CAST(N'1987-02-08T00:00:00.000' AS DateTime), N'F         ', N'            ', 38)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (39, N'5126192782FF', N'Chan', N'Arsenio', N'Y', CAST(N'1988-05-09T00:00:00.000' AS DateTime), N'F         ', N'            ', 39)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (40, N'4272726342AJ', N'Lawson', N'Leslie', N'M', CAST(N'1989-08-17T00:00:00.000' AS DateTime), N'F         ', N'            ', 40)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (41, N'5836433456CU', N'Erickson', N'Cora', N'R', CAST(N'1990-06-17T00:00:00.000' AS DateTime), N'M         ', N'            ', 41)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (42, N'1113583682XA', N'Prince', N'Moana', N'M', CAST(N'1950-11-20T00:00:00.000' AS DateTime), N'M         ', N'            ', 42)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (43, N'4411569571AG', N'Guerra', N'Carson', N'A', CAST(N'1951-11-21T00:00:00.000' AS DateTime), N'M         ', N'            ', 43)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (44, N'9184475464AS', N'Horne', N'Nathan', N'K', CAST(N'1952-09-08T00:00:00.000' AS DateTime), N'M         ', N'            ', 44)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (45, N'3825648198PO', N'Stevens', N'Brennan', N'O', CAST(N'1953-09-09T00:00:00.000' AS DateTime), N'M         ', N'            ', 45)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (46, N'5456299881AV', N'Kim', N'Tashya', N'L', CAST(N'1954-07-03T00:00:00.000' AS DateTime), N'M         ', N'            ', 46)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (47, N'5383939226BB', N'Robbins', N'Hall', N'L', CAST(N'1955-11-18T00:00:00.000' AS DateTime), N'M         ', N'            ', 47)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (48, N'3431785296ST', N'Cummings', N'Flynn', N'H', CAST(N'1956-06-05T00:00:00.000' AS DateTime), N'M         ', N'            ', 48)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (49, N'4672665263SK', N'Guy', N'Liberty', N'P', CAST(N'1957-11-21T00:00:00.000' AS DateTime), N'M         ', N'            ', 49)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (50, N'6298893296XF', N'Cannon', N'Elizabeth', N'U', CAST(N'1958-05-04T00:00:00.000' AS DateTime), N'M         ', N'            ', 50)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (51, N'2653439842MM', N'Moody', N'Bell', N'K', CAST(N'1959-12-15T00:00:00.000' AS DateTime), N'F         ', N'            ', 51)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (52, N'1145432179CD', N'Santos', N'Nash', N'S', CAST(N'1960-10-21T00:00:00.000' AS DateTime), N'F         ', N'            ', 52)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (53, N'7754362537UV', N'Levine', N'Boris', N'J', CAST(N'1961-10-27T00:00:00.000' AS DateTime), N'F         ', N'            ', 53)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (54, N'1723128364FM', N'Trevino', N'Chloe', N'C', CAST(N'1962-02-28T00:00:00.000' AS DateTime), N'F         ', N'            ', 54)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (55, N'7743638342VK', N'Payne', N'Hilary', N'D', CAST(N'1963-09-26T00:00:00.000' AS DateTime), N'F         ', N'            ', 55)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (56, N'6414152286BN', N'Castro', N'Whitney', N'L', CAST(N'1964-01-17T00:00:00.000' AS DateTime), N'F         ', N'            ', 56)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (57, N'7515138773KR', N'Strickland', N'Conan', N'R', CAST(N'1965-06-09T00:00:00.000' AS DateTime), N'F         ', N'            ', 57)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (58, N'2976249763DL', N'Zimmerman', N'Gage', N'V', CAST(N'1966-08-20T00:00:00.000' AS DateTime), N'F         ', N'            ', 58)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (59, N'7913391387NK', N'Pugh', N'Lareina', N'X', CAST(N'1967-06-13T00:00:00.000' AS DateTime), N'F         ', N'            ', 59)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (60, N'5189878182OC', N'Sparks', N'Brynn', N'T', CAST(N'1968-11-10T00:00:00.000' AS DateTime), N'F         ', N'            ', 60)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (61, N'9831464747ZH', N'Burgess', N'Alice', N'I', CAST(N'1969-08-27T00:00:00.000' AS DateTime), N'M         ', N'            ', 61)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (62, N'3989815885LL', N'Herman', N'Carolyn', N'J', CAST(N'1970-12-19T00:00:00.000' AS DateTime), N'M         ', N'            ', 62)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (63, N'8571649132IE', N'Parrish', N'Leslie', N'C', CAST(N'1971-10-30T00:00:00.000' AS DateTime), N'M         ', N'            ', 63)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (64, N'8844819349PD', N'Stein', N'Len', N'T', CAST(N'1972-02-04T00:00:00.000' AS DateTime), N'M         ', N'            ', 64)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (65, N'1467485176XZ', N'Williams', N'Quincy', N'W', CAST(N'1973-10-12T00:00:00.000' AS DateTime), N'M         ', N'            ', 65)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (66, N'8653395424VY', N'Moore', N'Barclay', N'I', CAST(N'1974-12-13T00:00:00.000' AS DateTime), N'M         ', N'            ', 66)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (67, N'9552129599SA', N'Jimenez', N'Chantale', N'Z', CAST(N'1975-01-16T00:00:00.000' AS DateTime), N'M         ', N'            ', 67)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (68, N'4714468223YT', N'Holloway', N'Vladimir', N'C', CAST(N'1976-03-20T00:00:00.000' AS DateTime), N'M         ', N'            ', 68)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (69, N'4654179631ZA', N'Mclean', N'Roary', N'S', CAST(N'1977-04-09T00:00:00.000' AS DateTime), N'M         ', N'            ', 69)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (70, N'2729591732RS', N'Horton', N'Regan', N'V', CAST(N'1978-10-23T00:00:00.000' AS DateTime), N'M         ', N'            ', 70)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (71, N'4839236627RI', N'Solis', N'Chastity', N'A', CAST(N'1979-04-03T00:00:00.000' AS DateTime), N'F         ', N'            ', 71)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (72, N'7997722129AN', N'Jennings', N'Boris', N'O', CAST(N'1980-05-11T00:00:00.000' AS DateTime), N'F         ', N'            ', 72)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (73, N'1413142568DL', N'Knowles', N'Cathleen', N'V', CAST(N'1981-07-21T00:00:00.000' AS DateTime), N'F         ', N'            ', 73)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (74, N'5356827474TW', N'French', N'Bruno', N'N', CAST(N'1982-09-25T00:00:00.000' AS DateTime), N'F         ', N'            ', 74)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (75, N'8398361599IW', N'Heath', N'Marny', N'Z', CAST(N'1983-11-26T00:00:00.000' AS DateTime), N'F         ', N'            ', 75)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (76, N'5897984225ZM', N'Fox', N'Hillary', N'I', CAST(N'1984-07-26T00:00:00.000' AS DateTime), N'F         ', N'            ', 76)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (77, N'1147555269OU', N'Swanson', N'Wylie', N'Z', CAST(N'1985-08-23T00:00:00.000' AS DateTime), N'F         ', N'            ', 77)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (78, N'8826694658IB', N'Mcclure', N'Lesley', N'M', CAST(N'1986-06-14T00:00:00.000' AS DateTime), N'F         ', N'            ', 78)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (79, N'9869949668QI', N'Randolph', N'Adam', N'S', CAST(N'1987-12-11T00:00:00.000' AS DateTime), N'F         ', N'            ', 79)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (80, N'1156166827BB', N'Fitzgerald', N'Charles', N'B', CAST(N'1988-04-21T00:00:00.000' AS DateTime), N'F         ', N'            ', 80)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (81, N'2325877749DN', N'Hewitt', N'Sydney', N'X', CAST(N'1989-06-24T00:00:00.000' AS DateTime), N'M         ', N'            ', 81)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (82, N'5494713656UK', N'Villarreal', N'Cody', N'Z', CAST(N'1990-06-17T00:00:00.000' AS DateTime), N'M         ', N'            ', 82)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (83, N'3527428635TR', N'Hardin', N'Madison', N'P', CAST(N'1991-07-07T00:00:00.000' AS DateTime), N'M         ', N'            ', 83)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (84, N'9882924531UQ', N'Mckee', N'Elijah', N'Q', CAST(N'1992-08-27T00:00:00.000' AS DateTime), N'M         ', N'            ', 84)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (85, N'2771143693FS', N'Rose', N'Guinevere', N'W', CAST(N'1993-02-12T00:00:00.000' AS DateTime), N'M         ', N'            ', 85)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (86, N'4553478526EV', N'Graves', N'Barrett', N'G', CAST(N'1994-03-20T00:00:00.000' AS DateTime), N'M         ', N'            ', 86)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (87, N'7774165689TI', N'Slater', N'Kadeem', N'T', CAST(N'1995-05-28T00:00:00.000' AS DateTime), N'M         ', N'            ', 87)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (88, N'9962476625LB', N'Wiggins', N'Sylvester', N'E', CAST(N'1996-11-09T00:00:00.000' AS DateTime), N'M         ', N'            ', 88)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (89, N'3489426142UX', N'Faulkner', N'Knox', N'H', CAST(N'1997-01-29T00:00:00.000' AS DateTime), N'M         ', N'            ', 89)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (90, N'3136185753GY', N'Reyes', N'Alisa', N'V', CAST(N'1998-03-18T00:00:00.000' AS DateTime), N'M         ', N'            ', 90)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (91, N'7569216326PQ', N'Turner', N'Sonya', N'J', CAST(N'1999-02-17T00:00:00.000' AS DateTime), N'F         ', N'            ', 91)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (92, N'3694237833MD', N'Gaines', N'Ciara', N'E', CAST(N'2000-01-03T00:00:00.000' AS DateTime), N'F         ', N'            ', 92)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (93, N'5866656862JG', N'Fields', N'Quinlan', N'M', CAST(N'2001-08-05T00:00:00.000' AS DateTime), N'F         ', N'            ', 93)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (94, N'9996647452JG', N'Valenzuela', N'Guy', N'G', CAST(N'2002-01-11T00:00:00.000' AS DateTime), N'F         ', N'            ', 94)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (95, N'5932345535MY', N'Barker', N'Timon', N'T', CAST(N'2003-07-20T00:00:00.000' AS DateTime), N'F         ', N'            ', 95)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (96, N'5813177266GV', N'Tate', N'MacKensie', N'Q', CAST(N'2004-09-23T00:00:00.000' AS DateTime), N'F         ', N'            ', 96)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (97, N'3138947561GO', N'Hopkins', N'Gage', N'A', CAST(N'2005-01-05T00:00:00.000' AS DateTime), N'F         ', N'            ', 97)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (98, N'8829194441GG', N'Neal', N'Tasha', N'W', CAST(N'2006-08-27T00:00:00.000' AS DateTime), N'F         ', N'            ', 98)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (99, N'8935164528WG', N'Dalton', N'Laith', N'D', CAST(N'2007-01-10T00:00:00.000' AS DateTime), N'F         ', N'            ', 99)
GO
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (100, N'2457456291KX', N'Garrison', N'Chadwick', N'G', CAST(N'2008-04-27T00:00:00.000' AS DateTime), N'F         ', N'            ', 100)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (101, N'1234567890AB', N'Clarke', N'Sean', N'M', CAST(N'1970-06-15T00:00:00.000' AS DateTime), N'M         ', N'            ', 101)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (102, N'1023456789LK', N'Picard', N'Jean-Luc', N'T', CAST(N'1975-04-02T00:00:00.000' AS DateTime), N'M         ', N'            ', 102)
INSERT [dbo].[Patient] ([patientID], [HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) VALUES (103, N'1223456789ZK', N'Picard', N'Jennifer', N'R', CAST(N'2008-03-15T00:00:00.000' AS DateTime), N'F         ', N'1023456789LK', 102)
SET IDENTITY_INSERT [dbo].[Patient] OFF
SET IDENTITY_INSERT [dbo].[Service] ON 

INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (1, N'K799', CAST(N'1950-10-17T00:00:00.000' AS DateTime), 326.01)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (2, N'D728', CAST(N'1952-05-07T00:00:00.000' AS DateTime), 352.88)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (3, N'J266', CAST(N'1954-07-29T00:00:00.000' AS DateTime), 481.18)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (4, N'Q327', CAST(N'1956-07-21T00:00:00.000' AS DateTime), 84.28)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (5, N'M479', CAST(N'1958-02-13T00:00:00.000' AS DateTime), 198.2)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (6, N'X675', CAST(N'1960-03-07T00:00:00.000' AS DateTime), 486.26)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (7, N'Q255', CAST(N'1962-07-31T00:00:00.000' AS DateTime), 129.34)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (8, N'M225', CAST(N'1964-05-18T00:00:00.000' AS DateTime), 407.04)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (9, N'Q128', CAST(N'1966-07-14T00:00:00.000' AS DateTime), 217.78)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (10, N'W256', CAST(N'1968-09-24T00:00:00.000' AS DateTime), 359.88)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (11, N'J167', CAST(N'1970-04-18T00:00:00.000' AS DateTime), 349.04)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (12, N'N914', CAST(N'1972-10-19T00:00:00.000' AS DateTime), 479.72)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (13, N'N429', CAST(N'1974-07-10T00:00:00.000' AS DateTime), 131.92)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (14, N'B273', CAST(N'1976-01-10T00:00:00.000' AS DateTime), 75.08)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (15, N'J627', CAST(N'1978-09-18T00:00:00.000' AS DateTime), 377.75)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (16, N'Y597', CAST(N'1980-09-11T00:00:00.000' AS DateTime), 190.84)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (17, N'M419', CAST(N'1982-04-20T00:00:00.000' AS DateTime), 389.69)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (18, N'Y675', CAST(N'1984-07-18T00:00:00.000' AS DateTime), 114.59)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (19, N'E215', CAST(N'1986-09-08T00:00:00.000' AS DateTime), 369.36)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (20, N'A882', CAST(N'1988-03-12T00:00:00.000' AS DateTime), 220.48)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (21, N'M511', CAST(N'1990-12-03T00:00:00.000' AS DateTime), 447.31)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (22, N'U648', CAST(N'1992-08-14T00:00:00.000' AS DateTime), 230.46)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (23, N'X183', CAST(N'1994-07-30T00:00:00.000' AS DateTime), 89.92)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (24, N'S348', CAST(N'1996-12-06T00:00:00.000' AS DateTime), 57.56)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (25, N'N878', CAST(N'1998-01-07T00:00:00.000' AS DateTime), 288.45)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (26, N'U785', CAST(N'2000-05-10T00:00:00.000' AS DateTime), 402.21)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (27, N'Y692', CAST(N'1950-07-12T00:00:00.000' AS DateTime), 197.71)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (28, N'W238', CAST(N'1952-02-27T00:00:00.000' AS DateTime), 157.33)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (29, N'A435', CAST(N'1954-07-17T00:00:00.000' AS DateTime), 138.39)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (30, N'W766', CAST(N'1956-09-19T00:00:00.000' AS DateTime), 159.64)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (31, N'H231', CAST(N'1958-09-11T00:00:00.000' AS DateTime), 319.43)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (32, N'B694', CAST(N'1960-07-06T00:00:00.000' AS DateTime), 425.25)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (33, N'E998', CAST(N'1962-01-29T00:00:00.000' AS DateTime), 396.43)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (34, N'N599', CAST(N'1964-02-25T00:00:00.000' AS DateTime), 33.57)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (35, N'P294', CAST(N'1966-03-07T00:00:00.000' AS DateTime), 471)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (36, N'E879', CAST(N'1968-07-20T00:00:00.000' AS DateTime), 247.04)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (37, N'U464', CAST(N'1970-12-06T00:00:00.000' AS DateTime), 49.16)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (38, N'U648', CAST(N'1972-12-22T00:00:00.000' AS DateTime), 58.75)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (39, N'O594', CAST(N'1974-08-03T00:00:00.000' AS DateTime), 492.71)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (40, N'L314', CAST(N'1976-01-23T00:00:00.000' AS DateTime), 334.32)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (41, N'W595', CAST(N'1978-08-18T00:00:00.000' AS DateTime), 118.96)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (42, N'F177', CAST(N'1980-09-18T00:00:00.000' AS DateTime), 438.37)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (43, N'B761', CAST(N'1982-08-21T00:00:00.000' AS DateTime), 478.31)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (44, N'P159', CAST(N'1984-06-22T00:00:00.000' AS DateTime), 121.31)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (45, N'H651', CAST(N'1986-07-02T00:00:00.000' AS DateTime), 141.34)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (46, N'G739', CAST(N'1988-02-22T00:00:00.000' AS DateTime), 399.92)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (47, N'U372', CAST(N'1990-07-08T00:00:00.000' AS DateTime), 139.06)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (48, N'K468', CAST(N'1992-10-30T00:00:00.000' AS DateTime), 68.3)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (49, N'F938', CAST(N'1994-06-11T00:00:00.000' AS DateTime), 363.16)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (50, N'Q784', CAST(N'1996-02-07T00:00:00.000' AS DateTime), 23.31)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (51, N'T428', CAST(N'1998-09-06T00:00:00.000' AS DateTime), 477.71)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (52, N'X428', CAST(N'2000-07-05T00:00:00.000' AS DateTime), 172.31)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (53, N'W941', CAST(N'1956-05-01T00:00:00.000' AS DateTime), 270.31)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (54, N'N239', CAST(N'1958-07-12T00:00:00.000' AS DateTime), 430.96)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (55, N'S228', CAST(N'1960-03-24T00:00:00.000' AS DateTime), 395.73)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (56, N'D926', CAST(N'1962-06-17T00:00:00.000' AS DateTime), 139.32)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (57, N'T398', CAST(N'1964-03-09T00:00:00.000' AS DateTime), 492.41)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (58, N'C183', CAST(N'1966-12-09T00:00:00.000' AS DateTime), 421.08)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (59, N'G279', CAST(N'1968-06-08T00:00:00.000' AS DateTime), 414.46)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (60, N'P565', CAST(N'1970-04-04T00:00:00.000' AS DateTime), 448.28)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (61, N'R227', CAST(N'1972-01-28T00:00:00.000' AS DateTime), 99.19)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (62, N'E845', CAST(N'1974-12-22T00:00:00.000' AS DateTime), 290.02)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (63, N'G219', CAST(N'1976-10-06T00:00:00.000' AS DateTime), 457.06)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (64, N'K945', CAST(N'1978-11-24T00:00:00.000' AS DateTime), 444.99)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (65, N'P742', CAST(N'1980-09-15T00:00:00.000' AS DateTime), 45.81)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (66, N'N414', CAST(N'1982-01-25T00:00:00.000' AS DateTime), 74.61)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (67, N'E264', CAST(N'1984-09-15T00:00:00.000' AS DateTime), 50.53)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (68, N'P278', CAST(N'1986-07-22T00:00:00.000' AS DateTime), 472.35)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (69, N'T653', CAST(N'1988-10-01T00:00:00.000' AS DateTime), 250.59)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (70, N'J123', CAST(N'1990-03-09T00:00:00.000' AS DateTime), 132.56)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (71, N'D165', CAST(N'1992-01-05T00:00:00.000' AS DateTime), 65.98)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (72, N'Y591', CAST(N'1994-03-07T00:00:00.000' AS DateTime), 84.78)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (73, N'L612', CAST(N'1996-05-05T00:00:00.000' AS DateTime), 192.21)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (74, N'O427', CAST(N'1998-06-02T00:00:00.000' AS DateTime), 415.3)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (75, N'D295', CAST(N'2000-08-09T00:00:00.000' AS DateTime), 299.35)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (76, N'N658', CAST(N'1956-02-26T00:00:00.000' AS DateTime), 33.72)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (77, N'Z163', CAST(N'1958-05-01T00:00:00.000' AS DateTime), 222.52)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (78, N'Q963', CAST(N'1960-11-23T00:00:00.000' AS DateTime), 377.59)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (79, N'I116', CAST(N'1962-06-11T00:00:00.000' AS DateTime), 191.69)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (80, N'F456', CAST(N'1964-07-29T00:00:00.000' AS DateTime), 45.78)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (81, N'F313', CAST(N'1966-02-17T00:00:00.000' AS DateTime), 156.32)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (82, N'W699', CAST(N'1968-04-18T00:00:00.000' AS DateTime), 207.82)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (83, N'W438', CAST(N'1970-09-09T00:00:00.000' AS DateTime), 470.25)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (84, N'C842', CAST(N'1972-04-29T00:00:00.000' AS DateTime), 228.62)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (85, N'A873', CAST(N'1974-08-01T00:00:00.000' AS DateTime), 421.48)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (86, N'H943', CAST(N'1976-12-30T00:00:00.000' AS DateTime), 201.54)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (87, N'B316', CAST(N'1978-08-10T00:00:00.000' AS DateTime), 276.09)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (88, N'K471', CAST(N'1980-12-28T00:00:00.000' AS DateTime), 315.39)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (89, N'Q698', CAST(N'1982-06-18T00:00:00.000' AS DateTime), 152.65)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (90, N'F699', CAST(N'1984-01-05T00:00:00.000' AS DateTime), 12.03)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (91, N'G425', CAST(N'1986-09-23T00:00:00.000' AS DateTime), 136.26)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (92, N'S481', CAST(N'1988-05-26T00:00:00.000' AS DateTime), 415.32)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (93, N'V261', CAST(N'1990-10-15T00:00:00.000' AS DateTime), 167.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (94, N'R336', CAST(N'1992-11-28T00:00:00.000' AS DateTime), 336.26)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (95, N'B919', CAST(N'1994-11-26T00:00:00.000' AS DateTime), 63.07)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (96, N'O187', CAST(N'1996-01-22T00:00:00.000' AS DateTime), 250.36)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (97, N'I699', CAST(N'1998-05-11T00:00:00.000' AS DateTime), 385.88)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (98, N'U246', CAST(N'2000-06-17T00:00:00.000' AS DateTime), 431.8)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (99, N'W736', CAST(N'2002-01-30T00:00:00.000' AS DateTime), 463.45)
GO
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (100, N'H627', CAST(N'2004-02-19T00:00:00.000' AS DateTime), 384.35)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (116, N'A005', CAST(N'2009-12-01T00:00:00.000' AS DateTime), 77.2)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (117, N'A115', CAST(N'2012-04-01T00:00:00.000' AS DateTime), 51.1)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (118, N'A555', CAST(N'2009-12-01T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (119, N'A556', CAST(N'2009-12-01T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (120, N'A557', CAST(N'2009-12-01T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (121, N'A600', CAST(N'2015-10-01T00:00:00.000' AS DateTime), 300.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (122, N'A605', CAST(N'2015-10-01T00:00:00.000' AS DateTime), 157)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (123, N'A665', CAST(N'2012-04-01T00:00:00.000' AS DateTime), 91.35)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (124, N'A917', CAST(N'2014-04-01T00:00:00.000' AS DateTime), 33.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (125, N'A927', CAST(N'2014-04-01T00:00:00.000' AS DateTime), 33.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (126, N'A937', CAST(N'2014-04-01T00:00:00.000' AS DateTime), 33.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (127, N'A947', CAST(N'2014-04-01T00:00:00.000' AS DateTime), 33.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (128, N'A957', CAST(N'2014-04-01T00:00:00.000' AS DateTime), 33.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (129, N'A967', CAST(N'2014-04-01T00:00:00.000' AS DateTime), 33.7)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (130, N'K738', CAST(N'2015-10-01T00:00:00.000' AS DateTime), 16)
INSERT [dbo].[Service] ([serviceID], [feeCode], [effectiveDate], [value]) VALUES (131, N'K739', CAST(N'2015-10-01T00:00:00.000' AS DateTime), 20.5)
SET IDENTITY_INSERT [dbo].[Service] OFF
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (1, 1, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (2, 2, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (3, 3, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (4, 4, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (5, 5, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (6, 6, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (7, 7, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (8, 8, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (9, 9, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (10, 1, 5, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (10, 10, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (11, 11, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (12, 12, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (13, 13, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (14, 14, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (15, 15, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (16, 16, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (17, 17, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (18, 18, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (19, 19, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (20, 20, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (21, 21, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (22, 22, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (23, 23, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (24, 24, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (25, 25, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (26, 26, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (27, 27, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (28, 28, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (29, 29, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (30, 30, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (31, 31, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (32, 32, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (34, 34, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (35, 35, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (36, 36, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (37, 37, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (38, 38, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (39, 39, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (40, 40, 1, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (41, 41, 3, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (42, 42, 2, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (43, 43, 1, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (44, 44, 3, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (45, 45, 2, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (46, 46, 3, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (47, 47, 2, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (48, 48, 2, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (49, 49, 1, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (50, 50, 2, N'FHCV')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (51, 51, 3, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (52, 52, 2, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (53, 53, 2, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (54, 54, 3, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (55, 55, 1, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (56, 56, 3, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (57, 57, 3, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (58, 58, 1, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (59, 59, 2, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (60, 60, 3, N'CMOH')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (61, 61, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (63, 63, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (64, 64, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (65, 65, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (66, 66, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (67, 67, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (68, 68, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (69, 69, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (70, 70, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (71, 71, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (72, 72, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (73, 73, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (74, 74, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (75, 75, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (76, 76, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (77, 77, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (78, 78, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (79, 79, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (80, 80, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (81, 81, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (82, 82, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (83, 83, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (84, 84, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (85, 85, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (85, 104, 1, N'UPRC')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (86, 86, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (87, 87, 3, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (88, 88, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (89, 89, 2, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (90, 90, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (91, 91, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (92, 92, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (93, 93, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (94, 94, 3, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (97, 97, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (98, 98, 1, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (100, 100, 2, N'DECL')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (118, 112, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (118, 113, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (118, 114, 1, N'FHCV')
GO
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (119, 112, 1, N'PAID')
INSERT [dbo].[ServiceLine] ([serviceID], [billID], [quantity], [status]) VALUES (120, 114, 1, N'CMOH')
ALTER TABLE [dbo].[AppointmentLine]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentLine_Appointment1] FOREIGN KEY([appointmentID])
REFERENCES [dbo].[Appointment] ([appointmentID])
GO
ALTER TABLE [dbo].[AppointmentLine] CHECK CONSTRAINT [FK_AppointmentLine_Appointment1]
GO
ALTER TABLE [dbo].[AppointmentLine]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentLine_Patient1] FOREIGN KEY([patientID])
REFERENCES [dbo].[Patient] ([patientID])
GO
ALTER TABLE [dbo].[AppointmentLine] CHECK CONSTRAINT [FK_AppointmentLine_Patient1]
GO
ALTER TABLE [dbo].[Bill]  WITH NOCHECK ADD  CONSTRAINT [FK_Bill_AppointmentLine] FOREIGN KEY([appointmentID], [patientID])
REFERENCES [dbo].[AppointmentLine] ([appointmentID], [patientID])
GO
ALTER TABLE [dbo].[Bill] NOCHECK CONSTRAINT [FK_Bill_AppointmentLine]
GO
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Address1] FOREIGN KEY([addressID])
REFERENCES [dbo].[Address] ([addressID])
GO
ALTER TABLE [dbo].[Patient] CHECK CONSTRAINT [FK_Patient_Address1]
GO
ALTER TABLE [dbo].[ServiceLine]  WITH CHECK ADD  CONSTRAINT [FK_ServiceLine_Bill] FOREIGN KEY([billID])
REFERENCES [dbo].[Bill] ([billID])
GO
ALTER TABLE [dbo].[ServiceLine] CHECK CONSTRAINT [FK_ServiceLine_Bill]
GO
ALTER TABLE [dbo].[ServiceLine]  WITH CHECK ADD  CONSTRAINT [FK_ServiceLine_Service1] FOREIGN KEY([serviceID])
REFERENCES [dbo].[Service] ([serviceID])
GO
ALTER TABLE [dbo].[ServiceLine] CHECK CONSTRAINT [FK_ServiceLine_Service1]
GO
USE [master]
GO
ALTER DATABASE [SQII_EMS] SET  READ_WRITE 
GO
