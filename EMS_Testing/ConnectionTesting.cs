﻿//********************************************************************************************************************************* 
// FILE          : ConnectionTesting.cs
// PROJECT       : INFO2180 Software Quality I - EMS System
// PROGRAMMER    : Victor Lauria
// VERSION DATE  : 2017-DEC-30
// DESCRIPTION   : This is the unit test for the connection library.
//               : It contains the documentation of each test performed during
//               : the development of this solution

// REFERENCES:
// UNIT TEST METHODS NAMING CONVENTION:
// [MethodName_StateUnderTest_ExpectedBehavior]
// Reference: http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
// UNIT TEST METHODS PATTERN:
// Arrange - Act - Assert (AAA)
// Reference: https://docs.microsoft.com/en-us/visualstudio/test/unit-test-basics
//**********************************************************************************************************************************


using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demographics;
using Billing;
using Connection;
using Interpreter;
using System.Collections.Generic;
using System.Linq;

namespace EMS_Testing
{
    [TestClass]
    public class ConnectionTesting
    {
        #region Fast Settup
        private bool patientHasAddress = true;
        private bool appointmentHasSecondPatient = true;
        private bool serviceLineHasService = true;
        private bool billHasServices = true;
        #endregion

        #region Regular Objects

        [TestCategory("Packager Class")]
        private Patient GetRegularPatient()
        {
            int patientID = 557;
            string HCN = "1234561231AZ";
            string lName = "LAST";
            string fName = "FIRST";
            char mInitial = 'I';
            string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
            string sex = "M";
            string headOfHouse = "9879876547QQ";

            Patient obj = new Patient();

            obj.PatientID = patientID;
            obj.SetFname(fName);
            obj.SetLname(lName);
            obj.SethCN(HCN);
            obj.SetMinitial(mInitial);
            obj.DateOfBirth = DateTime.Parse(dateOfBirth);
            obj.SetSex(sex);
            obj.SetHeadOfHouse(headOfHouse);

            if (patientHasAddress)
            {
                int addressID = 562;
                string adressLineOne = "299 Drive St";
                string adressLineTwo = "120 Tower 2";
                string city = "Kitchener";
                string province = "ON";
                string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

                obj.Address.SetAdressID(addressID);
                obj.Address.SetAdressLineOne(adressLineOne);
                obj.Address.SetAdressLineTwo(adressLineTwo);
                obj.Address.SetCity(city);
                obj.Address.SetProvince(province);
                obj.Address.SetPhoneNumber(phoneNumber);
            }

            return obj;
        }

        [TestCategory("Packager Class")]
        private string GetRegularPatientStr()
        {
            int patientID = 557;
            string HCN = "1234561231AZ";
            string lName = "LAST";
            string fName = "FIRST";
            char mInitial = 'I';
            string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
            string sex = "M";
            string headOfHouse = "9879876547QQ";

            int addressID = 562;
            string adressLineOne = "299 Drive St";
            string adressLineTwo = "120 Tower 2";
            string city = "Kitchener";
            string province = "ON";
            string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

            if (!patientHasAddress)
            {
                addressID = 0;
                adressLineOne = "";
                adressLineTwo = "";
                city = "";
                province = "";
                phoneNumber = ""; // (xxx)xxx-xxxx
            }

            string expectedOutput =
                $"OBJECT;Patient;{patientID};{HCN};{lName};{fName};{mInitial};{DateTime.Parse(dateOfBirth)};{sex};{headOfHouse};" +
                $"OBJECT;Address;{addressID};{adressLineOne};{adressLineTwo};{city};{province};{phoneNumber};";

            return expectedOutput;
        }

        [TestCategory("Packager Class")]
        private Appointment GetRegularAppointment()
        {
            int appointmentID = 505;
            DateTime dayAndTime = DateTime.Parse("2018-01-01 10:00:00");

            Appointment obj = new Appointment();

            obj.SetApointmentID(appointmentID);
            obj.SetDayAndTime(dayAndTime);

            obj.FirstPatient = GetRegularPatient();
            if (appointmentHasSecondPatient)
            {
                obj.SecondPatient = GetRegularPatient();
            }

            return obj;
        }

        [TestCategory("Packager Class")]
        private string GetRegularAppointmentStr()
        {
            int appointmentID = 505;
            DateTime dayAndTime = DateTime.Parse("2018-01-01 10:00:00");

            string expected =
                $"OBJECT;Appointment;{appointmentID};{dayAndTime.ToString()};" +
                GetRegularPatientStr();

            if (appointmentHasSecondPatient)
            {
                expected += GetRegularPatientStr();
            }

            return expected;
        }



        [TestCategory("Packager Class")]
        private Bill GetRegularBill()
        {
            int billID = 33;
            int flagRecal = 1;
            bool statusDone = false;

            Bill obj = new Bill();
            obj.SetBillID(billID);
            obj.SetFlagRecall(flagRecal);
            obj.StatusDone = statusDone;

            obj.Patient = GetRegularPatient();
            obj.Appointment = GetRegularAppointment();

            if (billHasServices)
            {
                obj.Services.Add(GetRegularServiceListOne());
                obj.Services.Add(GetRegularServiceListTwo());
            }

            return obj;
        }

        [TestCategory("Packager Class")]
        private string GetRegularBillStr()
        {
            int billID = 33;
            int flagRecal = 1;
            bool statusDone = false;

            string expected = $"OBJECT;Bill;{billID};{flagRecal};{statusDone};" +
                            GetRegularPatientStr() +
                            GetRegularAppointmentStr();
            if (billHasServices)
            {
                expected += GetRegularServiceListOneStr() +
                GetRegularServiceListTwoStr();
            }

            return expected;
        }

        [TestCategory("Packager Class")]
        private ServiceList GetRegularServiceListOne()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.UPRC.ToString();

            ServiceList obj = new ServiceList();

            obj.SetQuantity(quantity);
            obj.SetStatus(status);

            if (serviceLineHasService)
            {
                obj.Service = GetRegularServiceOne();
            }

            return obj;
        }

        [TestCategory("Packager Class")]
        private string GetRegularServiceListOneStr()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.UPRC.ToString();

            string expected = $"OBJECT;ServiceLine;{quantity};{status};";

            if (serviceLineHasService)
            {
                expected += GetRegularServiceOneStr();
            }
            else
            {
                double value = 0;
                expected += $"OBJECT;Service;0;X000;{DateTime.MinValue.ToString()};{value};";
            }

            return expected;
        }

        [TestCategory("Packager Class")]
        private ServiceList GetRegularServiceListTwo()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.PAID.ToString();

            ServiceList obj = new ServiceList();

            obj.SetQuantity(quantity);
            obj.SetStatus(status);

            if (serviceLineHasService)
            {
                obj.Service = GetRegularServiceTwo();
            }

            return obj;
        }

        [TestCategory("Packager Class")]
        private string GetRegularServiceListTwoStr()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.PAID.ToString();

            string expected = $"OBJECT;ServiceLine;{quantity};{status};";

            if (serviceLineHasService)
            {
                expected += GetRegularServiceTwoStr();
            }
            else
            {
                double value = 0;
                expected += $"OBJECT;Service;0;X000;{DateTime.MinValue.ToString()};{value};";
            }

            return expected;
        }

        [TestCategory("Packager Class")]
        private Service GetRegularServiceOne()
        {
            int serviceID = 65;
            string feeCode = "A661";
            DateTime effectiveDate = DateTime.Parse("05-05-1999");
            double serviceValue = 39.99;

            Service ss = new Service();

            ss.SetServiceID(serviceID);
            ss.SetFeeCode(feeCode);
            ss.SetServiceValue(serviceValue);
            ss.EffectiveDate = effectiveDate;

            return ss;
        }

        [TestCategory("Packager Class")]
        private string GetRegularServiceOneStr()
        {
            int serviceID = 65;
            string feeCode = "A661";
            DateTime effectiveDate = DateTime.Parse("05-05-1999");
            double serviceValue = 39.99;

            string expected = $"OBJECT;Service;{serviceID};{feeCode};{effectiveDate.ToString()};{serviceValue};";

            return expected;
        }

        [TestCategory("Packager Class")]
        private Service GetRegularServiceTwo()
        {
            int serviceID = 61;
            string feeCode = "A112";
            DateTime effectiveDate = DateTime.Parse("05-05-1992");
            double serviceValue = 98.95;

            Service ss = new Service();

            ss.SetServiceID(serviceID);
            ss.SetFeeCode(feeCode);
            ss.SetServiceValue(serviceValue);
            ss.EffectiveDate = effectiveDate;

            return ss;
        }

        [TestCategory("Packager Class")]
        private string GetRegularServiceTwoStr()
        {
            int serviceID = 61;
            string feeCode = "A112";
            DateTime effectiveDate = DateTime.Parse("05-05-1992");
            double serviceValue = 98.95;

            string expected = $"OBJECT;Service;{serviceID};{feeCode};{effectiveDate.ToString()};{serviceValue};";

            return expected;
        }

        #endregion

        #region Packager

        #region Demographics

        /// <summary>
        /// This method tests the Packager.PackPatient method.
        /// It assumes that the Patient class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void PackPatient_Test()
        {
            Patient input = GetRegularPatient();

            string expected = GetRegularPatientStr();
            string actual = Packager.PackPatient(input);

            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        /// This method tests the Packager.UnpackPatient method.
        /// It assumes that the Patient class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackPatient_Test()
        {
            string input = GetRegularPatientStr();

            Patient expected = GetRegularPatient();
            Patient actual = Packager.UnpackPatient(input);

            Assert.IsTrue(expected.Equals(actual));
        }


        /// <summary>
        /// This method tests the Packager.PackAddress method.
        /// It assumes that the Address class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void PackAddress_Test()
        {
            int addressID = 562;
            string adressLineOne = "299 Drive St";
            string adressLineTwo = "120 Tower 2";
            string city = "Kitchener";
            string province = "ON";
            string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

            Address obj = new Address();

            obj.SetAdressID(addressID);
            //obj.SetHCN(HCN);
            obj.SetAdressLineOne(adressLineOne);
            obj.SetAdressLineTwo(adressLineTwo);
            obj.SetCity(city);
            obj.SetProvince(province);
            obj.SetPhoneNumber(phoneNumber);

            string expectedResult = 
                $"OBJECT;Address;{addressID};{adressLineOne};{adressLineTwo};{city};{province};{phoneNumber};";

            string actualResult = Packager.PackAddress(obj);

            Assert.AreEqual(expectedResult, actualResult);
    }


        /// <summary>
        /// This method tests the Packager.UnpackAddress method.
        /// It assumes that the Address class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackAddress_Test()
        {
            int addressID = 562;
            //string HCN = "1234561231AZ";
            string adressLineOne = "299 Drive St";
            string adressLineTwo = "120 Tower 2";
            string city = "Kitchener";
            string province = "ON";
            string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

            string input =
                $"OBJECT;Address;{addressID};{adressLineOne};{adressLineTwo};{city};{province};{phoneNumber};";

            Address obj = Packager.UnpackAddress(input);

            Assert.AreEqual(addressID, obj.GetAdressID());
            Assert.AreEqual(adressLineOne, obj.GetAddressLineOne());
            Assert.AreEqual(adressLineTwo, obj.GetAddressLineTwo());
            Assert.AreEqual(city, obj.GetCity());
            Assert.AreEqual(province, obj.GetProvince());
            Assert.AreEqual(phoneNumber, obj.GetPhoneNumber());

        }


        /// <summary>
        /// This method tests the Packager.PackAppointment method.
        /// It assumes that the Appointment class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void PackAppointment_Test()
        {
            Appointment input = GetRegularAppointment();

            string expected = GetRegularAppointmentStr();
            string actual = Packager.PackAppointment(input);

            Assert.AreEqual(expected, actual);
    }


        /// <summary>
        /// This method tests the Packager.UnpackAppointment method.
        /// It assumes that the Appointment class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackAppointment_Test()
        {
            string input = GetRegularAppointmentStr();

            Appointment expected = GetRegularAppointment();
            Appointment actual = Packager.UnpackAppointment(input);

            Assert.IsTrue(expected.Equals(actual));
        }

        #endregion

        #region Billing

        /// <summary>
        /// This method tests the Packager.PackBill method.
        /// It assumes that the Bill class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void PackBill_Test()
        {
            Bill input = GetRegularBill();

            string expected = GetRegularBillStr();
            string actual = Packager.PackBill(input);

            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        /// This method tests the Packager.UnpackBill method.
        /// It assumes that the Bill class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackBill_Test()
        {
            string input = GetRegularBillStr();

            Bill expected = GetRegularBill();
            Bill actual = Packager.UnpackBill(input);

            Assert.IsTrue(expected.Equals(actual));
        }


        /// <summary>
        /// This method tests the Packager.PackServiceList method.
        /// It assumes that the ServiceList class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void PackServiceList_Test()
        {
            ServiceList obj = GetRegularServiceListOne();

            string expected = GetRegularServiceListOneStr();
            string actual = Packager.PackServiceList(obj);

            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        /// This method tests the Packager.UnpackServiceList method.
        /// It assumes that the ServiceList class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackServiceList_Test()
        {
            string input = GetRegularServiceListOneStr();

            ServiceList expected = GetRegularServiceListOne();
            ServiceList actual = Packager.UnpackServiceList(input);

            Assert.IsTrue(expected.Equals(actual));
        }


        /// <summary>
        /// This method tests the Packager.PackService method.
        /// It assumes that the Service class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void PackService_Test()
        {
            Service obj = GetRegularServiceOne();

            string expected = GetRegularServiceOneStr();
            string actual = Packager.PackService(obj);

            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        /// This method tests the Packager.UnpackService method.
        /// It assumes that the Service class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackService_Test()
        {
            string input = GetRegularServiceOneStr();

            Service expected = GetRegularServiceOne();
            Service actual = Packager.UnpackService(input);

            Assert.IsTrue(expected.Equals(actual));
        }

        #endregion

        #region Lists

        /// <summary>
        /// This method tests the Packager.UnpackListOfPatients method.
        /// It assumes that the Patient class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackListOfPatients_Test()
        {
            string input = GetRegularPatientStr() + GetRegularPatientStr() + GetRegularPatientStr();

            List<Patient> expected = new List<Patient>
            {
                GetRegularPatient(),
                GetRegularPatient(),
                GetRegularPatient()
            };

            List<Patient> actual = Packager.UnpackListOfPatients(input);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        /// <summary>
        /// This method tests the Packager.UnpackListOfPatients method.
        /// It assumes that the Patient class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackListOfPatients_TestTwo()
        {
            string input = "";

            List<Patient> expected = new List<Patient>();

            List<Patient> actual = Packager.UnpackListOfPatients(input);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        /// <summary>
        /// This method tests the Packager.UnpackListOfAppointments method.
        /// It assumes that the Appointment class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackListOfAppointments_Test()
        {
            string input = GetRegularAppointmentStr() + GetRegularAppointmentStr() + GetRegularAppointmentStr();

            List<Appointment> expected = new List<Appointment>
            {
                GetRegularAppointment(),
                GetRegularAppointment(),
                GetRegularAppointment()
            };

            List<Appointment> actual = Packager.UnpackListOfAppointments(input);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        /// <summary>
        /// This method tests the Packager.UnpackListOfBills method.
        /// It assumes that the Bill class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackListOfBills_Test()
        {
            string input = GetRegularBillStr() + GetRegularBillStr() + GetRegularBillStr();

            List<Bill> expected = new List<Bill>
            {
                GetRegularBill(),
                GetRegularBill(),
                GetRegularBill()
            };

            List<Bill> actual = Packager.UnpackListOfBills(input);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        /// <summary>
        /// This method tests the Packager.UnpackListOfServiceLine method.
        /// It assumes that the ServiceLine class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackListOfServiceLine_Test()
        {
            string input = 
                GetRegularServiceListOneStr() + 
                GetRegularServiceListTwoStr() + 
                GetRegularServiceListOneStr() + 
                GetRegularServiceListTwoStr();

            List<ServiceList> expected = new List<ServiceList>
            {
                GetRegularServiceListOne(),
                GetRegularServiceListTwo(),
                GetRegularServiceListOne(),
                GetRegularServiceListTwo()
            };

            List<ServiceList> actual = Packager.UnpackListOfServiceLine(input);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        /// <summary>
        /// This method tests the Packager.UnpackListOfServices method.
        /// It assumes that the Service class works properly.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void UnpackListOfServices_Test()
        {
            string input =
                GetRegularServiceOneStr() +
                GetRegularServiceTwoStr() +
                GetRegularServiceOneStr() +
                GetRegularServiceTwoStr();

            List<Service> expected = new List<Service>
            {
                GetRegularServiceOne(),
                GetRegularServiceTwo(),
                GetRegularServiceOne(),
                GetRegularServiceTwo()
            };

            List<Service> actual = Packager.UnpackListOfServices(input);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        #endregion

        #region Public Tools

        /// <summary>
        /// This method tests the Packager.SplitObjects method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void SplitObjects_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.SplitBillObjects method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void SplitBillObjects_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.SplitAppointmentObjects method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void SplitAppointmentObjects_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.GetInteger method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void GetInteger_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.GetDouble method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void GetDouble_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.GetDateTime method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void GetDateTime_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.GetBoolean method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void GetBoolean_Test()
        {

        }


        /// <summary>
        /// This method tests the Packager.GetChar method.
        /// </summary>
        [TestMethod]
        [TestCategory("Packager Class")]
        public void GetChar_Test()
        {

        }

        #endregion

        #endregion

        #region ClientConnection

        /// <summary>
        /// This method represents the manual test for
        /// ClientConnection.ConnectToServer method.
        /// Stepts to reproduce:
        /// 1. Open the client wihout a server
        /// 2. Expected to have the client wainting for the user to type
        ///    the IP address of the server
        /// 3. Open the server
        /// 4. Type the IP address of the server in the client application
        /// 5. Expected to have the client connected to the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientConnection Class")]
        public void ConnectToServer_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientConnection.SendRequest method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Send any kind of request to the server
        /// 3. Expects to have an answer from the server
        /// 4. Close the server
        /// 5. Send another request to the server
        /// 6. Expects to receive a message of connection
        ///    loss on client side and the app to be closed
        /// </summary>
        [TestMethod]
        [TestCategory("ClientConnection Class")]
        public void SendRequest_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientConnection.Disconnect method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Run Disconnect method
        /// 3. Expects to have a message in the server
        ///    side about a client being disconnected
        /// 4. Expects to have the client closed
        /// </summary>
        [TestMethod]
        [TestCategory("ClientConnection Class")]
        public void Disconnect_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }

        #endregion

        #region ClientInterpreter

        #region Demographics

        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.AddPatient method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Patient
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void AddPatient_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.AddAddress method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Address
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void AddAddress_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.AddAppointment method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Appointment
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void AddAppointment_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }

        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindPatient(string hcnNumber) method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Create a string with a valid HCN
        /// 3. Call the method
        /// 4. Expects to receive a Patient or null
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindPatientStr_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindPatient(Patient obj) method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Patient
        /// 3. Call the method
        /// 4. Expects to receive a list of Patient
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindPatientObj_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindPatient(int patientID) method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate an int with a positive number
        /// 3. Call the method
        /// 4. Expects to receive a Patient or null
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindPatientInt_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindAddress method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Create a string with a valid HCN
        /// 3. Call the method
        /// 4. Expects to receive an Address or null
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindAddress_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindAppointment method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate an int with a positive number
        /// 3. Call the method
        /// 4. Expects to receive an Appointment or null
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindAppointment_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindAppointments(int patientID) method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate an int with a positive number
        /// 3. Call the method
        /// 4. Expects to receive a list of Appointment
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindAppointmentsInt_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindAppointments(DateTime start) method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid DateTime
        /// 3. Call the method
        /// 4. Expects to receive a list of Appointment
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindAppointmentsDate_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.UpdatePatient method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Patient
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void UpdatePatient_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.UpdateAddress method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Address
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void UpdateAddress_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.UpdateAppointment method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Appointment
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void UpdateAppointment_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }

        #endregion

        #region Billing

        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.AddBill method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid Bill
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void AddBill_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.AddServiceList method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid ServiceList
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void AddServiceList_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindBill method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate two positive integers
        /// 3. Call the method
        /// 4. Expects to receive a Bill
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindBill_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindServiceList method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a positive integer
        /// 3. Call the method
        /// 4. Expects to receive a list of ServiceList
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindServiceList_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.FindServices method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Call the method
        /// 3. Expects to receive a list of Service
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void FindServices_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.UpdateServiceList method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid ServiceList
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void UpdateServiceList_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.MonthlyBillingSummary method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid DateTime
        /// 3. Call the method
        /// 4. Expects to receive a string with the summary
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void MonthlyBillingSummary_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.MonthlyBillingCSV method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid DateTime
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void MonthlyBillingCSV_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ClientInterpreter.MonthlyReconcileCSV method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Instantiate a valid DateTime
        /// 3. Call the method
        /// 4. Expects to receive an error message
        ///    from the server
        /// </summary>
        [TestMethod]
        [TestCategory("ClientInterpreter Class")]
        public void MonthlyReconcileCSV_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }

        #endregion

        #endregion

        #region ServerConnection

        /// <summary>
        /// This method represents the manual test for
        /// ServerConnection.StartServer method.
        /// Stepts to reproduce:
        /// 1. Open the server application
        /// 2. Expects to receive a message from the console
        ///    with the IP and wait for new clients to connect
        /// 3. Start a client application
        /// 4. Expects to the client to get connected
        /// </summary>
        [TestMethod]
        [TestCategory("ServerConnection Class")]
        public void StartServer_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }


        /// <summary>
        /// This method represents the manual test for
        /// ServerConnection.CloseAllSockets method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Trigger the method
        /// 3. Expect to have the connection closed
        /// </summary>
        [TestMethod]
        [TestCategory("ServerConnection Class")]
        public void CloseAllSockets_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }

        #endregion

        #region ServerInterpreter

        /// <summary>
        /// This method represents the manual test for
        /// ServerInterpreter.InterpretMessage method.
        /// Stepts to reproduce:
        /// 1. Open the server, then open the client
        /// 2. Send a request from the client
        /// 3. Expect that the interpreter get the message,
        ///    parse the action and arguments, and return an
        ///    answer that corresponds with the action requested
        ///    from the client.
        /// </summary>
        [TestMethod]
        [TestCategory("ServerInterpreter Class")]
        public void InterpretMessage_Test()
        {
            bool manualTestDone = true;
            Assert.IsTrue(manualTestDone);
        }

        #endregion

        #region Encrypter

        [TestMethod]
        [TestCategory("Encrypter Class")]
        public void EncryptAndDecryptMessage_Test()
        {
            string expected = "Hello there";
            string input = Encrypter.Encrypt(expected);
            string actual = Encrypter.Decrypt(input);

            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        [TestCategory("Encrypter Class")]
        public void CheckRandomnessOfCipherText_Test()
        {
            string input = "Hello there";

            string sampleOne = Encrypter.Encrypt(input);
            string sampleTwo = Encrypter.Encrypt(input);

            Assert.AreNotEqual(sampleOne, sampleTwo);
            Assert.AreEqual(input, Encrypter.Decrypt(sampleOne));
            Assert.AreEqual(input, Encrypter.Decrypt(sampleTwo));
        }


        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        [TestCategory("Encrypter Class")]
        public void DecryptInvalidMessage_Test()
        {
            string input = "Hello there";
            Encrypter.Decrypt(input);
        }

        #endregion

        #region Library Integration

        [TestMethod]
        [TestCategory("Connection Library Integration")]
        public void SendAndReceiveAppointmentList_Test()
        {
            List<Appointment> expected = new List<Appointment>
            {
                GetRegularAppointment(),
                GetRegularAppointment(),
                GetRegularAppointment()
            };

            string input = Packager.PackListOfAppointments(expected);
            string data = Encrypter.Encrypt(input);
            string output = Encrypter.Decrypt(data);

            List<Appointment> actual = Packager.UnpackListOfAppointments(output);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        [TestMethod]
        [TestCategory("Connection Library Integration")]
        public void SendAndReceiveBillList_Test()
        {
            List<Bill> expected = new List<Bill>
            {
                GetRegularBill(),
                GetRegularBill(),
                GetRegularBill()
            };

            string input = Packager.PackListOfBills(expected);
            string data = Encrypter.Encrypt(input);
            string output = Encrypter.Decrypt(data);

            List<Bill> actual = Packager.UnpackListOfBills(output);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        [TestMethod]
        [TestCategory("Connection Library Integration")]
        public void SendAndReceivePatientList_Test()
        {
            List<Patient> expected = new List<Patient>
            {
                GetRegularPatient(),
                GetRegularPatient(),
                GetRegularPatient()
            };

            string input = Packager.PackListOfPatients(expected);
            string data = Encrypter.Encrypt(input);
            string output = Encrypter.Decrypt(data);

            List<Patient> actual = Packager.UnpackListOfPatients(output);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }


        [TestMethod]
        [TestCategory("Connection Library Integration")]
        public void SendAndReceiveServiceList_Test()
        {
            List<Service> expected = new List<Service>
            {
                GetRegularServiceOne(),
                GetRegularServiceTwo(),
                GetRegularServiceOne(),
                GetRegularServiceTwo(),
            };

            string input = Packager.PackListOfServices(expected);
            string data = Encrypter.Encrypt(input);
            string output = Encrypter.Decrypt(data);

            List<Service> actual = Packager.UnpackListOfServices(output);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        #endregion
    }
}
