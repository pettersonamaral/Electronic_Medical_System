﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demographics;
using System.Globalization;
using System.Collections.Generic;
using Billing;
using DBManager;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace EMS_Testing
{
    [TestClass]
    public class DBSupportTesting
    {
        #region Fast Settup
        private bool patientHasAddress = true;
        private bool appointmentHasSecondPatient = true;
        private bool serviceLineHasService = true;
        private bool billHasServices = true;
        private static Random random = new Random();
        static DBConnection dBConnection = DBConnection.Instance;
        public String NewHCN { get ; set ; }
        public Int32 NewAddressID { get; set; }

        #endregion



        #region  Support Methods
        /// <summary>
        /// Support the create random numbers. Returns a random numric string
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        /// Source: https://stackoverflow.com/questions/1344221/how-can-i-generate-random-alphanumeric-strings-in-c?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        public static String RandomNumbers(Int32 length)
        {
            const String chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        /// <summary>
        /// Support the create random numbers. Returns a random alphabetic string
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        /// Source: https://stackoverflow.com/questions/1344221/how-can-i-generate-random-alphanumeric-strings-in-c?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        public static String RandomString(Int32 length)
        {
            const String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }



        #endregion



        #region Regular Objects

        /// <summary>
        /// Get a Patient Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Patient Class")]
        private Patient GetRegularPatient()
        {
            int patientID = 1;
            string HCN = "1234561231AZ";
            string lName = "LAST";
            string fName = "FIRST";
            char mInitial = 'I';
            string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
            string sex = "M";
            string headOfHouse = "9879876547QQ";

            Patient obj = new Patient();

            obj.PatientID = patientID;
            obj.SetFname(fName);
            obj.SetLname(lName);
            obj.SethCN(HCN);
            obj.SetMinitial(mInitial);
            obj.DateOfBirth = DateTime.Parse(dateOfBirth);
            obj.SetSex(sex);
            obj.SetHeadOfHouse(headOfHouse);

            if (patientHasAddress)
            {
                int addressID = 1;
                string adressLineOne = "299 Drive St";
                string adressLineTwo = "120 Tower 2";
                string city = "Kitchener";
                string province = "ON";
                string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

                obj.Address.SetAdressID(addressID);
                obj.Address.SetAdressLineOne(adressLineOne);
                obj.Address.SetAdressLineTwo(adressLineTwo);
                obj.Address.SetCity(city);
                obj.Address.SetProvince(province);
                obj.Address.SetPhoneNumber(phoneNumber);
            }

            return obj;
        }

        /// <summary>
        /// Get a Patient Class with out headOfHouse
        /// </summary>
        /// <returns></returns>
        [TestCategory("Patient Class")]
        private Patient GetRegularPatientWOHoH()
        {
            int patientID = 1;
            string HCN = "1234561232AZ";
            string lName = "WOHoH";
            string fName = "FIRST";
            char mInitial = 'I';
            string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
            string sex = "M";
            string headOfHouse = "";

            Patient obj = new Patient();

            obj.PatientID = patientID;
            obj.SetFname(fName);
            obj.SetLname(lName);
            obj.SethCN(HCN);
            obj.SetMinitial(mInitial);
            obj.DateOfBirth = DateTime.Parse(dateOfBirth);
            obj.SetSex(sex);
            obj.SetHeadOfHouse(headOfHouse);

            if (patientHasAddress)
            {
                int addressID = 1;
                string adressLineOne = "299 Drive St";
                string adressLineTwo = "120 Tower 2";
                string city = "Kitchener";
                string province = "ON";
                string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

                obj.Address.SetAdressID(addressID);
                obj.Address.SetAdressLineOne(adressLineOne);
                obj.Address.SetAdressLineTwo(adressLineTwo);
                obj.Address.SetCity(city);
                obj.Address.SetProvince(province);
                obj.Address.SetPhoneNumber(phoneNumber);
            }

            return obj;
        }

        /// <summary>
        /// Get a Appointment Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Appointment Class")]
        private Appointment GetRegularAppointment()
        {
            int appointmentID = 505;
            DateTime dayAndTime = DateTime.Parse("2018-01-01 10:00:00");

            Appointment obj = new Appointment();

            obj.SetApointmentID(appointmentID);
            obj.SetDayAndTime(dayAndTime);

            obj.FirstPatient = GetRegularPatient();
            if (appointmentHasSecondPatient)
            {
                obj.SecondPatient = GetRegularPatient();
            }

            return obj;
        }

        /// <summary>
        /// Get a Appointment Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Appointment Class")]
        private Appointment GetRegularAppointment1()
        {
            int appointmentID = 1;
            DateTime dayAndTime = DateTime.Parse("2011-11-11 11:11:11");

            Appointment obj = new Appointment();

            obj.SetApointmentID(appointmentID);
            obj.SetDayAndTime(dayAndTime);

            obj.FirstPatient = GetRegularPatient();
            if (appointmentHasSecondPatient)
            {
                obj.SecondPatient = GetRegularPatient();
            }

            return obj;
        }

        /// <summary>
        /// Get a Bill Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Bill Class")]
        private Bill GetRegularBill()
        {
            int billID = 33;
            int flagRecal = 1;
            bool statusDone = false;

            Bill obj = new Bill();
            obj.SetBillID(billID);
            obj.SetFlagRecall(flagRecal);
            obj.StatusDone = statusDone;

            obj.Patient = GetRegularPatient();
            obj.Appointment = GetRegularAppointment();

            if (billHasServices)
            {
                obj.Services.Add(GetRegularServiceListOne());
                obj.Services.Add(GetRegularServiceListTwo());
            }

            return obj;
        }

        /// <summary>
        /// Get a ServiceListOne Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceListOne Class")]
        private ServiceList GetRegularServiceListOne()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.UPRC.ToString();

            ServiceList obj = new ServiceList();

            obj.SetQuantity(quantity);
            obj.SetStatus(status);

            if (serviceLineHasService)
            {
                obj.Service = GetRegularServiceOne();
            }

            return obj;
        }

        /// <summary>
        /// Get a ServiceListOne Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceListOne Class")]
        private ServiceList GetRegularServiceListTwo()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.PAID.ToString();

            ServiceList obj = new ServiceList();

            obj.SetQuantity(quantity);
            obj.SetStatus(status);

            if (serviceLineHasService)
            {
                obj.Service = GetRegularServiceTwo();
            }

            return obj;
        }

        /// <summary>
        /// Get a ServiceOne Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceOne Class")]
        private Service GetRegularServiceOne()
        {
            int serviceID = 65;
            string feeCode = "A661";
            DateTime effectiveDate = DateTime.Parse("05-05-1999");
            double serviceValue = 39.99;

            Service ss = new Service();

            ss.SetServiceID(serviceID);
            ss.SetFeeCode(feeCode);
            ss.SetServiceValue(serviceValue);
            ss.EffectiveDate = effectiveDate;

            return ss;
        }

        /// <summary>
        /// Get a ServiceTwo Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceTwo Class")]
        private Service GetRegularServiceTwo()
        {
            int serviceID = 61;
            string feeCode = "A112";
            DateTime effectiveDate = DateTime.Parse("05-05-1992");
            double serviceValue = 98.95;

            Service ss = new Service();

            ss.SetServiceID(serviceID);
            ss.SetFeeCode(feeCode);
            ss.SetServiceValue(serviceValue);
            ss.EffectiveDate = effectiveDate;

            return ss;
        }

        #endregion


        #region Demographics DBSupport tests

        /// <summary>
        /// Test insert a new Address
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void InsertNEWAddressAndANewPatient()
        {
            // ARRANGE
            Int32 successfulReturn = 1, methodReturns = -1;
            Int32 newIndexAddress = DBSupport.GetNextAddressID();
            Patient obj = GetRegularPatient();
            if (NewHCN == null)
            {
                NewHCN = RandomNumbers(10);
                NewHCN = NewHCN + RandomString(2);
            }

            // ACT
            methodReturns = DBSupport.InsertANewAddress(obj.Address.GetAddressLineOne(),obj.Address.GetAddressLineTwo(), 
                obj.Address.GetCity(),obj.Address.GetProvince(),obj.Address.GetPhoneNumber(), NewHCN);


            if (methodReturns == 1 )
            {
                NewAddressID = DBSupport.GetNextAddressID() - 1;

                methodReturns = DBSupport.InsertANewAPatient(NewHCN, obj.GetLname(), obj.GetFname(), obj.GetMinitial(), obj.DateOfBirth, 
                    obj.GetSex(), obj.GetHeadOfHouse(), NewAddressID);
            }

            // ASSERT
            Assert.AreEqual(successfulReturn, methodReturns);
        }


        /// <summary>
        /// Test find a patiant by patientID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindPatientByLName()
        {
            // ARRANGE
            Int32 statusReturn = -1, successfulReturn = 1;
            DataTable dt = null;
            Patient obj = GetRegularPatientWOHoH();

            // ACT
            dt = DBSupport.FindPatientByLastName("LAST");
            if (dt.Rows.Count > 0)
            {
                statusReturn = 1;
            }


            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// Test find a patiant by patientID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindPatientByPatientID()
        {
            // ARRANGE
            Int32 statusReturn = 1, successfulReturn = 1;
            DataTable dt = null;
            Patient obj = GetRegularPatient();

            // ACT
            dt = DBSupport.FindPatient(1);
            statusReturn = dt.Rows.Count;

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// Test find a patiant by hcnNumber
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindPatientByHcnNumber()
        {
            // ARRANGE
            Int32 statusReturn = 1, successfulReturn = 1;
            DataTable dt = null;
            Patient obj = GetRegularPatient();            

            // ACT
            dt = DBSupport.FindPatient(1);
            statusReturn = dt.Rows.Count;
            if (statusReturn == 1)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["HCN"].ToString() == "") { statusReturn = 0; }
                    else { NewHCN = (row["HCN"]).ToString(); }
                }
            }

            dt = DBSupport.FindPatient(NewHCN);
            statusReturn = dt.Rows.Count;

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }



        /// <summary>
        /// Test find an address by addressID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindAddressByaddressID()
        {
            // ARRANGE
            Int32 statusReturn = 1, addressID = -137, successfulReturn = 1;
            DataTable dt = null;
            Patient obj = GetRegularPatient();


            // ACT
            addressID = obj.Address.GetAdressID();
            dt = DBSupport.FindAddress(addressID);
            statusReturn = dt.Rows.Count;

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// Test find a Address by hcnNumber
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindAddresByHcnNumber()
        {
            // ARRANGE
            Int32 statusReturn = 1, successfulReturn = 1;
            DataTable dt = null;
            Patient obj = GetRegularPatient();

            // ACT
            dt = DBSupport.FindPatient(1);
            statusReturn = dt.Rows.Count;
            if (statusReturn == 1)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["HCN"].ToString() == "") { statusReturn = 0; }
                    else { NewHCN = (row["HCN"]).ToString(); }
                }
            }

            dt = DBSupport.FindAddress(NewHCN);
            statusReturn = dt.Rows.Count;

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// Test will update and existente Patient.
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void UpdatePatient()
        {
            // ARRANGE
            Int32 successfulReturn = 1, methodReturns = -1;
            Int32 newIndexAddress = DBSupport.GetNextAddressID();
            Int32 patientID = 0;
            Patient obj = GetRegularPatient();
            DataTable dt = null;

            if (NewHCN == null)
            {
                NewHCN = RandomNumbers(10);
                NewHCN = NewHCN + RandomString(2);
            }

            // ACT
            methodReturns = DBSupport.InsertANewAddress(obj.Address.GetAddressLineOne(), obj.Address.GetAddressLineTwo(),
                obj.Address.GetCity(), obj.Address.GetProvince(), obj.Address.GetPhoneNumber(), NewHCN);


            if (methodReturns == 1)
            {
                NewAddressID = DBSupport.GetNextAddressID() - 1;

                methodReturns = DBSupport.InsertANewAPatient(NewHCN, obj.GetLname(), obj.GetFname(), obj.GetMinitial(), obj.DateOfBirth,
                    obj.GetSex(), obj.GetHeadOfHouse(), NewAddressID);
            }

            dt = DBSupport.FindPatient(NewHCN);
            methodReturns = dt.Rows.Count;

            if (methodReturns == 1)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["patientID"].ToString() == "") { NewAddressID = 0; }
                    else { patientID = Convert.ToInt32(row["patientID"]); }
                }

                NewAddressID = DBSupport.UpdatePatient(patientID, NewHCN, obj.GetLname(), obj.GetFname() + "UPDATED",
                    obj.GetMinitial(), obj.DateOfBirth, obj.GetSex(), obj.GetHeadOfHouse());
            }

            // ASSERT
                Assert.AreEqual(successfulReturn, methodReturns);
        }


        /// <summary>
        /// Test will update and existente Address.
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void UpdateAddress()
        {
            // ARRANGE
            Int32 successfulReturn = 1, methodReturns = -1;
            Int32 newIndexAddress = DBSupport.GetNextAddressID();
            Int32 addressID = 0;
            Patient obj = GetRegularPatient();
            DataTable dt = null;

            if (NewHCN == null)
            {
                NewHCN = RandomNumbers(10);
                NewHCN = NewHCN + RandomString(2);
            }

            // ACT
            methodReturns = DBSupport.InsertANewAddress(obj.Address.GetAddressLineOne(), obj.Address.GetAddressLineTwo(),
                obj.Address.GetCity(), obj.Address.GetProvince(), obj.Address.GetPhoneNumber(), NewHCN);


            if (methodReturns == 1)
            {
                NewAddressID = DBSupport.GetNextAddressID() - 1;

                methodReturns = DBSupport.InsertANewAPatient(NewHCN, obj.GetLname(), obj.GetFname(), obj.GetMinitial(), obj.DateOfBirth,
                    obj.GetSex(), obj.GetHeadOfHouse(), NewAddressID);
            }

            dt = DBSupport.FindPatient(NewHCN);
            methodReturns = dt.Rows.Count;

            if (methodReturns == 1)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["addressID"].ToString() == "") { NewAddressID = 0; }
                    else { addressID = Convert.ToInt32(row["addressID"]); }
                }

                NewAddressID = DBSupport.UpdateAddress(addressID, obj.Address.GetAddressLineOne() + "UPDATED", obj.Address.GetAddressLineTwo(),
                    obj.Address.GetCity(),obj.Address.GetProvince(),obj.Address.GetPhoneNumber());
            }

            // ASSERT
            Assert.AreEqual(successfulReturn, methodReturns);
        }

        /// <summary>
        /// Test find a appointment by patientID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindAppointmentByPatientID()
        {
            // ARRANGE
            Int32 statusReturn = 0, successfulReturn = 1;
            DataTable dt = null;

            // ACT
            dt = DBSupport.FindAppointments(1);
            if (dt.Rows.Count > 0)
            {
                statusReturn = 1;
            }

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }

        /// <summary>
        /// Test find a appointment by appointmentID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindAppointmentByAppointmentID()
        {
            // ARRANGE
            Int32 statusReturn = 1, successfulReturn = 1;
            DataTable dt = null;

            // ACT
            dt = DBSupport.FindAppointment(1);
            statusReturn = dt.Rows.Count;

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// Test find a appointment by appointmentID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void AppointmentLineByAppointmentID()
        {
            // ARRANGE
            Int32 statusReturn = 1, successfulReturn = 2;
            DataTable dt = null;

            // ACT
            dt = DBSupport.FindAnAppointmentLine(1);
            if (dt.Rows.Count > 0)
            {
                statusReturn = 2;
            }


            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// Test find a appointment by appointmentID
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void CountAppointmentByappointmentDate()
        {
            // ARRANGE
            Int32 statusReturn = 1, successfulReturn = 1;
            DateTime myDayAndTime = DateTime.MinValue;
            String myDate = "2018-01-01 10:00:00";

            // ACT
            myDayAndTime = DateTime.Parse(myDate);
            statusReturn = DBSupport.CountAppointment(myDayAndTime);

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }

        /// <summary>
        /// Test insert a new appointment
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void InsertNEWAppointment()
        {
            // ARRANGE
            Int32 successfulReturn = 1, methodReturns = -1;
            Int32 newIndexAddress = DBSupport.GetNextAddressID();
            Int32 newIndexAppointment = 0;
            Patient obj = GetRegularPatient();
            DateTime myDayAndTime = DateTime.MinValue;
            //String myDate = "2018-01-02 13:00:00.000";
            //myDayAndTime = DateTime.Parse(myDate);
            myDayAndTime = DateTime.Now;

            if (NewHCN == null)
            {
                NewHCN = RandomNumbers(10);
                NewHCN = NewHCN + RandomString(2);
            }

            // ACT
            methodReturns = DBSupport.InsertANewAddress(obj.Address.GetAddressLineOne(), obj.Address.GetAddressLineTwo(),
                obj.Address.GetCity(), obj.Address.GetProvince(), obj.Address.GetPhoneNumber(), NewHCN);


            if (methodReturns == 1)
            {
                NewAddressID = DBSupport.GetNextAddressID() - 1;

                methodReturns = DBSupport.InsertANewAPatient(NewHCN, obj.GetLname(), obj.GetFname(), obj.GetMinitial(), obj.DateOfBirth,
                    obj.GetSex(), obj.GetHeadOfHouse(), NewAddressID);
            }

            if (methodReturns == 1)
            {
                newIndexAppointment = DBSupport.GetNextAppointment();

                methodReturns = DBSupport.InsertANewAppointment(myDayAndTime, 1, 2);
            }

            // ASSERT
            Assert.AreEqual(successfulReturn, methodReturns);
        }

        /// <summary>
        /// "NO FUNCTIONAL YET" Test find a appointment by DateTime begin, DateTime end
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void FindAppointmentByDateTime()
        {
            // ARRANGE
            Int32 successfulReturn = 1, methodReturns = -1;
            DateTime myDayAndTime = DateTime.MinValue;
            String myDate = "2018-01-01 13:00:00.000";
            myDayAndTime = DateTime.Parse(myDate);
            myDayAndTime = myDayAndTime.AddMinutes(1);
            DataTable dt = null;

            // ACT
            myDayAndTime = DateTime.Parse(myDate);
            dt = DBSupport.FindAppointment(myDayAndTime, myDayAndTime.AddYears(1));
            methodReturns = dt.Rows.Count;
            if (methodReturns>0) { methodReturns = 1; }

            // ASSERT
                Assert.AreEqual(successfulReturn, methodReturns);
        }

        /// <summary>
        /// "NO FUNCTIONAL YET" Test will update and existente Appointment.
        /// </summary>
        [TestMethod]
        [TestCategory("DBSupport Class - Demographics")]
        public void UpdateAppointment()
        {
            // ARRANGE
            Int32 successfulReturn = 1, methodReturns = -1;
            DateTime myDayAndTime = DateTime.MinValue;
            String myDate = "2018-01-01 13:00:00.000";
            myDayAndTime = DateTime.Parse(myDate);
            myDayAndTime = myDayAndTime.AddMinutes(1);

            // ACT
            myDayAndTime = DateTime.Parse(myDate);
            methodReturns = DBSupport.UpdateAppointment(1, myDayAndTime.AddYears(1), 1,1);
  
            // ASSERT
            Assert.AreEqual(successfulReturn, methodReturns);
        }
        #endregion


        #region Billing DBSupport tests



        #endregion

    }
}
