﻿/*****************************************************************************************************
* FILE : 		DemographicsLibraryTesting.cs
* PROJECT : 	SQ-I Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2017-Dec-05

* DESCRIPTION/Requirements: 

` It is a Unit Test Class to test the Demographics Tracker Library Class Library stuff.  

* Refence:

*****************************************************************************************************/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demographics;
using System.Globalization;
using System.Collections.Generic;
using Billing;

namespace EMS_Testing
{
    //Petterson - I've added all these teste below into the BusinessTrackerTesting class.  



    //[TestClass]
    //public class DemographicsLibraryTesting
    //{
    //    [TestMethod]
    //    public void TestMethod1()
    //    {
    //        List<Patient> fakeListOfPatients = new List<Patient>(10);
    //        List<Appointment> fakeListOfAppointments = new List<Appointment>(10);

    //        // CREATE FAKE APPOINTMENTS  *************************************************************************
    //        //moved from Bill tests
    //        int appointmentID = 0;
    //        //int appointmentPatientID = 0;
    //        //int appointmentSecondPatientID = 0;
    //        DateTime appointmentDates = new DateTime();
    //        appointmentDates = DateTime.ParseExact("20171101", "yyyyMMdd", CultureInfo.InvariantCulture);

    //        foreach (Appointment appointmentRecord in fakeListOfAppointments)
    //        {
    //            appointmentRecord.SetApointmentID(appointmentID++);
    //            appointmentRecord.SetDayAndTime(appointmentDates.AddDays(1));
    //            //appointmentRecord.SetPatientID(appointmentPatientID++);
    //            //appointmentRecord.SetSecondPatientID(appointmentSecondPatientID);
    //        }


    //        // CREATE FAKE ADDRESSES  *************************************************************************
    //        //moved from Bill tests
    //        int addresstID = 0;
    //        int cityCounter = -1;
    //        int provinceCounter = -1;
    //        //int hcnCounter = -1;
    //        int officeCounter = 0;
    //        int streetCounter = 0;
    //        int phoneCounter = -1;
    //        List<string> provinces = new List<string>();
    //        provinces.Add("ON");
    //        provinces.Add("AB");
    //        provinces.Add("BC");
    //        provinces.Add("NL");
    //        provinces.Add("NS");
    //        provinces.Add("PE");
    //        provinces.Add("NB");
    //        provinces.Add("QC");
    //        provinces.Add("MB");
    //        provinces.Add("SK");
    //        List<string> cities = new List<string>();
    //        cities.Add("Kitchener");
    //        cities.Add("Waterloo");
    //        cities.Add("Cambrodge");
    //        cities.Add("JustSomeCity");
    //        cities.Add("Toronto");
    //        cities.Add("AnotherCity");
    //        cities.Add("FarAwayCity");
    //        cities.Add("Montreal");
    //        cities.Add("OneMoreCity");
    //        cities.Add("JustACiy");

    //        List<string> hcns = new List<string>();
    //        hcns.Add("1234567890AA");
    //        hcns.Add("1234567890AB");
    //        hcns.Add("1234567890AC");
    //        hcns.Add("1234567890AG");
    //        hcns.Add("1234567890AJ");
    //        hcns.Add("1234567890AL");
    //        hcns.Add("1234567890AN");
    //        hcns.Add("1234567890AV");
    //        hcns.Add("1234567890AI");
    //        hcns.Add("1234567890AP");

    //        List<string> phones = new List<string>();
    //        phones.Add("(519)635-1111");
    //        phones.Add("(519)635-1112");
    //        phones.Add("(519)635-1113");
    //        phones.Add("(519)635-1114");
    //        phones.Add("(519)635-1115");
    //        phones.Add("(519)635-1116");
    //        phones.Add("(519)635-1118");
    //        phones.Add("(519)635-1123");
    //        phones.Add("(519)635-1134");
    //        phones.Add("(519)635-1178");
    //        List<Address> fakeListOfAddresses = new List<Address>(10);

    //        foreach (Address addressRecord in fakeListOfAddresses)
    //        {
    //            addressRecord.SetAdressID(addresstID++);
    //            addressRecord.SetAdressLineOne("NameOf street" + streetCounter.ToString());
    //            addressRecord.SetAdressLineTwo("Office" + officeCounter.ToString());
    //            addressRecord.SetCity(cities[cityCounter++]);
    //            //addressRecord.SetHCN(hcns[hcnCounter++]);
    //            addressRecord.SetPhoneNumber(phones[phoneCounter++]);
    //            addressRecord.SetProvince(provinces[provinceCounter++]);
    //        }
    //    }

    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void Address_ObjInstatiation_Instantiated()
    //    {
    //        // ARRANGE
    //        int statusReturn = 0;

    //    // ACT
    //        Address testAddressObj = new Address();
    //        if (testAddressObj == null)
    //        {
    //            statusReturn = -1;
    //        }
    //        else
    //        {
    //            statusReturn = 0;
    //        }

    //        // ASSERT
    //        Assert.AreEqual(statusReturn, 0);
    //    }


    //    #region Fast Settup
    //    private bool patientHasAddress = true;
    //    private bool appointmentHasSecondPatient = true;
    //    private bool serviceLineHasService = true;
    //    private bool billHasServices = true;
    //    #endregion


    //    #region Regular Objects

    //    /// <summary>
    //    /// Get a Patient Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("Patient Class")]
    //    private Patient GetRegularPatient()
    //    {
    //        int patientID = 557;
    //        string HCN = "1234561231AZ";
    //        string lName = "LAST";
    //        string fName = "FIRST";
    //        char mInitial = 'I';
    //        string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
    //        string sex = "M";
    //        string headOfHouse = "9879876547QQ";

    //        Patient obj = new Patient();

    //        obj.PatientID = patientID;
    //        obj.SetFname(fName);
    //        obj.SetLname(lName);
    //        obj.SethCN(HCN);
    //        obj.SetMinitial(mInitial);
    //        obj.DateOfBirth = DateTime.Parse(dateOfBirth);
    //        obj.SetSex(sex);
    //        obj.SetHeadOfHouse(headOfHouse);

    //        if (patientHasAddress)
    //        {
    //            int addressID = 562;
    //            string adressLineOne = "299 Drive St";
    //            string adressLineTwo = "120 Tower 2";
    //            string city = "Kitchener";
    //            string province = "ON";
    //            string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

    //            obj.Address.SetAdressID(addressID);
    //            obj.Address.SetAdressLineOne(adressLineOne);
    //            obj.Address.SetAdressLineTwo(adressLineTwo);
    //            obj.Address.SetCity(city);
    //            obj.Address.SetProvince(province);
    //            obj.Address.SetPhoneNumber(phoneNumber);
    //        }

    //        return obj;
    //    }

    //    /// <summary>
    //    /// Get a Appointment Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("Appointment Class")]
    //    private Appointment GetRegularAppointment()
    //    {
    //        int appointmentID = 505;
    //        DateTime dayAndTime = DateTime.Parse("2018-01-01 10:00:00");

    //        Appointment obj = new Appointment();

    //        obj.SetApointmentID(appointmentID);
    //        obj.SetDayAndTime(dayAndTime);

    //        obj.FirstPatient = GetRegularPatient();
    //        if (appointmentHasSecondPatient)
    //        {
    //            obj.SecondPatient = GetRegularPatient();
    //        }

    //        return obj;
    //    }

    //    /// <summary>
    //    /// Get a Bill Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("Bill Class")]
    //    private Bill GetRegularBill()
    //    {
    //        int billID = 33;
    //        int flagRecal = 1;
    //        bool statusDone = false;

    //        Bill obj = new Bill();
    //        obj.SetBillID(billID);
    //        obj.SetFlagRecall(flagRecal);
    //        obj.StatusDone = statusDone;

    //        obj.Patient = GetRegularPatient();
    //        obj.Appointment = GetRegularAppointment();

    //        if (billHasServices)
    //        {
    //            obj.Services.Add(GetRegularServiceListOne());
    //            obj.Services.Add(GetRegularServiceListTwo());
    //        }

    //        return obj;
    //    }

    //    /// <summary>
    //    /// Get a ServiceListOne Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("ServiceListOne Class")]
    //    private ServiceList GetRegularServiceListOne()
    //    {
    //        int quantity = 1;
    //        string status = ServiceList.ServiceStatus.UNPROCESSED.ToString();

    //        ServiceList obj = new ServiceList();

    //        obj.SetQuantity(quantity);
    //        obj.SetStatus(status);

    //        if (serviceLineHasService)
    //        {
    //            obj.Service = GetRegularServiceOne();
    //        }

    //        return obj;
    //    }

    //    /// <summary>
    //    /// Get a ServiceListOne Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("ServiceListOne Class")]
    //    private ServiceList GetRegularServiceListTwo()
    //    {
    //        int quantity = 1;
    //        string status = ServiceList.ServiceStatus.PAID.ToString();

    //        ServiceList obj = new ServiceList();

    //        obj.SetQuantity(quantity);
    //        obj.SetStatus(status);

    //        if (serviceLineHasService)
    //        {
    //            obj.Service = GetRegularServiceTwo();
    //        }

    //        return obj;
    //    }

    //    /// <summary>
    //    /// Get a ServiceOne Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("ServiceOne Class")]
    //    private Service GetRegularServiceOne()
    //    {
    //        int serviceID = 65;
    //        string feeCode = "A661";
    //        DateTime effectiveDate = DateTime.Parse("05-05-1999");
    //        double serviceValue = 39.99;

    //        Service ss = new Service();

    //        ss.SetServiceID(serviceID);
    //        ss.SetFeeCode(feeCode);
    //        ss.SetServiceValue(serviceValue);
    //        ss.EffectiveDate = effectiveDate;

    //        return ss;
    //    }

    //    /// <summary>
    //    /// Get a ServiceTwo Class
    //    /// </summary>
    //    /// <returns></returns>
    //    [TestCategory("ServiceTwo Class")]
    //    private Service GetRegularServiceTwo()
    //    {
    //        int serviceID = 61;
    //        string feeCode = "A112";
    //        DateTime effectiveDate = DateTime.Parse("05-05-1992");
    //        double serviceValue = 98.95;

    //        Service ss = new Service();

    //        ss.SetServiceID(serviceID);
    //        ss.SetFeeCode(feeCode);
    //        ss.SetServiceValue(serviceValue);
    //        ss.EffectiveDate = effectiveDate;

    //        return ss;
    //    }

    //    #endregion





    //    /// <summary>
    //    /// This test attempts to set a valid AdressID
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAddressID_Valid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        int testID = 765;
    //        testAdressObj.SetAdressID(testID);
    //        // ASSERT
    //        Assert.AreEqual(testAdressObj.GetAdressID(), testID);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid AdressID
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAddressID_Invalid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        int testID = -765;
    //        bool statusRet=testAdressObj.SetAdressID(testID);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a valid adressLineOne
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineOne_Valid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        string testadressLineOne = "Doov Valley Dr 123";
    //        testAdressObj.SetAdressLineOne(testadressLineOne);
    //        // ASSERT
    //        Assert.AreEqual(testAdressObj.GetAddressLineOne(), testadressLineOne);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid adressLineOne( too long)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddressSet_AdressLineOne_Invalid_LongAdressLineOne()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testadressLineOne = "zxcvhbhjghbnmasdfghjklqwertyuiopzxcvbnm1234567890qwertyuiopasdfghjk";
    //        bool statusRet = testAdressObj.SetAdressLineOne(testadressLineOne);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid adressLineOne(empty)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineOne_Invalid_Empty()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testadressLineOne = "";
    //        bool statusRet = testAdressObj.SetAdressLineOne(testadressLineOne);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid characters adressLineOne
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineOne_Invalid_Characters()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testadressLineOne = "!@#";
    //        bool statusRet = testAdressObj.SetAdressLineOne(testadressLineOne);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a valid adressLineTwo
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineTwo_Valid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        string testadressLineTwo = "Office 4";
    //        testAdressObj.SetAdressLineTwo(testadressLineTwo);
    //        // ASSERT
    //        Assert.AreEqual(testAdressObj.GetAddressLineTwo(), testadressLineTwo);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid adressLineTwo( too long)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineTwo_Invalid_LongAdressLineTwo()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testadressLineTwo = "zxcvhbhjghbnmasdfghjklqwertyuiopzxcvbnm1234567890qwertyuiopasdfghjk";
    //        bool statusRet = testAdressObj.SetAdressLineTwo(testadressLineTwo);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid adressLineTwo(empty)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineTwo_Invalid_Empty()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = true;
    //        string testadressLineTwo = "";
    //        bool statusRet = testAdressObj.SetAdressLineTwo(testadressLineTwo);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid characters adressLineTwo
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetAdressLineTwp_Invalid_Characters()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testadressLineTwo = "!@#";
    //        bool statusRet = testAdressObj.SetAdressLineTwo(testadressLineTwo);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a valid city
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_City_Valid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        string testCity = "Moscow";
    //        testAdressObj.SetCity(testCity);
    //        // ASSERT
    //        Assert.AreEqual(testAdressObj.GetCity(), testCity);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid city( too long)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetCity_Invalid_LongCity()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testCity = "zxcvhbhjghbnmasdfghjklqwertyuiopzxcvbnm1234567890qwertyuiopasdfghjk";
    //        bool statusRet = testAdressObj.SetCity(testCity);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid city(empty)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetCity_Invalid_Empty()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testCity = "";
    //        bool statusRet = testAdressObj.SetCity(testCity);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid characters city
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_City_Invalid_Characters()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testCity = "!@#";
    //        bool statusRet = testAdressObj.SetCity(testCity);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a valid province
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_Province_Valid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        string testProvince = "ON";
    //        testAdressObj.SetProvince(testProvince);
    //        // ASSERT
    //        Assert.AreEqual(testAdressObj.GetProvince(), testProvince);
    //    }

    //    /// <summary>
    //    /// This test attempts to set a invalid province(empty)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetProvince_Invalid_Empty()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testProvince = "";
    //        bool statusRet = testAdressObj.SetProvince(testProvince);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid province name
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetProvince_Invalid_Name()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testProvince = "MM";
    //        bool statusRet = testAdressObj.SetProvince(testProvince);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid province format
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetProvince_Invalid_Format()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testProvince = "oN";
    //        bool statusRet = testAdressObj.SetProvince(testProvince);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid province lenght
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetProvince_Invalid_Lenght()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testProvince = "OON";
    //        bool statusRet = testAdressObj.SetProvince(testProvince);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }

    //    /// <summary>
    //    /// This test attempts to set a invalid province characters
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_SetProvince_Invalid_Characters()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testProvince = "1-";
    //        bool statusRet = testAdressObj.SetProvince(testProvince);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a valid phoneNumber
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_PhoneNumber_Valid()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        string testPhoneNumber = "(519)635-5129";
    //        testAdressObj.SetPhoneNumber(testPhoneNumber);
    //        // ASSERT
    //        Assert.AreEqual(testAdressObj.GetPhoneNumber(), testPhoneNumber);
    //    }

    //    /// <summary>
    //    /// This test attempts to set a invalid phoneNumber(empty)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_PhoneNumber_Invalid_Empty()
    //    {

    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testPhoneNumber = "";
    //        bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid phoneNumber area code
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_PhoneNumber_Invalid_Area_Code()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testPhoneNumber = "(985)-251-1724";
    //        bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid phoneNumber lenght
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_PhoneNumber_Invalid_Lenght()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testPhoneNumber = "(519)635-51295";
    //        bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a invalid phoneNumber format
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_PhoneNumber_Invalid_Format()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        bool statusReturnShouldBe = false;
    //        string testPhoneNumber = "519-635-5129";
    //        bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test checks function IsEmpty, sends empty string
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_IsEmpty_Empty()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        // ASSERT
    //        Assert.IsTrue(testAdressObj.IsEmpty);
    //    }


    //    /// <summary>
    //    /// This test checks function IsEmpty, sends non empty string
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Address Class")]
    //    public void SetAddress_IsEmpty_Non_Empty()
    //    {
    //        // ACT
    //        Address testAdressObj = new Address();
    //        testAdressObj.SetAdressLineOne("dadadad");
    //        // ASSERT
    //        Assert.IsFalse(testAdressObj.IsEmpty);
    //    }




    //    /// <summary>
    //    /// This test attempts to set a invalid AppointmentID
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void SetAddress_SetAppointmentID_Invalid()
    //    {

    //        // ACT
    //        Appointment testAppointmentObj = new Appointment();
    //        bool statusReturnShouldBe = false;
    //        int testID = -765;
    //        bool statusRet = testAppointmentObj.SetApointmentID(testID);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test attempts to set a valid dayAndTime
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void SetAddress_SetDayAndTime_Valid()
    //    {

    //        // ACT
    //        Appointment testAppointmentObj = new Appointment();
    //        bool statusReturnShouldBe = true;
    //        DateTime testDayAndTime = new DateTime(2017, 11, 15, 10, 0, 0); 
    //        bool statusRet = testAppointmentObj.SetDayAndTime(testDayAndTime);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid dayAndTime (10.30 am)
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void SetAddress_SetDayAndTime_Invalid_Wrong_Time_Week_Day()
    //    {

    //        // ACT
    //        Appointment testAppointmentObj = new Appointment();
    //        bool statusReturnShouldBe = false;
    //        DateTime testDayAndTime = new DateTime(2017, 11, 15, 10, 30, 0);
    //        bool statusRet = testAppointmentObj.SetDayAndTime(testDayAndTime);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }



    //    /// <summary>
    //    /// This test attempts to set a invalid dayAndTime late weekend appointment
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void SetAddress_SetDayAndTime_Invalid_Wrong_Time_Weekend_Day()
    //    {

    //        // ACT
    //        Appointment testAppointmentObj = new Appointment();
    //        bool statusReturnShouldBe = false;
    //        DateTime testDayAndTime = new DateTime(2017, 11, 18, 14, 0, 0);
    //        bool statusRet = testAppointmentObj.SetDayAndTime(testDayAndTime);
    //        // ASSERT
    //        Assert.AreEqual(statusRet, statusReturnShouldBe);
    //    }


    //    /// <summary>
    //    /// This test checks if appointment object was successfully created
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void Appointment_ObjInstatiation_Instantiated()
    //    {
    //        // ARRANGE
    //        int statusReturn = 0;

    //        // ACT
    //        Appointment testAppointmentObj = new Appointment();
    //        if (testAppointmentObj == null)
    //        {
    //            statusReturn = -1;
    //        }
    //        else
    //        {
    //            statusReturn = 0;
    //        }

    //        // ASSERT
    //        Assert.AreEqual(statusReturn, 0);
    //    }


    //    /// <summary>
    //    /// This tests a valid Appointment.
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void ValidateAppointment()
    //    {
    //        //Arrange + Assert
    //        Appointment myAppointment = GetRegularAppointment();


    //        //Act
    //        int Result = myAppointment.ValidateAppointment(myAppointment);

    //        //Assert
    //        Assert.AreEqual(0, Result);

    //        // This is failing because the head of household is not set. Isn't that optional?
    //    }


    //    /// <summary>
    //    /// This test attempts to set a valid AppointmentID
    //    /// </summary>
    //    [TestMethod]
    //    [TestCategory("Appointment Class")]
    //    public void SetAppointment_SetAppointmentID_Valid()
    //    {

    //        // ACT
    //        Appointment testAppointmentObj = new Appointment();
    //        int testID = 765;
    //        testAppointmentObj.SetApointmentID(testID);
    //        // ASSERT
    //        Assert.AreEqual(testAppointmentObj.GetAppointmentID(), testID);
    //    }

    //    /*
    //            /// <summary>
    //            /// This test attempts to set a valid PatienttID
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Appointment Class")]
    //            public void SetAppointment_SetPatientID_Valid()
    //            {

    //                // ACT
    //                Appointment testAppointmentObj = new Appointment();
    //                int testID = 765;
    //                testAppointmentObj.SetPatientID(testID);
    //                // ASSERT
    //                Assert.AreEqual(testAppointmentObj.GetPatientID(), testID);
    //            }


    //            /// <summary>
    //            /// This test attempts to set a invalid PatientID
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Appointment Class")]
    //            public void SetAddress_SetPatientID_Invalid()
    //            {

    //                // ACT
    //                Appointment testAppointmentObj = new Appointment();
    //                bool statusReturnShouldBe = false;
    //                int testID = -765;
    //                bool statusRet = testAppointmentObj.SetPatientID(testID);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to set a valid SecondPatienttID
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Appointment Class")]
    //            public void SetAppointment_SetSecondPatientID_Valid()
    //            {

    //                // ACT
    //                Appointment testAppointmentObj = new Appointment();
    //                int testID = 765;
    //                testAppointmentObj.SetSecondPatientID(testID);
    //                // ASSERT
    //                Assert.AreEqual(testAppointmentObj.GetSecondPatientID(), testID);
    //            }


    //            /// <summary>
    //            /// This test attempts to set a invalid SecondPatientID
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Appointment Class")]
    //            public void SetAddress_SetSecondPatientID_Invalid()
    //            {

    //                // ACT
    //                Appointment testAppointmentObj = new Appointment();
    //                bool statusReturnShouldBe = false;
    //                int testID = -765;
    //                bool statusRet = testAppointmentObj.SetSecondPatientID(testID);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test checks if Tracker object was successfully created
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Tracker Class")]
    //            public void Tracker_ObjInstatiation_Instantiated()
    //            {
    //                // ARRANGE
    //                int statusReturn = 0;

    //                // ACT
    //                Tracker testTrackerObj = new Tracker();
    //                if (testTrackerObj == null)
    //                {
    //                    statusReturn = -1;
    //                }
    //                else
    //                {
    //                    statusReturn = 0;
    //                }

    //                // ASSERT
    //                Assert.AreEqual(statusReturn, 0);
    //            }


    //            /// <summary>
    //            /// This test attempts to set an invalid patient object( not all fileds fullfield) 
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddPatient_Invalid_Empty_field()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                Patient testPatientObj = new Patient();
    //                testPatientObj.SetPatientID(0);
    //                testPatientObj.SetFname("Anastasiia");
    //                testPatientObj.SetLname("Korneva");
    //                testPatientObj.SethCN("1234567890AA");
    //                testPatientObj.SetdateOfBirth("");
    //                testPatientObj.SetSex("F");
    //                testPatientObj.SetAddressID(1);
    //                int statusReturnShouldBe = -3421;
    //                int statusRet = testTrackerObj.AddPatient(testPatientObj);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to set an valid patient object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddPatient_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                Patient testPatientObj = new Patient();
    //                //testPatientObj.SetPatientID(0);
    //                testPatientObj.SetFname("Anastasiia");
    //                testPatientObj.SetLname("Korneva");
    //                testPatientObj.SethCN("1234567890AA");
    //                testPatientObj.SetdateOfBirth("23-06-1998");
    //                testPatientObj.SetSex("F");
    //                testPatientObj.SetAddressID(1);
    //                int statusReturnShouldBe = 0;
    //                int statusRet = testTrackerObj.AddPatient(testPatientObj);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to set the same patient srcond time
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddPatient_Invalid_Duplicate_Patient()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                Patient testPatientObj = new Patient();
    //                //testPatientObj.SetPatientID(0);
    //                testPatientObj.SetFname("Anastasiia");
    //                testPatientObj.SetLname("Korneva");
    //                testPatientObj.SethCN("1234567890AA");
    //                testPatientObj.SetdateOfBirth("23-06-1998");
    //                testPatientObj.SetSex("F");
    //                testPatientObj.SetAddressID(1);
    //                int statusReturnShouldBe = 0;
    //                int statusRet = testTrackerObj.AddPatient(testPatientObj);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }

    //            /// <summary>
    //            /// This test attempts to set an valid address object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddAddress_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                Address testAddressObj = new Address(2, "1234567890AA", "ghgh", "office 2", "Kitchener", "ON", "519-635-5129");
    //                int statusReturnShouldBe = 0;
    //                int statusRet = testTrackerObj.AddAddress(testAddressObj);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to set an invalid address object, try to add the same adress second time
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddAddress__Invalid_Duplicate_Adress()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                Address testAddressObj = new Address(2, "1234567890AA", "ghgh", "office 2", "Kitchener", "ON", "519-635-5129");
    //                int statusReturnShouldBe = 0;
    //                int statusRet = testTrackerObj.AddAddress(testAddressObj);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to set an invalid address object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddAddress__Invalid_Input()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                Address testAddressObj = new Address(2, "1234567890AA", "", "office 2", "Kitchener", "ON", "519-635-5129");
    //                int statusReturnShouldBe = 0;
    //                int statusRet = testTrackerObj.AddAddress(testAddressObj);
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to set an valid appointment object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddApoointment_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to set an invalid address object, try to add the appointment for booked time
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddAddress__Invalid_Duplicate_Appointment()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to set an invalid appointment object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_AddAAppointment_Invalid_Input()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to find appointment by existing patient ID
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindApoointment_ByPatientID_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find appointment by not existing patient ID
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindApoointment_ByPatientID_IDNotExists()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find appointment by existing day
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindApoointment_ByDayAndTime_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find appointment by not existing day
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindApoointment_ByDayAndTime_IfNotExists()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find addres by HCN in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindAddress_ByHCN_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find addres by HCN in list not in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindAddress_ByHCN_IfNotExists()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find patient by HCN or last name is HCN or last name are in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindPatient_ByObj_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find addres by patient by HCN or last name is HCN or last name are not in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindPatient_ByObj_IfNotExists()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find patient by patientID is it is in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindPatient_ByPatientID_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find patient by patientID is it is not in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindPatient_ByPatientID_IfNotExists()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find appointment by appointment is it is in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindAppointment_ByAppointmentID_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to find y appointment is it is not in list
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_FindAppointment_ByAppointmentID_IfNotExists()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to update valid patient
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdatePatient_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to update patient to invalid one
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdatePatient_Invalid_Patient_Obj()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to update patient to null object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdatePatient_Invalid_Null_Patient_Obj()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to update valid adress
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdateAdress_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to update adress to invalid one
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdateAddress_Invalid_Adress_Obj()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to adress patient to null object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdateAdress_Invalid_Null_Adress_Obj()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to update valid appointment
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdateAppointment_Valid()
    //            {
    //                // ACT
    //                Demographics.Tracker testTrackerObj = new Demographics.Tracker();
    //                int statusReturnShouldBe = 0;
    //                int statusRet = 0;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }



    //            /// <summary>
    //            /// This test attempts to update appointment to invalid one
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdateAppointment_Invalid_Appointment_Obj()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }


    //            /// <summary>
    //            /// This test attempts to adress appointment to null object
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class")]
    //            public void SetTracker_UpdateAppointment_Invalid_Null_Appointment_Obj()
    //            {
    //                // ACT

    //                int statusReturnShouldBe = -1;
    //                int statusRet = -1;
    //                // ASSERT
    //                Assert.AreEqual(statusRet, statusReturnShouldBe);
    //            }
    //            // ---------------------------------------------- 
    //            // ----------Demographic File I/O stuff---------- 
    //            // ----------------------------------------------

    //            /// <summary>
    //            /// Test to read all Demographics files and load to a list.
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void ReadFullServiceFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test to write all Demographics files and load to a list.
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void WriteDemographicsFiles()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test add a patient object on patient list from the patient file.  
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void AddPatientFromFileToList()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test add a patient object into a patient file.  
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void AddPatientToFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test add a address object on address list from the address file.  
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void AddAddressFromFileToList()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test add a address object into a address file.  
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void AddAddressToFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test add an appointment object on appointment list from the appointment file.  
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void AddAppointmentFromFileToList()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test add an appointment object into a appointment file.  
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void AddAppointmentToFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test creating a line into the DB Patient file. 
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void WriteOnePatientObjIntoFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test creating a line into the DB Address file. 
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void WriteOneAddressObjIntoFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Test creating a line into the DB Appointment file. 
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void WriteOneAppointmentObjIntoFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Creates a Patient object by parsing a string line from file.
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void ReadPatientFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Creates a Address object by parsing a string line from file.
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void ReadAddressFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }


    //            /// <summary>
    //            /// Creates a Appointment object by parsing a string line from file.
    //            /// </summary>
    //            [TestMethod]
    //            [TestCategory("Demographics Tracker Class FileIO stuff")]
    //            public void ReadAppointmentFile()
    //            {
    //                bool result = false;
    //                //string service = "";

    //                //
    //                // TODO: Add test logic here
    //                //

    //                // act
    //                //result = fileIO.ReadFullServiceFile(ref service);

    //                //Assert
    //                Assert.AreEqual(true, result);
    //            }
    //    */
    //}
}
