﻿/*****************************************************************************************************
* FILE : 		BusinessTrackerTesting.cs
* PROJECT : 	SQ-II Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2018-Apr-17

* DESCRIPTION/Requirements: 

` It is a Unit Test Class to test the BusinessTrackerTesting Library Class Library stuff.  

* Refence:

*****************************************************************************************************/


using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demographics;
using System.Globalization;
using System.Collections.Generic;
using Billing;

namespace EMS_Testing
{
    [TestClass]
    public class BusinessTrackerTesting
    {
        #region Fast Settup
        private bool patientHasAddress = true;
        private bool appointmentHasSecondPatient = true;
        private bool serviceLineHasService = true;
        private bool billHasServices = true;
        private BusinessTracker.BusinessTracker myBusinessTracker = BusinessTracker.BusinessTracker.Instance;

        #endregion


        #region Regular Objects

        /// <summary>
        /// Get a Patient Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Patient Class")]
        private Patient GetRegularPatient()
        {
            int patientID = 2;
            string HCN = "9879876547QQ";
            string lName = "LAST";
            string fName = "FIRST";
            char mInitial = 'I';
            string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
            string sex = "M";
            string headOfHouse = "1234561232AZ";

            Patient obj = new Patient();

            obj.PatientID = patientID;
            obj.SetFname(fName);
            obj.SetLname(lName);
            obj.SethCN(HCN);
            obj.SetMinitial(mInitial);
            obj.DateOfBirth = DateTime.Parse(dateOfBirth);
            obj.SetSex(sex);
            obj.SetHeadOfHouse(headOfHouse);

            if (patientHasAddress)
            {
                int addressID = 2;
                string adressLineOne = "299 Drive St";
                string adressLineTwo = "120 Tower 2";
                string city = "Kitchener";
                string province = "ON";
                string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

                obj.Address.SetAdressID(addressID);
                obj.Address.SetAdressLineOne(adressLineOne);
                obj.Address.SetAdressLineTwo(adressLineTwo);
                obj.Address.SetCity(city);
                obj.Address.SetProvince(province);
                obj.Address.SetPhoneNumber(phoneNumber);
            }

            return obj;
        }

        /// <summary>
        /// Get a Patient Class with out headOfHouse
        /// </summary>
        /// <returns></returns>
        [TestCategory("Patient Class")]
        private Patient GetRegularPatientWOHoH()
        {
            int patientID = 1;
            string HCN = "1234561232AZ";
            string lName = "WOHoH";
            string fName = "FIRST";
            char mInitial = 'I';
            string dateOfBirth = "05-05-2007"; //dd-MM-yyyy
            string sex = "M";
            string headOfHouse = "";

            Patient obj = new Patient();

            obj.PatientID = patientID;
            obj.SetFname(fName);
            obj.SetLname(lName);
            obj.SethCN(HCN);
            obj.SetMinitial(mInitial);
            obj.DateOfBirth = DateTime.Parse(dateOfBirth);
            obj.SetSex(sex);
            obj.SetHeadOfHouse(headOfHouse);

            if (patientHasAddress)
            {
                int addressID = 1;
                string adressLineOne = "299 Drive St";
                string adressLineTwo = "120 Tower 2";
                string city = "Kitchener";
                string province = "ON";
                string phoneNumber = "(519)555-1345"; // (xxx)xxx-xxxx

                obj.Address.SetAdressID(addressID);
                obj.Address.SetAdressLineOne(adressLineOne);
                obj.Address.SetAdressLineTwo(adressLineTwo);
                obj.Address.SetCity(city);
                obj.Address.SetProvince(province);
                obj.Address.SetPhoneNumber(phoneNumber);
            }

            return obj;
        }

        /// <summary>
        /// Get a Appointment Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Appointment Class")]
        private Appointment GetRegularAppointment()
        {
            int appointmentID = 505;
            DateTime dayAndTime = DateTime.Parse("2018-01-01 10:00:00");

            Appointment obj = new Appointment();

            obj.SetApointmentID(appointmentID);
            obj.SetDayAndTime(dayAndTime);

            obj.FirstPatient = GetRegularPatient();
            obj.FirstPatient.SetPatientID(1);
            if (appointmentHasSecondPatient)
            {
                obj.SecondPatient = GetRegularPatient();
                obj.SecondPatient.SetPatientID(1 + obj.FirstPatient.GetPatientID());
            }

            return obj;
        }

        /// <summary>
        /// Get a Appointment Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Appointment Class")]
        private Appointment GetRegularAppointment1()
        {
            int appointmentID = 1;
            DateTime dayAndTime = DateTime.Parse("2018-01-02 11:00:00");

            Appointment obj = new Appointment();

            obj.SetApointmentID(appointmentID);
            obj.SetDayAndTime(dayAndTime);

            obj.FirstPatient = GetRegularPatient();
            if (appointmentHasSecondPatient)
            {
                obj.SecondPatient = GetRegularPatient();
            }

            return obj;
        }

        /// <summary>
        /// Get a Bill Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("Bill Class")]
        private Bill GetRegularBill()
        {
            int billID = 33;
            int flagRecal = 1;
            bool statusDone = false;

            Bill obj = new Bill();
            obj.SetBillID(billID);
            obj.SetFlagRecall(flagRecal);
            obj.StatusDone = statusDone;

            obj.Patient = GetRegularPatient();
            obj.Appointment = GetRegularAppointment();

            if (billHasServices)
            {
                obj.Services.Add(GetRegularServiceListOne());
                obj.Services.Add(GetRegularServiceListTwo());
            }

            return obj;
        }

        /// <summary>
        /// Get a ServiceListOne Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceListOne Class")]
        private ServiceList GetRegularServiceListOne()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.UPRC.ToString();

            ServiceList obj = new ServiceList();

            obj.SetQuantity(quantity);
            obj.SetStatus(status);

            if (serviceLineHasService)
            {
                obj.Service = GetRegularServiceOne();
            }

            return obj;
        }

        /// <summary>
        /// Get a ServiceListOne Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceListOne Class")]
        private ServiceList GetRegularServiceListTwo()
        {
            int quantity = 1;
            string status = ServiceList.ServiceStatus.PAID.ToString();

            ServiceList obj = new ServiceList();

            obj.SetQuantity(quantity);
            obj.SetStatus(status);

            if (serviceLineHasService)
            {
                obj.Service = GetRegularServiceTwo();
            }

            return obj;
        }

        /// <summary>
        /// Get a ServiceOne Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceOne Class")]
        private Service GetRegularServiceOne()
        {
            int serviceID = 65;
            string feeCode = "A661";
            DateTime effectiveDate = DateTime.Parse("05-05-1999");
            double serviceValue = 39.99;

            Service ss = new Service();

            ss.SetServiceID(serviceID);
            ss.SetFeeCode(feeCode);
            ss.SetServiceValue(serviceValue);
            ss.EffectiveDate = effectiveDate;

            return ss;
        }

        /// <summary>
        /// Get a ServiceTwo Class
        /// </summary>
        /// <returns></returns>
        [TestCategory("ServiceTwo Class")]
        private Service GetRegularServiceTwo()
        {
            int serviceID = 61;
            string feeCode = "A112";
            DateTime effectiveDate = DateTime.Parse("05-05-1992");
            double serviceValue = 98.95;

            Service ss = new Service();

            ss.SetServiceID(serviceID);
            ss.SetFeeCode(feeCode);
            ss.SetServiceValue(serviceValue);
            ss.EffectiveDate = effectiveDate;

            return ss;
        }

        #endregion


        #region Patient Class
        [TestMethod]
        public void TestMethod1()
        {
            List<Patient> fakeListOfPatients = new List<Patient>(10);
            List<Appointment> fakeListOfAppointments = new List<Appointment>(10);

            // CREATE FAKE APPOINTMENTS  *************************************************************************
            //moved from Bill tests
            int appointmentID = 0;
            //int appointmentPatientID = 0;
            //int appointmentSecondPatientID = 0;
            DateTime appointmentDates = new DateTime();
            appointmentDates = DateTime.ParseExact("20171101", "yyyyMMdd", CultureInfo.InvariantCulture);

            foreach (Appointment appointmentRecord in fakeListOfAppointments)
            {
                appointmentRecord.SetApointmentID(appointmentID++);
                appointmentRecord.SetDayAndTime(appointmentDates.AddDays(1));
                //appointmentRecord.SetPatientID(appointmentPatientID++);
                //appointmentRecord.SetSecondPatientID(appointmentSecondPatientID);
            }


            // CREATE FAKE ADDRESSES  *************************************************************************
            //moved from Bill tests
            int addresstID = 0;
            int cityCounter = -1;
            int provinceCounter = -1;
            //int hcnCounter = -1;
            int officeCounter = 0;
            int streetCounter = 0;
            int phoneCounter = -1;
            List<string> provinces = new List<string>();
            provinces.Add("ON");
            provinces.Add("AB");
            provinces.Add("BC");
            provinces.Add("NL");
            provinces.Add("NS");
            provinces.Add("PE");
            provinces.Add("NB");
            provinces.Add("QC");
            provinces.Add("MB");
            provinces.Add("SK");
            List<string> cities = new List<string>();
            cities.Add("Kitchener");
            cities.Add("Waterloo");
            cities.Add("Cambrodge");
            cities.Add("JustSomeCity");
            cities.Add("Toronto");
            cities.Add("AnotherCity");
            cities.Add("FarAwayCity");
            cities.Add("Montreal");
            cities.Add("OneMoreCity");
            cities.Add("JustACiy");

            List<string> hcns = new List<string>();
            hcns.Add("1234567890AA");
            hcns.Add("1234567890AB");
            hcns.Add("1234567890AC");
            hcns.Add("1234567890AG");
            hcns.Add("1234567890AJ");
            hcns.Add("1234567890AL");
            hcns.Add("1234567890AN");
            hcns.Add("1234567890AV");
            hcns.Add("1234567890AI");
            hcns.Add("1234567890AP");

            List<string> phones = new List<string>();
            phones.Add("(519)635-1111");
            phones.Add("(519)635-1112");
            phones.Add("(519)635-1113");
            phones.Add("(519)635-1114");
            phones.Add("(519)635-1115");
            phones.Add("(519)635-1116");
            phones.Add("(519)635-1118");
            phones.Add("(519)635-1123");
            phones.Add("(519)635-1134");
            phones.Add("(519)635-1178");
            List<Address> fakeListOfAddresses = new List<Address>(10);

            foreach (Address addressRecord in fakeListOfAddresses)
            {
                addressRecord.SetAdressID(addresstID++);
                addressRecord.SetAdressLineOne("NameOf street" + streetCounter.ToString());
                addressRecord.SetAdressLineTwo("Office" + officeCounter.ToString());
                addressRecord.SetCity(cities[cityCounter++]);
                //addressRecord.SetHCN(hcns[hcnCounter++]);
                addressRecord.SetPhoneNumber(phones[phoneCounter++]);
                addressRecord.SetProvince(provinces[provinceCounter++]);
            }
        }

        /// <summary>
        /// Run a add patient test, with patient data with out Hoh
        /// </summary>
        [TestMethod]
        [TestCategory("AddPatientWithOutHoH BusinessTracker")]
        public void AddPatientWithOutHoH()
        {
            // ARRANGE
            int statusReturn = 0;

            // ACT
            Patient myPatient = GetRegularPatientWOHoH();
            statusReturn = myBusinessTracker.AddPatient(myPatient);

            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Run a Update patient test, with patient data with out Hoh
        /// </summary>
        [TestMethod]
        [TestCategory("UpdatePatientWithOutHoH BusinessTracker")]
        public void UpdatePatientWithOutHoH()
        {
            // ARRANGE
            int statusReturn = 0;

            // ACT
            Patient myPatient = GetRegularPatientWOHoH();
            statusReturn = myBusinessTracker.UpdatePatient(myPatient);

            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Run a add patient test
        /// </summary>
        [TestMethod]
        [TestCategory("AddPatientWithHo BusinessTracker")]
        public void AddPatientWithHoH()
        {
            // ARRANGE
            int statusReturn = 0;

            // ACT
            Patient myPatient = GetRegularPatient();
            statusReturn = myBusinessTracker.AddPatient(myPatient);

            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }

        /// <summary>
        /// Find a patient by patientID
        /// </summary>
        [TestMethod]
        [TestCategory("FindPatient by patientID BusinessTracker")]
        public void FindPatientByPatientID()
        {
            // ARRANGE
            Int32 statusReturn = -1;

            // ACT
            Patient myPatient = myBusinessTracker.FindPatient(1);
            if (myPatient != null)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Find a appointment list by patientID
        /// </summary>
        [TestMethod]
        [TestCategory("FindAppointmentList by patientID BusinessTracker")]
        public void FindAppointmentListByPatiantID()
        {
            // ARRANGE
            Int32 statusReturn = -1;

            // ACT
            List<Appointment> myAppointmentList = myBusinessTracker.FindAppointments(1);
            if (myAppointmentList != null)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }

        /// <summary>
        /// Find a appointment list by patientID
        /// </summary>
        [TestMethod]
        [TestCategory("FindAppointmentList by Data BusinessTracker")]
        public void FindAppointmentListByData()
        {
            // ARRANGE
            Int32 statusReturn = -1;
            DateTime dayAndTime = DateTime.Parse("2018-01-02 11:00:00");
            DateTime dayAndTime2 = DateTime.Parse("2019-01-02 11:00:00");


            // ACT
            List<Appointment> myAppointmentList = myBusinessTracker.FindAppointments(dayAndTime, dayAndTime2);
            if (myAppointmentList.Count > 0)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Find a patient by last name
        /// </summary>
        [TestMethod]
        [TestCategory("FindPatient by Last Name")]
        public void FindPatientByLastName()
        {
            // ARRANGE
            Int32 statusReturn = -1;
            Patient myPatient = GetRegularPatient();


            // ACT
            List<Patient> myPatientList = myBusinessTracker.FindPatient(myPatient);
            if (myPatientList == null)
            {
                statusReturn = -100;
            }
            if (myPatientList.Count > 0)
            {
                statusReturn = 0;
            }
            


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Find a patient by patient obj
        /// </summary>
        [TestMethod]
        [TestCategory("FindPatient by patient obj BusinessTracker")]
        public void FindPatientByPatientObj()
        {
            // ARRANGE
            Int32 statusReturn = -1;

            // ACT
            Patient myPatient = GetRegularPatientWOHoH();
            List<Patient> myPatientList = myBusinessTracker.FindPatient(myPatient);
            if (myPatientList != null)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }

        /// <summary>
        /// Find a patient by Hcn
        /// </summary>
        [TestMethod]
        [TestCategory("FindPatient by HCN BusinessTracker")]
        public void FindPatientByHcn()
        {
            // ARRANGE
            Int32 statusReturn = -1;

            // ACT
            Patient myPatient = myBusinessTracker.FindPatient("1234561232AZ");
            if (myPatient != null)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Find an Address by addressID
        /// </summary>
        [TestMethod]
        [TestCategory("FindAddress by addressID BusinessTracker")]
        public void FindAddressByAddressID()
        {
            // ARRANGE
            Int32 statusReturn = -1;

            // ACT
            Address myPatient = myBusinessTracker.FindAddress(1);
            if (myPatient != null)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }

        /// <summary>
        /// Find an Address by Hcn
        /// </summary>
        [TestMethod]
        [TestCategory("FindAddress by Hcn BusinessTracker")]
        public void FindAddressByHcn()
        {
            // ARRANGE
            Int32 statusReturn = -1;

            // ACT
            Address myPatient = myBusinessTracker.FindAddress("1234561232AZ");
            if (myPatient != null)
            {
                statusReturn = 0;
            }


            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// This tests the constructor and see if it gets the default values.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestPatientconstructor()
        {
            //Arrange + Act
            Patient patient = new Patient();

            //Assert
            Assert.AreEqual(0, patient.PatientID);
            Assert.AreEqual("", patient.GetFname());
            Assert.AreEqual("", patient.GetLname());
            Assert.AreEqual("", patient.GethCN());
            Assert.AreEqual(' ', patient.GetMinitial());
            Assert.AreEqual(DateTime.MinValue, patient.DateOfBirth);
            Assert.AreEqual("", patient.GetSex());
            Assert.AreEqual("", patient.GetHeadOfHouse());
            Assert.IsTrue(patient.Address.IsEmpty);
        }

        /// <summary>
        /// This tests the PatientID property.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestPatientID()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            patient.PatientID = 5;

            //Assert
            Assert.AreEqual(5, patient.PatientID);
        }

        /// <summary>
        /// This tests the SetPatientID method.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetPatientID()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            patient.SetPatientID(15);

            //Assert
            Assert.AreEqual(15, patient.GetPatientID());
        }

        /// <summary>
        /// This tests the invalid case for first name which is the empty string
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestEmptyString()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This tests the maximum length for patients First name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("hellohellohellohellohellohelllohellohellohellohellohellohellohellohellohello");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This tests the actual length for patients first name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestExactMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("hellohellohellohellohellohelllohellohellohellohellohellohell");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("hellohellohellohellohellohelllohellohellohellohellohellohell", patient.GetFname());
        }

        /// <summary>
        /// This tests for non-character situations in setting first name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestNonLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("HelloBo8");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This test the null scenario for setting first name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This tests for empty string in patients last name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_TestEmptyString()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This tests the maximum length for patients last name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_TestMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("hellohellohellohellohellohelllohellohellohellohellohellohellohellohellohello");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This test the actual length of the last name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_TestExactMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("hellohellohellohellohellohelllohellohellohellohellohellohell");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("hellohellohellohellohellohelllohellohellohellohellohellohell", patient.GetLname());
        }

        /// <summary>
        /// This tests the non letter situation when setting patients last name
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLaname_TestNonLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("HelloBo8");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This tests the null situation when setting the last name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This tests the valid HCN and see if it returns true.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_ValidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000000000az");
            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000az", patient.GethCN());

        }

        /// <summary>
        /// This tests the invalid HCN situation.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_InvalidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000-000-0az-00");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the actual format of the HCN.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Format()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000000000az");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000az", patient.GethCN());
        }

        /// <summary>
        /// This tests the length of HCN
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Length_BelowMaximum()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("000-000-000-az");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Length_AboveMaximum()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000-0000-000-az");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the null situation of HCN.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the empty situation of HCN.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Empty()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the format of date of birth for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Format_Valid()
        {
            //Arrange
            Patient patient = new Patient();

            string date = "11-02-1992";

            //Act
            patient.DateOfBirth = DateTime.Parse(date);

            //Assert
            Assert.AreEqual(DateTime.Parse(date), patient.DateOfBirth);
        }

        /// <summary>
        /// This tests the invalid format of Date of birth for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Format_InValid()
        {
            ////Arrange
            //Patient patient = new Patient();
            ////Act
            //bool Result = patient.SetdateOfBirth("11-021992");
            ////Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual("", patient.GetdateOfBirth());
        }

        /// <summary>
        /// This tests for invalid character entered in patients Date of birth.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Invalid_Character()
        {
            ////Arrange
            //Patient patient = new Patient();

            ////Act
            //bool Result = patient.SetdateOfBirth("11-Feb1992");

            ////Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual("", patient.GetdateOfBirth());

        }

        /// <summary>
        /// This tests for the valid length of Date of birth.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Length_valid()
        {
            //Arrange
            Patient patient = new Patient();
            DateTime date = DateTime.Parse("11-02-1992");
            //Act
            patient.DateOfBirth = date;

            //Assert
            Assert.AreEqual(date, patient.DateOfBirth);
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is null.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is empty.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_Empty()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is valid.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_valid()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("F");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("F", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is invalid.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_InValid()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("A");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where a different character has been entered which is not a letter.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_NotLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("8");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the length of the sex for a patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_Length()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("Femalee");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }


        /// <summary>
        /// This tests the valid HCN for head of house and see if it returns true.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_ValidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetHeadOfHouse("0000000000AZ");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000AZ", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the invalid HCN situation for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_InvalidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetHeadOfHouse("00000000AZ00");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the actual format of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Format()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("0000000000AZ");
            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000AZ", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the wrong format of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_WrongFormat()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("AZ0000000000");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the length of HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Length()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("000000000000AZ");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the null situation of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Null()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse(null);
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the empty situation of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Empty()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("");
            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }


        /// <summary>
        /// This tests the situation where the middle initial of the patient is null.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_null()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            //bool Result = patient.SetMinitial(null);
            //Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the situation where the middle initial of the patient is empty.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_Empty()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetMinitial(' ');

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual(' ', patient.GetMinitial());
        }

        /// <summary>
        /// This tests the situation where the middle initial of the patient is valid
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_valid()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetMinitial('H');

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual('H', patient.GetMinitial());
        }

        /// <summary>
        /// This tests the situation where the middle initial of the patient is invalid
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_InValid()
        {
            //Arrange
            //Patient patient = new Patient();

            ////Act
            //bool Result = patient.SetMinitial('_');

            ////Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual(' ', patient.GetMinitial());
        }

        /// <summary>
        /// This tests the situation where a different character has been entered which is not a letter
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_NotLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetMinitial('8');

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual(' ', patient.GetMinitial());
        }

        /// <summary>
        /// This tests a valid patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            patient.SetPatientID(22);
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));

            patient.Address.SetAdressID(1);
            patient.Address.SetAdressLineOne("123 asdads");
            patient.Address.SetCity("Kitcherner");
            patient.Address.SetProvince("ON");
            patient.Address.SetPhoneNumber("(519)222-0000");

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(0, Result);

            // This is failing because the head of household is not set. Isn't that optional?
        }

        /// <summary>
        /// This tests a valid address.
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void TestValidateAddress()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Address myAddress = new Address();
            patient.SetPatientID(22);
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));

            patient.Address.SetAdressID(1);
            patient.Address.SetAdressLineOne("123 asdads");
            patient.Address.SetCity("Kitcherner");
            patient.Address.SetProvince("ON");
            patient.Address.SetPhoneNumber("(519)222-0000");

            //Act
            myAddress = patient.Address;
            int Result = myAddress.ValidateAdress();

            //Assert
            Assert.AreEqual(0, Result);

            // This is failing because the head of household is not set. Isn't that optional?
        }

        /// <summary>
        /// This test patients having an invalid first name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidFName()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3101, Result);
        }

        /// <summary>
        /// This test patients having an invalid Last name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidLName()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3102, Result);
        }

        /// <summary>
        /// This test patients having an invalid health card number.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidHCN()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3103, Result);
        }

        /// <summary>
        /// This test patients having an invalid date of birth.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidDateOfBirth()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3105, Result);
        }

        /// <summary>
        /// This test patients having an invalid sex.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidSex()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3106, Result);
        }

        /// <summary>
        /// This test patients having an invalid address id.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidAddress()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3107, Result);
        }

        #endregion


        #region Address Class

        [TestMethod]
        [TestCategory("Address Class")]
        public void Address_ObjInstatiation_Instantiated()
        {
            // ARRANGE
            int statusReturn = 0;

            // ACT
            Address testAddressObj = new Address();
            if (testAddressObj == null)
            {
                statusReturn = -1;
            }
            else
            {
                statusReturn = 0;
            }

            // ASSERT
            Assert.AreEqual(statusReturn, 0);
        }


        /// <summary>
        /// Run a update Address test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("Update an Address BusinessTracker")]
        public void UpdateAddress()
        {
            // ARRANGE
            int statusReturn = -1;

            // ACT
            Patient myObj = GetRegularPatientWOHoH();
            statusReturn = myBusinessTracker.UpdateAddress(myObj.Address);

            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// This test attempts to set a valid AdressID
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAddressID_Valid()
        {

            // ACT
            Address testAdressObj = new Address();
            int testID = 765;
            testAdressObj.SetAdressID(testID);
            // ASSERT
            Assert.AreEqual(testAdressObj.GetAdressID(), testID);
        }


        /// <summary>
        /// This test attempts to set a invalid AdressID
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAddressID_Invalid()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            int testID = -765;
            bool statusRet = testAdressObj.SetAdressID(testID);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a valid adressLineOne
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineOne_Valid()
        {

            // ACT
            Address testAdressObj = new Address();
            string testadressLineOne = "Doov Valley Dr 123";
            testAdressObj.SetAdressLineOne(testadressLineOne);
            // ASSERT
            Assert.AreEqual(testAdressObj.GetAddressLineOne(), testadressLineOne);
        }


        /// <summary>
        /// This test attempts to set a invalid adressLineOne( too long)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddressSet_AdressLineOne_Invalid_LongAdressLineOne()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testadressLineOne = "zxcvhbhjghbnmasdfghjklqwertyuiopzxcvbnm1234567890qwertyuiopasdfghjk";
            bool statusRet = testAdressObj.SetAdressLineOne(testadressLineOne);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid adressLineOne(empty)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineOne_Invalid_Empty()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testadressLineOne = "";
            bool statusRet = testAdressObj.SetAdressLineOne(testadressLineOne);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid characters adressLineOne
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineOne_Invalid_Characters()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testadressLineOne = "!@#";
            bool statusRet = testAdressObj.SetAdressLineOne(testadressLineOne);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a valid adressLineTwo
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineTwo_Valid()
        {

            // ACT
            Address testAdressObj = new Address();
            string testadressLineTwo = "Office 4";
            testAdressObj.SetAdressLineTwo(testadressLineTwo);
            // ASSERT
            Assert.AreEqual(testAdressObj.GetAddressLineTwo(), testadressLineTwo);
        }



        /// <summary>
        /// This test attempts to set a invalid adressLineTwo( too long)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineTwo_Invalid_LongAdressLineTwo()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testadressLineTwo = "zxcvhbhjghbnmasdfghjklqwertyuiopzxcvbnm1234567890qwertyuiopasdfghjk";
            bool statusRet = testAdressObj.SetAdressLineTwo(testadressLineTwo);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid adressLineTwo(empty)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineTwo_Invalid_Empty()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = true;
            string testadressLineTwo = "";
            bool statusRet = testAdressObj.SetAdressLineTwo(testadressLineTwo);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }



        /// <summary>
        /// This test attempts to set a invalid characters adressLineTwo
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetAdressLineTwp_Invalid_Characters()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testadressLineTwo = "!@#";
            bool statusRet = testAdressObj.SetAdressLineTwo(testadressLineTwo);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a valid city
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_City_Valid()
        {

            // ACT
            Address testAdressObj = new Address();
            string testCity = "Moscow";
            testAdressObj.SetCity(testCity);
            // ASSERT
            Assert.AreEqual(testAdressObj.GetCity(), testCity);
        }


        /// <summary>
        /// This test attempts to set a invalid city( too long)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetCity_Invalid_LongCity()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testCity = "zxcvhbhjghbnmasdfghjklqwertyuiopzxcvbnm1234567890qwertyuiopasdfghjk";
            bool statusRet = testAdressObj.SetCity(testCity);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid city(empty)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetCity_Invalid_Empty()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testCity = "";
            bool statusRet = testAdressObj.SetCity(testCity);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid characters city
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_City_Invalid_Characters()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testCity = "!@#";
            bool statusRet = testAdressObj.SetCity(testCity);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a valid province
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_Province_Valid()
        {

            // ACT
            Address testAdressObj = new Address();
            string testProvince = "ON";
            testAdressObj.SetProvince(testProvince);
            // ASSERT
            Assert.AreEqual(testAdressObj.GetProvince(), testProvince);
        }

        /// <summary>
        /// This test attempts to set a invalid province(empty)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetProvince_Invalid_Empty()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testProvince = "";
            bool statusRet = testAdressObj.SetProvince(testProvince);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid province name
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetProvince_Invalid_Name()
        {

            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testProvince = "MM";
            bool statusRet = testAdressObj.SetProvince(testProvince);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid province format
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetProvince_Invalid_Format()
        {
            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testProvince = "oN";
            bool statusRet = testAdressObj.SetProvince(testProvince);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid province lenght
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetProvince_Invalid_Lenght()
        {
            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testProvince = "OON";
            bool statusRet = testAdressObj.SetProvince(testProvince);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }

        /// <summary>
        /// This test attempts to set a invalid province characters
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_SetProvince_Invalid_Characters()
        {
            // ACT
            Address testAdressObj = new Address();
            bool statusReturnShouldBe = false;
            string testProvince = "1-";
            bool statusRet = testAdressObj.SetProvince(testProvince);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a valid phoneNumber
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_PhoneNumber_Valid()
        {

            // ACT - Should accept most common input formats
            Address testAdressObj = new Address();
            string testOne =  "(519)635-5129";
            string testTwo =   "519-635-5129";
            string testThree = "519 635 5129";
            string testFour = "(519)635 5129";

            string expected = "519-635-5129";

            // ASSERT
            testAdressObj.SetPhoneNumber(testOne);
            Assert.AreEqual(testAdressObj.GetPhoneNumber(), expected);

            testAdressObj.SetPhoneNumber(testTwo);
            Assert.AreEqual(testAdressObj.GetPhoneNumber(), expected);

            testAdressObj.SetPhoneNumber(testThree);
            Assert.AreEqual(testAdressObj.GetPhoneNumber(), expected);

            testAdressObj.SetPhoneNumber(testFour);
            Assert.AreEqual(testAdressObj.GetPhoneNumber(), expected);
        }


        /// <summary>
        /// This test attempts to set a invalid phoneNumber(empty)
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_PhoneNumber_Invalid_Empty()
        {

            // ACT
            Address testAdressObj = new Address();
            string testPhoneNumber = "";
            bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
            // ASSERT
            Assert.IsFalse(statusRet);
        }


        /// <summary>
        /// This test attempts to set a invalid phoneNumber area code
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_PhoneNumber_Invalid_Area_Code()
        {
            // ACT
            Address testAdressObj = new Address();
            string testPhoneNumber = "(985)-251-1724";
            bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
            // ASSERT
            Assert.IsFalse(statusRet);
        }


        /// <summary>
        /// This test attempts to set a invalid phoneNumber lenght
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_PhoneNumber_Invalid_Lenght()
        {
            // ACT
            Address testAdressObj = new Address();
            string testPhoneNumber = "(519)635-51295";
            bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
            // ASSERT
            Assert.IsFalse(statusRet);
        }


        /// <summary>
        /// This test attempts to set a invalid phoneNumber format
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_PhoneNumber_Invalid_Format()
        {
            // ACT
            Address testAdressObj = new Address();
            string testPhoneNumber = "519 111 aaa1";
            bool statusRet = testAdressObj.SetPhoneNumber(testPhoneNumber);
            // ASSERT
            Assert.IsFalse(statusRet);
        }


        /// <summary>
        /// This test checks function IsEmpty, sends empty string
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_IsEmpty_Empty()
        {
            // ACT
            Address testAdressObj = new Address();
            // ASSERT
            Assert.IsTrue(testAdressObj.IsEmpty);
        }


        /// <summary>
        /// This test checks function IsEmpty, sends non empty string
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void SetAddress_IsEmpty_Non_Empty()
        {
            // ACT
            Address testAdressObj = new Address();
            testAdressObj.SetAdressLineOne("dadadad");
            // ASSERT
            Assert.IsFalse(testAdressObj.IsEmpty);
        }
        #endregion


        #region Appointment Class
        /// <summary>
        /// Run a add appointment test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("AddAppointment BusinessTracker")]
        public void AddAppointment()
        {
            // ARRANGE
            int statusReturn = -1;

            // ACT
            Appointment myAppointment = GetRegularAppointment();
            statusReturn = myBusinessTracker.AddAppointment(myAppointment);

            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }

        /// <summary>
        /// Run a update appointment test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("UpdateAppointment BusinessTracker")]
        public void UpdateAppointment()
        {
            // ARRANGE
            int statusReturn = -1;

            // ACT
            Appointment myAppointment = GetRegularAppointment1();
            statusReturn = myBusinessTracker.UpdateAppointment(myAppointment);

            // ASSERT
            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Run a add patient test, with patient data with out Hoh
        /// </summary>
        [TestMethod]
        [TestCategory("AddAppointment BusinessTracker")]
        public void FindAppointmentByAppointmentID()
        {
            // ARRANGE
            Int32 statusReturn = -1, successfulReturn = 1;
            Appointment myAppointment = null;

            // ACT
            myAppointment = myBusinessTracker.FindAppointment(1);
            if (myAppointment != null)
            {
                statusReturn = 1;
            }

            // ASSERT
            Assert.AreEqual(successfulReturn, statusReturn);
        }


        /// <summary>
        /// This test attempts to set a invalid AppointmentID
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void SetAddress_SetAppointmentID_Invalid()
        {

            // ACT
            Appointment testAppointmentObj = new Appointment();
            bool statusReturnShouldBe = false;
            int testID = -765;
            bool statusRet = testAppointmentObj.SetApointmentID(testID);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a valid dayAndTime
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void SetAddress_SetDayAndTime_Valid()
        {

            // ACT
            Appointment testAppointmentObj = new Appointment();
            bool statusReturnShouldBe = true;
            DateTime testDayAndTime = new DateTime(2017, 11, 15, 10, 0, 0);
            bool statusRet = testAppointmentObj.SetDayAndTime(testDayAndTime);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid dayAndTime (10.30 am)
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void SetAddress_SetDayAndTime_Invalid_Wrong_Time_Week_Day()
        {

            // ACT
            Appointment testAppointmentObj = new Appointment();
            bool statusReturnShouldBe = false;
            DateTime testDayAndTime = new DateTime(2017, 11, 15, 10, 30, 0);
            bool statusRet = testAppointmentObj.SetDayAndTime(testDayAndTime);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test attempts to set a invalid dayAndTime late weekend appointment
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void SetAddress_SetDayAndTime_Invalid_Wrong_Time_Weekend_Day()
        {

            // ACT
            Appointment testAppointmentObj = new Appointment();
            bool statusReturnShouldBe = false;
            DateTime testDayAndTime = new DateTime(2017, 11, 18, 14, 0, 0);
            bool statusRet = testAppointmentObj.SetDayAndTime(testDayAndTime);
            // ASSERT
            Assert.AreEqual(statusRet, statusReturnShouldBe);
        }


        /// <summary>
        /// This test checks if appointment object was successfully created
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void Appointment_ObjInstatiation_Instantiated()
        {
            // ARRANGE
            int statusReturn = 0;

            // ACT
            Appointment testAppointmentObj = new Appointment();
            if (testAppointmentObj == null)
            {
                statusReturn = -1;
            }
            else
            {
                statusReturn = 0;
            }

            // ASSERT
            Assert.AreEqual(statusReturn, 0);
        }


        /// <summary>
        /// This tests a valid Appointment.
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void ValidateAppointment()
        {
            //Arrange + Assert
            Appointment myAppointment = GetRegularAppointment();


            //Act
            int Result = myAppointment.ValidateAppointment();

            //Assert
            Assert.AreEqual(0, Result);

            // This is failing because the head of household is not set. Isn't that optional?
        }


        /// <summary>
        /// This test attempts to set a valid AppointmentID
        /// </summary>
        [TestMethod]
        [TestCategory("Appointment Class")]
        public void SetAppointment_SetAppointmentID_Valid()
        {

            // ACT
            Appointment testAppointmentObj = new Appointment();
            int testID = 765;
            testAppointmentObj.SetApointmentID(testID);
            // ASSERT
            Assert.AreEqual(testAppointmentObj.GetAppointmentID(), testID);
        }
        #endregion
    }
}

