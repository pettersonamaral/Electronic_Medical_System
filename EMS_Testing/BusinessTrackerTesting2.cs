﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billing;
using Demographics;
using BusinessTracker;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EMS_Testing
{
    public partial class BusinessTrackerTesting
    {
        #region testEnum

        // Month numeration
        public enum monthEnum { January, February, March, April, May, June, July, August, September, October, November, December };
        public enum ServiceStatus { UPRC, PAID, DECL, FHCV, CMOH }   //UPRC -> SHORT FOR UNPROCESSED

        #endregion

        #region Billing 


        [TestMethod]
        [TestCategory("AddBill BusinessTracker")]
        public void AddBill_T_Success()
        {
            // ARRANGE
            var myBill = GetRegularBill();
            var retCode = 0;
            var assertStatus = -1;


            // ACT
            retCode = myBusinessTracker.AddBill(myBill);

            if (retCode > 0)
            {
                assertStatus = 0;
            }

            // ASSERT
            Assert.AreEqual(0, assertStatus);
        }


        /// <summary>
        /// Run a update appointment test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("FindBill BusinessTracker")]
        public void FindBill_T_Success()
        {
            // ARRANGE
            var statusReturn = -1;
            var patientID = 5;
            var appointmentID = 5;

            // ACT
            Bill myBill = myBusinessTracker.FindBill(patientID,appointmentID);
            
            // ASSERT

            if(myBill != null) { statusReturn = 0;}

            Assert.AreEqual(0,statusReturn);
        }


        /// <summary>
        /// Run a update appointment test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("FindBill BusinessTracker")]
        public void FindBill_T2_Success()
        {
            // ARRANGE
            var statusReturn = -1;
            var billID = 5;
            
            // ACT
            Bill myBill = myBusinessTracker.FindBill(billID);

            // ASSERT

            if (myBill != null) { statusReturn = 0; }

            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Run a update appointment test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("FindService BusinessTracker")]
        public void FindService_T_Success()
        {
            // ARRANGE
            var statusReturn = -1;
            var serviceID = 5;
           
            // ACT
            Service myService = myBusinessTracker.FindService(serviceID);

            // ASSERT
            if (myService != null) { statusReturn = 0; }

            Assert.AreEqual(0, statusReturn);
        }


        /// <summary>
        /// Run a update appointment test, with appointment data
        /// </summary>
        [TestMethod]
        [TestCategory("FindServices BusinessTracker")]
        public void FindServices_T_Success()
        {
            // ARRANGE
            var statusReturn = -1;
            var allServices = "";

            // ACT
            List<Service> myServices = myBusinessTracker.FindServices();

            // ASSERT
            if (myServices.Count > 0) { statusReturn = 0; }

            Assert.AreEqual(0, statusReturn);
        }

        /// <summary>
        /// Run an AddServiceLine method test
        /// </summary>
        [TestMethod]
        [TestCategory("AddServiceLine BusinessTracker")]
        public void AddServiceLine_T_Success()                               
        {
            // ARRANGE
            var mybillID      = 1;    // Existent Service Bill
            var myServiceID   = 11;    // Service to be added
            var retCode       = 0;
            var assertStatus  = -1;

            // Define new ServiceList properties
            var newServiceList = new ServiceList
            {
                Quantity  = 5,
                Service   = myBusinessTracker.FindService(myServiceID),
                Status    = "PAID"
            };

            // ACT
            retCode = myBusinessTracker.AddServiceList(mybillID,newServiceList);
            if (retCode > 0) { assertStatus = 0; }

            // ASSERT
            Assert.AreEqual(0, assertStatus);
        }


        /// <summary>
        /// Run a UpdateBill method test
        /// </summary>
        [TestMethod]
        [TestCategory("UpdateBill BusinessTracker")]
        public void UpdateBill_T_Success()
        {
            // ARRANGE
            var retCode = 0;
            var assertStatus = -1;
            var billPatient = 10;
            var billAppointment = 10;
            var billToUpdate = myBusinessTracker.FindBill(billPatient, billAppointment);

            //ACT
            // Updates to be processed
            billToUpdate.FlagRecall = 1;
            billToUpdate.StatusDone = true;

            retCode = myBusinessTracker.UpdateBill(billToUpdate);
            if (retCode > 0) { assertStatus = 0; }

            // ASSERT
            Assert.AreEqual(0, assertStatus);

        }


        /// <summary>
        /// Run a UpdateBill method test
        /// </summary>
        [TestMethod]
        [TestCategory("UpdateServiceList BusinessTracker")]
        public void UpdateServiceList_T_Success()
        {
            // ARRANGE
            var retCode = 0;
            var assertStatus = -1;
            var myBillID     = 20;
            var myServiceID  = 20;
            var serviceListToUpdate = myBusinessTracker.FindServiceList(myBillID, myServiceID);
            var myService = myBusinessTracker.FindService(myServiceID);

            //ACT
            // Updates to be processed
            serviceListToUpdate.Quantity = 5;
            serviceListToUpdate.Status = ServiceList.ServiceStatus.UPRC.ToString();

            retCode = myBusinessTracker.UpdateServiceList(myBillID, serviceListToUpdate);
            if (retCode > 0) { assertStatus = 0; }

            // ASSERT
            Assert.AreEqual(0, assertStatus);
        }


        /// <summary>
        ///  Tests monthly billing generation based on selected Month
        /// </summary>
        [TestMethod]
        [TestCategory("GenerateMonthlyBilling BusinessTracker")]
        public void GenerateMonthlyBilling_T_Success()
        {
            // ARRANGE
            var retCode = 0;
            var assertStatus = -1;
            var selectedMonth = new DateTime(2017, 5, 5); // Random date containing the desired month

            //ACT
            // Updates to be processed
            var billingSummary = myBusinessTracker.MonthlyBillingSummary(selectedMonth);
            if (billingSummary.Length > 0) { assertStatus = 0; }

            // ASSERT
            Assert.AreEqual(0, assertStatus);
        }

        #endregion Billing

    }
}
