﻿//********************************************************************************************************************************* 
// FILE          : Bill.cs
// PROJECT       : INFO2180 Software Quality I - EMS System
// PROGRAMMER    : Paulo Casado (AKA PC) 
// VERSION DATE  : 2017-NOV-15
// DESCRIPTION   : 

// REFERENCES:
// UNIT TEST METHODS NAMING CONVENTION:
// [MethodName_StateUnderTest_ExpectedBehavior]
// Reference: http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
// UNIT TEST METHODS PATTERN:
// Arrange - Act - Assert (AAA)
// Reference: https://docs.microsoft.com/en-us/visualstudio/test/unit-test-basics
//**********************************************************************************************************************************

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billing;
using Demographics;
using System.Globalization;


namespace EMS_Testing
{
    [TestClass]
    public class BillingLibraryTesting
    {

        //===========================================================================================================================================//
        //                                                          CLASS MEMBERS
        //===========================================================================================================================================//
        List<Bill> fakeListOfBills = new List<Bill>(10);
        List<Patient> fakeListOfPatients = new List<Patient>(10);
        List<Appointment> fakeListOfAppointments = new List<Appointment>(10);
        List<Service> fakeListOfServices = new List<Service>(10);
        List<ServiceList> fakeServiceList = new List<ServiceList>(20);


        /// <summary>
        /// TEST CLASS CONSTRUCTOR
        /// </summary>
        /// 
        [TestMethod]
        [TestCategory("Fake Constructor")]
        public void BillingClassLibraryTesting()
        {
            // CREATE FAKE BILLS *************************************************************************

            int billCount = 0;
            //int billPatientCount = 0;
            //int billAppointmentCount = 0;
            Random billRandRecallFlag = new Random();
            List<Bill> fakeListOfBills = new List<Bill>(10);

            // Generate 10 Bill objects inside the list

            foreach (Bill billRecord in fakeListOfBills)
            {
                billRecord.SetBillID(billCount++);
                //billRecord.SetPatientID(billPatientCount++);
                //billRecord.SetAppointmentID(billAppointmentCount++);
                billRecord.SetFlagRecall(billRandRecallFlag.Next(0, 3));
                billRecord.StatusDone = false;
            }


            // CREATE FAKE PATIENTS  *************************************************************************


            int patientCount = 0;
            string patientGenericFName = "JOHN";
            string patientGenericLName = "DOE";
            char patientGenericMInit = 'K';

            Random patientAdder = new Random();
            List<Patient> fakeListOfPatients = new List<Patient>(10);

            // Generate 10 Patient objects inside the list
            foreach (Patient patientRecord in fakeListOfPatients)
            {
                patientRecord.SethCN((patientAdder.Next(0, 1999999999).ToString() + "AZ"));
                patientRecord.SetPatientID(patientCount++);
                patientRecord.SetFname(patientGenericFName + patientAdder.Next(0, 100).ToString());
                patientRecord.SetLname(patientGenericLName + patientAdder.Next(0, 100).ToString());
                patientRecord.SetMinitial(patientGenericMInit);
                patientRecord.DateOfBirth = DateTime.Parse("01-01-200" + patientAdder.Next(0, 9).ToString());
                patientRecord.SetSex("F");
                patientRecord.SetHeadOfHouse("");
                //patientRecord.SetAddressID(patientCount++);
            }


            // CREATE FAKE APPOINTMENTS  *************************************************************************

            int appointmentID = 0;
            //int appointmentPatientID = 0;
            //int appointmentSecondPatientID = 0;
            DateTime appointmentDates = new DateTime();
            appointmentDates = DateTime.ParseExact("20171101", "yyyyMMdd", CultureInfo.InvariantCulture);

            List<Appointment> fakeListOfAppointments = new List<Appointment>(10);

            foreach (Appointment appointmentRecord in fakeListOfAppointments)
            {
                appointmentRecord.SetApointmentID(appointmentID++);
                appointmentRecord.SetDayAndTime(appointmentDates.AddDays(1));
                //appointmentRecord.SetPatientID(appointmentPatientID++);
                //appointmentRecord.SetSecondPatientID(appointmentSecondPatientID);
            }

            // CREATE FAKE LIST OF SERVICES  *************************************************************************

            int serviceID = 0;
            DateTime serviceEffectiveDate = DateTime.ParseExact("20171101", "yyyyMMdd", CultureInfo.InvariantCulture);
            Random serviceRandomAdder = new Random();
            List<Service> fakeListOfServices = new List<Service>(10);


            foreach (Service serviceRecord in fakeListOfServices)
            {
                serviceRecord.SetServiceID(serviceID++);
                serviceRecord.SetFeeCode("A" + serviceRandomAdder.Next(111, 999).ToString());
                serviceRecord.EffectiveDate = serviceEffectiveDate.AddDays(2);
                serviceRecord.SetServiceValue(serviceRandomAdder.NextDouble() * 100);
            }

            // CREATE FAKE SERVICE LIST  *****************************************************************************

            //int serviceListID = 0;
            Random serviceListAdder = new Random();

            List<ServiceList> fakeServiceList = new List<ServiceList>(20);

            foreach (Bill billRecord in fakeListOfBills)
            {
                foreach (ServiceList set in fakeServiceList)
                {
                    //set.SetServiceID(serviceListID++);
                    //set.SetBillID(billRecord.GetBillID());
                    set.SetQuantity(serviceListAdder.Next(1, 5));
                }
            }




        }

        //  BILL CLASS UNIT TESTING ******************************************************************************************************************************//

        /// <summary>
        /// This method tests the instantiation of a Bill object through the Bill class constructor.
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void Bill_ObjInstatiation_Instantiated()
        {
            // ACT
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsNotNull(testBillObj);
        }


        /// <summary>
        /// This test attempts to set a valid BillID
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void SetBillID_SetBillID_Valid()
        {
            // ARRANGE
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsTrue(testBillObj.SetBillID(1000000000));
        }


        /// <summary>
        /// This test attempts to set an invalid negative BillID
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void SetBillID_SetNegativeBillID_Invalid()
        {
            // ARRANGE
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsFalse(testBillObj.SetBillID(-1));
        }


        /// <summary>
        /// This test attempts to set a Bill ID in the limit range of an Int datatype
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void SetBillID_SetLimitIntRangeBillID_Valid()
        {
            // ARRANGE
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsTrue(testBillObj.SetBillID(2147483647));
        }


        /// <summary>
        /// This test attempts to set a valid Recall flag to re-schedule a patient 
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void SetFlagRecall_SetFlagRecall_Valid()
        {
            // ARRANGE
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsTrue(testBillObj.SetFlagRecall(3));
        }


        /// <summary>
        /// This test attempts to set an invalid, over the int range Recall flag value
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void SetFlagRecall_SetFlagRecallOverTheRange_Invalid()
        {
            // ARRANGE
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsFalse(testBillObj.SetFlagRecall(5));
        }


        /// <summary>
        /// This test attempts to set an invalid negative value for a Recall flag.
        /// </summary>
        [TestMethod]
        [TestCategory("BilL Class")]
        public void SetFlagRecall_SetNegativeFlagRecall_Invalid()
        {
            // ARRANGE
            Bill testBillObj = new Bill();

            // ASSERT
            Assert.IsFalse(testBillObj.SetFlagRecall(-3));
        }


        // SERVICE CLASS UNIT TESTING ******************************************************************************************************************************//


        /// <summary>
        /// This method tests the instantiation of a Service object through the Service class constructor.
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void Service_ObjInstatiation_Instantiated()
        {
            // ARRANGE
            Service testServiceObj = new Service();

            // ASSERT
            Assert.IsNotNull(testServiceObj);
        }


        /// <summary>
        /// This test attempts to set a valid Service ID through the Mutator method
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetServiceID_SetServiceID_Valid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            int testServiceID = 101;

            // ASSERT
            Assert.IsTrue(testServiceObj.SetServiceID(testServiceID));
        }


        /// <summary>
        /// This test attempts to set a negative value to the Service ID through the Mutator method
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetServiceID_SetNegativeServiceID_Invalid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            int testServiceID = -1000;

            // ASSERT
            Assert.IsFalse(testServiceObj.SetServiceID(testServiceID));
        }


        /// <summary>
        /// This test attempts to set a valid Service ID a the limit of int range through the Mutator method
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetServiceID_SetLimitIntRangeServiceID_Valid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            int testServiceID = 2147483647;

            // ASSERT
            Assert.IsTrue(testServiceObj.SetServiceID(testServiceID));
        }


        /// <summary>
        /// This test attempts to set a valid fee Code through the Mutator method
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetFeeCode_SetFeeCode_Valid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            string testFeeCode = "A123";

            // ASSERT
            Assert.IsTrue(testServiceObj.SetFeeCode(testFeeCode));
        }


        /// <summary>
        /// This test attempts to set a invalid fee Code by changing the place of the alpha numeric characters. 
        /// In this case, making the fee code different from the MoH format.
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetFeeCode_SetWrongFormatFeeCode_Invalid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            string testFeeCode = "123A";

            // ASSERT
            Assert.IsFalse(testServiceObj.SetFeeCode(testFeeCode));
        }


        /// <summary>
        /// This test attempts to set a invalid fee Code by adding one more character to the fee Code.
        /// In this case, making the fee code different from the MoH format.
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetFeeCode_SetAddittionalCharacterToFeeCode_Invalid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            string testFeeCode = "A3214";

            // ASSERT
            Assert.IsFalse(testServiceObj.SetFeeCode(testFeeCode));
        }


        /// <summary>
        /// This test attempts to set a valid service value through the Mutator method
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetServiceValue_SetServiceValue_Valid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            double testServiceValue = 150.75;

            // ASSERT
            Assert.IsTrue(testServiceObj.SetServiceValue(testServiceValue));
        }


        /// <summary>
        /// This test attempts to set a invalid negative service value through the Mutator method
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetServiceValue_SetNegativeServiceValue_Invalid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            double testServiceValue = -150.75;

            // ASSERT
            Assert.IsFalse(testServiceObj.SetServiceValue(testServiceValue));
        }


        /// <summary>
        /// This test attempts to set a valid no charge (zero value) service value
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetServiceValue_SetNoChargeValue_Valid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            double testServiceValue = 0;

            // ASSERT
            Assert.IsTrue(testServiceObj.SetServiceValue(testServiceValue));
        }


        /// <summary>
        /// This test attempts to set a valid effective date format for an specific service
        /// </summary>
        [TestMethod]
        [TestCategory("Service Class")]
        public void SetEffectiveDate_SetEffectiveDate_Valid()
        {
            // ARRANGE
            Service testServiceObj = new Service();
            DateTime testEffectiveDate = new DateTime(2015 - 12 - 01);

            // ACT
            testServiceObj.EffectiveDate = testEffectiveDate;

            // ASSERT
            Assert.IsTrue(testServiceObj.EffectiveDate.Equals(testEffectiveDate));
        }


        // SERVICELIST CLASS UNIT TESTING ******************************************************************************************************************************//

        /// <summary>
        /// This method tests the instantiation of a ServiceList object through the ServiceList class constructor.
        /// </summary>
        [TestMethod]
        [TestCategory("Servicelist Class")]
        public void ServiceList_ObjInstatiation_Instantiated()
        {
            // ARRANGE
            ServiceList testServiceListObj = new ServiceList();

            // ASSERT
            Assert.IsNotNull(testServiceListObj);
        }


        /// <summary>
        /// This test attempts to set a valid quantity for a specific service within a service list.
        /// </summary>
        [TestMethod]
        [TestCategory("ServiceList Class")]
        public void SetServiceQuantity_SetServiceQuantity_Valid()
        {
            // ARRANGE
            ServiceList testServiceListObj = new ServiceList();
            int serviceQuantity = 1000;

            // ASSERT
            Assert.IsTrue(testServiceListObj.SetQuantity(serviceQuantity));
        }



        /// <summary>
        /// This test attempts to set an invalid negative value for the service quantity within a service list
        /// </summary>
        [TestMethod]
        [TestCategory("ServiceList Class")]
        public void SetServiceQuantity_SetNegativeServiceQuantity_Invalid()
        {
            // ARRANGE
            ServiceList testServiceListObj = new ServiceList();
            int serviceQuantity = -1000;

            // ASSERT
            Assert.IsFalse(testServiceListObj.SetQuantity(serviceQuantity));
        }



        /// <summary>
        /// This test attempts to set a valid zero quantity value to a service within a service list.
        /// This option was made valid as an alternative to "delete" a quantity during a service list update.
        /// </summary>
        [TestMethod]
        [TestCategory("ServiceList Class")]
        public void SetServiceQuantity_SetZeroQuantity_Valid()
        {
            // ARRANGE
            ServiceList testServiceListObj = new ServiceList();
            int serviceQuantity = 0;

            // ASSERT
            Assert.IsTrue(testServiceListObj.SetQuantity(serviceQuantity));
        }

        /*

        // TRACKER CLASS UNIT TESTING ******************************************************************************************************************************


        /// <summary>
        /// This test attempts to instantiate a Billing Tracker class object.
        /// </summary>
        //[TestMethod]
        //[TestCategory("Billing Tracker Class")]
        //public void BillingTracker_TrackerObjInstantiation_Valid()
        //{
        //    // ARRANGE
        //    int statusReturn = 0;


        //    // ACT
        //    Billing.Tracker testTrackerObj = new Billing.Tracker();
        //    if (testTrackerObj == null) { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_FAILURE; }
        //    else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }

        //    // ASSERT
        //    Assert.AreEqual(statusReturn, Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS);




        /// <summary>
        /// This test attempts to set a new list of Bills
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void SetListOfBills_SetNewListOfBills_Valid()
        {
            // ARRANGE
            int statusReturn = 0;
            List<Bill> testListOfBills = new List<Bill>();


            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.SetListOfBills(testListOfBills) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_LIST_OF_BILLS; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }

            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS);
        }


        /// <summary>
        /// This test attempts to set a not initiated (actually set to null) list of Bills.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void SetListOfBills_SetNotProperlyInitiatedListOfBills_Invalid()
        {
            // ARRANGE
            int statusReturn = 0;
            List<Bill> testListOfBill = null;


            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.SetListOfBills(testListOfBill) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_LIST_OF_BILLS; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }

            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.ERR_SET_NEW_LIST_OF_BILLS);
        }



        /// <summary>
        /// This test attempts to set a new list of service codes based on the MoH sample format
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void SetAvailableServices_SetNewListOfServices_Valid()
        {
            // ARRANGE
            int statusReturn = 0;
            List<Service> testListOfServices = new List<Service>();

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.SetAvailableServices(testListOfServices) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }

            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS);
        }


        /// <summary>
        /// This test attempts to set a not initiated (actually set to null) list of available services from the MoH 
        /// service list.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void SetAvailableServices_SetNotProperlyInitiatedListOfServices_Invalid()
        {
            // ARRANGE
            int statusReturn = 0;
            List<Service> testListOfServices = null;

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.SetAvailableServices(testListOfServices) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }


            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES);
        }


        /// <summary>
        /// This test attempts to set a new Service list related to a specific bill
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void SetListOfServices_SetNewServiceList_Valid()
        {
            // ARRANGE
            int statusReturn = 0;
            List<ServiceList> testServiceList = new List<ServiceList>();

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.SetListOfServices(testServiceList) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_SERVICE_LIST; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }

            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS);
        }


        /// <summary>
        /// This test attempts to set a not properly initialized (set to null) service list (which is specific to a Bill).
        /// service list.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void SetListOfServices_SetNotProperlyInitiatedServiceList_Invalid()
        {
            // ARRANGE
            int statusReturn = 0;
            List<ServiceList> testServiceList = null;

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.SetListOfServices(testServiceList) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_SERVICE_LIST; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }


            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.ERR_SET_NEW_SERVICE_LIST);
        }



        /// <summary>
        /// This test attempts to Add a new bill to the already set list of bills
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void AddBill_AddNewBillToListOfBills_Valid()
        {
            // ARRANGE
            int statusReturn = 0;
            Bill testBill = new Bill();

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.AddBill(testBill) < 0) { statusReturn = Billing.Tracker.ERR_ADD_BILL; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }


            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS);
        }



        /// <summary>
        /// This test attempts to Add a bill not properly initialized (actually set to null).
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void AddBill_AddNotInitializedBillToListOfBills_Invalid()
        {
            // ARRANGE
            int statusReturn = 0;
            Bill testBill = null;

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.AddBill(testBill) < 0) { statusReturn = Billing.Tracker.ERR_ADD_BILL; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }


            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.ERR_ADD_BILL);
        }



        /// <summary>
        /// This test attempts to add a new service list to the ListOfServiceList within the billing tracker
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void AddServiceLine_AddNewServiceLine_Valid()
        {
            // ARRANGE
            int statusReturn = 0;
            ServiceList testServiceList = new ServiceList();

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.AddServiceLine(testServiceList) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_SERVICE_LIST; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }


            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS);
        }



        /// <summary>
        /// This test attempts to add a not properly initialized ServiceList object to the ListOfServiceList within the billing tracker
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void AddServiceLine_AddNotInitializedServiceListToListOfServices_Invalid()
        {
            // ARRANGE
            int statusReturn = 0;
            ServiceList testServiceList = null;

            // ACT
            Billing.Tracker testTrackerObj = new Billing.Tracker();
            if (testTrackerObj.AddServiceLine(testServiceList) < 0) { statusReturn = Billing.Tracker.ERR_SET_NEW_SERVICE_LIST; }
            else { statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS; }


            // ASSERT
            Assert.AreEqual(statusReturn, Billing.Tracker.ERR_SET_NEW_SERVICE_LIST);
        }



        /// <summary>
        /// This test attempts to add a not properly initialized ServiceList object to the ListOfServiceList within the billing tracker
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void FindBill_FindSpecificBillInsideTheListOfBills_Valid()
        {
            //// ARRANGE
            //int statusReturn        = 0;
            //Bill testBill           = new Bill();
            //Bill testFoundBill      = new Bill();
            //testBill                = fakeListOfBills[5];  // Gets a random Bill information to be found.
            //int testPatientID       = testBill.GetPatientID();
            //int testAppointmentID   = testBill.GetAppointmentID();


            //// ACT
            //Billing.Tracker testTrackerObj = new Billing.Tracker();

            //testFoundBill = testTrackerObj.FindBill(testPatientID, testAppointmentID);

            ////if (testFoundBill == null) { statusReturn = Billing.Tracker.ERR_FIND_BILL; }
            ////else {statusReturn = Billing.Tracker.RETURN_BILLING_TRACKER_SUCESS;}


            //// ASSERT
            //Assert.AreEqual(testFoundBill, testBill);
        }



        /// <summary>
        /// This test attempts find and return a specific MoH valid service object within the list of available services.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void FindOneService_FindSpecificServiceInsideAvailableServices_Valid()
        {

        }



        /// <summary>
        /// This test attempts find and return a list with one MoH valid service object based on a unique "feecode"[OVERLOAD #1].
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void FindService_FindReturnListWithSpecificServiceInsideAvailableServices_Valid()
        {

        }



        /// <summary>
        /// This test attempts find and return a list with one (or more) MoH valid service(s) object based on "effectiveDate"[OVERLOAD #2].
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void FindService_FindSpecificServiceInsideAvailableServices_Valid()
        {

        }



        /// <summary>
        /// This test attempts a list of services related to an specific bill.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void FindServiceList_FindServiceListFromABill_Valid()
        {

        }



        /// <summary>
        /// This test attempts find a specific ServiceList obj inside a list of ServiceLists and update the content
        /// based on the new ServiceList obj argument passed to the function.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void UpdateServiceList_FindAndReplaceTheItemsOfTheCurrentServiceList_Valid()
        {

        }



        /// <summary>
        /// This test verify if the current method is capable of getting current information stored in memory
        ///  (inside the trackers lists) and generate the specific month billing information
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void GenerateMonthlyBilling_GenerateMonthlyBilling_Valid()
        {

        }



        /// <summary>
        /// This test verify if the current method is capable simulating a response of MoH based on the Monthly billing
        /// generated by the system. In this case, one of the 4 MoH codes (PAID,DECL,FHCV and CMOH)  is randomly added 
        /// to the end of each billing record.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void GenerateMonthlyBillingMoH_SimulatesResponseFromMoH_Valid()
        {

        }



        /// <summary>
        /// This test verify if the current method is capable of process the current information (included the MoH response) 
        /// and provide a summary of billings in the format of the requirements.
        /// </summary>
        [TestMethod]
        [TestCategory("Billing Tracker Class")]
        public void GenerateMonthlyBillingSummary_GenerateBillingSummary_Valid()
        {

        }

    */
    }
}
