﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demographics;

namespace EMS_Testing
{
    [TestClass]
    public class PatientTests
    {
        /* Petterson - I've moved this contente to BusinessTrackerTesting
        /// <summary>
        /// This tests the constructor and see if it gets the default values.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestPatientconstructor()
        {
            //Arrange + Act
            Patient patient = new Patient();

            //Assert
            Assert.AreEqual(0, patient.PatientID);
            Assert.AreEqual("", patient.GetFname());
            Assert.AreEqual("", patient.GetLname());
            Assert.AreEqual("", patient.GethCN());
            Assert.AreEqual(' ', patient.GetMinitial());
            Assert.AreEqual(DateTime.MinValue, patient.DateOfBirth);
            Assert.AreEqual("", patient.GetSex());
            Assert.AreEqual("", patient.GetHeadOfHouse());
            Assert.IsTrue(patient.Address.IsEmpty);
        }

        /// <summary>
        /// This tests the PatientID property.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestPatientID()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            patient.PatientID = 5;

            //Assert
            Assert.AreEqual(5, patient.PatientID);
        }

        /// <summary>
        /// This tests the SetPatientID method.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetPatientID()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            patient.SetPatientID(15);

            //Assert
            Assert.AreEqual(15, patient.GetPatientID());
        }

        /// <summary>
        /// This tests the invalid case for first name which is the empty string
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestEmptyString()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This tests the maximum length for patients First name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("hellohellohellohellohellohelllohellohellohellohellohellohellohellohellohello");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This tests the actual length for patients first name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestExactMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("hellohellohellohellohellohelllohellohellohellohellohellohell");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("hellohellohellohellohellohelllohellohellohellohellohellohell", patient.GetFname());
        }

        /// <summary>
        /// This tests for non-character situations in setting first name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_TestNonLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname("HelloBo8");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This test the null scenario for setting first name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetFname_null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetFname(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetFname());
        }

        /// <summary>
        /// This tests for empty string in patients last name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_TestEmptyString()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This tests the maximum length for patients last name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_TestMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("hellohellohellohellohellohelllohellohellohellohellohellohellohellohellohello");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This test the actual length of the last name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_TestExactMaxLength()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("hellohellohellohellohellohelllohellohellohellohellohellohell");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("hellohellohellohellohellohelllohellohellohellohellohellohell", patient.GetLname());
        }

        /// <summary>
        /// This tests the non letter situation when setting patients last name
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLaname_TestNonLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname("HelloBo8");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This tests the null situation when setting the last name for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSetLname_null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetLname(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetLname());
        }

        /// <summary>
        /// This tests the valid HCN and see if it returns true.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_ValidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000000000az");
            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000az", patient.GethCN());

        }

        /// <summary>
        /// This tests the invalid HCN situation.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_InvalidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000-000-0az-00");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the actual format of the HCN.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Format()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000000000az");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000az", patient.GethCN());
        }

        /// <summary>
        /// This tests the length of HCN
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Length_BelowMaximum()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("000-000-000-az");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Length_AboveMaximum()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("0000-0000-000-az");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the null situation of HCN.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the empty situation of HCN.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHCN_Empty()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SethCN("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GethCN());
        }

        /// <summary>
        /// This tests the format of date of birth for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Format_Valid()
        {
            //Arrange
            Patient patient = new Patient();

            string date = "11-02-1992";

            //Act
            patient.DateOfBirth = DateTime.Parse(date);

            //Assert
            Assert.AreEqual(DateTime.Parse(date), patient.DateOfBirth);
        }

        /// <summary>
        /// This tests the invalid format of Date of birth for patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Format_InValid()
        {
            ////Arrange
            //Patient patient = new Patient();
            ////Act
            //bool Result = patient.SetdateOfBirth("11-021992");
            ////Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual("", patient.GetdateOfBirth());
        }

        /// <summary>
        /// This tests for invalid character entered in patients Date of birth.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Invalid_Character()
        {
            ////Arrange
            //Patient patient = new Patient();

            ////Act
            //bool Result = patient.SetdateOfBirth("11-Feb1992");

            ////Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual("", patient.GetdateOfBirth());

        }

        /// <summary>
        /// This tests for the valid length of Date of birth.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestDateOfBirth_Length_valid()
        {
            //Arrange
            Patient patient = new Patient();
            DateTime date = DateTime.Parse("11-02-1992");
            //Act
            patient.DateOfBirth = date;

            //Assert
            Assert.AreEqual(date, patient.DateOfBirth);
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is null.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_null()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex(null);

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is empty.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_Empty()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is valid.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_valid()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("F");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("F", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where the sex of the patient is invalid.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_InValid()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("A");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the situation where a different character has been entered which is not a letter.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_NotLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("8");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }

        /// <summary>
        /// This tests the length of the sex for a patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestSex_Length()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetSex("Femalee");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetSex());
        }


        /// <summary>
        /// This tests the valid HCN for head of house and see if it returns true.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_ValidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetHeadOfHouse("0000000000AZ");

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000AZ", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the invalid HCN situation for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_InvalidCase()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetHeadOfHouse("00000000AZ00");

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the actual format of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Format()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("0000000000AZ");
            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("0000000000AZ", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the wrong format of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_WrongFormat()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("AZ0000000000");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the length of HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Length()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("000000000000AZ");
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the null situation of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Null()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse(null);
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the empty situation of the HCN for head of house.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestHeadOfHouseHCN_Empty()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            bool Result = patient.SetHeadOfHouse("");
            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual("", patient.GetHeadOfHouse());
        }


        /// <summary>
        /// This tests the situation where the middle initial of the patient is null.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_null()
        {
            //Arrange
            Patient patient = new Patient();
            //Act
            //bool Result = patient.SetMinitial(null);
            //Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual("", patient.GetHeadOfHouse());
        }

        /// <summary>
        /// This tests the situation where the middle initial of the patient is empty.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_Empty()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetMinitial(' ');
            
            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual(' ', patient.GetMinitial());
        }

        /// <summary>
        /// This tests the situation where the middle initial of the patient is valid
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_valid()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetMinitial('H');

            //Assert
            Assert.AreEqual(true, Result);
            Assert.AreEqual('H', patient.GetMinitial());
        }

        /// <summary>
        /// This tests the situation where the middle initial of the patient is invalid
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_InValid()
        {
            //Arrange
            //Patient patient = new Patient();

            ////Act
            //bool Result = patient.SetMinitial('_');

            ////Assert
            //Assert.AreEqual(false, Result);
            //Assert.AreEqual(' ', patient.GetMinitial());
        }

        /// <summary>
        /// This tests the situation where a different character has been entered which is not a letter
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestMiddleInitial_NotLetter()
        {
            //Arrange
            Patient patient = new Patient();

            //Act
            bool Result = patient.SetMinitial('8');

            //Assert
            Assert.AreEqual(false, Result);
            Assert.AreEqual(' ', patient.GetMinitial());
        }

        /// <summary>
        /// This tests a valid patient.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            patient.SetPatientID(22);
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));

            patient.Address.SetAdressID(1);
            patient.Address.SetAdressLineOne("123 asdads");
            patient.Address.SetCity("Kitcherner");
            patient.Address.SetProvince("ON");
            patient.Address.SetPhoneNumber("(519)222-0000");

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(0, Result);

            // This is failing because the head of household is not set. Isn't that optional?
        }

        /// <summary>
        /// This tests a valid address.
        /// </summary>
        [TestMethod]
        [TestCategory("Address Class")]
        public void TestValidateAddress()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Address myAddress = new Address();
            patient.SetPatientID(22);
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));

            patient.Address.SetAdressID(1);
            patient.Address.SetAdressLineOne("123 asdads");
            patient.Address.SetCity("Kitcherner");
            patient.Address.SetProvince("ON");
            patient.Address.SetPhoneNumber("(519)222-0000");

            //Act
            myAddress = patient.Address;
            int Result = myAddress.ValidateAdress();

            //Assert
            Assert.AreEqual(0, Result);

            // This is failing because the head of household is not set. Isn't that optional?
        }

        /// <summary>
        /// This test patients having an invalid first name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidFName()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3101, Result);
        }

        /// <summary>
        /// This test patients having an invalid Last name.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidLName()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3102, Result);
        }

        /// <summary>
        /// This test patients having an invalid health card number.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidHCN()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3103, Result);
        }

        /// <summary>
        /// This test patients having an invalid date of birth.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidDateOfBirth()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3105, Result);
        }

        /// <summary>
        /// This test patients having an invalid sex.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidSex()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            patient.Address = new Address();
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3106, Result);
        }

        /// <summary>
        /// This test patients having an invalid address id.
        /// </summary>
        [TestMethod]
        [TestCategory("Patient Class")]
        public void TestValidatePatient_InvalidAddress()
        {
            //Arrange + Assert
            Patient patient = new Patient();
            Assert.AreEqual(true, patient.SetFname("Humaira"));
            Assert.AreEqual(true, patient.SetLname("Siddiqa"));
            Assert.AreEqual(true, patient.SethCN("0000000000AZ"));
            Assert.AreEqual(true, patient.SetSex("Female"));
            patient.DateOfBirth = DateTime.Parse("11-02-1992");
            Assert.AreEqual(true, patient.SetHeadOfHouse("0000000000AZ"));
            patient.SetPatientID(22);

            //Act
            int Result = patient.ValidatePatient();

            //Assert
            Assert.AreEqual(-3107, Result);
        }
        */
    }
}
