﻿/*****************************************************************************************************
* FILE : 		SupportClassLibraryTesting.cs
* PROJECT : 	SQ-I Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2017-Dec-05

* DESCRIPTION/Requirements: 

` It is a Unit Test Class to test the Support Class Library stuff.  

* Refence:

*****************************************************************************************************/


using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Support;
using System.Reflection;
using System.IO;

namespace EMS_Testing
{
    /// <summary>
    /// This class test all support(Logging/FileIO) methods. 
    /// </summary>
    [TestClass]
    public class SupportClassLibraryTest
    {
        // Attributes - Support related
        private static FileIO fileIO = new FileIO();
        private static Logging logging = new Logging();

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        //
        //---------------------Loggin Class -----------------------
        //

        /// <summary>
        /// Test the method that return the path to writing logs. 
        /// </summary>
        [TestMethod]
        [TestCategory("Loggin Class")]
        public void LoggingClassTestIfLogDirectoryExistis()
        {
            //
            // TODO: Add test logic here
            //
            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Testing logging.SetProgram");
            bool result = false;

            // act
            if (Directory.Exists(logging.GetLogPath()) == true) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test the method that return a string with the actual filename that must be used.
        /// </summary>
        [TestMethod]
        [TestCategory("Loggin Class")]
        public void LoggingClassTestIfLogFileExistis()
        {
            //
            // TODO: Add test logic here
            //
            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Testing logging.SetProgram");
            bool result = false;
            
            // act
            if (File.Exists(logging.GetFileName()) == true) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test how many files the system will manage. 
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOGetFileNames()
        {
            bool result = false;
            int howManyFileExists = 9;   // Set up here the max file name that that system can management. 
            
            //
            // TODO: Add test logic here
            //
            string[] fileNames = fileIO.GetFileNames();

            // act
            if (fileNames.Length == howManyFileExists) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into ADDRESS file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOWriteFullAddressFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullAddressFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into ADDRESS file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOWriteFullAddressFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullAddressFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }



        /// <summary>
        /// Test to write something into Monthlybilling file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOWriteFullMonthlybillingFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullMonthlybillingFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Monthlybilling file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOWriteFullMonthlybillingFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullMonthlybillingFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }






        /// <summary>
        /// Test to write something into Patiant file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOWriteFullPatiantFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullPatiantFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Patiant file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void FileIOWriteFullPatiantFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullPatiantFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Appointment file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullAppointmentFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullAppointmentFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Appointment file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullPatiantFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullPatiantFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Bill file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullBillFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullBillFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Bill file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullBillFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullBillFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Serviceline file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullServicesMasterFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullServicesMasterFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Serviceline file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullServicesMasterFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullServicesMasterFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Service file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullResponseFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullResponseFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Service file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("Test WriteFullServiceFileOverwrite from FileIO")]
        public void WriteFullResponseFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullResponseFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Serviceline file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullServicelineFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullServicelineFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Serviceline file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullServicelineFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullServicelineFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Service file with append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("File IO Class")]
        public void WriteFullServiceFileAppend()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullServiceFileAppend("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to write something into Service file without append flagged
        /// </summary>
        [TestMethod]
        [TestCategory("Test WriteFullServiceFileOverwrite from FileIO")]
        public void WriteFullServiceFileOverwrite()
        {
            bool result = false;
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.WriteFullServiceFileOverwrite("txtPatient")) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }



        /// <summary>
        /// Test to read the whole ADDRESS file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullAddressFile from FileIO")]
        public void ReadFullAddressFile()
        {
            bool result = false;
            string address = ""; 
            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.ReadFullAddressFile(ref address)) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to read the whole Patiant file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullPatiantFile from FileIO")]
        public void ReadFullPatiantFile()
        {
            bool result = false;
            string patiant = "";

            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.ReadFullPatiantFile(ref patiant)) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to read the whole Appointment file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullAppointmentFile from FileIO")]
        public void ReadFullAppointmentFile()
        {
            bool result = false;
            string appointment = "";

            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.ReadFullAppointmentFile(ref appointment)) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to read the whole Bill file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullBillFile from FileIO")]
        public void ReadFullBillFile()
        {
            bool result = false;
            string bill = "";

            //
            // TODO: Add test logic here
            //

            // act
            result = fileIO.ReadFullBillFile(ref bill);

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to read the whole Serviceline file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullServicesMasterFile from FileIO")]
        public void ReadFullServicesMasterFile()
        {
            bool result = false;
            string ServicesMaster = "";

            //
            // TODO: Add test logic here
            //

            // act
            result = fileIO.ReadFullServicesMasterFile(ref ServicesMaster);

            //Assert
            Assert.AreEqual(true, result);
        }



        /// <summary>
        /// Test to read the whole Service file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullResponseFile from FileIO")]
        public void ReadFullResponseFile()
        {
            bool result = false;
            string Response = "";

            //
            // TODO: Add test logic here
            //

            // act
            result = fileIO.ReadFullResponseFile(ref Response);

            //Assert
            Assert.AreEqual(true, result);
        }


        /// <summary>
        /// Test to read the whole Serviceline file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullServicelineFile from FileIO")]
        public void ReadFullServicelineFile()
        {
            bool result = false;
            string serviceLine = "";

            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.ReadFullServicelineFile(ref serviceLine)) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to read the whole Serviceline file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullServicelineFile from FileIO")]
        public void ReadFullMonthlybillingFile()
        {
            bool result = false;
            string serviceLine = "";

            //
            // TODO: Add test logic here
            //

            // act
            if (fileIO.ReadFullMonthlybillingFile(ref serviceLine)) { result = true; };

            //Assert
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test to read the whole Service file 
        /// </summary>
        [TestMethod]
        [TestCategory("Test ReadFullServiceFile from FileIO")]
        public void ReadFullServiceFile()
        {
            bool result = false;
            string service = "";

            //
            // TODO: Add test logic here
            //

            // act
            result = fileIO.ReadFullServiceFile(ref service);

            //Assert
            Assert.AreEqual(true, result);
        }


    }
}
