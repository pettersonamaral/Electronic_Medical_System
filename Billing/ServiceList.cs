﻿
/**********************************************************************************************************************************
 * FILE          : ServiceList.cs
 * PROJECT       : INFO2180 Software Quality I - EMS System
 * PROGRAMMER    : ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
 * VERSION DATE  : 2017-NOV-15
 * DESCRIPTION   : This file contains the definition of the Service List Class, which simulates a list of diferent services that
 *                 can be part of the same bill.
 *                 

 * RECENT UPDATES: (2017-NOV-23) - Definition of Setters/Getters methods
 *                               - Commenting of the same methods
 *                 (2017-DEC-03) - Adequate Doxygen commenting changes
 *                               - Added Error Codes into Error Class
 *                 (2017-DEC-08) - Fixing "SetBillID" validation error.
 *                (2017-DEC-21) - Adding the MoH service status codes into service List class (extracted from Bill class). 
 *                                 Changed from BillStatus to ServiceStatus.
 *                                 
 **********************************************************************************************************************************/

using System;

#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()

namespace Billing
{
    // SERVICE LIST CLASS DEFINITION ****************************************************************************************************************


    /// <summary>
    /// <b> Description: </b>The ServiceList class indicates the services and quantities provided to the patient in a specific appointment
    /// </summary>
    public sealed class ServiceList : IEquatable<ServiceList>
    {
        #region Constants
        public enum ServiceStatus { UPRC, PAID, DECL, FHCV, CMOH }   //UPRC -> SHORT FOR UNPROCESSED   //PC_UPDATE  //TODO CHECK THE REST OF THE SOLUTION
        #endregion

        #region Attributes

        private int quantity;           /// indicates the number of times a specific service was provided at an appointment
        private string status;              /* holds the Service current status code according to Ministry of Health reference */

        #endregion

        #region Properties

        public int Quantity { get => quantity; set => SetQuantity(value); }     //PC_UPDATE
        public string Status { get => status; set => SetStatus(value); }        //PC_UPDATE
        public Service Service { get; set; }

        #endregion

        #region Constructor

        ///<summary>
        ///Initialize the ServiceList Class Object members with pre-defined values.
        ///</summary>
        ///<seealso cref="Other ServiceList Methods and Billing Library Classes methods"/>
        public ServiceList()
        {
            // Initializes the class members
            quantity = 0;
            status = ServiceStatus.UPRC.ToString();
            Service = new Service();
        }

        #endregion

        #region Accessors

        /// <summary>
        /// Method to access the quantity member value.
        /// </summary>
        /// <returns>return current quantity value</returns>
        /// <seealso cref="Other ServiceList Methods and Billing Library Classes methods"/>
        public int GetQuantity()
        {
            return quantity;
        }


        /// <summary>
        /// Accessor method to get the member "status" value.
        /// </summary>
        /// <returns>return current value of bill status </returns>
        /// <seealso cref="Other ServiceList Methods and Billing Library Classes methods"/>
        public string GetStatus()
        {
            return status;
        }

        #endregion

        #region Mutators

        /// <summary>
        /// Sets/Validates a new value for the quantity member.
        /// </summary>
        /// <param name="newQuantity">carries the new value of the quantity member</param>
        /// <returns>represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other ServiceList Methods and Billing Library Classes methods"/>
        public bool SetQuantity(int newQuantity)
        {
            // Local Variables
            bool statusReturn = false;       // indicates if there is any error code associated to this method

            // check for positive values
            if (newQuantity >= 0)
            {
                quantity = newQuantity;
                statusReturn = true;
            }

            // Returns an error code specified in the "Error" Class
            return statusReturn;
        }


        /// <summary>
        /// Sets/Validates a new value for the status member.
        /// </summary>
        /// <param name="newStatus">holds the "new" status member value</param>
        /// <returns>Represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other ServiceList Methods and Billing Library Classes methods"/>
        public bool SetStatus(string newStatus)
        {
            // Local Variables
            bool statusReturn = false;       // indicates if there is any error code associated to this method

            newStatus = newStatus.Trim();

            // Compares method parameter with "billStatus" Enum components
            foreach (ServiceStatus value in Enum.GetValues(typeof(ServiceStatus)))
            {
                if (newStatus.Equals(value.ToString()))
                {
                    // if there is a match, a new status is set
                    status = newStatus;
                    statusReturn = true;
                    break;
                }
            }

            return statusReturn;
        }

        #endregion

        #region Overridden Methods 

        public bool Equals(ServiceList other)
        {
            if (other == null) { return false; }

            bool retCode = false;

            if (quantity == other.quantity &&
                status == other.status &&
                Service.Equals(other.Service))
            {
                retCode = true;
            }

            return retCode;
        }


        public static bool operator ==(ServiceList left, ServiceList right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }


        public static bool operator !=(ServiceList left, ServiceList right)
        {
            return !(left == right);
        }


        public override string ToString()
        {
            return $"{Service.ToString()} - {status.ToString()}";
        }

        #endregion
    }
}

