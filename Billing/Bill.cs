﻿
/* ******************************************************************************************************************************************
* FILE          : Bill.cs
* PROJECT       : INFO2180 Software Quality I - EMS System
* PROGRAMMER    : ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA
* VERSION DATE  : 2017-NOV-15
* 
* 
* DESCRIPTION   : This file contains the Bill Class definition, which simulates a EMS Bill. The class is meant to validate any 
*                 instantiated Bill object through constructors/modifiers methods.
*                The Bill class will identify attributes such as patientID, 
*                 appointmentID and current status of the charging process through the Ministry of Health. It also can
*                  identify wether if a patient has to come back to another visit (recall).
*  RECENT UPDATES: (2017-NOV-23) - Definition of Setters/Getters methods
*                                - Commenting of the same methods
*                  (2017-NOV-23) - Fixing Doxygen commenting
*                                - Passing ERROR codes into Errors class
*                (2017-DEC-21) - Removed status code reference from MoH response, and added it o the ServiceList.
*                                Acessor and Mutator for that member was also removed.
*                                
* Reference:
* ****************************************************************************************************************************************** */

using Demographics;
using System;
using System.Collections.Generic;
using System.Linq;

#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()

namespace Billing
{

    //===========================================================================================================================================//
    //                                                          ENUM CONSTANTS
    //===========================================================================================================================================//



    // BILL CLASS DEFINITION *************************************************************************************************************************

    /// <summary>
    /// <b> Description: </b>The Bill class sets the footprint for a formal EMS System bill that refers an specific patient
    /// patient appointment and related services. The Bill class will identify attributes such as patientID, 
    /// appointmentID and current status of the charging process through the Ministry of Health. It also can
    /// identify wether if a patient has to come back to another visit (recall).
    /// </summary>
    /// 
    public class Bill : IEquatable<Bill>
    {
        #region Attributes

        private int billID;         /* holds a unique identifier for each instantiated Bill object */
        private int flagRecall;     /* identifies if patient is required to return after 1,2, or 3 weeks */
        private bool statusDone;     /* identifies if current Bill object has been processed */

        #endregion

        #region Properties

        public int BillID { get => billID; set => SetBillID(value); }                       // PC_UPDATE
        public int FlagRecall { get => flagRecall; set => SetFlagRecall(value); }           // PC_UPDATE
        public bool StatusDone { get => statusDone; set => statusDone = value; }
        public Patient Patient { get; set; }
        public Appointment Appointment { get; set; }
        public List<ServiceList> Services { get; set; }                                     // PC_UPDATE

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize the Class Bill Object members with pre-defined values.
        /// </summary>
        /// <seealso cref="Other Bill Methods and Billing Library Classes methods"/>
        /// 
        public Bill()
        {
            // Initializes the Bill Class members
            billID = 0;                    // standard (or initial) condition: unique ID will be set at the Billing "Tracker" Class 
            flagRecall = 0;                    // standard (or initial) condition: no recall was made
            statusDone = false;                // standard (or initial) condition: not complete

            Patient = new Patient();
            Appointment = new Appointment();
            Services = new List<ServiceList>();
        }

        #endregion

        #region Accessors

        /// <summary>
        /// Accessor method to get the member "billID" value
        /// </summary>
        /// <returns >returns current BillID value</returns>
        /// <seealso cref="Other Bill Methods and Billing Library Classes methods"/>
        public int GetBillID()
        {
            return billID;
        }


        /// <summary>
        /// Accessor method to get the member "flagRecall" value.
        /// </summary>
        /// <returns>Represents member "flagRecall" current value.</returns>
        /// <seealso cref="Other Bill Methods and Billing Library Classes methods"/>
        public int GetFlagRecall()
        {
            return flagRecall;
        }

        #endregion

        #region Mutators

        /// <summary>
        /// Sets/Validates a new value for the billID member.
        /// </summary>
        /// <param name="newBillID">holds the "new" billID member value</param>
        /// <returns>Represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Bill Methods and Billing Library Classes methods"/>
        public bool SetBillID(int newBillID)
        {
            // Local Variables
            bool statusReturn = false;       // indicates if there is any error code associated to this method

            // check for positive values
            if (newBillID >= 0)
            {
                billID = newBillID;
                statusReturn = true;
            }

            // Returns an error code specified in the "Error" Class
            return statusReturn;
        }


        /// <summary>
        /// Sets/Validates a new value for the flagRecall member.
        /// The flag must be a number between 0 and 3
        /// </summary>
        /// <param name="newFlagRecall">olds the "new" flagRecall member value</param>
        /// <returns>Represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Bill Methods and Billing Library Classes methods"/>
        public bool SetFlagRecall(int newFlagRecall)
        {
            // Local Variables
            bool statusReturn = false;       // indicates if there is any error code associated to this method

            // check if number is between "0" (no recall) and "3" (three weeks recall)
            if ((newFlagRecall >= 0) && (newFlagRecall <= 3))
            {
                flagRecall = newFlagRecall;
                statusReturn = true;
            }

            return statusReturn;
        }

        #endregion

        #region Overridden Methods

        public bool Equals(Bill other)
        {
            if (other == null) { return false; }
            bool retCode = false;

            if (flagRecall == other.flagRecall &&
                statusDone == other.statusDone &&
                Patient.Equals(other.Patient) &&
                Appointment.Equals(other.Appointment) &&
                Services.SequenceEqual(other.Services))
            {
                retCode = true;
            }

            return retCode;
        }


        public static bool operator ==(Bill left, Bill right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }


        public static bool operator !=(Bill left, Bill right)
        {
            return !(left == right);
        }


        public override string ToString()
        {
            return $"{Appointment.GetDayAndTime().ToShortDateString()} - {Patient.ToString()}";
        }


        public string ToString(bool fullView)
        {
            string retCode = "";
            if (fullView)
            {
                retCode = $"Appointment: {Appointment.ToString()}\nPatient:\n{Patient.ToString(true)}\n";

            }
            else
            {
                if (flagRecall > 0)
                {
                    retCode += $"Recall in {flagRecall} weeks\n";        
                }
                retCode += $"Process status: {statusDone}\n";
            }

            if (Services.Count > 0) { retCode += "Services:\n"; }

            foreach (ServiceList sl in Services)
            {
                retCode += sl.ToString() + "\n";
            }

            return retCode;
        }

        #endregion
    }
}
