﻿
/************************************************************************************************************************
* FILE          : Service.cs
* PROJECT       : INFO2180 Software Quality I - EMS System
* PROGRAMMER    : ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* VERSION DATE  : 2017-NOV-15
* DESCRIPTION   : The file contains the Service class definition, which simulates a specific service as an object that has as 
*                as members a serviceID, a description and a value.
* RECENT UPDATES: (2017-NOV-23) - Definition of Setters/Getters methods
*                               - Commenting of the same methods
*                 (2017-DEC-03) - Added "feeCode" member to represent billing requirements format
*                               - Added "effectiveDate" member to represent date when service became valid
*                               - Added Error codes/ messages into Error class
*                               
* Reference:  
* *********************************************************************************************************************/

using System;
using System.Text.RegularExpressions;

#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()

namespace Billing
{

    // SERVICE CLASS DEFINITION ***********************************************************************************************

    /// <summary>
    /// <b> Description: </b>The ServiceList class indicates the services and quantities provided to the patient in a specific appointment
    /// </summary>
    public sealed class Service : IEquatable<Service>
    {
        #region Constants
        public const int FEE_CODE_MAX_SIZE = 4;
        #endregion

        #region Attributes

        private int serviceID;         /* holds a unique identifier for each instantiated service object   */
        private string feeCode;           /* represents Ministry of Health service coding                     */
        private DateTime effectiveDate;     /* carries date when service became valid  (YYYYMMDD)               */
        private double serviceValue;      /* indicates the current service value                              */

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize the Class Service Object members with pre-defined values
        /// </summary>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public Service()
        {
            // Set Service Initial Values
            serviceID = 0;
            feeCode = "X000";
            effectiveDate = DateTime.MinValue;
            serviceValue = 0;
        }

        /// <summary>
        /// Alternate Constructor: Initialize the Class Service Object members with parameters
        /// </summary>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public Service(int newServiceID, string newFeeCode, DateTime newEffectiveDate, double newServiceValue )
        {
            // Set Service Initial Values
            serviceID     = newServiceID;
            feeCode       = newFeeCode;
            effectiveDate = newEffectiveDate;
            serviceValue  = newServiceValue;
        }

        #endregion

        #region Properties
         
        public int ServiceID { get => serviceID; set => SetServiceID(value); }
        public string FeeCode { get => feeCode; set => SetFeeCode(value); }
        public double ServiceValue { get => serviceValue; set => SetServiceValue(value); }
        public DateTime EffectiveDate { get => effectiveDate; set => effectiveDate = value; }

        #endregion

        #region Accessors

        /// <summary>
        /// Accessor method to get the member "serviceID" value.
        /// </summary>
        /// <returns>returns current value of serviceID member</returns>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public int GetServiceID()
        {
            return serviceID;
        }


        /// <summary>
        /// Accessor method to get the member "feeCode" value. 
        /// </summary>
        /// <returns>returns current value of feeCode member</returns>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public string GetFeeCode()
        {
            return feeCode;
        }


        /// <summary>
        /// Accessor method to get the member "serviceValue" value.
        /// </summary>
        /// <returns>returns current value of serviceValue member</returns>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public double GetServiceValue()
        {
            return serviceValue;
        }

        #endregion

        #region Mutators

        /// <summary>
        /// Sets/Validates a new value for the serviceID member.
        /// </summary>
        /// <param name="newServiceID">holds the "new" serviceID member value</param>
        /// <returns>represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public bool SetServiceID(int newServiceID)
        {
            // Local Variables
            bool statusReturn = false;       // indicates if there is any error code associated to this method

            //if correct, set new ID into the class member
            if (newServiceID >= 0)
            {
                serviceID = newServiceID;
                statusReturn = true;
            }

            // Returns either zero, or an error code specified in the "Error" Class
            return statusReturn;
        }


        /// <summary>
        /// Sets/Validates a new value for the service FeeCode.
        /// </summary>
        /// <param name="newFeeCode">holds the "new" service description member value</param>
        /// <returns>Represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public bool SetFeeCode(string newFeeCode)
        {
            // Local Variables
            bool statusReturn = false;
            Regex feeCodeFormat = new Regex("[A-Z][0-9][0-9][0-9]");

            newFeeCode = newFeeCode.Trim();

            // Check for blank entry
            if (newFeeCode.Length == FEE_CODE_MAX_SIZE && 
                feeCodeFormat.IsMatch(newFeeCode))
            {
                feeCode = newFeeCode;
                statusReturn = true;
            }

            return statusReturn;
        }


        /// <summary>
        /// Sets/Validates a new value for the serviceValue description.
        /// </summary>
        /// <param name="newServiceValue">holds the "new" serviceValue member value</param>
        /// <returns>Represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Service Methods and Billing Library Classes methods"/>
        public bool SetServiceValue(double newServiceValue)
        {
            // Local Variables
            bool statusReturn = false;       // indicates if there is any error code associated to this method

            //if correct, set new ID into the class member
            if (newServiceValue >= 0.0)
            {
                serviceValue = newServiceValue;
                statusReturn = true;
            }

            // Returns either zero, or an error code specified in the "Error" Class
            return statusReturn;
        }

        #endregion

        #region Overridden Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Service other)
        {
            if (other == null) { return false; }

            bool retCode = false;

            if (feeCode == other.feeCode &&
                effectiveDate.Equals(other.effectiveDate) &&
                (Math.Abs(serviceValue - other.serviceValue) < 0.001))
            {
                retCode = true;
            }

            return retCode;
        }


        public static bool operator ==(Service left, Service right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }


        public static bool operator !=(Service left, Service right)
        {
            return !(left == right);
        }



        /// <summary>
        /// Overridden method ToString() that shows the fee code, effective date and value.
        /// E.G.: X000 - Effective date: 01/01/2000 - $ 52.00
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{feeCode} - Effective date: {effectiveDate.ToShortDateString()} - {serviceValue.ToString("C0")}";
        }

        #endregion
    }
}


