﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
    public interface IBilling
    {
        // =============== ADD ================
        int AddBill(Bill obj);
        int AddServiceList(int billID, ServiceList obj);


        // =============== FIND ===============
        Bill FindBill(int patientID, int appointmentID);
        List<Service> FindServices(string serviceCode = "");
        Service FindService(int serviceID);


        // ============== UPDATE ==============
        int UpdateBill(Bill obj);
        int UpdateServiceList(int billID, ServiceList obj);


        // ============= SPECIAL ==============
        string MonthlyBillingSummary(DateTime date);
        List<string> GenerateMonthlyBillingCSV(DateTime date);
        List<string> MonthlyReconcileCSV(DateTime date);
    }
}
