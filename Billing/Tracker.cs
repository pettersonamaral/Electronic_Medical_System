﻿/********************************************************************************************************************************* 
* FILE          : Tracker.cs
* PROJECT       : INFO2180 Software Quality I - EMS System
* PROGRAMMER    : ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* VERSION DATE  : 2017-NOV-15
* DESCRIPTION   : 
* RECENT UPDATES: (2017-NOV-23) - Definition of Setters/Getters methods
*                               - Commenting of the same methods
*                 (2017-DEC-03) - Added "feeCode" member to represent billing requirements format
*                               - Added "effectiveDate" member to represent date when service became valid
*                               - Added Error codes/ messages into Error class
*                 (2017-DEC-08) - Fixing references to other methods, adding method to read Bill Sample
*                 (2018-JAN-04) - Added a new "FindService" method with the ServiceID parameter
*                               - Got rid of one of the constructors and added lists initialization within the onde left
*                               - Changed the return of the "GenerateMonthlyBillingSummary" to a string with the formatted info
**********************************************************************************************************************************/

// FOR DEBUGGIN, JUST COMMENT IT OUT IF WANT THE ORIGNAL VERSION *****************************************************************

//#define LOAD_FAKE_TEST_DATA

//********************************************************************************************************************************

/*

// Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Support;
using System.Globalization;
using Demographics;
using System.Reflection;

namespace Billing
{

    /// <summary>
    /// <b> Description: </b>The Tracker class auxiliates the data manipulation and the communication of the Billing Class Library with the other
    /// </summary>
    public class Tracker
    {
        //===========================================================================================================================================//
        //                                                          CONSTANTS
        //===========================================================================================================================================//

        //ERROR CODES
        public const int RETURN_GENERIC_SUCCESS = 0;
        public const int RETURN_BILLING_TRACKER_SUCESS = 4500;
        public const int RETURN_BILLING_TRACKER_FAILURE = -4500;
        public const int ERR_SET_NEW_LIST_OF_BILLS = -4501;
        public const int ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES = -4502;
        public const int ERR_SET_NEW_SERVICE_LIST = -4503;
        public const int ERR_FIND_BILL = -4504;
        public const int ERR_ADD_BILL = -4505;
        public const int ERR_FAILED_UPDATE_SERVICE_LIST = -4506;
        public const int ERR_DATE_AVAILABLE_SERVICE_LIST = -4507;
        public const int ERR_VALUE_AVAILABLE_SERVICE_LIST = -4508;
        public const int ERR_SET_VALUE_NEW_SERVICE = -4509;
        public const int ERR_SET_FEE_CODE_NEW_SERVICE = -4510;
        public const int AVAILABLE_SERVICES_EMPTY = -4511;
        public const int AVAILABLE_SERVICE_EXISTENT = -4512;
        public const int HCN_NOT_FOUND = -4513;


        // Additional Constants to support FileIO interaction
        public const int WriteBillObjIntoFileFail = -4514;
        public const int ErrorReadFileToStringArray = -4550;
        public const int GetIntegerFailFormat = -4551;
        public const int GetIntegerFailOverflow = -4552;
        public const int GetDoubleFailFormat = -4553;
        public const int GetDoubleFailOverflow = -4554;
        public const int ERR_ReadServiceFile = -4555;
        public const int WriteServiceListObjIntoFileFail = -4515;
        public const int ReadPatientFileFailure = -4516;
        public const int ReadServiceListFileFailure = -4517;
        private const string DivBillingTrackerFiles = "|";
        private const string DivDemographicTrackerFiles = "|";
        public static string[] FieldSeparator = { DivDemographicTrackerFiles };
        public const int BILL_RECORD_LENGTH = 36;
        public const int BILL_RECORD_RESPONSE_LENGTH = 40;
        public const int SERVICE_RECORD_LENGTH = 23;


        //===========================================================================================================================================//
        //                                                          CLASS MEMBERS
        //===========================================================================================================================================//

        private static FileIO fileIO;
        private static Logging logging;
        private static Demographics.Tracker demoTracker;

        private List<Bill> listOfBills;
        private List<ServiceList> listOfServices;
        private List<Service> availableServices;  // where to carry the PROVIDED master table conetent
        private List<string> listOfReconciledServicesFromMoH;
        private List<string> listOfFormattedBillingRecords;

        public enum BillingClasses { Bill, Service, ServiceList };

        private struct brokenDownMoHSample
        {
            public string dateOfService { get; set; }
            public string HCN { get; set; }
            public string Gender { get; set; }
            public string billingCode { get; set; }
            public string valueOfSeviceCode { get; set; }
            public string statusMoH { get; set; }
            public string distinctEncounter { get; set; }

        }

        //===========================================================================================================================================//
        //                                                          CLASS METHODS
        //===========================================================================================================================================//

        // Constructor ********************************************************************************************************************************

        /// <summary>
        /// Initialize the Class Tracker Object members with pre-defined values.
        /// </summary>
        /// <param name="fileHandler">parameter from support class library still to be assessed</param>
        /// <param name="logHandler">parameter from support class library still to be assessed</param>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public Tracker(FileIO fileHandler, Logging logHandler, Demographics.Tracker demoHandler)
        {
            // Initializes support members
            fileIO = fileHandler;
            logging = logHandler;
            demoTracker = demoHandler;

            //Initialize the list members 
            listOfBills = new List<Bill>();
            listOfServices = new List<ServiceList>();
            availableServices = new List<Service>();            // where to carry the PROVIDED master table conetent

            // Initialize a list of reconciled bills that should be filled by a flat file information
            listOfReconciledServicesFromMoH = new List<string>();
            listOfFormattedBillingRecords = new List<string>();
        }

        /// <summary>
        ///  Alternative Constructor... empty for now
        /// </summary>
        public Tracker()
        {

        }


        // Destructor *************************************************************************************************************

        /// <summary>
        /// Destroys Class Tracker Object once it is out of scope.
        /// </summary>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        ~Tracker()
        {

        }



        // Acessors ****************************************************************************************************************

        /// <summary>
        /// Method that provide access to the value of the "ListOfBills" member
        /// </summary>
        /// <returns>returns the list of bills with the current set of objects</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public List<Bill> GetListOfBills()
        {
            return listOfBills;
        }



        /// <summary>
        /// Method that provide access to the value of the "ListOfServices" member.
        /// </summary>
        /// <returns>Meant to hold the list of services and fees associated with one bill.</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public List<ServiceList> GetListOfServices()
        {
            return listOfServices;
        }



        /// <summary>
        /// Method that provide access to the value of the "availableServices" member.
        /// </summary>
        /// <returns>holds the loaded MoH services code "Master Table"</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public List<Service> GetAvailableServices()
        {
            return availableServices;
        }



        // Mutators ****************************************************************************************************************

        /// <summary>
        /// Set the member listOfBills new value.
        /// </summary>
        /// <param name="newListOfBills">new listOfBills.</param>
        /// <returns>represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int SetListOfBills(List<Bill> newListOfBills)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method

            if (newListOfBills == null) { statusReturn = ERR_SET_NEW_LIST_OF_BILLS; }
            else
            {
                try
                {
                    listOfBills = newListOfBills;
                    statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                }
                catch (Exception e)
                {
                    statusReturn = ERR_SET_NEW_LIST_OF_BILLS;
                    Console.WriteLine("[Billing SetListOfBills] " + e.Message);
                }
            }

            return statusReturn;
        }



        /// <summary>
        /// Set the member listOfServices new value.
        /// </summary>
        /// <param name="newListOfServices">new listOfServices</param>
        /// <returns>represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int SetListOfServices(List<ServiceList> newServiceList)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method

            if (newServiceList == null) { statusReturn = ERR_SET_NEW_SERVICE_LIST; }
            else
            {
                try
                {
                    listOfServices = newServiceList;
                    statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                }
                catch (Exception e)
                {
                    statusReturn = ERR_SET_NEW_SERVICE_LIST;
                    Console.WriteLine("[Billing SetListOfBills] " + e.Message);
                }
            }

            return statusReturn;
        }



        /// <summary>
        /// Set the member availableServices new value
        /// </summary>
        /// <param name="newAvailableServices">new list of availableServices</param>
        /// <returns>represents the function operation status, which can be identified with error codes</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int SetAvailableServices(List<Service> newAvailableServices)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method

            if (newAvailableServices == null) { statusReturn = ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES; }
            else
            {
                try
                {
                    availableServices = newAvailableServices;
                    statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                }
                catch (Exception e)
                {
                    statusReturn = ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES;
                    Console.WriteLine("[Billing SetListOfBills] " + e.Message);
                }
            }

            return statusReturn;
        }



        // Additional Methods ****************************************************************************************************************

        /// <summary>
        /// Method to add a new Bill object to the current list of Bills in memory
        /// </summary>
        /// <param name="newBill">Bill Class object</param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int AddBill(Bill newBill)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method
            int newBillID = 0;

            try
            {
                // Check if the Bill Already Exists
                if (CheckIfObjBillAlreadyExists(newBill) > 0)
                {
                    // Generates a unique BillID
                    newBillID = (GetListOfBills().Count) + 1;

                    // Creates a try-catch block to handle any potential exception manipulating the list

                    // Attribute new Bill ID to the instantiated Bill object
                    newBill.SetBillID(newBillID);
                    // Add Bill object to the list of Bills
                    listOfBills.Add(newBill);
                    WriteOneBillObjIntoFile(newBill);

                    statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                }
                else
                {
                    // Changes the method statusReturn to a specific code to represent exception
                    statusReturn = ERR_ADD_BILL;
                }

            }
            catch (Exception issue)
            {
                // In case of any exception
                Console.WriteLine(issue.Message);

                // Changes the method statusReturn to a specific code to represent exception
                statusReturn = ERR_ADD_BILL;
            }

            return statusReturn;
        }


        /// <summary>
        /// Method to add a new Bill object to the current list of Bills in memory
        /// </summary>
        /// <param name="newBill">Bill Class object</param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int AddBillFromFileToList(Bill newBill)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method
            int newBillID = 0;

            // Generates a unique BillID
            newBillID = (GetListOfBills().Count) + 1;

            // Creates a try-catch block to handle any potential exception manipulating the list
            try
            {
                // Attribute new Bill ID to the instantiated Bill object
                newBill.SetBillID(newBillID);
                // Add Bill object to the list of Bills
                listOfBills.Add(newBill);

                statusReturn = RETURN_GENERIC_SUCCESS;
            }
            catch (Exception issue)
            {
                // In case of any exception
                Console.WriteLine(issue.Message);

                // Changes the method statusReturn to a specific code to represent exception
                statusReturn = ERR_ADD_BILL;
            }

            return statusReturn;
        }




        /// <summary>
        /// Method to add a new ServiceLine object to the current list of Bills in memory
        /// </summary>
        /// <param name="newServiceList"> holds a new service list</param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int AddServiceLine(ServiceList newServiceList)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method

            // Creates a try-catch block to handle any potential exception manipulating the list
            try
            {
                // Attribute new ID to the instantiated ServiceList object
                //newServiceList.SetServiceID(newServiceListID);

                // Add ServiceList object to the "list" of ServiceLists
                listOfServices.Add(newServiceList);
                WriteOneServiceListObjIntoFile(newServiceList);

                statusReturn = RETURN_BILLING_TRACKER_SUCESS;
            }
            catch (Exception issue)
            {
                // In case of error, display the exception Message
                Console.WriteLine(issue.Message);

                // Changes the method statusReturn to a specific code to represent a memory issue
                statusReturn = ERR_SET_NEW_SERVICE_LIST;
            }
            return statusReturn;
        }


        /// <summary>
        /// Method to add a new ServiceLine object to the current list of Bills in memory
        /// </summary>
        /// <param name="newServiceList"> holds a new service list</param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int AddServiceLineFromFileToList(ServiceList newServiceList)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method

            // Creates a try-catch block to handle any potential exception manipulating the list
            try
            {
                // Attribute new ID to the instantiated ServiceList object
                //newServiceList.SetServiceID(newServiceListID);

                // Add ServiceList object to the "list" of ServiceLists
                listOfServices.Add(newServiceList);


                statusReturn = RETURN_BILLING_TRACKER_SUCESS;
            }
            catch (Exception issue)
            {
                // In case of error, display the exception Message
                Console.WriteLine(issue.Message);

                // Changes the method statusReturn to a specific code to represent a memory issue
                statusReturn = ERR_SET_NEW_SERVICE_LIST;
            }
            return statusReturn;
        }


        /// <summary>
        /// Method to add a new Service object to the current list of Services in memory
        /// </summary>
        /// <param name="newService"> holds a new service obj</param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int AddService(Service newService)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method
            int newServiceID = 0;


            // Generates a unique ServiceListID
            // Get the current number of items inside the list and add one to keep the Service ID sequencial
            newServiceID = (availableServices.Count() + 1);

            // Creates a try-catch block to handle any potential exception manipulating the list
            try
            {
                // Attribute new ID to the instantiated ServiceList object
                newService.SetServiceID(newServiceID);

                // Add ServiceList object to the "list" of ServiceLists
                availableServices.Add(newService);
                WriteOneServiceObjIntoFile(newService);

                statusReturn = RETURN_BILLING_TRACKER_SUCESS;
            }
            catch (Exception ex)
            {
                // Changes the method statusReturn to a specific code to represent a memory issue
                statusReturn = ERR_SET_NEW_SERVICE_LIST;
                Console.WriteLine("[AddService] " + ex.Message);
            }
            return statusReturn;
        }


        /// <summary>
        /// Method to add a new Service object to the current list of Services in memory
        /// </summary>
        /// <param name="newService"> holds a new service obj</param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int AddServiceFromFileToList(Service newService)
        {
            // Local Variables
            int statusReturn = 0;       // indicates if there is any error code associated to this method
            int newServiceID = 0;


            // Generates a unique ServiceListID
            // Get the current number of items inside the list and add one to keep the Service ID sequencial
            newServiceID = (availableServices.Count() + 1);

            // Creates a try-catch block to handle any potential exception manipulating the list
            try
            {
                // Attribute new ID to the instantiated ServiceList object
                newService.SetServiceID(newServiceID);

                // Add ServiceList object to the "list" of ServiceLists
                availableServices.Add(newService);

                statusReturn = RETURN_BILLING_TRACKER_SUCESS;
            }
            catch (Exception ex)
            {
                // Changes the method statusReturn to a specific code to represent a memory issue
                statusReturn = ERR_SET_NEW_SERVICE_LIST;
                Console.WriteLine("[AddService] " + ex.Message);
            }
            return statusReturn;
        }


        /// <summary>
        /// Method to find an existent Bill object within the listOfBills list.
        /// </summary>
        /// <param name="patientID"> refer to an specific patient ID number</param>
        /// <param name="appointmentID"> refer to an specific appointment ID number</param>
        /// <returns>returns the found Bill /or a "empty" Bill object</returns>
        /// <seealso cref = "Other Tracker Methods and Billing Library Classes methods" />
        public Bill FindBill(int patientID, int appointmentID)
        {
            // Local Variables
            Bill foundBill = new Bill();

            if (listOfBills.Count != 0)
            {
                foreach (Bill obj in listOfBills)
                {
                    // checks for specific bill associated to an specific patientID and appointmentID
                    if ((obj.GetPatientID().Equals(patientID)) && ((obj.GetAppointmentID().Equals(appointmentID))))
                    {
                        foundBill = obj;
                    }
                }
            }
            return foundBill;
        }




        /// <summary>
        /// This version of the FindService uses the object member "serviceID" as a parameter 
        /// for searching the equivalent Service object.
        /// </summary>
        /// <param name="serviceID">refers to the Service object member "serviceID"</param>
        /// <returns></returns>
        public Service FindService(int serviceID)
        {
            // Local variables
            Service serviceFound = new Service();

            // Look for service inside the service list and return a string
            // Check if list is empty
            try
            {
                if ((availableServices != null) && (availableServices.Count() != 0))
                {
                    foreach (Service item in availableServices)
                    {
                        if (item.GetServiceID() == serviceID)
                        {
                            serviceFound = item;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Report an error...
                Console.WriteLine("[FindService] " + ex.Message);
            }
            return serviceFound;
        }




        /// <summary>
        /// Function that find service based on service feeCode stablished by MoH
        /// </summary>
        /// <param name="serviceCode"></param>
        /// <param name="ServiceEffectiveFromThisDateForward"></param>
        /// <returns>returns Service object based on the entered fee code</returns>
        /// <seealso cref = "Other Tracker Methods and Billing Library Classes methods" />
        public Service FindOneService(string serviceCode)
        {
            //Local variables
            Service FoundService = new Service();

            foreach (Service item in availableServices)
            {
                // If there is a match in service code, assign the service obj.
                if (item.GetFeeCode() == serviceCode)
                {
                    FoundService = item;
                }
            }

            return FoundService;
        }


        

        /// <summary>
        /// Overload function that find services based on effective date. If a va
        /// </summary>
        /// <param name="ServiceEffectiveFromThisDateForward"></param>
        /// <returns>returns a list of of available services based on the effective date.</returns>
        /// <seealso cref = "Other Tracker Methods and Billing Library Classes methods" />
        public List<Service> FindService(string serviceCode)
        {
            //Local variables
            List<Service> FoundServices = new List<Service>();
            //DateTime ServiceEffectiveDate = new DateTime();

            if (serviceCode == "")
            {
                FoundServices = availableServices;
            }
            else
            {
                foreach (Service item in availableServices)
                {
                    // If there is a match in service code, assign the service obj.
                    if (item.GetFeeCode() == serviceCode)
                    {
                        FoundServices.Add(item);
                    }
                }
            }

            return FoundServices;
        }




        /// <summary>
        /// Overload function that find services based on effective date. If a va
        /// </summary>
        /// <param name="ServiceEffectiveFromThisDateForward"></param>
        /// <returns>returns a list of of available services based on the effective date.</returns>
        /// <seealso cref = "Other Tracker Methods and Billing Library Classes methods" />
        public List<Service> FindService(DateTime? ServiceEffectiveFromThisDateForward)
        {
            //Local variables
            List<Service> FoundServices = new List<Service>();
            DateTime ServiceEffectiveDate = new DateTime();


            // Check if date was entered, or the object has remained null
            if (ServiceEffectiveFromThisDateForward.HasValue)
            {
                ServiceEffectiveDate = ServiceEffectiveFromThisDateForward.Value;

                // if list is not empty, get all the services that have the same effective date
                if (listOfServices.Count != 0)
                {
                    foreach (Service item in availableServices)
                    {
                        if (ServiceEffectiveDate == item.GetEffectiveDate()) { FoundServices.Add(item); }
                    }
                }

            }
            else
            {
                // If no date is entered, return the whole list of services availabe 
                FoundServices = availableServices;
            }

            return FoundServices;
        }




        /// <summary>
        /// Finds a list(s) of services based on BillID
        /// </summary>
        /// <param name="EncounterBillID"></param>
        /// <returns></returns>
        /// <seealso cref = "Other Tracker Methods and Billing Library Classes methods" />
        public List<ServiceList> FindServiceList(int EncounterBillID)
        {
            //Local variables
            List<ServiceList> FoundServiceList = new List<ServiceList>();
            ServiceList serviceListToAdd = null;
            foreach (ServiceList set in listOfServices)
            {
                if (set.GetBillID() == EncounterBillID)
                {
                    serviceListToAdd = new ServiceList();
                    serviceListToAdd = set;

                    FoundServiceList.Add(serviceListToAdd);
                }
            }

            return FoundServiceList;
        }



        
        /// <summary>
        /// Finds and replaces the content from a ServiceList object 
        /// </summary>
        /// <param name="updatedBillServiceList"></param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int UpdateServiceList(ServiceList updatedBillServiceList)
        {
            // Local variables
            int statusReturn = 0;
            try
            {
                if (updatedBillServiceList != null)
                {
                    foreach (ServiceList serviceListRecord in listOfServices)
                    {
                        // Find specific servicelist based on BillID common to both objects
                        if ((serviceListRecord.GetBillID() == updatedBillServiceList.GetBillID()) && (serviceListRecord.GetServiceID() == updatedBillServiceList.GetServiceID()))
                        {
                            // Set the new values to the serviceList object
                            serviceListRecord.SetServiceID(updatedBillServiceList.GetServiceID());
                            serviceListRecord.SetQuantity(updatedBillServiceList.GetQuantity());
                            serviceListRecord.SetStatus(updatedBillServiceList.GetStatus());

                            // Return Success status
                            statusReturn = ServiceList.RETURN_SERVICE_LIST_SUCCESS;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Return Error code if object is not found within the list
                statusReturn = ERR_FAILED_UPDATE_SERVICE_LIST;
                Console.WriteLine("[UpdateServiceList] " + e.Message);
            }
            return statusReturn;
        }




        /// <summary>
        /// Finds and replaces the content from a Bill object 
        /// </summary>
        /// <param name="updatedBill"></param>
        /// <returns>return status/Error code</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int UpdateBill(Bill updatedBill)
        {
            // Local variables
            int statusReturn = 0;
            try
            {
                if (updatedBill != null)
                {
                    foreach (Bill billRecord in listOfBills)
                    {
                        // Find specific servicelist based on BillID common to both objects
                        if (billRecord.GetBillID() == billRecord.GetBillID())
                        {
                            // Set the new values to the Bill object

                            billRecord.SetAppointmentID(updatedBill.GetAppointmentID());
                            billRecord.SetPatientID(updatedBill.GetPatientID());
                            billRecord.SetFlagRecall(updatedBill.GetFlagRecall());
                            billRecord.SetStatusDone(updatedBill.GetStatusDone());

                            // Return Success status
                            statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Return Error code if object is not found within the list
                statusReturn = RETURN_BILLING_TRACKER_FAILURE;
                Console.WriteLine("[UpdateBill] " + e.Message);
            }
            return statusReturn;
        }





        /// <summary>
        ///  Generates a summary of the billing of the month based on the date
        /// </summary>
        /// <param name="date">date reference from the month in interest</param>
        /// <returns>returns string containing the content of the monthly</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public int GenerateMonthlyBillingSummary(DateTime date, List<string> dataFromMoH)
        {
            //Local variables
            int statusReturn = 0;
            int totalEncountersBilled = 0;
            double totalBilledProcedures = 0.0;
            double receivedTotal = 0.0;
            double toBePaid = 0.0;
            double valueConverter = 0.0;
            double receivedPercentage = 0.0;
            double averageBilling = 0.0;
            int numEncountersToFollowUp = 0;

            brokenDownMoHSample sampleMoHItem = new brokenDownMoHSample();
            List<brokenDownMoHSample> parsedMoH = new List<brokenDownMoHSample>();

            // Iterate through data received from MoH represented in individual strings
            foreach (string item in dataFromMoH)
            {
                // parse data into subtitems for easier manipulation
                sampleMoHItem.distinctEncounter = item.Substring(0, 20);
                sampleMoHItem.dateOfService = item.Substring(0, 8);
                sampleMoHItem.HCN = item.Substring(9, 11);
                sampleMoHItem.Gender = item.Substring(20, 1);
                sampleMoHItem.billingCode = item.Substring(21, 4);
                sampleMoHItem.valueOfSeviceCode = item.Substring(25, 11);
                sampleMoHItem.statusMoH = item.Substring(36, 4);

                // Add parsed item into list
                parsedMoH.Add(sampleMoHItem);
            }

            // check total encounters billed
            // Make sure to capture same appointments among different billed services
            // For example if the patient was charged for more than one service in one unique appointment...           
            foreach (brokenDownMoHSample set in parsedMoH)
            {
                foreach (brokenDownMoHSample subSet in parsedMoH)
                {
                    // First time using this kind of expression..., still need more thorough testing
                    totalEncountersBilled = parsedMoH.Count(x => x.distinctEncounter == subSet.distinctEncounter);
                }
            }

            // Get Number of Billed procedures
            totalBilledProcedures = parsedMoH.Count();

            // Get Received total value in Dollars
            foreach (brokenDownMoHSample set in parsedMoH)
            {
                if (set.statusMoH == "PAID")
                {
                    double.TryParse(set.valueOfSeviceCode, out valueConverter);
                    receivedTotal += (valueConverter / 10000);
                }
            }

            // Get Received total value in Dollars
            foreach (brokenDownMoHSample set in parsedMoH)
            {
                //if ((set.statusMoH != "PAID") || (set.statusMoH != "DECL"))                       // Considering Declined wont be paid at all
                if (set.statusMoH != "PAID")                                                        // Considering Declined can be eventually paid                 
                {
                    double.TryParse(set.valueOfSeviceCode, out valueConverter);

                    // Also get the number of appointments that will need follow-up
                    numEncountersToFollowUp++;
                }
                //Sum of Dollar converted values
                toBePaid += (valueConverter / 10000);
            }

            // Percentage of total received considering that "DECL" won't be paid at all.
            receivedPercentage = ((receivedTotal / toBePaid) * 100);

            // Average Billing calculation
            averageBilling = (receivedTotal / totalEncountersBilled);

            // Aux to currency formatting
            string sym = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;

            // Display Summary into console
            Console.WriteLine("\n Monthly Billing Summary................. [  {0}   ]\n", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).ToUpper());
            Console.WriteLine(" Total Encounters Billed:................ [ # {0} ] ", totalEncountersBilled.ToString("#,##0.00").PadLeft(9, ' '));
            Console.WriteLine(" Total Billed Procedures:................ [ {0}{1,10:#,##0.00} ] ", sym, totalBilledProcedures.ToString("F"));
            Console.WriteLine(" Received Total:......................... [ {0}{1,10:#,##0.00} ] ", sym, receivedTotal.ToString("F"));
            Console.WriteLine(" Received Percentage:.................... [ % {0} ] ", receivedPercentage.ToString("#,##0.00").PadLeft(9, ' '));
            Console.WriteLine(" Average Billing:........................ [ {0}{1,10:#,##0.00} ] ", sym, averageBilling.ToString("F"));
            Console.WriteLine(" Num.Encounters to Follow-up............. [ # {0} ] ", numEncountersToFollowUp.ToString("#,##0.00").PadLeft(9, ' '));

            return statusReturn;
        }





        /// <summary>
        ///  Generates a summary of the billing of the month based on the date
        /// </summary>
        /// <param name="date">date reference from the month in interest</param>
        /// <returns>returns string containing the content of the monthly</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public string GenerateMonthlyBillingSummary(DateTime date)
        {

            //Local variables
            string condensedSummaryBilling = "";
            string bSummaryMonth = "";
            string bSummaryTEncountersBilled = "";
            string bSummaryTBilledProcedures = "";
            string bSummaryReceivedTotal = "";
            string bSummaryReceivedPercentage = "";
            string bSummaryAverageBilling = "";
            string bSummaryFollowUpNumbers = "";
            int totalEncountersBilled = 0;
            double totalBilledProcedures = 0.0;
            double receivedTotal = 0.0;
            double toBePaid = 0.0;
            double valueConverter = 0.0;
            double receivedPercentage = 0.0;
            double averageBilling = 0.0;
            int numEncountersToFollowUp = 0;


            brokenDownMoHSample sampleMoHItem = new brokenDownMoHSample();
            List<brokenDownMoHSample> parsedMoH = new List<brokenDownMoHSample>();

            // Iterate through data received from MoH represented in individual strings
            foreach (string item in listOfReconciledServicesFromMoH)
            {
                // parse data into subtitems for easier manipulation
                sampleMoHItem.distinctEncounter = item.Substring(0, 20);
                sampleMoHItem.dateOfService = item.Substring(0, 8);
                sampleMoHItem.HCN = item.Substring(9, 11);
                sampleMoHItem.Gender = item.Substring(20, 1);
                sampleMoHItem.billingCode = item.Substring(21, 4);
                sampleMoHItem.valueOfSeviceCode = item.Substring(25, 11);
                sampleMoHItem.statusMoH = item.Substring(36, 4);

                // Add parsed item into list
                parsedMoH.Add(sampleMoHItem);
            }

            // check total encounters billed
            // Make sure to capture same appointments among different billed services
            // For example if the patient was charged for more than one service in one unique appointment...           
            foreach (brokenDownMoHSample set in parsedMoH)
            {
                foreach (brokenDownMoHSample subSet in parsedMoH)
                {
                    // First time using this kind of expression..., still need more thorough testing
                    totalEncountersBilled = parsedMoH.Count(x => x.distinctEncounter == subSet.distinctEncounter);
                }
            }

            // Get Number of Billed procedures
            totalBilledProcedures = parsedMoH.Count();

            // Get Received total value in Dollars
            foreach (brokenDownMoHSample set in parsedMoH)
            {
                if (set.statusMoH == "PAID")
                {
                    double.TryParse(set.valueOfSeviceCode, out valueConverter);
                    receivedTotal += (valueConverter / 10000);
                }
            }

            // Get Received total value in Dollars
            foreach (brokenDownMoHSample set in parsedMoH)
            {
                //if ((set.statusMoH != "PAID") || (set.statusMoH != "DECL"))                       // Considering Declined wont be paid at all
                if (set.statusMoH != "PAID")                                                        // Considering Declined can be eventually paid                 
                {
                    double.TryParse(set.valueOfSeviceCode, out valueConverter);

                    // Also get the number of appointments that will need follow-up
                    numEncountersToFollowUp++;
                }
                //Sum of Dollar converted values
                toBePaid += (valueConverter / 10000);
            }

            // Percentage of total received considering that "DECL" won't be paid at all.
            receivedPercentage = ((receivedTotal / toBePaid) * 100);

            // Average Billing calculation
            averageBilling = (receivedTotal / totalEncountersBilled);

            // Aux to currency formatting
            string sym = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;


            // Display Summary into console

            //Console.WriteLine("\n Monthly Billing Summary................. [  {0}   ]\n", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).ToUpper());
            bSummaryMonth = String.Format("\n Monthly Billing Summary................. [  {0}   ]\n\n", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).ToUpper());

            //Console.WriteLine(" Total Encounters Billed:................ [ # {0} ] ", totalEncountersBilled.ToString("#,##0.00").PadLeft(9, ' '));
            bSummaryTEncountersBilled = String.Format(" Total Encounters Billed:................ [ # {0} ] \n", totalEncountersBilled.ToString("#,##0.00").PadLeft(9, ' '));

            //Console.WriteLine(" Total Billed Procedures:................ [ {0}{1,10:#,##0.00} ] ", sym, totalBilledProcedures.ToString("F"));
            bSummaryTBilledProcedures = String.Format(" Total Billed Procedures:................ [ {0}{1,10:#,##0.00} ] \n", sym, totalBilledProcedures.ToString("F"));

            //Console.WriteLine(" Received Total:......................... [ {0}{1,10:#,##0.00} ] ", sym, receivedTotal.ToString("F"));
            bSummaryReceivedTotal = String.Format(" Received Total:......................... [ {0}{1,10:#,##0.00} ] \n", sym, receivedTotal.ToString("F"));

            //Console.WriteLine(" Received Percentage:.................... [ % {0} ] ", receivedPercentage.ToString("#,##0.00").PadLeft(9, ' '));
            bSummaryReceivedPercentage = String.Format(" Received Percentage:.................... [ % {0} ] \n", receivedPercentage.ToString("#,##0.00").PadLeft(9, ' '));

            //Console.WriteLine(" Average Billing:........................ [ {0}{1,10:#,##0.00} ] ", sym, averageBilling.ToString("F"));
            bSummaryAverageBilling = String.Format(" Average Billing:........................ [ {0}{1,10:#,##0.00} ] \n", sym, averageBilling.ToString("F"));

            //Console.WriteLine(" Num.Encounters to Follow-up............. [ # {0} ] ", numEncountersToFollowUp.ToString("#,##0.00").PadLeft(9, ' '));
            bSummaryFollowUpNumbers = String.Format(" Num.Encounters to Follow-up............. [ # {0} ] \n", numEncountersToFollowUp.ToString("#,##0.00").PadLeft(9, ' '));


            condensedSummaryBilling = bSummaryMonth +
                                        bSummaryTEncountersBilled +
                                        bSummaryTBilledProcedures +
                                        bSummaryReceivedTotal +
                                        bSummaryReceivedPercentage +
                                        bSummaryAverageBilling +
                                        bSummaryFollowUpNumbers;

            // Erase the processed contents after Summary is displayed.
            listOfReconciledServicesFromMoH.Clear();

            return condensedSummaryBilling;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="FeeCodeToSearch"></param>
        /// <returns></returns>
        public int CheckForServiceCodeDuplicate(string FeeCodeToSearch)
        {
            // Local variables
            int statusReturn = 0;

            // Check if "availableServices" list is empty
            if (availableServices.Count == 0)
            {
                statusReturn = AVAILABLE_SERVICES_EMPTY;
            }
            else
            {
                // Search for FeeCode inside the "avaialable" Service list
                foreach (Service serviceRecord in availableServices)
                {
                    if (serviceRecord.GetFeeCode() == FeeCodeToSearch)
                    {
                        statusReturn = AVAILABLE_SERVICE_EXISTENT;
                        break;
                    }
                    else
                    {
                        statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                    }
                }
            }
            return statusReturn;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="OfficialBillInfoFromFileIO"></param>
        /// <returns></returns>
        public int ReceiveBillingRecordsFromFileIO()
        {
            // Local variables
            int statusReturn = 0;
            //DateTime tempDateConversion = new DateTime();
            //nt tempListCount = 0;
            //double tempValue = 0.0;
            //int multiplier = 10000;
            int lastBIllID = 0;

            // Variable to get result out of FileIO methods
            List<string[]> billInfoFromFileIO = ReadBillingRecordsFromFile();

            // Received Billing Info format (BROKEN DOWN)
            // ECOUNTER_DATE | HCN           | Gender | Fee Code | Service Value 
            // 20171106      | 5534567890EE  | M      | A005     | 0000077200000 

            try
            {

                // ITERATE THROUGH THE LIST OF BILLING RECORDS
                foreach (string[] billingRecord in billInfoFromFileIO)
                {
                    // CHECK IF PATIENT RELATED TO THE BILL EXISTS

                    int checkedPatientID = CheckForPatientID(billingRecord[1]);

                    if (checkedPatientID > 0) // if positive, Patient existence was validated
                    {
                        // CHECK FOR PATIENT ID AND APPOINTMENT DATE TO VERIFY IF APPOINTMENT IS EXISTENT

                        // transform string date into DateTime

                        int checkedAppointmentID = CheckForAppointmentID(billingRecord[0], checkedPatientID);

                        if (checkedAppointmentID > 0) // if positive, Appointment existence was validated
                        {
                            // CHECK FOR PATIENT ID AND APPOINTMENT ID TO FIND BILL ID
                            int checkedBillID = CheckForBillID(checkedPatientID, checkedAppointmentID);

                            if (checkedBillID > 0) // if positive, Bill ID existence was validated
                            {
                                // AT THIS POINT THE RECORD INDICATES WE HAVE AN EQUIVALENT BILL OBJ 
                                // LETS CHECK IF THE SERVICE IN THE RECORD IS ALREADY IN THE SERVICE LIST, SO WE DONT DUPLICATE A RECORD

                                int checkedServiceID = CheckForServiceID(billingRecord[3]);

                                if (checkedServiceID > 0) // if positive, the service was validated within the available service list
                                {
                                    // CHECK IF THE FOUND SERVICE ID RELATES TO THE FOUND BILLID
                                    if (CheckForServiceInBill(checkedBillID, checkedServiceID) > 0) // if positive, the record processed is already in the system
                                    {
                                        // IN THIS CASE, THE SERVICE RELATED TO THE BILLING RECORD FROM FILE IO IS 
                                        // CONSIDERED AN ADDITIONAL PROVIDED SERVICE AND THE QUANTITY ON THE SERVICELIST IS
                                        // INCREMENTED BY ONE

                                        IncrementServiceListQuantity(checkedBillID, checkedServiceID);
                                    }

                                }
                            }
                            else // APPOINTMENT AND PATIENT WERE VALIDATED, AND A BILL, AND A SERVICELIST OBJ WILL BE CREATED
                            {
                                // NEW BILL OBJ 
                                Bill billToAdd = new Bill();

                                // Set Bill PatientID
                                billToAdd.SetPatientID(checkedPatientID);

                                // Set Bill AppointmentID 
                                billToAdd.SetAppointmentID(checkedAppointmentID);

                                // Set Bill FlagRecall 
                                billToAdd.SetFlagRecall(0); // ASSUMED AS BEING 0 SINCE THERE IS NO INFO ON BILLING RECORD

                                // Set Bill Process Status
                                billToAdd.SetStatusDone(false); // ASSUMED TO BE NOT PROCESSED SINCE THERE IS NO INFO YET FROM MOH

                                // ADD BILL TO THE LIST OF BILLS
                                //#ADD_LOG 
                                AddBill(billToAdd);

                                // Get current BillID from the list
                                lastBIllID = (listOfBills.Count());



                                // NEW SERVICELIST OBJ CREATED
                                ServiceList serviceListToBeAdded = new ServiceList();

                                // Set ServiceList Bill ID
                                serviceListToBeAdded.SetBillID(lastBIllID);

                                // Set ServiceList ServiceID
                                int checkedServiceID = CheckForServiceID(billingRecord[3]);
                                serviceListToBeAdded.SetServiceID(checkedServiceID);

                                // Set ServiceList Quantity to 1
                                serviceListToBeAdded.SetQuantity(1);

                                // Set ServiceList Status to Unprocessed
                                serviceListToBeAdded.SetStatus("UNPROCESSED");

                                // ADD SERVICELIST TO THE LIST OF SERVICES
                                listOfServices.Add(serviceListToBeAdded);

                            }
                        }
                        else // APPOINTMENT DOES NOT EXIST, SO THE RECORD IS NOT VALIDATED, MOVE TO THE NEXT FILE IO RECORD
                        {
                            //#ADD_LOG
                            continue;
                        }

                    }
                    else // PATIENT DOES NOT EXIST, SO THE RECORD IS NOT VALIDATED, MOVE TO THE NEXT FILE IO RECORD
                    {
                        //#ADD_LOG
                        continue;

                        // NO PATIENT WITHIN THE PATIENT LIST MATCH THE RECORD HCN

                        // RULE: DROP THE RECORD
                    }
                }
            }
            catch (Exception e)
            {
                // ADD LOG
                statusReturn = ERR_DATE_AVAILABLE_SERVICE_LIST;
                Console.WriteLine("[ReceiveBillingRecordsFromFileIO] " + e.Message);
            }
            return statusReturn;
        }





        /// <summary>
        ///  Gets a HCN and Returns a PatientID, or a negative value if not found
        /// </summary>
        /// <param name="HCNToFind"></param>
        /// <returns></returns>
        public int CheckForPatientID(string HCNToFind)
        {
            // Local variables
            int foundPatientID = 0;

            List<Patient> listToCheckPatient = demoTracker.GetPatientList();

            foreach (Patient patientRecord in listToCheckPatient)
            {
                if (patientRecord.GethCN() == HCNToFind)
                {
                    foundPatientID = patientRecord.GetPatientID();
                    break;
                }
                else
                {
                    foundPatientID = RETURN_BILLING_TRACKER_FAILURE;
                }
            }
            return foundPatientID;
        }





        /// <summary>
        /// Gets a Appointment Date and PatientID and returns appointmentID, or a negative value of not found
        /// </summary>
        /// <param name="appointmentDate"></param>
        /// <param name="patientID"></param>
        /// <returns></returns>
        public int CheckForAppointmentID(string appointmentDate, int patientID)
        {
            // Local variables
            int foundAppointmentID = 0;
            DateTime tempDate = new DateTime();
            string tempDateString = "";
            List<Appointment> listToCheckAppointment = demoTracker.GetAppointmentList();

            // Look at the Appointment List
            foreach (Appointment appointmentRecord in listToCheckAppointment)
            {
                if (appointmentRecord.GetPatientID() == patientID)
                {
                    // ASSUMES A PERSON ONLY HAS ONE APPOINTMENT IN A DAY
                    // HAS TO CONVERT APPOINTMENT DATE TO MATCH BILLING RECORD INFO
                    tempDate = appointmentRecord.GetDayAndTime();
                    tempDateString = tempDate.ToString("yyyyMMdd");

                    // If patient ID is found, then look to see if the FileIO encounter date has matches
                    if (tempDateString == appointmentDate)
                    {
                        // Appointment was validated and the AppointmentID is returned

                        foundAppointmentID = appointmentRecord.GetApointmentID();
                        break;
                    }
                    else
                    {
                        // If appointment is not validated, then return a negative value
                        foundAppointmentID = RETURN_BILLING_TRACKER_FAILURE;
                    }
                }
            }
            return foundAppointmentID;
        }





        /// <summary>
        /// Gets PatientID and AppointmentID and returns BillID
        /// </summary>
        /// <param name="patientID"></param>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        public int CheckForBillID(int patientID, int appointmentID)
        {
            // Local variables
            int foundBillID = 0;

            // Look into current list of <Bills>
            foreach (Bill billRecord in listOfBills)
            {
                if (billRecord.GetPatientID() == patientID)
                {
                    // If patient ID is found, then look to see if the FileIO encounter date has matches
                    if (billRecord.GetAppointmentID() == appointmentID)
                    {
                        // Appointment was validated and the AppointmentID is returned
                        foundBillID = billRecord.GetBillID();
                        break;
                    }
                    else
                    {
                        // If appointment is not validated, then return a negative value
                        foundBillID = RETURN_BILLING_TRACKER_FAILURE;
                    }
                }
            }
            return foundBillID;
        }






        /// <summary>
        /// Gets Service FeeCode and returns the serviceID
        /// </summary>
        /// <param name="serviceFeeCode"></param>
        /// <returns></returns>
        public int CheckForServiceID(string serviceFeeCode)
        {
            // Local variables
            int foundServiceID = 0;

            // Look into current list of <Bills>
            foreach (Service availableServiceRecord in availableServices)
            {
                if (availableServiceRecord.GetFeeCode() == serviceFeeCode)
                {
                    // Appointment was validated and the AppointmentID is returned
                    foundServiceID = availableServiceRecord.GetServiceID();
                    break;
                }
                else
                {
                    // If appointment is not validated, then return a negative value
                    foundServiceID = RETURN_BILLING_TRACKER_FAILURE;
                }
            }
            return foundServiceID;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="serviceID"></param>
        /// <returns></returns>
        public int CheckForServiceInBill(int billID, int serviceID)
        {
            // Local variables
            int foundServiceID = 0;

            // Look into current list of <Bills>
            foreach (ServiceList serviceListRecord in listOfServices)
            {
                if (serviceListRecord.GetBillID() == billID)
                {
                    // If patient ID is found, then look to see if the FileIO encounter date has matches
                    if (serviceListRecord.GetServiceID() == serviceID)
                    {
                        // Appointment was validated and the AppointmentID is returned
                        foundServiceID = serviceListRecord.GetServiceID();
                        break;
                    }
                    else
                    {
                        // If appointment is not validated, then return a negative value
                        foundServiceID = RETURN_BILLING_TRACKER_FAILURE;
                    }
                }
            }
            return foundServiceID;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="serviceID"></param>
        /// <returns></returns>
        public int IncrementServiceListQuantity(int billID, int serviceID)
        {
            // Local variables
            int statusReturn = 0;
            int currentQuantity = 0;

            foreach (ServiceList serviceListRecord in listOfServices)
            {
                if ((serviceListRecord.GetBillID() == billID) && (serviceListRecord.GetServiceID() == serviceID))
                {
                    // Incremement ServiceList Quantity
                    currentQuantity = serviceListRecord.GetQuantity();
                    serviceListRecord.SetQuantity(currentQuantity + 1);

                    // SERVICE LIST QUANTITY IS INCREMENTED
                    //#ADD_LOG
                    statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                    break;
                }
                else
                {
                    statusReturn = RETURN_BILLING_TRACKER_FAILURE;
                }

            }
            return statusReturn;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string[]> ReadBillingRecordsFromFile()
        {
            // Local variables
            List<string[]> listOfBillingRecords = new List<string[]>();
            string billRecords = "";
            string[] getFileLines = null;
            string txtBillRecord = "";


            // Received Billing reconciled Info format

            // ECOUNTER_DATE (8) | HCN (12)      | Gender (1) | Fee Code (4) | Service Value (13) 
            // 20171106          | 5534567890EE  | M          | A005         | 0000077200000 

            // Substring data position
            int[] subStringLimitsAppointmentDate = { 0, 8 };
            int[] subStringLimitsHCN = { 8, 12 };
            int[] subStringLimitsGender = { 20, 1 };
            int[] subStringLimitsFeeCode = { 21, 4 };
            int[] subStringLimitsServiceValue = { 25, 11 };

            //Read file and transfer ALL the data into only one string
            if (fileIO.ReadFullBillFile(ref billRecords) == true) // Read the Bill file
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingRecordsFile - read billing file - successful");

                getFileLines = billRecords.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillFile - Parse a line from file to array of strings - [successful]");
            }

            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingRecordsFromFile - read billing file - successful");
            try
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingRecordsFromFile - Split lines from billing file - successful");
                for (Int32 i = 0; i < getFileLines.Length; i++)
                {
                    string[] getRecordInfo = new string[5];

                    //myPatientObj = null;
                    txtBillRecord = getFileLines[i];
                    //Check if total number of characters matches the record supported format
                    if ((txtBillRecord.Length == BILL_RECORD_LENGTH) && (txtBillRecord != "")) // Billing Record Date | HCN | Gender | Service Code | Service Value
                    {
                        // Get billing record Date
                        getRecordInfo[0] = txtBillRecord.Substring(subStringLimitsAppointmentDate[0], subStringLimitsAppointmentDate[1]);

                        // Get billing record HCN
                        getRecordInfo[1] = txtBillRecord.Substring(subStringLimitsHCN[0], subStringLimitsHCN[1]);

                        // Get billing record Gender
                        getRecordInfo[2] = txtBillRecord.Substring(subStringLimitsGender[0], subStringLimitsGender[1]);

                        // Get billing record Service Code
                        getRecordInfo[3] = txtBillRecord.Substring(subStringLimitsFeeCode[0], subStringLimitsFeeCode[1]);

                        // Get billing record Service Value
                        getRecordInfo[4] = txtBillRecord.Substring(subStringLimitsServiceValue[0], subStringLimitsServiceValue[1]);

                        // Add completely parsed record into the list of billing records
                        listOfBillingRecords.Add(getRecordInfo);
                    }
                    else
                    {
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Billing Record line - Adding to the list of Billing Records, " + i + "/" + (getFileLines.Length - 1) + " - skipped because it doesn't match the criteria");
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create a Billing Record - [failure]");
            }

            return listOfBillingRecords;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string[]> ReadServiceMasterSample()
        {
            // Local variables
            List<string[]> listOfRegisteredServices = new List<string[]>();

            string masterServiceRecords = "";
            string[] getFileLines = null;
            //string[] getRecordInfo = new string[4];
            string txtServiceRecord = "";


            // Service Master List
            // SERVICE CODE (4) |EFFECTIVE DATE |SERVICE VALUE
            // A003             |20091201       |00000772000

            // Substring data position
            int[] subStringLimitsServiceFeeCode = { 0, 4 };
            int[] subStringLimitsEffectiveDate = { 4, 8 };
            int[] subStringLimitsServiceValue = { 12, 11 };


            try
            {
                //Read file and transfer ALL the data into only one string
                if (fileIO.ReadFullServicesMasterFile(ref masterServiceRecords) == true) // Read the Bill file
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceMasterSample - read registered file - successful");

                    getFileLines = masterServiceRecords.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceMasterSample - Parse a line from file to array of strings - [successful]");
                }

                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceMasterSample - Split lines from billing file - successful");
                for (Int32 i = 0; i < getFileLines.Length; i++)
                {
                    //myPatientObj = null;
                    txtServiceRecord = getFileLines[i];
                    if ((txtServiceRecord.Length == SERVICE_RECORD_LENGTH) && (txtServiceRecord != "")) // Master Sample:  A003|20091201|00000772000
                    {
                        //Sets a new string
                        string[] getRecordInfo = new string[4];

                        // Get Service Code
                        getRecordInfo[0] = txtServiceRecord.Substring(subStringLimitsServiceFeeCode[0], subStringLimitsServiceFeeCode[1]);

                        // Get Service Effective Date
                        getRecordInfo[1] = txtServiceRecord.Substring(subStringLimitsEffectiveDate[0], subStringLimitsEffectiveDate[1]);

                        // Get Service Value
                        getRecordInfo[2] = txtServiceRecord.Substring(subStringLimitsServiceValue[0], subStringLimitsServiceValue[1]);

                        // Add completely parsed record into the list of billing records
                        listOfRegisteredServices.Add(getRecordInfo);
                    }

                    else
                    {
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service Master Sample line - Adding to the list available services, " + i + "/" + (getFileLines.Length - 1) + " - skipped because it doesn't match the criteria");
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create an Service Code - [failure]");
            }
            return listOfRegisteredServices;
        }




        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string[]> ReadBillingResponseFromFile()
        {
            // Local variables
            List<string[]> listOfBillingResponseRecords = new List<string[]>();
            string billResponseRecords = "";
            string[] getFileLines = null;
            string txtBillResponseRecord = "";


            // ECOUNTER_DATE (8) | HCN (12)      | Gender (1) | Fee Code (4) | Service Value (13)  | Status
            // 20171106          | 5534567890EE  | M          | A005         | 0000077200000       | PAID

            // Substring data position
            int[] subStringLimitsAppointmentDate = { 0, 8 };
            int[] subStringLimitsHCN = { 8, 12 };
            int[] subStringLimitsGender = { 20, 1 };
            int[] subStringLimitsFeeCode = { 21, 4 };
            int[] subStringLimitsServiceValue = { 25, 11 };
            int[] subStringLimitsStatusCode = { 36, 4 };


            //Read file and transfer ALL the data into only one string
            if (fileIO.ReadFullResponseFile(ref billResponseRecords) == true) // Read the Bill file
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingResponseFromFile - read billing response file - successful");

                getFileLines = billResponseRecords.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingResponseFromFile - Parse a line from file to array of strings - [successful]");
            }

            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingResponseFromFile - read billing response file - successful");
            try
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingResponseFromFile - Split lines from billing response file - successful");
                for (Int32 i = 0; i < getFileLines.Length; i++)
                {

                    string[] getRecordInfo = new string[6];

                    //myPatientObj = null;
                    txtBillResponseRecord = getFileLines[i];
                    //fieldsSplitted = txtBillResponseRecord.Split(FieldSeparator, StringSplitOptions.None);
                    if ((txtBillResponseRecord.Length == BILL_RECORD_RESPONSE_LENGTH) && (txtBillResponseRecord != "")) // Billing Record Date | HCN | Gender | Service Code | Service Value | Status
                    {
                        // Get billing record Date
                        getRecordInfo[0] = txtBillResponseRecord.Substring(subStringLimitsAppointmentDate[0], subStringLimitsAppointmentDate[1]);

                        // Get billing record HCN
                        getRecordInfo[1] = txtBillResponseRecord.Substring(subStringLimitsHCN[0], subStringLimitsHCN[1]);

                        // Get billing record Gender
                        getRecordInfo[2] = txtBillResponseRecord.Substring(subStringLimitsGender[0], subStringLimitsGender[1]);

                        // Get billing record Service Code
                        getRecordInfo[3] = txtBillResponseRecord.Substring(subStringLimitsFeeCode[0], subStringLimitsFeeCode[1]);

                        // Get billing record Service Value
                        getRecordInfo[4] = txtBillResponseRecord.Substring(subStringLimitsServiceValue[0], subStringLimitsServiceValue[1]);

                        // Get billing record MOH response
                        getRecordInfo[5] = txtBillResponseRecord.Substring(subStringLimitsStatusCode[0], subStringLimitsStatusCode[1]);

                        // Add completely parsed record into the list of billing records
                        listOfBillingResponseRecords.Add(getRecordInfo);
                    }
                    else
                    {
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Billing Response Record line - Adding to the list of Billing Response Records, " + i + "/" + (getFileLines.Length - 1) + " - skipped because it doesn't match the criteria");
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create a Billing Response Record - [failure]");
            }

            return listOfBillingResponseRecords;
        }



        /// <summary>
        /// Receive a command "{ Bill, Service, ServiceList }" as the first argument, and will return by ref on the second element an array of string with the content of the file passed on the first argument.   
        /// </summary>
        /// <param name="fileName"> enum that must be { Bill, Service, ServiceList }.</param>
        /// <param name="getFileLines"> A ref to array of strings must be passed here.</param>
        /// /// <see cref="public static string PackAddress(Address obj)"> This method was used as a reference.</see>/>
        /// <returns>int, "0" - If was is done ok.</returns>        
        private int ReadBillFileToStringArray(BillingClasses fileName, ref string[] getFileLines)
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string strFromFileDatabase = "";

            try
            {
                switch (fileName)
                {
                    case BillingClasses.Bill:
                        {
                            if (fileIO.ReadFullBillFile(ref strFromFileDatabase) == true) // Read the Bill File
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFullBillFile - read address file - [successful]");

                                getFileLines = strFromFileDatabase.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFullBillFile - Parse a line from file to array of strings - [successful]");
                            }
                            break;
                        }
                    case BillingClasses.Service:
                        {
                            if (fileIO.ReadFullServiceFile(ref strFromFileDatabase) == true) // Read the Service File
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFullServiceFile - read appointment file - [successful]");

                                getFileLines = strFromFileDatabase.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFullServiceFile - Parse a line from file to array of strings - [successful]");
                            }
                            break;
                        }
                    case BillingClasses.ServiceList:
                        {
                            if (fileIO.ReadFullServicelineFile(ref strFromFileDatabase) == true) // Read the ServiceList File
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFullServicelineFile - read servicelist file - [successful]");

                                getFileLines = strFromFileDatabase.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadFullServicelineFile - Parse a line from file to array of strings - [successful]");
                            }
                            break;
                        }
                    default:
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillFileToStringArray - Invalid input parameter");
                            setMadeSuccess = ErrorReadFileToStringArray;
                            break;
                        }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillFileToStringArray - An Exception was reached when tried to parse a line from file to array of strings - [failure]");
                setMadeSuccess = ErrorReadFileToStringArray;
            }
            return setMadeSuccess;
        }

        // File IO **************************************************

        /// <summary> 
        /// This method will start the reading process for all Billing file. 
        /// </summary> 
        /// <returns>bool - true if it was done well.</returns>
        public bool ReadBillingFiles()
        {
            bool setMadeSuccess = true;

            if (ReadBillFile()                            != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }
            if (ReadServiceListFile()                     != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }
            if (ReadServiceFile()                         != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }
            if (ReceiveServicesMasterSampleFromFileIO()   != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; } 	// Read/Load Master Service File info
            if (ReceiveBillingRecordsFromFileIO()         != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }	// Read/Load Billing Records from FileIO
            //if (ReceiveBillingResponseRecordsFromFileIO() != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }    // Read/Load from File IO and update info on the trackers 

            return setMadeSuccess;
        }


        /// <summary> 
        /// This method will start the wrinting process for all Billing file. 
        /// </summary> 
        /// <returns>bool - true if it was done well.</returns>
        public bool WriteBillingFiles()
        {
            bool setMadeSuccess = true;

            if (WriteAllBillObjsIntoFile()          != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }
            if (WriteAllServiceListObjsIntoFile()   != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }
            if (WriteAllServiceObjsIntoFile()       != RETURN_GENERIC_SUCCESS) { setMadeSuccess = false; }

            return setMadeSuccess;
        }


        // BILL OBJECT **************************************************************************************************************

        /// <summary>
        /// [ CHECK IF IT EXISTS ] Check if exists an billID item inside a list. 
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfBillAlreadyExists(string stringBillID)
        {
            bool setMadeSuccess = false;
            int billID = 0;

            try
            {
                int.TryParse(stringBillID, out billID);

                for (int i = 0; i < listOfBills.Count; i++)
                {
                    if (listOfBills[i].GetBillID() == billID)
                    {
                        setMadeSuccess = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                setMadeSuccess = false;
                Console.WriteLine("[CheckIfBillAlreadyExists] " + ex.Message);
            }

            return setMadeSuccess;
        }


        public int CheckIfObjBillAlreadyExists(Bill toBeChecked)
        {
            // Local variable
            int statusReturn = 0;

            try
            {



                if (listOfBills.Count() == 0)
                {
                    statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                }
                else
                {
                    // Checks if PatientID and AppointmentID are the same... in this case, the Bill shouldn't be addded to the listof Bills
                    foreach (Bill billRecord in listOfBills)
                    {
                        if ((billRecord.GetAppointmentID() == toBeChecked.GetAppointmentID()) && (billRecord.GetPatientID() == toBeChecked.GetPatientID()))
                        {
                            // We found a duplicate, so we should skip the addition of this Bill obj to the list
                            statusReturn = RETURN_BILLING_TRACKER_FAILURE;
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "[CheckIfObjBillAlreadyExists] - Bill Object already exists in the internal memory");
                            break;
                        }
                        else
                        {
                            // No duplicate found
                            statusReturn = RETURN_BILLING_TRACKER_SUCESS;
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "[CheckIfObjBillAlreadyExists] - Bill Object already does not exist in the internal memory");
                        }
                    }
                }
            }
            catch (Exception)
            {
                // Some issue during process. Should be Logged
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "[CheckIfObjBillAlreadyExists] - Problem checking Bill duplicate");
            }

            return statusReturn;
        }




        /// <summary>
        /// [ WRITE ONE BILL ] reate a line into the db Patient file. 
        /// </summary>
        /// <param name="obj">The object Patient</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private static Int32 WriteOneBillObjIntoFile(Bill obj)
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string txtBill = "", txtBillID = "";

            try
            {
                // Fill the info
                txtBill += obj.GetBillID().ToString() + DivBillingTrackerFiles;
                txtBillID = txtBill;
                txtBill += obj.GetPatientID().ToString() + DivBillingTrackerFiles;
                txtBill += obj.GetAppointmentID().ToString() + DivBillingTrackerFiles;
                txtBill += obj.GetFlagRecall().ToString() + DivBillingTrackerFiles;
                txtBill += obj.GetStatusDone().ToString() + "\n";

                if (fileIO.WriteFullBillFileAppend(txtBill) == true)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the BillId-Obj |" + txtBillID);
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a Bill object into a file - [failure]");
                setMadeSuccess = WriteBillObjIntoFileFail;
            }
            return setMadeSuccess;
        }




        /// <summary>
        /// [ WRITE ALL BILLS ] Write all Patient Objs into the db Patient file. 
        /// </summary>
        private Int32 WriteAllBillObjsIntoFile()
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string txtBill = "", txtBillID = "";
            bool firstInteraction = true;

            try
            {
                foreach (var obj in listOfBills) // Loop through List with Patient
                {
                    // Fill the info
                    txtBill = "";
                    txtBill += obj.GetBillID().ToString() + DivBillingTrackerFiles;
                    txtBillID = txtBill;
                   
                    txtBill += obj.GetPatientID().ToString() + DivBillingTrackerFiles;
                    txtBill += obj.GetAppointmentID().ToString() + DivBillingTrackerFiles;
                    txtBill += obj.GetFlagRecall().ToString() + DivBillingTrackerFiles;
                    txtBill += obj.GetStatusDone().ToString() + "\n";

                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullBillFileOverwrite(txtBill) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (Overwrite) the BillID-Obj |" + txtBillID);
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullBillFileAppend(txtBill) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the BillID-Obj |" + txtBillID);
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a Patient BillID into a file - [failure]");
                setMadeSuccess = WriteBillObjIntoFileFail;
            }
            return setMadeSuccess;
        }



      
        /// <summary>
        /// Creates a Patient object by parsing a string
        /// </summary>
        /// <param name="message">Patients information</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        private Int32 ReadBillFile()
        {
            Int32 setMadeSuccess = 0; //Return variable, "0" means successful. 
            Bill myBillObj = null;
            string txtBill = "";
            string[] fileLines = null;
            string[] fieldsSplitted = null;

            // Read the Appointment array of strings
            if (ReadBillFileToStringArray(BillingClasses.Bill, ref fileLines) == RETURN_GENERIC_SUCCESS 
                && fileLines != null)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadPatientFile - read patient file - successful");
                try
                {
                    for (Int32 i = 0; i < fileLines.Length; i++)
                    {
                        myBillObj = null;
                        txtBill = fileLines[i];
                        fieldsSplitted = txtBill.Split(FieldSeparator, StringSplitOptions.None);
                        if ((fieldsSplitted.Length == 5) && (fileLines[i] != "")) // ObjTag + PatientTag + 9 fields + ""
                        {
                            // new Bill()
                            myBillObj = new Bill();

                            // Get BillID
                            int x = Demographics.Tracker.GetInteger(fieldsSplitted[0]);
                            if (x != RETURN_GENERIC_SUCCESS) { myBillObj.SetBillID(x); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Bill, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the ID criteria");
                                continue;
                            }

                            // BillID check - if Bill already exists skip this registre
                            if (CheckIfBillAlreadyExists(fieldsSplitted[0]))
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Bill, " + i + "/" + (fileLines.Length - 1) + " - skipped because it billID already exists");
                                continue;
                            }

                            // PatientID
                            int y = Demographics.Tracker.GetInteger(fieldsSplitted[1]);
                            if (y != RETURN_GENERIC_SUCCESS) { myBillObj.SetPatientID(y); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the patientID criteria");
                                continue;
                            }

                            // PatientID check - if PatientID already exists skip this registre
                            if (!demoTracker.CheckIfPatientIDAlreadyExists(fieldsSplitted[1]))
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Patient, " + i + "/" + (fileLines.Length - 1) + " - skipped because it patientID doesn't exist");
                                continue;
                            }

                            // AppointmentID
                            int z = Demographics.Tracker.GetInteger(fieldsSplitted[2]);
                            if(z != RETURN_GENERIC_SUCCESS) { myBillObj.SetAppointmentID(z); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the appointmentID criteria");
                                continue;
                            }

                            // AppointmentID check - if PatientID already exists skip this registre
                            if (!demoTracker.CheckIfAppointmentIDAlreadyExists(fieldsSplitted[2]))
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Appointment, " + i + "/" + (fileLines.Length - 1) + " - skipped because it appointmentID doesn't exist");
                                continue;
                            }

                            // FlagRecall
                            int w = Demographics.Tracker.GetInteger(fieldsSplitted[3]);
                            if (w == RETURN_GENERIC_SUCCESS) { myBillObj.SetFlagRecall(w); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding FlagRecall, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the flagRecall criteria");
                                continue;
                            }

                            // statusDone
                            bool boolVal;
                            if (Boolean.TryParse(fieldsSplitted[4], out boolVal))
                            {
                                myBillObj.SetStatusDone(boolVal);
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding StatusDone, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the statusDone criteria");
                                continue;
                            }


                            if (AddBillFromFileToList(myBillObj) == RETURN_GENERIC_SUCCESS)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddBill - Added the Bill obj, " + i + "/" + (fileLines.Length - 1) + " - [successful]");
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddBill - Added the Bill obj, " + i + "/" + (fileLines.Length - 1) + " - [fail]");
                            }
                        }
                        else
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Bill line - Adding Bill, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the amount of fields criteria");
                        }
                    }
                }
                catch (Exception)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create an Bill Object - [failure]");
                    setMadeSuccess = ReadPatientFileFailure;
                }
            }
            return setMadeSuccess;
        }


        




        // SERVICE LIST/LINE OBJECT **************************************************************************************************************

        /// <summary>
        /// [ CHECK IF SERVICE LIST EXISTS ] Check if exists an billID item inside a list. 
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfServiceListAlreadyExists(string stringServiceID, string stringBillID)
        {
            bool setMadeSuccess = false;
            int billID = 0;
            int serviceID = 0;

            try
            {
                int.TryParse(stringBillID, out billID);
                int.TryParse(stringServiceID, out serviceID);

                for (int i = 0; i < listOfBills.Count; i++)
                {
                    if ((listOfServices[i].GetBillID() == billID) && (listOfServices[i].GetServiceID() == serviceID))
                    {
                        setMadeSuccess = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                setMadeSuccess = false;
                Console.WriteLine("[CheckIfServiceListAlreadyExists] " + ex.Message);
            }

            return setMadeSuccess;
        }




        /// <summary>
        /// [WRITE ONE SERVICELIST]
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static Int32 WriteOneServiceListObjIntoFile(ServiceList obj)
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string txtServiceList = "", txtServiceID = "", txtBillID = ""; // just because it is a composite ID

            try
            {
                // Fill the info
                txtServiceList += obj.GetBillID().ToString() + DivBillingTrackerFiles;
                txtServiceList += obj.GetServiceID().ToString() + DivBillingTrackerFiles;
                txtServiceList += obj.GetQuantity().ToString() + DivBillingTrackerFiles;
                txtServiceList += obj.GetStatus() +"\n";

                // Get Composite key for logging
                txtBillID = obj.GetBillID().ToString() + DivBillingTrackerFiles;
                txtServiceID += obj.GetServiceID().ToString() + DivBillingTrackerFiles;


                if (fileIO.WriteFullServicelineFileAppend(txtServiceList) == true)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the ServiceList-Obj |" + txtServiceID + " - " + txtBillID);
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a ServiceList object into a file - Service List Obj Reference -> Bill ID: "+ txtBillID + "Service ID: " + txtServiceID + "[failure]");
                setMadeSuccess = WriteServiceListObjIntoFileFail;
            }
            return setMadeSuccess;
        }




        /// <summary>
        /// [ WRITE ALL BILLS ] Write all Patient Objs into the db Patient file. 
        /// </summary>
        private Int32 WriteAllServiceListObjsIntoFile()
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string txtServiceList = "", txtServiceID = "", txtBillID = ""; // just because it is a composite ID
            bool firstInteraction = true;

            try
            {
                foreach (var obj in listOfServices) // Loop through List with Patient
                {
                    // Fill the info
                    txtServiceList = "";
                    txtServiceList += obj.GetBillID().ToString() + DivBillingTrackerFiles;
                    txtServiceList += obj.GetServiceID().ToString() + DivBillingTrackerFiles;
                    txtServiceList += obj.GetQuantity().ToString() + DivBillingTrackerFiles;
                    txtServiceList += obj.GetStatus() + "\n";

                    // Get Composite key for logging
                    txtBillID = "";
                    txtServiceID = "";
                    txtBillID = obj.GetBillID().ToString() + DivBillingTrackerFiles;
                    txtServiceID += obj.GetServiceID().ToString() + DivBillingTrackerFiles;

                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullServicelineFileOverwrite(txtServiceList) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (Overwrite) the ServiceList-Obj |Reference -> Bill ID: " + txtBillID + "Service ID: " + txtServiceID);
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullServicelineFileAppend(txtServiceList) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the ServiceList-Obj |Reference -> Bill ID: " + txtBillID + "Service ID: " + txtServiceID);
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a ServiceList object into a file - Service List Obj Reference -> Bill ID: " + txtBillID + "Service ID: " + txtServiceID + "[failure]");
                setMadeSuccess = WriteBillObjIntoFileFail;
            }
            return setMadeSuccess;
        }




        /// <summary>
        /// Creates a Patient object by parsing a string
        /// </summary>
        /// <param name="message">Patients information</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        private Int32 ReadServiceListFile()
        {
            Int32 setMadeSuccess = 0; //Return variable, "0" means successful. 
            ServiceList myServiceListObj = null;
            string txtServiceList = "";
            string[] fileLines = null;
            string[] fieldsSplitted = null;

            // Read the Appointment array of strings
            if (ReadBillFileToStringArray(BillingClasses.ServiceList, ref fileLines) == RETURN_GENERIC_SUCCESS
                && fileLines != null)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceListFile - read patient file - successful");
                try
                {
                    for (Int32 i = 0; i < fileLines.Length; i++)
                    {
                        myServiceListObj = null;
                        txtServiceList = fileLines[i];
                        fieldsSplitted = txtServiceList.Split(FieldSeparator, StringSplitOptions.None);
                        if ((fieldsSplitted.Length == 4) && (fileLines[i] != "")) // ObjTag + PatientTag + 9 fields + ""
                        {
                            // new Patient()
                            myServiceListObj = new ServiceList();

                            // Get ServiceID
                            int x = Demographics.Tracker.GetInteger(fieldsSplitted[1]);
                            if (x != RETURN_GENERIC_SUCCESS) { myServiceListObj.SetServiceID(x); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an ServiceList line - Adding ServiceList, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the ID criteria");
                                continue;
                            }


                            // Get BillID
                            int y = Demographics.Tracker.GetInteger(fieldsSplitted[0]);
                            if (y != RETURN_GENERIC_SUCCESS) { myServiceListObj.SetBillID(y); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an ServiceList line - Adding ServiceList, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the ID criteria");
                                continue;
                            }


                            // Get Quantity
                            int z = Demographics.Tracker.GetInteger(fieldsSplitted[2]);
                            if (z != RETURN_GENERIC_SUCCESS) { myServiceListObj.SetQuantity(z); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an ServiceList line - Adding ServiceList, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the quantity criteria");
                                continue;
                            }

                            // Get Status
                            if (myServiceListObj.SetStatus(fieldsSplitted[3]) < 0)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an ServiceList line - Adding ServiceList, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the Status criteria");
                                continue;
                            }

                            if (AddServiceLineFromFileToList(myServiceListObj) == RETURN_GENERIC_SUCCESS)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddServiceLine - Added the ServiceList obj, " + i + "/" + (fileLines.Length - 1) + " - [successful]");
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddServiceLine - Added the ServiceList obj " + i + "/" + (fileLines.Length - 1) + " - [fail]");
                            }
                        }
                        else
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddServiceLine - Added the ServiceList obj, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the amount of fields criteria");
                        }
                    }
                }
                catch (Exception)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create an ServiceList Object - [failure]");
                    setMadeSuccess = ReadPatientFileFailure;
                }
            }
            return setMadeSuccess;
        }


        // SERVICE OBJECT **************************************************************************************************************

        /// <summary>
        /// Converts a string to an int
        /// </summary>
        /// <param name="number">A number in a string</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>
        /// <see cref="private static Patient ReadPatientFile()"> This method support the method ReadPatientFile.</see>
        /// <returns>The number or an error code</returns>
        public static int GetInteger(string number)
        {
            int errCode = RETURN_GENERIC_SUCCESS;

            try
            {
                errCode = Int32.Parse(number);
            }
            catch (FormatException) { errCode = GetIntegerFailFormat; }
            catch (OverflowException) { errCode = GetIntegerFailOverflow; }

            return errCode;
        }



        /// <summary>
        /// Converts a string to an Double
        /// </summary>
        /// <param name="number">A number in a string</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>
        /// <see cref="private static Patient ReadPatientFile()"> This method support the method ReadPatientFile.</see>
        /// <returns>The number or an error code</returns>
        public static Double GetBouble(string number)
        {
            int errCode = RETURN_GENERIC_SUCCESS;
            Double getDouble = 0.0;

            try
            {
                getDouble = Convert.ToDouble(number);
            }
            catch (FormatException) { errCode = GetDoubleFailFormat; }
            catch (OverflowException) { errCode = GetDoubleFailOverflow; }

            return errCode;
        }


        
        /// <summary>
        /// Converts a string to datetime
        /// </summary>
        /// <param name="number">A number in a string</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>
        /// <see cref="private static Patient ReadPatientFile()"> This method support the method ReadPatientFile.</see>
        /// <returns>The number or an error code</returns>
        public static DateTime GetDateTime(string strDateTime)
        {
            DateTime tempDateConversion = default(DateTime);

            try
            {
                DateTime.TryParseExact(strDateTime, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out tempDateConversion);           
            }
            catch
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "GetDateTime - An Exception was reached when tried to parse a string to DateTime - [failure]");                
            }

            return tempDateConversion;
        }


        
        /// <summary>
        /// [ CHECK IF IT EXISTS ] Check if exists an billID item inside a list. 
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns> true if the item was found</returns>
        public bool CheckIfServiceAlreadyExists(string stringServiceID)
        {
            bool setMadeSuccess = false;
            int serviceID = 0;

            try
            {
                int.TryParse(stringServiceID, out serviceID);

                for (int i = 0; i < availableServices.Count; i++)
                {
                    if (availableServices[i].GetServiceID() == serviceID)
                    {
                        setMadeSuccess = true;
                        break;
                    }
                }
            }
            catch
            {
                setMadeSuccess = false;
            }

            return setMadeSuccess;
        }



        /// <summary>
        /// [ WRITE ONE BILL ] reate a line into the db Patient file. 
        /// </summary>
        /// <param name="obj">The object Patient</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>"0" - If was is done ok.</returns>
        private static Int32 WriteOneServiceObjIntoFile(Service obj)
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string txtService = "", txtServiceID = "";

            try
            {
                // Fill the info
                txtService += obj.GetServiceID().ToString() + DivBillingTrackerFiles;
                txtServiceID = txtService;

                txtService += obj.GetFeeCode().ToString() + DivBillingTrackerFiles;
                txtService += obj.GetEffectiveDate().ToString() + DivBillingTrackerFiles;
                txtService += obj.GetServiceValue().ToString() + DivBillingTrackerFiles + "\n";

                if (fileIO.WriteFullBillFileAppend(txtService) == true)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the PatientID-Obj |" + txtServiceID);
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "WriteOneServiceObjIntoFile - An Exception was reached when tried to parse a SERVICE into a file - [failure]");
                setMadeSuccess = WriteBillObjIntoFileFail;
            }
            return setMadeSuccess;
        }



        /// <summary>
        /// [ WRITE ALL BILLS ] Write all Patient Objs into the db Patient file. 
        /// </summary>
        private Int32 WriteAllServiceObjsIntoFile()
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            string txtService = "", txtServiceID = "";
            bool firstInteraction = true;

            try
            {
                foreach (var obj in availableServices) // Loop through List with Patient
                {
                    // Fill the info
                    txtService += obj.GetServiceID().ToString() + DivBillingTrackerFiles;
                    txtServiceID = txtService;

                    txtService += obj.GetFeeCode().ToString() + DivBillingTrackerFiles;
                    txtService += obj.GetEffectiveDate().ToString() + DivBillingTrackerFiles;
                    txtService += obj.GetServiceValue().ToString() + DivBillingTrackerFiles + "\n";

                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullBillFileOverwrite(txtService) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (Overwrite) the Service-Obj |" + txtServiceID);
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullBillFileAppend(txtService) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Done, writed (append) the Service-Obj |" + txtServiceID);
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "WriteAllServiceObjsIntoFile - An Exception was reached when tried to parse a SERVICE into a file - [failure]");
                setMadeSuccess = WriteBillObjIntoFileFail;
            }
            return setMadeSuccess;
        }



        /// <summary>
        /// Creates a Patient object by parsing a string
        /// </summary>
        /// <param name="message">Patients information</param>
        /// <see cref="public static string PackPatient(Patient obj)"> This method was used as a reference.</see>/>
        /// <returns>The object if the string is valid or NULL otherwise</returns>
        private Int32 ReadServiceFile()
        {
            Int32 setMadeSuccess = RETURN_GENERIC_SUCCESS; //Return variable, "0" means successful. 
            Service myServiceObj = null;
            string txtRecordLine = "";
            string[] fileLines = null;
            string[] fieldsSplitted = null;

            // Read the Appointment array of strings
            if (ReadBillFileToStringArray(BillingClasses.Service, ref fileLines) == RETURN_GENERIC_SUCCESS
                && fileLines != null)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceFile - read service file - [successful]");
                try
                {
                    for (Int32 i = 0; i < fileLines.Length; i++)
                    {
                        myServiceObj = null;
                        txtRecordLine = fileLines[i];
                        fieldsSplitted = txtRecordLine.Split(FieldSeparator, StringSplitOptions.None);
                        if ((fieldsSplitted.Length == 4) && (fileLines[i] != "")) // ObjTag + PatientTag + 4 fields + ""
                        {
                            // new Service()
                            myServiceObj = new Service();

                            // ServiceID
                            int z = GetInteger(fieldsSplitted[0]);
                            if (z != RETURN_GENERIC_SUCCESS) { myServiceObj.SetServiceID(z); }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service line - Adding Service, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the ID criteria");
                                continue;
                            }

                            // ServiceID check - if addressID already exists skip this registre
                            if (CheckIfServiceAlreadyExists(fieldsSplitted[0]))
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service line - Adding Service, " + i + "/" + (fileLines.Length - 1) + " - skipped because it ServiceID already exists");
                                continue;
                            }

                            // feeCode
                            if (myServiceObj.SetFeeCode(fieldsSplitted[1]) != Service.RETURN_SERVICE_SUCCESS)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service line - Adding Service, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the feeCode criteria");
                                continue;
                            }

                            // effectiveDate
                            DateTime tmpDatetime = GetDateTime(fieldsSplitted[2]);
                            if (myServiceObj.SetEffectiveDate(tmpDatetime) != Service.RETURN_SERVICE_SUCCESS)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service line - Adding Service, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the Last Name criteria");
                                continue;
                            }

                            // serviceValue
                            Double tmpDouble = GetBouble(fieldsSplitted[3]);
                            if (myServiceObj.SetServiceValue(tmpDouble) != Service.RETURN_SERVICE_SUCCESS)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service line - Adding Service, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the First Name criteria");
                                continue;
                            }


                            if (AddServiceFromFileToList(myServiceObj) == RETURN_GENERIC_SUCCESS)
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddService - Added the Service obj, " + i + "/" + (fileLines.Length - 1) + " - [successful]");
                            }
                            else
                            {
                                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "AddService - Added the Service obj " + i + "/" + (fileLines.Length - 1) + " - [fail]");
                            }
                        }
                        else
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "Read an Service line - Adding Service, " + i + "/" + (fileLines.Length - 1) + " - skipped because it doesn't match the amount of fields criteria");
                        }
                    }
                }
                catch (Exception)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "An Exception was reached when tried to parse a line object to fields, and to create an Service Object - [failure]");
                    setMadeSuccess = ERR_ReadServiceFile;
                }
            }
            return setMadeSuccess;
        }



        // MONTHLY BILLING REPORT [100 %]  **************************************************************************************************************


        ///// <summary>
        ///// Generates a list of strings containing the bill information formatted according 
        ///// to MoH (sample) specifications. The bills are selected based on the chosen month.
        ///// </summary>
        ///// <param name="selectedMonth">Current list of Bills</param>
        ///// <returns>returns a string list containing the billing information in the required format</returns>
        ///// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public List<string> GenerateMonthlyBilling(DateTime selectedMonth)
        {
            // Local variables
            string billRecords = "";
            string billDatePart = "";
            string billHCNPart = "";
            string billGenderPart = "";
            string billFeeCodePart = "";
            string billServiceValuePart = "";
            string billCompleteRecord = "";
            double workingNumber = 0.0;
            int multiplicator = 10000;   // help to transform double into bill sample service value format
            int specificAppointmentID = 0;
            int specificPatientID = 0;
            bool firstInteraction = true;


            // List to carry the final formatted list of strings based on the selected month
            List<string> BillsFormattedForFileIO = new List<string>();


            try
            {
                // traverse the whole list
                foreach (Bill billRecord in listOfBills)
                {
                    //  Get appointment date from Bill List
                    specificAppointmentID = billRecord.GetAppointmentID();

                    //  Get appointment date from AppointmentList



                    foreach (Demographics.Appointment appRecord in demoTracker.GetAppointmentList())
                    {
                        // Define the appointments for the selected Month
                        if (appRecord.GetDayAndTime().Month == selectedMonth.Month)
                        {
                            // Get string formatted bill date based on appointment DateTime variable.
                            if (appRecord.GetApointmentID() == specificAppointmentID)
                            {
                                // Add the date portion within the bill string following specified format
                                billDatePart = appRecord.GetDayAndTime().ToString("yyyyMMdd");

                                // Get patientID from Appointments within the bill list
                                specificPatientID = appRecord.GetPatientID();
                            }
                        }
                    }

                    // Get patientID from patient list
                    foreach (Demographics.Patient patientRecord in demoTracker.GetPatientList())
                    {
                        // select only patients within the pre-defined month in previous operation
                        if (patientRecord.PatientID == specificPatientID)
                        {
                            // Get patient HCN from matching patientID
                            billHCNPart = patientRecord.GethCN();

                            // Get patient gender
                            billGenderPart = patientRecord.GetSex();

                            // Add unique portion of the bill record in the string
                            billRecords = billDatePart + billHCNPart + billGenderPart;

                            // Get feeCode and Price and start to add records to the main string
                            // Use already gotten parameters to get Services from Service list associated to the specific billID
                            foreach (ServiceList serviceListRecord in listOfServices)
                            {
                                // use cascade structure to get serviceList billIDs from specified month following previous operations
                                if (serviceListRecord.GetBillID() == billRecord.GetBillID())
                                {
                                    // Get Service Code and price (connects Bill and Service through ServiceList)
                                    foreach (Service serviceRecord in availableServices)
                                    {
                                        if (serviceRecord.GetServiceID() == serviceListRecord.GetServiceID())
                                        {
                                            // Get service information related to selected bills
                                            billFeeCodePart = serviceRecord.GetFeeCode();
                                            workingNumber = (serviceRecord.GetServiceValue() * multiplicator);
                                            billServiceValuePart = workingNumber.ToString("00000000000");

                                            // Completes the bill record string following specified format

                                            billCompleteRecord = billRecords + billFeeCodePart + billServiceValuePart + "\n";
                                            //billRecords += billFeeCodePart + billServiceValuePart + "\n";

                                            // ADD TO A LIST OF FILEFORMATTEDBILLS
                                            BillsFormattedForFileIO.Add(billCompleteRecord);

                                            // Erase "billCompleteRecord" for a new record to be composed
                                            billCompleteRecord = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Assign the formatted list of generated billings to class member
                listOfFormattedBillingRecords = BillsFormattedForFileIO;


                foreach (var txtBillLine in listOfFormattedBillingRecords) // Loop through List with Patient
                {
                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullMonthlybillingFileOverwrite(txtBillLine) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "BillsFormattedForFileIO - [Done], writed (Overwrite) the Bill-record");
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullMonthlybillingFileAppend(txtBillLine) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "BillsFormattedForFileIO - [Done], writed (Append) the Bill-record");
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "BillsFormattedForFileIO - An Exception was reached when tried to parse of the bill records into a file - [failure]");
            }




            // Returns list of formmated bill strings
            return BillsFormattedForFileIO;
        }


        // BILLING RESPONSE [100 %] **************************************************************************************************************

        /// <summary>
        /// NEED TO BE TESTED ONCE THE SYSTEM LOADS THE SAMPLE FILES...
        /// </summary>
        /// <param name="OfficialProcessedBillInfoFromFileIO"></param>
        /// <returns></returns>
        public int ReceiveBillingResponseRecordsFromFileIO()
        {
            // Local variables
            int statusReturn = 0;
            int trackPatientID = 0;
            int trackAppointmentID = 0;
            int trackBillID = 0;
            int trackServiceID = 0;

            // Variable to receive data from FileIO

            List<string[]> billingInfoFromFileIO = ReadBillingResponseFromFile();

            if (billingInfoFromFileIO != null)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingResponseFromFile - read billing MOH response file - successful");
            }



            // Received Billing reconciled Info format
            // ECOUNTER_DATE | HCN           | Gender | Fee Code | Service Value | Status
            // 20171106      | 5534567890EE  | M      | A005     | 0000077200000 | PAID

            try
            {
                foreach (string[] item in billingInfoFromFileIO)
                {
                    foreach (Patient patientRecord in demoTracker.GetPatientList())
                    {
                        // Compare HCNs
                        if (item[1] == patientRecord.GethCN())
                        {
                            // Record Patient ID for tracking
                            trackPatientID = patientRecord.GetPatientID();

                            foreach (Appointment appointmentRecord in demoTracker.GetAppointmentList())
                            {
                                // Convert appointment date in record format
                                if (item[0] == appointmentRecord.GetDayAndTime().ToString("yyyyMMdd"))
                                {
                                    // Record AppointmentID for tracking
                                    trackAppointmentID = appointmentRecord.GetApointmentID();

                                    foreach (Bill billRecord in listOfBills)
                                    {
                                        if ((trackPatientID == billRecord.GetPatientID()) && (trackAppointmentID == billRecord.GetAppointmentID()))
                                        {
                                            // Record BillID for tracking
                                            trackBillID = billRecord.GetBillID();

                                            foreach (Service serviceRecord in availableServices)
                                            {
                                                if (item[3] == serviceRecord.GetFeeCode())
                                                {
                                                    // Record ServiceID dor tracking
                                                    trackServiceID = serviceRecord.GetServiceID();

                                                    foreach (ServiceList serviceListRecord in listOfServices)
                                                    {
                                                        if ((serviceRecord.GetServiceID() == serviceListRecord.GetServiceID() && (billRecord.GetBillID() == serviceListRecord.GetBillID())))
                                                        {
                                                            // Change Service Status from ServiceLine (also known as ServiceList) based on the info of the string[].
                                                            // Example:     // ECOUNTER_DATE | HCN           | Gender | Fee Code | Service Value | Status
                                                            //                |20171106      | 5534567890EE  | M      | A005     | 0000077200000 | PAID
                                                            serviceListRecord.SetStatus(item[5]);

                                                            // Change Bill "Action" Status
                                                            billRecord.SetStatusDone(true);
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //Write to file a new bill into a file
                WriteAllBillObjsIntoFile();

                //Write to file a new serviceLine record into a file
                WriteAllServiceListObjsIntoFile();


                // Load the tracker list with date. 
                foreach (string[] item in billingInfoFromFileIO)
                {
                    string tmpStr = item[0] + item[1] + item[2] + item[3] + item[4] + item[5];
                    listOfReconciledServicesFromMoH.Add(tmpStr);
                }
            }
            catch (Exception)
            {
                // Add Log
                //logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadBillingRecordsFile - read billing file - successful");
            }
            return statusReturn;
        }



        /// <summary>
        /// Simulates the response from a processed billing summary from MoH
        /// </summary>
        /// <param name="date">date reference from the month in interest</param>
        /// <returns>returns string containing the content of the MoH proccessed billing</returns>
        /// <seealso cref="Other Tracker Methods and Billing Library Classes methods"/>
        public List<string> GenerateMonthlyBillingMoH(DateTime date)
        {
            // Local variables
            List<string> monthlyBillingReconciled = new List<string>();
            DateTime toMatchMonth = new DateTime();
            Random random = new Random();
            int reconcileCodeNum = 0;
            string processedItem = "";
            string completeRecord = "";
            ServiceList.ServiceStatus reconcileCodeEnum;
            bool firstInteraction = true;

            try
            {
                foreach (string item in listOfFormattedBillingRecords)
                {
                    // Extract date from bill string and compare to selected month presented in the arguments
                    toMatchMonth = DateTime.ParseExact(item.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                    if (date.Month == toMatchMonth.Month)
                    {
                        reconcileCodeNum = random.Next(1, 5);
                        if (Enum.IsDefined(typeof(ServiceList.ServiceStatus), reconcileCodeNum))
                        {
                            // REMOVE "NEXT LINE" FROM ITEM
                            processedItem = item.Substring(0, item.Length - 1);

                            reconcileCodeEnum = (ServiceList.ServiceStatus)reconcileCodeNum;
                            completeRecord = processedItem + reconcileCodeEnum + "\n";
                            monthlyBillingReconciled.Add(completeRecord);
                        }
                    }
                }

                foreach (var txtBillReconciledLine in monthlyBillingReconciled) // Loop through List with Patient
                {
                    if (firstInteraction == true)
                    {
                        if (fileIO.WriteFullResponseFileOverwrite(txtBillReconciledLine) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "GenerateMonthlyBillingMoH - [Done], writed (Overwrite) the Bill Reconciled-record |" + txtBillReconciledLine + "|");
                            firstInteraction = false;
                        }
                    }
                    else
                    {
                        if (fileIO.WriteFullResponseFileAppend(txtBillReconciledLine) == true)
                        {
                            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "GenerateMonthlyBillingMoH - [Done], writed (Append) the Bill Reconciled-record    |" + txtBillReconciledLine + "|");
                        }
                    }
                }
            }
            catch (Exception)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "GenerateMonthlyBillingMoH - An Exception was reached when tried to parse of the bill reconciled records into a file - [failure]");
            }

            // TEMPORAILY TESTING METHOD TO PROCESS MOH INFO
            ReceiveBillingResponseRecordsFromFileIO();

            return monthlyBillingReconciled;
        }


        // SERVICE MASTER LIST  [100 %] **************************************************************************************************************

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int ReceiveServicesMasterSampleFromFileIO()
        {
            // Local variables
            int statusReturn = 0;
            DateTime tempDateConversion = new DateTime();
            double tempValueConversion = 0.0;
            int valueMultiplier = 10000;
            int currentServicesCount = 0;

            // Variable to receive list from FileIO
            List<string[]> ServiceMasterFileIO = ReadServiceMasterSample();

            if (ServiceMasterFileIO != null)
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceMasterSample - read billing file - [successful]");
            }
            else
            {
                logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReadServiceMasterSample - read billing file - [failure]");
            }


            // (BROKEN DOWN EXAMPLE)
            // Master sample format:  FEE CODE | EFFECTIVE DATE | SERVICE VALUE
            // Example:               A003     | 20091201       | 00000772000


            // ITERATE THROUGH THE LIST OF STRING[] TO RESOLVE EACH SERVICE RECORD

            foreach (string[] serviceMasterRecord in ServiceMasterFileIO)
            {
                
                // IF LIST IS EMPTY, INSERT THE FIRST SERVICE
                if (availableServices.Count() == 0)
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Current list of available services is empty - first record will be entered");

                    try
                    {
                        // string that put the record together for FileIO
                        string completeStringServiceRecord = serviceMasterRecord[0] + serviceMasterRecord[1] + serviceMasterRecord[2] + serviceMasterRecord[3] + "\n";

                        fileIO.WriteFullServiceFileOverwrite(completeStringServiceRecord);

                        // Set new Service object to be inserted
                        Service firstService = new Service();

                        // Set first Service ID as 1
                        if (firstService.SetServiceID(1) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object ID - [Success]"); }
                        else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object ID - [Failure]"); }

                        // Set first Service feeCode
                        if (firstService.SetFeeCode(serviceMasterRecord[0]) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object feeCode - [Success]"); }
                        else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object feeCode - [Failure]"); }

                        // Set first service Effective date
                        if (DateTime.TryParseExact(serviceMasterRecord[1], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out tempDateConversion))
                        {
                            if (firstService.SetEffectiveDate(tempDateConversion) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object Effectice Date - [Success]"); }
                            else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object Effectice Date - [Failure]"); }
                        }

                        // Set first Service value
                        if (Double.TryParse(serviceMasterRecord[2], out tempValueConversion))
                        {
                            if (firstService.SetServiceValue((tempValueConversion / valueMultiplier)) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object Effectice Date - [Success]"); }
                            else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Set first object Effectice Date - [Failure]"); }
                        }

                        // Add first Service to the List
                        availableServices.Add(firstService);
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - First Service Object was created and added to the memory - [Success]");

                    }
                    catch (Exception ex)
                    {
                        // #ADD_LOG
                        statusReturn = RETURN_BILLING_TRACKER_FAILURE;
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - First Service Object was created and added to the memory - [Failure]");
                        Console.WriteLine("[ReceiveServicesMasterSampleFromFileIO] " + ex.Message);
                    }


                }
                // IF THE LIST IS NOT EMPTY, CHECK FOR DUPLICATE FEECODE
                else if (CheckForServiceCodeDuplicate(serviceMasterRecord[0]) > 0) // If non-existent, return positive value
                {
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service obj list not empty");
                    logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - No Service duplicate was found, a new object will be created");

                    try
                    {
                        string completeStringServiceRecord = serviceMasterRecord[0] + serviceMasterRecord[1] + serviceMasterRecord[2] + serviceMasterRecord[3] + "\n";

                        fileIO.WriteFullServiceFileAppend(completeStringServiceRecord);

                        //Add new service to the list
                        Service serviceToAdd = new Service();

                        // Set new ServiceID
                        currentServicesCount = availableServices.Count();
                        if (serviceToAdd.SetServiceID(++currentServicesCount) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service ID set -  [Success] "); }
                        else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service ID set -  [Failure] "); }

                        // Set new Service FeeCode
                        if (serviceToAdd.SetFeeCode(serviceMasterRecord[0]) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service Fee Code set -  [Success] "); }
                        else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service Fee Code set -  [Success] "); }

                        // Set new Service Effective Date
                        if (DateTime.TryParseExact(serviceMasterRecord[1], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out tempDateConversion))
                        {
                            if (serviceToAdd.SetEffectiveDate(tempDateConversion) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service Effective Date set -  [Success] "); }
                            else { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service Effective Date set -  [Failure] "); }
                        }

                        // Set first Service value
                        if (Double.TryParse(serviceMasterRecord[2], out tempValueConversion))
                        {
                            if (serviceToAdd.SetServiceValue((tempValueConversion / valueMultiplier)) > 0) { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service Value set -  [Success] "); }
                            else { { logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - Service Value set -  [Failure] "); } }
                        }

                        // Add first Service to the List
                        availableServices.Add(serviceToAdd);
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - New Service Object was created and added to the memory - [Success]");
                    }
                    catch (Exception ex)
                    {
                        // #ADD_LOG
                        statusReturn = RETURN_BILLING_TRACKER_FAILURE;
                        logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, "ReceiveServicesMasterSampleFromFileIO - New Service Object was created and added to the memory - [Failure]");
                        Console.WriteLine("[ReceiveServicesMasterSampleFromFileIO] " + ex.Message);
                    }
                }
            }
            return statusReturn;
        }

    }
}
*/
