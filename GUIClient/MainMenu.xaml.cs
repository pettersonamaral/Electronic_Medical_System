﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Interpreter;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
      
        public MainMenu()
        {
            
            InitializeComponent();
        }

        private void PatientButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PatientPages.PatientMenu(this));
        }

        private void ScheduleButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SchedulePages.SchedulingMenu());
        }

        private void BillingButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new BillingPages.BillingMenu(this));
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool connected = 
                ClientInterpreter.Instance.Connect(IPAddress.Parse(ServerAddressTextBox.Text), int.Parse(PortTextBox.Text));

                if (connected)
                {
                    ConnectButton.IsEnabled = false;
                    ConnectButton.Content = "Connected!";
                    ServerAddressTextBox.IsEnabled = false;
                    PortTextBox.IsEnabled = false;

                    PatientButton.IsEnabled = true;
                    ScheduleButton.IsEnabled = true;
                    BillingButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
