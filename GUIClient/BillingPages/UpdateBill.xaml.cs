﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Billing;
using Demographics;
using Interpreter;

namespace GUIClient.BillingPages
{
    /// <summary>
    /// Interaction logic for UpdateBill.xaml
    /// </summary>
    public partial class UpdateBill : Page
    {
        IDemographics demographics = ClientInterpreter.Instance;
        IBilling billing = ClientInterpreter.Instance;

        Bill bill = null;
        bool changedBill = false;
        bool initializing = true;

        #region Page Initialization

        public UpdateBill(Bill existingBill)
        {
            bill = existingBill;
            InitializeComponent();

            if (bill.GetBillID() == 0)
            {
                MessageBox.Show("Invalid Bill", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                NavigationService.GoBack();
            }

            PatientTextBlock.Text = bill.Patient.ToString();
            AppointmentTextBlock.Text = bill.Appointment.ToString();
            FlagComboBox.Text = bill.FlagRecall.ToString();

            UpdateServicesListBoxInBill();

            initializing = false;
        }

        #endregion

        #region Code Behind

        private void UpdateServiceList()
        {
            ServiceListBox.Items.Clear();

            try
            {
                List<Service> services = billing.FindServices(FindServiceTextBox.Text.Trim());

                if (services == null) { return; }

                services = services.OrderBy(x => x.FeeCode).ToList();

                foreach (Service s in services)
                {
                    ServiceListBox.Items.Add(s);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateServicesListBoxInBill()
        {
            ServicesInTheBillListBox.Items.Clear();
            foreach (ServiceList sl in bill.Services)
            {
                ServicesInTheBillListBox.Items.Add(sl);
            }
        }

        private void UpdateAddButton()
        {
            AddButton.IsEnabled = false;

            if (ServiceListBox.SelectedIndex != -1)
            {
                Service selected = ServiceListBox.SelectedItem as Service;

                bool exists = false;
                foreach (ServiceList sl in bill.Services)
                {
                    if (sl.Service == selected) { exists = true; break; }
                }

                if (!exists)
                {
                    AddButton.IsEnabled = true;
                }
            }
        }

        private void UpdateRemoveButton()
        {
            RemoveButton.IsEnabled = false;

            if (ServicesInTheBillListBox.SelectedIndex != -1)
            {
                RemoveButton.IsEnabled = true;
            }
        }

        private void AddService()
        {
            if (ServiceListBox.SelectedIndex != -1)
            {
                Service service = ServiceListBox.SelectedItem as Service;
                ServiceList newsl = new ServiceList { Service = service, Quantity = 1 };

                bool exists = false;
                foreach (ServiceList sl in bill.Services)
                {
                    if (sl.Service == service) { exists = true; break; }
                }

                if (!exists)
                {
                    bill.Services.Add(newsl);
                    changedBill = true;
                }
                else
                {
                    MessageBox.Show("Service already in the bill", "Information", 
                        MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            UpdateServicesListBoxInBill();
        }

        private void RemoveService()
        {
            if (ServicesInTheBillListBox.SelectedIndex != -1)
            {
                ServiceList sl = ServicesInTheBillListBox.SelectedItem as ServiceList;
                bill.Services.Remove(sl);

                changedBill = true;
            }
            UpdateServicesListBoxInBill();
        }

        private bool SaveChanges()
        {
            bool retCode = false;

            // Set Flag
            bill.FlagRecall = int.Parse(FlagComboBox.Text);

            try
            {
                int feedback = billing.UpdateBill(bill);
                if (feedback == 0)
                {
                    retCode = true;
                    changedBill = false;
                    MessageBox.Show("Changes Saved", "Saved", 
                        MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Unable to save changes", $"Error {feedback}", 
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return retCode;
        }

        private bool CheckChanges()
        {
            bool retCode = true;

            if (changedBill)
            {
                retCode = false;
                MessageBoxResult result = MessageBox.Show(
                    "Save changes before going back?", "Question", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    retCode = SaveChanges();
                }
                else if (result == MessageBoxResult.No)
                {
                    retCode = true;
                }
            }

            return retCode;
        }

        #endregion

        #region Event Handlers

        private void FindServiceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateServiceList();
        }

        private void ServiceListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateAddButton();
        }

        private void ServicesInTheBillListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateRemoveButton();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            AddService();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            RemoveService();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckChanges()) { return; }
            if (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void BtnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckChanges()) { return; }
            while (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void ProcessButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: do the thing or remove button
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveChanges();
        }

        #endregion
    }
}
