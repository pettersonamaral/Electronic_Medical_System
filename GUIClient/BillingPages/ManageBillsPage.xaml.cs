﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Billing;
using Demographics;
using Interpreter;

namespace GUIClient.BillingPages
{
    /// <summary>
    /// Interaction logic for ManageBillsPage.xaml
    /// </summary>
    public partial class ManageBillsPage : Page
    {
        #region Attributes and Interfaces

        Appointment selectedAppointment = null;

        IDemographics demographics = ClientInterpreter.Instance;
        IBilling billing = ClientInterpreter.Instance;

        #endregion

        #region Page Initialization

        public ManageBillsPage()
        {
            InitializeComponent();
        }

        #endregion

        #region Code Behind

        private void UpdateDetails()
        {
            TimeSlotTextBlock.Text = "";
            FirstPatientInfoTextBlock.Text = "";
            SecondPatientInfoTextBlock.Text = "";
            FirstPatientButton.IsEnabled = false;
            SecondPatientButton.IsEnabled = false;

            if (selectedAppointment == null) { return; }

            TimeSlotTextBlock.Text = selectedAppointment.GetDayAndTime().ToString();

            // Process FirstPatient
            FirstPatientInfoTextBlock.Text = selectedAppointment.FirstPatient.ToString(false);
            FirstPatientButton.IsEnabled = true;

            Bill bill = billing.FindBill(
                selectedAppointment.FirstPatient.PatientID, selectedAppointment.GetAppointmentID());

            if (bill == null)
            {
                FirstPatientInfoTextBlock.Text += "\nNo bill found";
                FirstPatientButton.Content = "Create Bill";
            }
            else
            {
                FirstPatientInfoTextBlock.Text += $"\n{bill.ToString(false)}";
                FirstPatientButton.Content = "Update Bill";
                
                if (bill.StatusDone)
                {
                    FirstPatientButton.IsEnabled = false;
                }
            }

            // Process SecondPatient
            if (selectedAppointment.SecondPatient == null) { return; }

            SecondPatientInfoTextBlock.Text = selectedAppointment.SecondPatient.ToString(false); ;
            SecondPatientButton.IsEnabled = true;

            bill = billing.FindBill(
                selectedAppointment.SecondPatient.PatientID, selectedAppointment.GetAppointmentID());

            if (bill == null)
            {
                SecondPatientInfoTextBlock.Text += "\nNo bill found";
                SecondPatientButton.Content = "Create Bill";
            }
            else
            {
                SecondPatientInfoTextBlock.Text += $"\n{bill.ToString(false)}";
                SecondPatientButton.Content = "Update Bill";

                if (bill.StatusDone)
                {
                    SecondPatientButton.IsEnabled = false;
                }
            }
        }

        private void UpdateList()
        {
            AppointmentListBox.Items.Clear();

            DateTime? fromQ = FromDatePicker.SelectedDate;
            DateTime? toQ = ToDatePicker.SelectedDate;

            if (fromQ == null || toQ == null) { return; }

            DateTime from = fromQ.Value;
            DateTime to = toQ.Value;

            if (to < from) { return; }

            try
            {
                List<Appointment> apps = demographics.FindAppointments(from.Date, to.AddDays(1));

                if (apps == null) { return; }

                apps = apps.OrderBy(x => x.GetDayAndTime()).ToList();

                foreach(Appointment a in apps)
                {
                    AppointmentListBox.Items.Add(a);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void CheckBillAndNavigate(Patient patient)
        {
            try
            {
                Bill bill = billing.FindBill(
                    patient.PatientID, selectedAppointment.GetAppointmentID());

                if (bill == null)
                {
                    // create bill
                    bill = new Bill();
                    bill.Appointment = selectedAppointment;
                    bill.Patient = patient;

                    int feedback = billing.AddBill(bill);

                    if (feedback != 0)
                    {
                        MessageBox.Show("Failed to create a bill", $"Error {feedback}",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        bill = billing.FindBill(
                            patient.PatientID, selectedAppointment.GetAppointmentID());
                        NavigationService.Navigate(new UpdateBill(bill));
                    }
                }
                else
                {
                    // check update
                    if (!bill.StatusDone)
                    {
                        NavigationService.Navigate(new UpdateBill(bill));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Event Handlers

        private void FromDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateList();
        }

        private void ToDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateList();
        }

        private void AppointmentListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AppointmentListBox.SelectedIndex == -1)
            {
                selectedAppointment = null;
            }
            else
            {
                selectedAppointment = AppointmentListBox.SelectedItem as Appointment;
            }

            UpdateDetails();
        }

        private void FirstPatientButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedAppointment != null && selectedAppointment.FirstPatient != null)
                CheckBillAndNavigate(selectedAppointment.FirstPatient);
        }

        private void SecondPatientButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedAppointment != null && selectedAppointment.SecondPatient != null)
                CheckBillAndNavigate(selectedAppointment.SecondPatient);
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            while (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        #endregion
    }
}
