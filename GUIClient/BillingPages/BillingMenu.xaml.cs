﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUIClient.BillingPages
{
    /// <summary>
    /// Interaction logic for BillingMenu.xaml
    /// </summary>
    public partial class BillingMenu : Page
    {
        private Page MainWindow;
        public BillingMenu(Page page)
        {
            //Creating a reference to main Window
            MainWindow = page;
            InitializeComponent();
        }


        private void BtnBack_Click_1(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void BtnServices_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ServicesPage());
        }

        private void BtnReports_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new BillingPages.MonthlyReport(MainWindow));
        }

        private void BtnBills_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ManageBillsPage());
        }
    }
}
