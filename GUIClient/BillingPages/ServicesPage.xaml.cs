﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Billing;
using Interpreter;

namespace GUIClient.BillingPages
{
    /// <summary>
    /// Interaction logic for ServicesPage.xaml
    /// </summary>
    public partial class ServicesPage : Page
    {
        IBilling billing = ClientInterpreter.Instance;

        public ServicesPage()
        {
            InitializeComponent();
        }

        #region Code Behind

        private void UpdateContents()
        {
            FeeCodeTextBlock.Text = "";
            EffectiveDateTextBlock.Text = "";
            ValueTextBlock.Text = "";

            if (ServiceListBox.SelectedIndex != -1)
            {
                Service service = ServiceListBox.SelectedItem as Service;

                FeeCodeTextBlock.Text = service.FeeCode;
                EffectiveDateTextBlock.Text = service.EffectiveDate.ToShortDateString();
                ValueTextBlock.Text = service.ServiceValue.ToString("C0");
            }
        }

        private void UpdateList()
        {
            ServiceListBox.Items.Clear();

            try
            {
                List<Service> services = billing.FindServices(FindServiceTextBox.Text.Trim());
                
                if (services == null) { return; }

                services = services.OrderBy(x => x.FeeCode).ToList();

                foreach(Service s in services)
                {
                    ServiceListBox.Items.Add(s);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Event Handlers

        private void BtnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            while (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void ServiceListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateContents();
        }

        private void FindServiceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateList();
        }

        #endregion
    }
}
