﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Demographics;
using Billing;
using Interpreter;

namespace GUIClient.BillingPages
{
    /// <summary>
    /// Interaction logic for MonthlyReport.xaml
    /// </summary>
    public partial class MonthlyReport : Page
    {
        private Page MainWindow;
        IBilling billing = ClientInterpreter.Instance;
        public MonthlyReport( Page page)
        {
            //Creating a reference to main Window
            MainWindow = page;
            InitializeComponent();
            
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if(NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            if (DateReport.SelectedDate.HasValue)
            {
                DateTime date = DateReport.SelectedDate.Value;
                date = DateTime.Parse($"{date.Year}-{date.Month}-01");
                tBoxMonthlyBill.Text = billing.MonthlyBillingSummary(date); 
            }
            else
            {
                MessageBox.Show("Please select a valid Date!");
            }

            
            
        }

        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();

            if (dlg.ShowDialog().GetValueOrDefault())
            {
                dlg.PrintVisual(tBoxMonthlyBill, "Monthly Billing Report");
            }
        }

        private void BtnServices_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnMain_Click(object sender, RoutedEventArgs e)
        {
            while (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }
    }
}
