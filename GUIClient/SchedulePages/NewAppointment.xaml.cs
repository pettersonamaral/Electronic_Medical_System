﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Interpreter;
using Demographics;

namespace GUIClient.SchedulePages
{
    /// <summary>
    /// Interaction logic for NewAppointment.xaml
    /// </summary>
    public partial class NewAppointment : Page
    {
        #region Attributes and Interfaces

        // Attributes
        DateTime timeSlot;
        Patient selectedPatient = null;
        Appointment appointment = new Appointment();

        // Interfaces
        IDemographics demographics = ClientInterpreter.Instance;

        #endregion

        #region Page Initialization

        public NewAppointment(DateTime timeSlot)
        {
            this.timeSlot = timeSlot;
            appointment.FirstPatient = null;
            appointment.SecondPatient = null;
            appointment.SetDayAndTime(timeSlot);

            InitializeComponent();
            TimeSlotTextBlock.Text = timeSlot.ToString();
            UpdateDetails();
        }

        #endregion

        #region Code Behind

        private void UpdatePatientList()
        {
            PatientListBox.Items.Clear();

            Patient pLast = new Patient();

            if (!pLast.SetLname(FindPatientTextBox.Text.Trim())) { return; }

            List<Patient> patients = null;

            try
            {
                patients = demographics.FindPatient(pLast);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (patients == null) { return; }

            foreach(Patient p in patients)
            {
                PatientListBox.Items.Add(p);
            }
        }

        private void UpdateDetails()
        {
            if (appointment.FirstPatient == null)
            {
                FirstPatientInfoTextBlock.Text = " Not Set ";
            }
            else
            {
                FirstPatientInfoTextBlock.Text = appointment.FirstPatient.ToString();
            }

            if (appointment.SecondPatient == null)
            {
                SecondPatientInfoTextBlock.Text = " Not Set ";
            }
            else
            {
                SecondPatientInfoTextBlock.Text = appointment.SecondPatient.ToString();
            }
        }

        private void UpdateButtons()
        {
            FirstPatientButton.IsEnabled = false;
            SecondPatientButton.IsEnabled = false;
            CreateAppointmentButton.IsEnabled = false;

            if (appointment.IsValid)
            {
                CreateAppointmentButton.IsEnabled = true;
            }

            if (appointment.FirstPatient != null)
            {
                FirstPatientButton.Content = "Remove";
                if (appointment.SecondPatient == null)
                {
                    FirstPatientButton.IsEnabled = true;
                }
            }
            else
            {
                FirstPatientButton.Content = "Add";
                if (selectedPatient != null)
                {
                    FirstPatientButton.IsEnabled = true;
                }
            }

            if (appointment.SecondPatient != null)
            {
                SecondPatientButton.Content = "Remove";
                if (appointment.FirstPatient != null)
                {
                    SecondPatientButton.IsEnabled = true;
                }
            }
            else
            {
                SecondPatientButton.Content = "Add";
                if (appointment.FirstPatient != null && selectedPatient != null)
                {
                    SecondPatientButton.IsEnabled = true;
                }
            }

            UpdateDetails();
        }

        #endregion

        #region Event Handlers

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            while (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void FindPatientTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (FindPatientTextBox.Text.Length > 0)
            {
                UpdatePatientList();
            }
        }

        private void PatientListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PatientListBox.SelectedIndex != -1)
            {
                selectedPatient = PatientListBox.SelectedItem as Patient;
            }
            else
            {
                selectedPatient = null;
            }
            UpdateButtons();
        }

        private void FirstPatientButton_Click(object sender, RoutedEventArgs e)
        {
            if (appointment.FirstPatient == null && selectedPatient != null)
            {
                appointment.FirstPatient = selectedPatient;
            }
            else
            {
                appointment.FirstPatient = null;
            }
            UpdateButtons();
        }

        private void SecondPatientButton_Click(object sender, RoutedEventArgs e)
        {
            if (appointment.SecondPatient == null && selectedPatient != null)
            {
                if (appointment.FirstPatient != selectedPatient)
                {
                    appointment.SecondPatient = selectedPatient;
                }
            }
            else
            {
                appointment.SecondPatient = null;
            }
            UpdateButtons();
        }

        #endregion

        private void CreateAppointmentButton_Click(object sender, RoutedEventArgs e)
        {
            if (appointment.IsValid)
            {
                try
                {
                    if (demographics.AddAppointment(appointment) == 0)
                    {
                        MessageBox.Show("Successfully Created the appointment!", 
                            "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                        if (NavigationService.CanGoBack)
                        {
                            NavigationService.GoBack();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Failed to create the appointment", 
                            "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
