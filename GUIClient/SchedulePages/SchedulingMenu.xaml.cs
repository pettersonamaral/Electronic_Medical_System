﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Billing;
using Demographics;
using Interpreter;

namespace GUIClient.SchedulePages
{
    /// <summary>
    /// Interaction logic for SchedulingMenu.xaml
    /// </summary>
    public partial class SchedulingMenu : Page
    {
        #region Attributes and Interfaces

        // Interfaces
        IDemographics demographics = ClientInterpreter.Instance;
        IBilling billing = ClientInterpreter.Instance;
        
        // Attributes - Date Time Related
        DateTime selectedDate = DateTime.MinValue;
        int[] timeSlots = { 9, 10, 11, 13, 14, 15 };

        // Attributes - Appointment
        Appointment selectedAppointment = null;

        #endregion

        #region Page Builder

        public SchedulingMenu()
        {
            InitializeComponent();
            BuildTimeSlotList();
            CheckTimeSlotSelection();
        }

        #endregion

        #region Code Behind

        private void BuildTimeSlotList()
        {
            TimeSlotListBox.Items.Clear();

            if (selectedDate.Equals(DateTime.MinValue)) { return; }

            List<Appointment> apps = demographics.FindAppointments(selectedDate.Date, selectedDate.AddDays(1));

            bool weekDay = false;
            if (selectedDate.DayOfWeek <= DayOfWeek.Friday && selectedDate.DayOfWeek >= DayOfWeek.Monday)
            {
                weekDay = true;
            }

            for (int i = 0; i < timeSlots.Length; i++)
            {
                if (!weekDay && i > 1) { break; }

                string resTag = "";
                if (apps != null)
                {
                    foreach (Appointment ap in apps)
                    {
                        if (ap.GetDayAndTime().Equals(selectedDate.Date.AddHours(timeSlots[i])))
                        {
                            resTag = " - Scheduled";
                        }
                    }
                }

                string tstr = $"{timeSlots[i]}:00 to {timeSlots[i] + 1}:00 {resTag}";
                TimeSlotListBox.Items.Add(tstr);
            }
        }

        private void CheckTimeSlotSelection()
        {
            TimeSlotDetailsTextBlock.Text = "";
            NewAppButton.IsEnabled = false;
            UpdateAppButton.IsEnabled = false;
            selectedAppointment = null;

            int index = TimeSlotListBox.SelectedIndex;
            if (index == -1) { return; }

            List<Appointment> apps = demographics.FindAppointments(selectedDate.Date, selectedDate.AddDays(1));

            selectedDate = selectedDate.Date;
            selectedDate = selectedDate.AddHours(timeSlots[index]);

            TimeSlotDetailsTextBlock.Text = $"TimeSlot Selected: {selectedDate.ToString()}";

            bool scheduled = false;
            if (apps != null)
            {
                foreach (Appointment a in apps)
                {
                    if (a.GetDayAndTime().Equals(selectedDate))
                    {
                        selectedAppointment = a;
                        scheduled = true;
                    }
                }
            }

            PrintAppointmentInformation();

            if (scheduled)
            {
                // Display Summary
            }

        }


        private void PrintAppointmentInformation()
        {
            UpdateAppButton.IsEnabled = false;

            if (selectedAppointment == null)
            {
                TimeSlotDetailsTextBlock.Text += $"\n\nThis time slot is free";
                NewAppButton.IsEnabled = true;
                return;
            }

            bool disableUpdate = false;

            // Check First Patient
            TimeSlotDetailsTextBlock.Text += $"\n\nFirst Patient:\n" +
                $"{selectedAppointment.FirstPatient.GetFname()} " +
                $"{selectedAppointment.FirstPatient.GetLname()}";
            bool hasBill = AddBillInfo(selectedAppointment.FirstPatient);
            if (hasBill) { disableUpdate = true; }

            // Check Second Patient
            if (selectedAppointment.SecondPatient != null)
            {
                TimeSlotDetailsTextBlock.Text += $"\n\nSecond Patient:\n" +
                    $"{selectedAppointment.SecondPatient.GetFname()} " +
                    $"{selectedAppointment.SecondPatient.GetLname()}";
                hasBill = AddBillInfo(selectedAppointment.FirstPatient);
                if (hasBill) { disableUpdate = true; }
            }

            UpdateAppButton.IsEnabled = !disableUpdate;
        }

        private bool AddBillInfo(Patient patient)
        {
            bool retCode = false;
            TimeSlotDetailsTextBlock.Text += $"\n\nBill Details:";

            Bill bill = billing.FindBill(patient.PatientID,
                selectedAppointment.GetAppointmentID());

            if (bill == null)
            {
                TimeSlotDetailsTextBlock.Text += $"\nNo bills found";
            }
            else
            {
                retCode = true;
                TimeSlotDetailsTextBlock.Text += $"\n{bill.ToString(false)}";
            }
            return retCode;
        }


        #endregion

        #region Navigation - Events

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if(NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void CalendarDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime? calendPicked = CalendarDatePicker.SelectedDate;
            if (calendPicked == null)
            {
                selectedDate = DateTime.MinValue;
            }
            else
            {
                selectedDate = calendPicked.Value;
            }
            BuildTimeSlotList();
        }

        private void TimeSlotListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckTimeSlotSelection();
        }

        private void NewAppButton_Click(object sender, RoutedEventArgs e)
        {
            if (!selectedDate.Equals(DateTime.MinValue))
            {
                NavigationService.Navigate(new NewAppointment(selectedDate));
                CalendarDatePicker.SelectedDate = null;
            }
        }

        private void UpdateAppButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedAppointment != null)
            {
                NavigationService.Navigate(new UpdateAppointment(selectedAppointment));
            }
        }

        #endregion
    }
}
