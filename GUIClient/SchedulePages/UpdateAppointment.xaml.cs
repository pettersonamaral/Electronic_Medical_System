﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Demographics;

namespace GUIClient.SchedulePages
{
    /// <summary>
    /// Interaction logic for UpdateAppointment.xaml
    /// </summary>
    public partial class UpdateAppointment : Page
    {
        Appointment appointment = null;

        public UpdateAppointment(Appointment existingAppointment)
        {
            appointment = existingAppointment;
            InitializeComponent();
        }


        #region Event Handler

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            while (NavigationService.CanGoBack) { NavigationService.GoBack(); }
        }

        private void CalendarDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TimeSlotListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void UpdateAppointmentButton_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion
    }
}
