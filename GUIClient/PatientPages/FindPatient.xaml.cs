﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Demographics;
using Interpreter;

namespace GUIClient.PatientPages
{
    class PatientInfo
    {
        public Patient Patient { get; set; }

        public override string ToString()
        {
            if (Patient == null)
            {
                throw new Exception("Invalid patient.");
            }

            return string.Format("{0}, {1} - {2}", Patient.GetLname(), Patient.GetFname(), Patient.Address.GetPhoneNumber());
        }
    }

    /// <summary>
    /// Interaction logic for FindPatient.xaml
    /// </summary>
    public partial class FindPatient : Page
    {
        IDemographics demographics = ClientInterpreter.Instance;

        private Page MainWindow;
        public FindPatient(Page page)
        {
            MainWindow = page;
            InitializeComponent();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(MainWindow);
            //use the following code to go back to the main menu
            while(NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private bool CheckFindPatient(Patient patient)
        {
            bool FindPatientResult = patient.SetLname(tBoxFindLname.Text);
            if(FindPatientResult != true)
            {
                MessageBox.Show("Patient could not be found!");
            }
            return FindPatientResult;
        }

        private void FindPatients()
        {
            Patient patient = new Patient();
            if (CheckFindPatient(patient) != true)
            {
                return;
            }
            List<Patient> FindPatientResult = demographics.FindPatient(patient);

            if (FindPatientResult == null)
            {
                MessageBox.Show("Patient Could not be found.");
                return;
            }

            listPatientResult.Items.Clear();
            foreach (Patient FoundPatient in FindPatientResult)
            {
                listPatientResult.Items.Add(new PatientInfo() { Patient = FoundPatient });
            }
        }


        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            FindPatients();
        }

        private void btnDetails_Click(object sender, RoutedEventArgs e)
        {
            PatientInfo patient = (PatientInfo)listPatientResult.SelectedItem;
            NavigationService.Navigate(new PatientPages.AddPatientPage(MainWindow, AddPagePatientOptions.DetailsPatient, patient.Patient));
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            PatientInfo patient = (PatientInfo)listPatientResult.SelectedItem;
            NavigationService.Navigate(new PatientPages.AddPatientPage(MainWindow, AddPagePatientOptions.UpdatePatient, patient.Patient));
           
        }

        private void tBoxFindLname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                FindPatients();
            }
        }
    }
}
