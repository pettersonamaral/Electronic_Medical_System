﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUIClient.PatientPages
{
    /// <summary>
    /// Interaction logic for PatientMenu.xaml
    /// </summary>
    public partial class PatientMenu : Page
    {
        private Page MainWindow;
        public PatientMenu(Page page)
        {
            MainWindow = page;
            InitializeComponent();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void AddPatientButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PatientPages.AddPatientPage(MainWindow, AddPagePatientOptions.AddPatient, null));
        }

        private void FindPatientButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new PatientPages.FindPatient(MainWindow));
        }


    }
}
