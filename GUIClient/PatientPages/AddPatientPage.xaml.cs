﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Humaira use those three
using Demographics;
using Billing;
using Interpreter;


namespace GUIClient.PatientPages
{
    public enum AddPagePatientOptions
    {
        AddPatient,
        DetailsPatient,
        UpdatePatient
    }

    /// <summary>
    /// Interaction logic for AddPatientPage.xaml
    /// </summary>
    public partial class AddPatientPage : Page
    {
        // Humaira use this
        IDemographics demographics = ClientInterpreter.Instance;
        AddPagePatientOptions patientMode;
        int PatientID = 0;
        int AddressID = 0;

        private Page MainWindow;
        public AddPatientPage(Page page, AddPagePatientOptions options, Patient patient)
        {
            MainWindow = page;
            InitializeComponent();

            patientMode = options;
            if (options == AddPagePatientOptions.DetailsPatient)
            {
                // Show detils patient
                if (patient == null)
                {
                    throw new Exception("Patient is null");
                }

                btnSave.IsEnabled = false;
                // This will populate the Add patient form
                PatientForm(patient);

            }
            else if (options == AddPagePatientOptions.UpdatePatient)
            {
                // Show update Patient
                if (patient == null)
                {
                    throw new Exception("Patient is null");
                }

                btnSave.IsEnabled = true;//Button will be enabled
              // This will populate the Add patient form
                PatientForm(patient);
                PatientID = patient.PatientID; //this will provide the patientID and complete the patient object for update patient
                AddressID = patient.Address.GetAdressID();//this will provide the address ID and complete the patient object for update patient

            }
            else
            {
                // Add new patient
                btnSave.IsEnabled = true;
            }
            
        }

        /// <summary>
        /// This function is filling the patient add form again then called from 
        /// update/details as the same page is being re-used
        /// </summary>
        /// <param name="patient"></param>
        private void PatientForm(Patient patient)
        {
            tBoxFname.Text = patient.GetFname();
            tBoxLname.Text = patient.GetLname();
            if(patient.GetMinitial()!=' ')
            {
                tBoxMInitial.Text = patient.GetMinitial().ToString();
            }
            selectDob.SelectedDate = patient.DateOfBirth;
            tBoxHcn.Text = patient.GethCN();
            cBoxSex.Text = CheckSex(patient);
            tBoxHeadOfHouse.Text = patient.GetHeadOfHouse();
            tBoxAddress.Text = patient.Address.GetAddressLineOne();
            tBoxCity.Text = patient.Address.GetCity();
            cBoxProvince.Text = patient.Address.GetProvince();
            tBoxPhoneNo.Text = patient.Address.GetPhoneNumber();
        }

        private string CheckSex(Patient patient)
        {
            string patientSex = "";
            string stored = patient.GetSex();
            if (stored == "F")
            {
                patientSex = "Female";
            }
            else if (stored == "M")
            {
                patientSex = "Male";

            }
            else if (stored == "H")
            {
                patientSex = "Hermaphrodite";
            }

            else if(stored == "I")
            {
                patientSex = "Intersex";
            }

            return patientSex;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(MainWindow);
        }

        private bool AddFirstName(Patient patient)
        {
            bool FnameResult = patient.SetFname(tBoxFname.Text);
            if (FnameResult != true)
            {
                MessageBox.Show("Please enter a valid First Name");
            }
            return FnameResult;
            
        }

        private bool AddLastName(Patient patient)
        {
            bool LnameResult = patient.SetLname(tBoxLname.Text);
            if(LnameResult !=true)
            {
                MessageBox.Show("Please enter a valid Last Name");
            }
            return LnameResult;
        }

        private bool AddMidInitial(Patient patient)
        {
            if(tBoxMInitial.Text.Length >1)
            {
                MessageBox.Show("Middle initial has to be a character");
                return false;
            }
            else if(tBoxMInitial.Text.Length ==1)
            {
                bool MidInitialResult = patient.SetMinitial(tBoxMInitial.Text[0]);
                if (MidInitialResult !=true)
                {
                    MessageBox.Show("Please enter a valid character");
                }
                return MidInitialResult;
            }
            return true;
        }

        private bool AddDoB(Patient patient)
        {
            if(selectDob.SelectedDate.HasValue)
            {
                patient.DateOfBirth = selectDob.SelectedDate.Value;
                return true;
            }
            
            // bool DobResult = patient.DateOfBirth(selectDob.SelectedDate.ToString);
            return false;
        }




        private bool AddHcN(Patient patient)
        {
            bool HcNResult = patient.SethCN(tBoxHcn.Text);
            if(HcNResult !=true)
            {
                MessageBox.Show("Please enter a valid HCN");
            }

            return HcNResult;
        }

        private bool AddSex(Patient patient)
        {
            bool AddSexResult = patient.SetSex(cBoxSex.Text);
            if(AddSexResult != true)
            {
                MessageBox.Show("Please enter a valid Gender. F for Female, M for Male, I for Intersex and H for Hermaphrodite");
            }
            return AddSexResult;
        }

        private bool AddHeadOfHouse(Patient patient)
        {
            bool HeadOfHouseResult = patient.SetHeadOfHouse(tBoxHeadOfHouse.Text);
            if(HeadOfHouseResult!=true)
            {
                MessageBox.Show("Invalid input");
            }
            return HeadOfHouseResult;
        }

        private bool AddAddress(Patient patient)
        {
            bool AddressResult = patient.Address.SetAdressLineOne(tBoxAddress.Text);
            if(AddressResult !=true)
            {
                MessageBox.Show("Please enter a valid address");
            }
            return AddressResult;
        }

        private bool AddCity(Patient patient)
        {
            bool CityResult = patient.Address.SetCity(tBoxCity.Text);
            if(CityResult!=true)
            {
                MessageBox.Show("Please enter a valid city");
            }
            return CityResult;
        }

        private bool AddProvince(Patient patient)
        {

        bool ProvResult = patient.Address.SetProvince(cBoxProvince.SelectionBoxItem.ToString());
            if(ProvResult !=true)
            {
                MessageBox.Show("Please enter a valid province");
            }
            return ProvResult;
        }

        private bool AddPhoneNum(Patient patient)
        {
            bool PhNumResult = patient.Address.SetPhoneNumber(tBoxPhoneNo.Text);
            if(PhNumResult!= true)
            {
                MessageBox.Show("Please enter a valid phone number. Format (000)000-0000");
            }
            return PhNumResult;
        }

        //This button will validate all the text boxes
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = new Patient();
            //AddFirstName(patient);
            if(AddFirstName(patient)!=true)
            {
                return;
            }

            if(AddLastName(patient)!=true)
            {
                return;
            }

            if(AddMidInitial(patient)!=true)
            {
                return;
            }

            if(AddDoB(patient)!=true)
            {
                return;
            }
            if(AddHcN(patient) !=true)
            {
                return;
            }
            if(AddSex(patient)!=true)
            {
                return;
            }
            if(AddHeadOfHouse(patient)!=true)
            {
                return;
            }
            if(AddAddress(patient)!=true)
            {
                return;
            }
            if(AddCity(patient)!=true)
            {
                return;
            }
            if(AddProvince(patient)!=true)
            {
                return;
            }
            if(AddPhoneNum(patient)!=true)
            {
                return;
            }

            if (patientMode == AddPagePatientOptions.AddPatient)
            {
                int AddPatientResult = demographics.AddPatient(patient);
                if (AddPatientResult != 0)
                {
                    MessageBox.Show("Unable to add patient. Error Code: " + AddPatientResult.ToString());
                }
                else
                {
                    MessageBox.Show("Patient Successfully Added!");
                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }

                }
            }
            else if (patientMode == AddPagePatientOptions.UpdatePatient)
            {
                patient.PatientID = PatientID;
                patient.Address.SetAdressID(AddressID);
                int UpdatePatientResult = demographics.UpdatePatient(patient);
                if (UpdatePatientResult != 0)
                {
                    MessageBox.Show("Unable to update patient. Error Code: " + UpdatePatientResult.ToString());
                }
                else
                {
                    MessageBox.Show("Patient Successfully Updated!");
                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }

                }
            }


        }

        private void tBoxHeadOfHouse_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tBoxHeadOfHouse.Text == "") { return; }

            Patient p = new Patient();
            
            if (!p.SetHeadOfHouse(tBoxHeadOfHouse.Text)) { return; }

            Patient head = demographics.FindPatient(tBoxHeadOfHouse.Text);

            if (head == null) { return; }

            tBoxAddress.Text = head.Address.GetAddressLineOne();
            tBoxCity.Text = head.Address.GetCity();
            cBoxProvince.Text = head.Address.GetProvince();
            tBoxPhoneNo.Text = head.Address.GetPhoneNumber();
        }
    }
}
