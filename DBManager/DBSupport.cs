﻿/*****************************************************************************************************
* FILE : 		Support.cs
* PROJECT : 	SQ-II Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2018-Apr-01

* DESCRIPTION/Requirements: 

This class provides all functionality to DBManager

*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Support;
using System.Reflection;

namespace DBManager
{
    public partial class DBSupport
    {
        #region Attributes
        static private SqlTransaction trans = null;
        static private SqlCommand cmd = new SqlCommand();
        static private DBConnection dBConnection = DBConnection.Instance;
        private static Logging logging = new Logging();

        #endregion


        #region constants
        private const Int32 SUCCESS_GENERIC_RETURN = 0;
        #endregion


        #region Petterson's Methodos
        #region Petterson's Methodos that return Int32
        /// <summary>
        /// Just a simple test to see one way to access and get information from DB. 
        /// </summary>
        /// <returns></returns>
        public static Int32 CountPatients()
        {
            Int32 statusReturn = -1;
            String query = "";
            query = "SELECT * FROM Patient";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                //trans = dBConnection.Conn.BeginTransaction();    //Start a transaction
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection
                // cmd.Transaction = trans;

                cmd.CommandText = query;                           //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                DataTable dt = new DataTable();
                da.Fill(dt);


                statusReturn = dt.Rows.Count;


                //foreach (DataRow row in dt.Rows)                  //Traverse rows
                //{
                //    numberOfPacients += 1;
                //}



                // Commit this trans
                // trans.Commit();                                  //Commit a transaction
                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }

            return statusReturn;
        }


        /// <summary>
        /// Return the next available index for address table. Returns an Int32 as the number for the next AddressID
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 GetNextAddressID()
        {
            Int32 numberToReturn = -1;
            String query = "SELECT MAX(addressID)+1 AS AddressID FROM Address";
            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                            //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    if (row["AddressID"].ToString() == "") { numberToReturn = 1; }
                    else { numberToReturn = Convert.ToInt32(row["AddressID"]); }
                }

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }
            return numberToReturn;
        }


        /// <summary>
        /// Return the next available index for patiant table. Returns an Int32 as the number for the next PatientID. 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 GetNextPatientID()
        {
            Int32 numberToReturn = -1;
            String query = "SELECT MAX(patientID)+1 AS patientID FROM Patient";

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                            //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    if (row["AddressID"].ToString() == "") { numberToReturn = 1; }
                    else { numberToReturn = Convert.ToInt32(row["patientID"]); }
                }

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }
            return numberToReturn;
        }


        /// <summary>
        /// Return the next available index for Appointment table. Returns an Int32 as the number for the next AppointmentID. 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 GetNextAppointment()
        {
            Int32 numberToReturn = -1;
            String query = "SELECT MAX(appointmentID)+1 AS appointmentID FROM Appointment";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                            //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    if (row["appointmentID"].ToString() == "") { numberToReturn = 1; }
                    else { numberToReturn = Convert.ToInt32(row["appointmentID"]); }
                }

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }
            return numberToReturn;
        }


        /// <summary>
        /// Counts how many Patients exist by an hcnNumber. Returns an Int32 with the number of finds
        /// </summary>
        /// <param name="hcnNumber"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 CountPatients(String hcnNumber)
        {
            DataTable dt = new DataTable();
            Int32 statusReturn = -1;
            String query = "SELECT * FROM Patient WHERE HCN = '";

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                query = query + hcnNumber.ToString() + "'";         //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dt);
                statusReturn = dt.Rows.Count;


                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("CountPatients by hcnNumber - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// Counts how many Patients exist by an patientID. Returns an Int32 with the number of finds
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 CountPatient(Int32 patientID)
        {
            DataTable dt = new DataTable();
            Int32 statusReturn = -1;
            String query = "SELECT * FROM Patient WHERE patientID = '";

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                query = query + patientID.ToString() + "'";         //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dt);
                statusReturn = dt.Rows.Count;

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("CountPatient by patientID - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// Counts how many Appointment exist by an appointmentDate. Returns an Int32 with the number of finds
        /// </summary>
        /// <param name="appointmentDate"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 CountAppointment(DateTime appointmentDate)
        {
            DataTable dt = new DataTable();
            Int32 statusReturn = -1;
            String query = "SELECT * FROM Appointment WHERE dateAndTime = '";

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                query = query + appointmentDate.ToString() + "'";   //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dt);
                statusReturn = dt.Rows.Count;

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("CountAppointment by appointmentDate - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// Counts how many Appointment exist by an appointmentID. Returns an Int32 with the number of finds
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static Int32 CountAppointment(Int32 appointmentID)
        {
            DataTable dt = new DataTable();
            Int32 statusReturn = -1;
            String query = "SELECT * FROM Appointment WHERE appointmentID = '";

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                query = query + appointmentID.ToString() + "'";     //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dt);
                statusReturn = dt.Rows.Count;

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("CountAppointment by appointmentID - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// This method will add an appointment. Returns 1 if it was Done OK.
        /// </summary>
        /// <returns></returns>
        public static Int32 InsertANewAppointment(DateTime appointmentDate, Int32 patientID, Int32 patientID2 = 0)

        {
            Int32 statusReturn = SUCCESS_GENERIC_RETURN;
            Int32 nextAppointment = 0;
            String query = "";
            Boolean goodToGo = false;

            try
            {
                #region Appointment
                //Check if it already exists an appointment on this date and if exists a patient base on that ID
                if (CountAppointment(appointmentDate) == 0 &&
                    (CountPatient(patientID) == 1)) { goodToGo = true; }
                else { return statusReturn; }

                if (patientID2 != 0)
                {
                    if ((CountPatient(patientID2) == 1 )
                        && (patientID != patientID2))  { goodToGo = true; }
                    else { return statusReturn; }
                }
                

                //Get the next available appointmentID
                nextAppointment = GetNextAppointment();

                if (nextAppointment > 0) { goodToGo = true; }//Simple check if it is greater than zero.
                else { return statusReturn; }


                dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                                   //Open connection
                trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                cmd.Connection = dBConnection.Conn;                         //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;


                query = "INSERT [dbo].[Appointment] ([dateAndTime]) " +
                        "VALUES ( @date)";

                cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query

                //Add Parameters                                            //cmd.Parameters.Add("@patientID"     , SqlDbType.Int);
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@date", SqlDbType.DateTimeOffset);


                //Add values                                                //cmd.Parameters["@patientID"].Value  = patientID;
                cmd.Parameters["@date"].Value = appointmentDate;

                //Runs a conditional Insert 
                if ((goodToGo == true) &&
                    (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }
                #endregion


                #region Appointment Line Patient#1
                if (statusReturn == 1) //If is insert appontment 
                {
                    query = "INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) " +
                        "VALUES ( @appointmentID, @patientID)";

                    cmd.CommandText = query;                                //Load command.CommandText Attribute with value from query

                    //Add Parameters                                        //cmd.Parameters.Add("@patientID"     , SqlDbType.Int);
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@appointmentID"     , SqlDbType.Int);
                    cmd.Parameters.Add("@patientID"         , SqlDbType.Int);

                    //Add values                                            //cmd.Parameters["@patientID"].Value  = patientID;
                    cmd.Parameters["@appointmentID"].Value  = nextAppointment;
                    cmd.Parameters["@patientID"].Value      = patientID;

                    //Runs a conditional Insert 
                    if ((goodToGo == true) &&
                        (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }
                }
                #endregion


                #region Appointment Line Patient#2
                if ((statusReturn == 1) && (patientID2 != 0)) //If is insert appontment 
                {
                    query = "INSERT [dbo].[AppointmentLine] ([appointmentID], [patientID]) " +
                        "VALUES ( @appointmentID, @patientID)";

                    cmd.CommandText = query;                                //Load command.CommandText Attribute with value from query

                    //Add Parameters                                        //cmd.Parameters.Add("@patientID"     , SqlDbType.Int);
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@appointmentID", SqlDbType.Int);
                    cmd.Parameters.Add("@patientID", SqlDbType.Int);

                    //Add values                                            //cmd.Parameters["@patientID"].Value  = patientID;
                    cmd.Parameters["@appointmentID"].Value = nextAppointment;
                    cmd.Parameters["@patientID"].Value = patientID2;

                    //Runs a conditional Insert 
                    if ((goodToGo == true) &&
                        (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }
                }
                #endregion
                if (statusReturn == 1)
                {
                    trans.Commit();                                         //Commit the transaction
                }
                else { trans.Rollback(); }


                dBConnection.Conn.Close();                                  //Close connection
                dBConnection.Disconnect();                                  //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("InsertANewAppointment - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// This method will add an patiant. Returns 1 if it was Done OK. 
        /// </summary>
        /// <returns>the number of rows affected </returns>
        /// <exception cref="Exception"/>
        public static Int32 InsertANewAPatient(String HCN, String lName, String fName, Char mInitial, DateTime dateOfBirth, String sex, String headOfHouse, Int32 addressID)
        {
            //Method variables
            Int32 statusReturn = SUCCESS_GENERIC_RETURN;
            String query = "";
            Boolean goodToGo = false;

            try
            {
                if (CountPatients(HCN) == 0) { goodToGo = true; }           //Check if it already exists
                else { return statusReturn; }

                dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                                   //Open connection
                trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                cmd.Connection = dBConnection.Conn;                         //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;


                query = "INSERT [dbo].[Patient] ([HCN], [lastName], [firstName], [mInitial], [dateBirth], [gender], [headOfHouse], [addressID]) " +
                        "VALUES ( @HCN, @lName, @fName, @mInitial, @dateOfBirth, @sex, @headOfHouse, @addressID)";


                cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query


                //Add Parameters                                            //cmd.Parameters.Add("@patientID"     , SqlDbType.Int);
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@HCN"           , SqlDbType.NVarChar, 12);
                cmd.Parameters.Add("@lName"         , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@fName"         , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@mInitial"      , SqlDbType.NChar, 1);
                cmd.Parameters.Add("@dateOfBirth"   , SqlDbType.DateTime);
                cmd.Parameters.Add("@sex"           , SqlDbType.NChar, 10);
                cmd.Parameters.Add("@headOfHouse"   , SqlDbType.NChar, 12);
                cmd.Parameters.Add("@addressID"     , SqlDbType.Int);

                //Add values                                                //cmd.Parameters["@patientID"].Value  = patientID;
                cmd.Parameters["@HCN"].Value        = HCN;
                cmd.Parameters["@lName"].Value      = lName;
                cmd.Parameters["@fName"].Value      = fName;
                cmd.Parameters["@mInitial"].Value   = mInitial;
                cmd.Parameters["@dateOfBirth"].Value= dateOfBirth;
                cmd.Parameters["@sex"].Value        = sex;
                cmd.Parameters["@headOfHouse"].Value= headOfHouse;
                cmd.Parameters["@addressID"].Value  = addressID;



                //Runs a conditional Insert 
                if ((goodToGo == true) && 
                    (dBConnection.ConnectionStatus==true)) {statusReturn = cmd.ExecuteNonQuery();}

                if (statusReturn == 1)
                { 
                    trans.Commit();                                         //Commit the transaction
                }
                else { trans.Rollback(); }   
                

                dBConnection.Conn.Close();                                  //Close connection
                dBConnection.Disconnect();                                  //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("InsertANewAPatient - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// This method will add a new address. Returns 1 if it was Done OK.
        /// I've added HCN because I need to test if already exists another
        /// patient with this HCN, If not I will be sure that it is not duplicated. 
        /// </summary>
        /// <param name="adressLine1"></param>
        /// <param name="adressLine2"></param>
        /// <param name="city"></param>
        /// <param name="province"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="HCN"></param>
        /// <returns>the number of rows affected </returns>
        /// <exception cref="Exception"/>
        public static Int32 InsertANewAddress(String adressLine1, String adressLine2, String city, String province, String phoneNumber, String HCN)
        {
            //Method variables
            Int32 statusReturn      = SUCCESS_GENERIC_RETURN;
            String query            = "";
            Boolean goodToGo        = false;

            try
            {
                if (CountPatients(HCN) == 0) { goodToGo = true; }           //Check if it already exists a patient with this HCN, a new address just can happens with there return 0.
                else { return statusReturn; }

                dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                                   //Open connection
                trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                cmd.Connection  = dBConnection.Conn;                        //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;


                query = "INSERT Address([addressLine1], [addressLine2], [city], [province], [numPhone]) " +
                        "VALUES( @adressLine1, @adressLine2, @city, @province, @phoneNumber)";


                cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query


                //Add Parameters                                            //cmd.Parameters.Add("@addressID"     , SqlDbType.Int);
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@adressLine1"   , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@adressLine2"   , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@city"          , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@province"      , SqlDbType.NChar, 2);
                cmd.Parameters.Add("@phoneNumber"   , SqlDbType.NChar, 13);

                //Add values                                                //cmd.Parameters["@addressID"].Value    = addressID;
                cmd.Parameters["@adressLine1"].Value  = adressLine1;
                cmd.Parameters["@adressLine2"].Value  = adressLine2;
                cmd.Parameters["@city"].Value         = city;
                cmd.Parameters["@province"].Value     = province;
                cmd.Parameters["@phoneNumber"].Value  = phoneNumber;




                //Runs a conditional Insert 
                if (goodToGo) { statusReturn = cmd.ExecuteNonQuery(); }

                if (statusReturn == 1)
                {
                    trans.Commit();                                         //Commit the transaction
                }
                else { trans.Rollback(); }     
                

                dBConnection.Conn.Close();                                  //Close connection
                dBConnection.Disconnect();                                  //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("InsertANewAddress - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// This method will update and existente Appointment. Returns 1 if it was Done OK. 
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <param name="appointmentDate"></param>
        /// <param name="patientID"></param>
        /// <param name="patientID2"></param>
        /// <returns>the number of rows affected </returns>
        /// <exception cref="Exception"/>
        public static Int32 UpdateAppointment(Int32 appointmentID, DateTime appointmentDate, Int32 patientID, Int32 patientID2 = 0)
        {
            //Method variables
            Int32 statusReturn = 0;
            String query = "";
            Boolean goodToGo = false;
            DataTable dt = null;

            Int32 myAppointmentID = 0;
            DateTime myDayAndTime = DateTime.MinValue;
            Int32 myPatientID01 = -1, myPatientID02 = 0;

            try
            {
                #region  UpdateAppointment Initialization
                //Check if it already exists an appointment on this date and if exists a patient base on that ID
                dt = FindAppointment(appointmentID);                                      //Check if it already exists
                if (dt.Rows.Count == 1)             { goodToGo = true; }
                else { return statusReturn; }

                //Retive data from AppointmentLine
                dt = FindAnAppointmentLine(appointmentID);
                if (dt.Rows.Count > 0) { goodToGo = true; }
                else { return statusReturn; }

                //Grab the data from AppointmentLine datatable
                switch (dt.Rows.Count) 
                {
                    case 1://If has just a row. 
                        myAppointmentID = Convert.ToInt32(dt.Rows[0][0]);
                        myPatientID01 = Convert.ToInt32(dt.Rows[0][1]);
                        break;
                    case 2:
                        myAppointmentID = Convert.ToInt32(dt.Rows[0][0]);
                        myPatientID01 = Convert.ToInt32(dt.Rows[0][1]);
                        if (Convert.ToInt32(dt.Rows[1][0]) == myAppointmentID)
                        { myPatientID02 = Convert.ToInt32(dt.Rows[1][1]); }
                        break;
                    default:
                        statusReturn = 0;
                        return statusReturn;
                }
                #endregion

                #region  Talking with the Database
                //Change the date from an appointment, where patient01 is the same and have no patient02
                if (dt.Rows.Count == 1)
                { 
                    dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                    dBConnection.Conn.Open();                                   //Open connection
                    trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                    cmd.Connection = dBConnection.Conn;                         //Load command.Connection Attribute with value from connection
                    cmd.Transaction = trans;


                    query = "UPDATE [dbo].[Appointment] " +
                            "SET [dateAndTime] = @date " +
                            "WHERE Appointment.appointmentID = '" + myAppointmentID.ToString() + "'";

                    cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query

                    //Add Parameters                                            
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@date", SqlDbType.DateTimeOffset);

                    //Add values                                                
                    cmd.Parameters["@date"].Value = appointmentDate;

                    //Runs a conditional Insert 
                    if ((goodToGo == true) &&
                        (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }

                    //-------------------------------------------------------------------------------------------------------
                    //   -----   AppointmentLine #1
                    if (statusReturn == 1)
                    {
                        statusReturn = 0;
                        query = "UPDATE [dbo].[AppointmentLine] " +
                                "SET [appointmentID] = @appointmentID " +
                                ", [patientID] = @patientID " +
                                "WHERE appointmentID = '" + myAppointmentID.ToString() + "' AND " + " patientID = '" + myPatientID01.ToString() + "'";

                        cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query

                        //Add Parameters                                            
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@appointmentID", SqlDbType.Int);
                        cmd.Parameters.Add("@patientID", SqlDbType.Int);

                        //Add values                                                
                        cmd.Parameters["@appointmentID"].Value = appointmentID;
                        cmd.Parameters["@patientID"].Value = patientID;


                        //Runs a conditional Insert 
                        if ((goodToGo == true) &&
                            (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }
                    }
                }
                else if (dt.Rows.Count == 2)
                {
                    dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                    dBConnection.Conn.Open();                                   //Open connection
                    trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                    cmd.Connection = dBConnection.Conn;                         //Load command.Connection Attribute with value from connection
                    cmd.Transaction = trans;


                    query = "UPDATE [dbo].[Appointment] " +
                            "SET [dateAndTime] = @date " +
                            "WHERE Appointment.appointmentID = '" + myAppointmentID.ToString() + "'";

                    cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query

                    //Add Parameters                                            
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@date", SqlDbType.DateTimeOffset);

                    //Add values                                                
                    cmd.Parameters["@date"].Value = appointmentDate;

                    //Runs a conditional Insert 
                    if ((goodToGo == true) &&
                        (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }


                    //-------------------------------------------------------------------------------------------------------
                    //   -----   AppointmentLine #1
                    if (statusReturn == 1)
                    {
                        statusReturn = 0;
                        query = "UPDATE [dbo].[AppointmentLine] " +
                                "SET [appointmentID] = @appointmentID " +
                                ", [patientID] = @patientID " +
                                "WHERE appointmentID = '" + myAppointmentID.ToString() + "' AND " + " patientID = '" + myPatientID01.ToString() + "'";

                        cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query

                        //Add Parameters                                            
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@appointmentID", SqlDbType.Int);
                        cmd.Parameters.Add("@patientID", SqlDbType.Int);

                        //Add values                                                
                        cmd.Parameters["@appointmentID"].Value = appointmentID;
                        cmd.Parameters["@patientID"].Value = patientID;


                        //Runs a conditional Insert 
                        if ((goodToGo == true) &&
                            (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }

                        //-------------------------------------------------------------------------------------------------------
                        //-------------------------------------------------------------------------------------------------------
                        //   -----   AppointmentLine #2
                        if (statusReturn == 1)
                        {
                            statusReturn = 0;
                            query = "UPDATE [dbo].[AppointmentLine] " +
                                "SET [appointmentID] = @appointmentID " +
                                ", [patientID] = @patientID " +
                                "WHERE appointmentID = '" + myAppointmentID.ToString() + "' AND " + " patientID = '" + myPatientID02.ToString() + "'";

                            cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query

                            //Add Parameters   
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@appointmentID", SqlDbType.Int);
                            cmd.Parameters.Add("@patientID", SqlDbType.Int);

                            //Add values                                                
                            cmd.Parameters["@appointmentID"].Value = appointmentID;
                            cmd.Parameters["@patientID"].Value = patientID2;

                            //Runs a conditional Insert 
                            if ((goodToGo == true) &&
                                (dBConnection.ConnectionStatus == true)) { statusReturn = cmd.ExecuteNonQuery(); }
                        }
                    }
                }
                #endregion

                if (statusReturn == 1)
                {
                    trans.Commit();                                         //Commit the transaction
                }
                else { trans.Rollback(); }


                dBConnection.Conn.Close();                                  //Close connection
                dBConnection.Disconnect();                                  //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("UpdateAppointment - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }


        /// <summary>
        /// This method will update and existente Patient. Returns 1 if it was Done OK. 
        /// </summary>
        /// <returns>the number of rows affected </returns>
        /// <exception cref="Exception"/>
        public static Int32 UpdatePatient(Int32 patientID, String HCN, String lName, String fName, Char mInitial, DateTime dateOfBirth, String sex, String headOfHouse)
        {
            //Method variables
            Int32 statusReturn  = SUCCESS_GENERIC_RETURN;
            String query        = "";
            Boolean goodToGo    = false;
            DataTable dt        = null;

            try
            {
                dt = FindPatient(HCN);                                      //Check if it already exists
                if (dt.Rows.Count == 1) { goodToGo = true; }
                else { return statusReturn; }

                dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                                   //Open connection
                trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                cmd.Connection = dBConnection.Conn;                         //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;


                query = "UPDATE [dbo].[Patient] " +
                        "SET [HCN] = @HCN" +
                        ", [lastName] = @lName" +
                        ", [firstName] = @fName" +
                        ", [mInitial] = @mInitial" +
                        ", [dateBirth] = @dateOfBirth" +
                        ", [gender] = @sex" +
                        ", [headOfHouse] = @headOfHouse " +
                        "WHERE Patient.patientID = '" + patientID.ToString() + "'";


                cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query


                //Add Parameters                                            //cmd.Parameters.Add("@patientID"     , SqlDbType.Int);
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@HCN"               , SqlDbType.NVarChar, 12);
                cmd.Parameters.Add("@lName"             , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@fName"             , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@mInitial"          , SqlDbType.NChar, 1);
                cmd.Parameters.Add("@dateOfBirth"       , SqlDbType.DateTime);
                cmd.Parameters.Add("@sex"               , SqlDbType.NChar, 10);
                cmd.Parameters.Add("@headOfHouse"       , SqlDbType.NChar, 12);
                                                                            //cmd.Parameters.Add("@addressID"     , SqlDbType.Int);

                //Add values                                                //cmd.Parameters["@patientID"].Value  = patientID;
                cmd.Parameters["@HCN"].Value            = HCN;
                cmd.Parameters["@lName"].Value          = lName;
                cmd.Parameters["@fName"].Value          = fName;
                cmd.Parameters["@mInitial"].Value       = mInitial;
                cmd.Parameters["@dateOfBirth"].Value    = dateOfBirth;
                cmd.Parameters["@sex"].Value            = sex;
                cmd.Parameters["@headOfHouse"].Value    = headOfHouse;
                                                                            //cmd.Parameters["@addressID"].Value      = addressID;



                //Runs a new extra test connection
                if (dBConnection.ConnectionStatus == true) { statusReturn = cmd.ExecuteNonQuery(); }

                if (statusReturn == 1)
                {
                    trans.Commit();                                         //Commit the transaction
                }
                else { trans.Rollback(); }


                dBConnection.Conn.Close();                                  //Close connection
                dBConnection.Disconnect();                                  //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("UpdatePatient - [Failure]."));
                throw ex;
            }

            return statusReturn;
        }


        /// <summary>
        /// This method will update and existente Address. Returns 1 if it was Done OK. 
        /// </summary>
        /// <returns>the number of rows affected </returns>
        /// <exception cref="Exception"/>
        public static Int32 UpdateAddress(Int32 addressID, String adressLine1, String adressLine2, String city, String province, String phoneNumber)
        {
            //Method variables
            Int32 statusReturn = SUCCESS_GENERIC_RETURN;
            String query = "";
            Boolean goodToGo = false;
            DataTable dt = null;

            try
            {
                dt = FindAddress(addressID);                                //Check if it already exists um patient with this HCN, a new address just can happens with there return 0. 
                if (dt.Rows.Count == 1) { goodToGo = true; }

                dBConnection.Connect();                                     //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                                   //Open connection
                trans = dBConnection.Conn.BeginTransaction();               //Start a transaction

                cmd.Connection = dBConnection.Conn;                         //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;

                query = "UPDATE [dbo].[Address] " +
                            "SET [addressLine1] = @adressLine1" +
                            ", [addressLine2] = @adressLine2" +
                            ", [city] = @city" +
                            ", [province] = @province" +
                            ", [numPhone] = @phoneNumber " +
                            "WHERE Address.addressID = '" + addressID.ToString() + "'";

                cmd.CommandText = query;                                    //Load command.CommandText Attribute with value from query


                //Add Parameters                                            //cmd.Parameters.Add("@addressID"     , SqlDbType.Int);
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@adressLine1"       , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@adressLine2"       , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@city"              , SqlDbType.NVarChar, 60);
                cmd.Parameters.Add("@province"          , SqlDbType.NChar, 2);
                cmd.Parameters.Add("@phoneNumber"       , SqlDbType.NChar, 13);

                //Add values                                                //cmd.Parameters["@addressID"].Value    = addressID;
                cmd.Parameters["@adressLine1"].Value    = adressLine1;
                cmd.Parameters["@adressLine2"].Value    = adressLine2;
                cmd.Parameters["@city"].Value           = city;
                cmd.Parameters["@province"].Value       = province;
                cmd.Parameters["@phoneNumber"].Value    = phoneNumber;


                //Runs a new extra test connection
                if (dBConnection.ConnectionStatus == true) { statusReturn = cmd.ExecuteNonQuery(); }

                if (statusReturn == 1)
                {
                    trans.Commit();                                         //Commit the transaction
                }
                else { trans.Rollback(); }


                dBConnection.Conn.Close();                                  //Close connection
                dBConnection.Disconnect();                                  //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("UpdateAddress - [Failure]."));
                throw ex;
            }
            return statusReturn;
        }
        #endregion


        #region Petterson's Methodos that return DataTable
        /// <summary>
        /// This function returns a result table of a search into Appointment table by (DateTime begin, DateTime end)
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindAppointment(DateTime begin, DateTime end)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM Appointment WHERE dateAndTime between '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + begin + "' and '" + end + "'";     //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("FindAppointment by (DateTime begin, DateTime end) - [Failure]."));
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search into Appointment table by appointmentID
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindAppointment(Int32 appointmentID)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM Appointment WHERE appointmentID = '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + appointmentID.ToString() + "'";    //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("FindAppointment by appointmentID- [Failure]."));
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search in Appointment table by patientID
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindAppointments(Int32 patientID)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT Appointment.* FROM Appointment " +
                "INNER JOIN AppointmentLine ON AppointmentLine.appointmentID = Appointment.appointmentID " +
                "AND AppointmentLine.patientID = '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + patientID.ToString() + "'";    //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("FindAppointment by patientID- [Failure]."));
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search into AppointmentLine table by appointmentID
        /// </summary>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindAnAppointmentLine(Int32 appointmentID)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM AppointmentLine WHERE appointmentID = '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + appointmentID.ToString() + "'";    //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("AppointmentLine by appointmentID- [Failure]."));
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search into address table by addressID
        /// </summary>
        /// <param name="addressID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindAddress(Int32 addressID)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM Address WHERE addressID = '" ;

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                query = query + addressID.ToString() + "'";         //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("FindAddress by addressID - [Failure]."));
                throw ex;
            }
            return dataRetrieved;            
        }


        /// <summary>
        /// This function returns a result table of a search into address table by hcnNumberD
        /// </summary>
        /// <param name="hcnNumberD"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindAddress(String hcnNumberD)
        {
            DataTable dataRetrieved = new DataTable();
            String query            = "SELECT Address.* FROM Address INNER JOIN Patient ON Address.addressID = Patient.addressID and Patient.HCN = '";

            try
            {
                dBConnection.Connect();                             //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                           //Open connection
                cmd.Connection = dBConnection.Conn;                 //Load command.Connection Attribute with value from connection

                query = query + hcnNumberD.ToString() + "'";        //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("FindAddress by hcnNumberD - [Failure]."));
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search into patient table by hcnNumber
        /// </summary>
        /// <param name="hcnNumber"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindPatient(String hcnNumber)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM Patient WHERE HCN = '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + hcnNumber.ToString() + "'";        //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search into patient table by patientID
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindPatient(Int32 patientID)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM Patient WHERE patientID = '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + patientID.ToString() + "'";        //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }
            return dataRetrieved;
        }


        /// <summary>
        /// This function returns a result table of a search into patient table by Last name
        /// </summary>
        /// <param name="patientID"></param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static DataTable FindPatientByLastName(String patientLName)
        {
            DataTable dataRetrieved = new DataTable();
            String query = "SELECT * FROM Patient WHERE LastName LIKE '";

            try
            {
                dBConnection.Connect();                            //Call Connect method from DBConnection Class
                dBConnection.Conn.Open();                          //Open connection
                cmd.Connection = dBConnection.Conn;                //Load command.Connection Attribute with value from connection

                query = query + patientLName + "%'";                //Load command.CommandText Attribute with value from query

                //Run the query argument into a DataAdapter and then fill the datatable object
                SqlDataAdapter da = new SqlDataAdapter(query, dBConnection.Conn);
                da.Fill(dataRetrieved);

                dBConnection.Conn.Close();                          //Close connection
                dBConnection.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                throw ex;
            }
            return dataRetrieved;
        }
        #endregion
        #endregion


        #region Support Methods
        /// <summary>
        /// Method to return the Connection String value. 
        /// </summary>
        /// <param name="val"> Connection Name </param>
        /// <returns></returns>
        /// <exception cref="Exception"/>
        public static String ConnectionVal(String val)
        {
            return ConfigurationManager.ConnectionStrings[val].ConnectionString;
        }


        /// <summary>
        /// This method will log an exception
        /// </summary>
        /// <param name="ex">The exception caught</param>
        /// <see cref="ServerMain.cs, Victor code."/>
        private static void DBSupportException(Exception ex)
        {
            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, ex.Message); //Call the class
        }
        #endregion
    }
}
