﻿/*****************************************************************************************************
* FILE : 		DBConnection.cs
* PROJECT : 	SQ-II Term Project 
* PROGRAMMER : 	ANASTASIIA KORNEVA, HUMAIRA SIDDIQA, PAULO CASADO, PETTERSON AMARAL, VICTOR LAURIA 
* FIRST VERSION:2018-Apr-03

* DESCRIPTION/Requirements: 

This class provides all functionality to DBConnection

*****************************************************************************************************/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Support;

namespace DBManager
{
    public sealed class DBConnection
    {
        #region Properties
        public bool ConnectionStatus    { get; set; }                                   //Connection Status either Connected = True or Disconnected = False
        public DataTable ChartSource    { get; set; }                                   //Chart Source
        public DataTable SelectClause   { get; set; }                                   //Datareader
        public String HostDBService     { get; set; }                                   //Hold the current connection provider
        public String DbName            { get; set; }                                   //Database name
        public SqlConnection Conn       { get; set; }                                   //Database Connection 
        #endregion


        #region Attributes and Constants
        public const String CONNECTEDSTATUS = "Connected";
        public const String DISCONNECTEDSTATUS = "Disconnected";
        public const String DBNAME = "Database name: ";
        private SqlCommand      cmd = new SqlCommand();                                 //Database Command
        private static volatile DBConnection instance;                                  //Implementation Strategy - Multithreaded Singleton
        private static object syncRoot = new Object();                                  //Implementation Strategy - Multithreaded Singleton
        private static Logging logging = new Logging();

        #endregion


        #region Constructor and Singleton

        /// <summary>
        /// Default constructor
        /// </summary>
        private DBConnection()
        {
            ConnectionStatus = false;
            HandleException(new Exception("DBConnection - instantiated successfully"));
        }


        // --- Starts Singleton Pattern
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        public static DBConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new DBConnection();
                    }
                }

                return instance;
            }
        }
        // --- ends Singleton Pattern
        #endregion

        #region Methods
        /// <summary>
        /// Method to handle the connection 
        /// </summary>       
        /// <returns>bool</returns>
        public bool Connect()
        {
            bool setReturMethod = false;
            ConnectionStatus = false;
            Conn = new SqlConnection();

            try
            {               
                Conn.ConnectionString = DBSupport.ConnectionVal("SQII_EMS");
                DbName = Conn.Database.ToString();

                ConnectionStatus = true;
                setReturMethod = true;
            }
            catch (Exception e)
            {
                setReturMethod = false;
                ConnectionStatus = false;
                throw e;
            }
            return setReturMethod;
        }


        /// <summary>
        /// Method to handle the disconnection
        /// </summary>
        /// <returns></returns>
        public bool Disconnect()
        {
            bool setReturMethod = false;

            try
            {
                Conn.Dispose();
                cmd.Dispose();
                ConnectionStatus = false;
                setReturMethod = true;
                HostDBService = "";
                DbName = "";
            }
            catch (Exception e)
            {
                throw e;
            }
            return setReturMethod;
        }

        /// <summary>
        /// This method will log an exception
        /// </summary>
        /// <param name="x">The exception caught</param>
        /// 
        private void HandleException(Exception x)
        {
            logging.SetProgram(MethodBase.GetCurrentMethod().DeclaringType, x.Message); //Call the class
        }
        #endregion
    }
}
