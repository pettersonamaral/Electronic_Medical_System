﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billing;
using Support;

namespace DBManager
{
    public partial class DBSupport
    {
        #region Constants [JUST FOR REFERENCE]

        // ==================== BILLING.TRACKER     (4500-4599) ======================

        public const int RETURN_BILLING_TRACKER_SUCESS = 4500;
        public const int RETURN_BILLING_TRACKER_FAILURE = -4500;
        public const int ERR_SET_NEW_LIST_OF_BILLS = -4501;
        public const int ERR_SET_NEW_LIST_OF_AVAILABLE_SERVICES = -4502;
        public const int ERR_SET_NEW_SERVICE_LIST = -4503;
        public const int ERR_FIND_BILL = -4504;
        public const int ERR_ADD_BILL = -4505;
        public const int ERR_FAILED_UPDATE_SERVICE_LIST = -4506;
        public const int ERR_DATE_AVAILABLE_SERVICE_LIST = -4507;
        public const int ERR_VALUE_AVAILABLE_SERVICE_LIST = -4508;
        public const int ERR_SET_VALUE_NEW_SERVICE = -4509;
        public const int ERR_SET_FEE_CODE_NEW_SERVICE = -4510;
        public const int ERR_UPDATE_BILL = -4511;                                // PC_UPDATE: Newly Added constant
        public const int ERR_ADD_SERVICELIST= -4512;                             // PC_UPDATE: Newly Added constant

        #endregion Constants

        #region Paulo's Methods

        #region Unit Tested [Happy Path]

        /// <summary>
        /// Adds new Bill record to the database. If sucessfull, returns 4500 code, otherwise returns -4505 code
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int AddBill(Bill obj)
        {
            var retCode = RETURN_BILLING_TRACKER_SUCESS;
            var cmdCode = 0;
            var queryBuilder = new StringBuilder();

            // Check parameter validity
            if (obj == null) { retCode = ERR_ADD_BILL; }

            // Organize INSERT parameters
            var thePatientID = obj.Patient.PatientID;
            var theAppointmentID = obj.Appointment.GetAppointmentID();
            var theFlagRecall = obj.FlagRecall;
            var theStatusDone = obj.StatusDone;

            try
            {
                dBConnection.Connect(); //Call Connect method from DBConnection Class
                dBConnection.Conn.Open(); //Open connection
                trans = DBConnection.Instance.Conn.BeginTransaction(); //Start a transaction

                cmd.Connection =
                    DBConnection.Instance.Conn; //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;

                queryBuilder.Append("INSERT INTO[dbo].[Bill] ");
                queryBuilder.Append(" ([patientID] ");
                queryBuilder.Append(" ,[appointmentID] ");
                queryBuilder.Append(" ,[flagRecall] ");
                queryBuilder.Append(" ,[statusDone]) ");
                queryBuilder.Append("VALUES ");
                queryBuilder.Append("  (@patientID ");
                queryBuilder.Append(" ,@appointmentID ");
                queryBuilder.Append(" ,@flagRecall ");
                queryBuilder.Append(" ,@statusDone) ");


                // Build command
                cmd.CommandText = queryBuilder.ToString(); //Load command.CommandText Attribute with value from query

                // Add command parameters
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@patientID", SqlDbType.Int).Value = thePatientID;
                cmd.Parameters.Add("@appointmentID", SqlDbType.Int).Value = theAppointmentID;
                cmd.Parameters.Add("@flagRecall", SqlDbType.Int).Value = theFlagRecall;
                cmd.Parameters.Add("@statusDone", SqlDbType.Bit).Value = theStatusDone;

                //Runs a new extra test connection
                if (DBConnection.Instance.ConnectionStatus == true) { cmdCode = cmd.ExecuteNonQuery(); }
                if (cmdCode > 0) { trans.Commit(); }
                else { trans.Rollback(); }

                dBConnection.Conn.Close(); //Close connection
                dBConnection.Disconnect(); //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("Failed to Add Bill..."));
                retCode = ERR_ADD_BILL;
                throw ex;
            }
            finally
            {
                // Assures connection will be closed
                dBConnection.Conn.Close();
            }
            return retCode;
        }

        /// <summary>
        ///  This method assess the database to retrieve a set of Service data based on the Service ID
        /// </summary>
        /// <param name="serviceID"></param>
        /// <returns></returns>
        public static DataTable FindService(int serviceID)
        {
            // Locals
            var dt = new DataTable();
            var querybuilder = new StringBuilder();

            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query
                querybuilder.Append("SELECT serviceID, feeCode, effectiveDate, value FROM Service ");
                querybuilder.AppendFormat("WHERE serviceID = '{0}'", serviceID);

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return dt;
        }

        /// <summary>
        /// This method asses the database to retrieve the list of available MOH services
        /// </summary>
        /// <returns></returns>
        public static DataTable FindServices(string serviceCode = "")
        {
            // Locals
            var dt = new DataTable();
            var querybuilder = new StringBuilder();

            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query
                querybuilder.Append("SELECT serviceID, feeCode, effectiveDate, value FROM Service ");
                querybuilder.AppendFormat("WHERE feeCode LIKE '{0}%'", serviceCode);

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return dt;
        }

        /// <summary>
        /// Retrieve ServiceLine info from database according to a billID
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        public static DataTable FindServiceList(int billID)
        {
            // Locals
            var dt = new DataTable();
            var querybuilder = new StringBuilder();

            try
            {

                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query
                querybuilder.Append("SELECT serviceID, billID, quantity, status FROM ServiceLine ");
                querybuilder.AppendFormat("WHERE billID = {0}", billID);

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return dt;
        }

        /// <summary>
        /// Retrieve ServiceLine info from database according to a billID and serviceID
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        public static DataTable FindServiceList(int billID, int serviceID)
        {
            // Locals
            var dt = new DataTable();
            var querybuilder = new StringBuilder();

            try
            {

                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query
                querybuilder.Append("SELECT serviceID, billID, quantity, status FROM ServiceLine ");
                querybuilder.AppendFormat("WHERE billID = {0} AND serviceID = {1}", billID, serviceID);

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return dt;
        }

        /// <summary>
        /// Return Bill Information from database
        /// </summary>
        /// <param name="patientID"></param>
        /// <param name="appointmentID"></param>
        /// <returns></returns>
        public static DataTable FindBill(int patientID, int appointmentID)
        {
            // Locals
            var dt = new DataTable();
            var querybuilder = new StringBuilder();

            try
            {

                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query
                querybuilder.Append("SELECT billID, patientID, appointmentID, flagRecall, statusDone FROM Bill ");
                querybuilder.AppendFormat("WHERE patientID = {0}  AND appointmentID = {1} ", patientID, appointmentID);

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return dt;
        }

        /// <summary>
        /// Add ServiceLine to the database, based on passe billID and ServiceList obj.
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int AddServiceList(int billID, ServiceList obj)
        {
            // Locals
            var retCode = RETURN_BILLING_TRACKER_SUCESS;
            var cmdCode = 0;
            var dtBill = new DataTable();
            var theBill = new Bill();
            var queryBuilder = new StringBuilder();
            var thePatientID = 0;
            var theAppointmentID = 0;


            try
            {
                // Check parameter validity
                if (obj == null) { retCode = ERR_ADD_SERVICELIST; }

                // Find equivalent Bill
                dtBill = FindBill(billID);

                // check validity of Bill Datatable
                if (dtBill.Rows.Count == 0) { return ERR_ADD_SERVICELIST; }

                // Extract specific Bill Info
                //Travese the table and grab Bill table data
                foreach (DataRow row in dtBill.Rows)
                {
                    theBill.BillID = Convert.ToInt32(row[0]);
                    thePatientID = Convert.ToInt32(row[1]);
                    theAppointmentID = Convert.ToInt32(row[2]);
                    theBill.FlagRecall = Convert.ToInt32(row[3]);
                    theBill.StatusDone = Convert.ToBoolean(row[4]);
                }

                // Organize INSERT parameters
                var theServiceID = obj.Service.ServiceID;
                var theBillID = billID;
                var theQuantity = obj.Quantity;
                var theStatus = obj.Status;


                // Set connection
                dBConnection.Connect(); //Call Connect method from DBConnection Class
                dBConnection.Conn.Open(); //Open connection
                trans = DBConnection.Instance.Conn.BeginTransaction(); //Start a transaction

                cmd.Connection = DBConnection.Instance.Conn; //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;

                queryBuilder.Append("INSERT INTO[dbo].[ServiceLine] ");
                queryBuilder.Append(" ([serviceID]");
                queryBuilder.Append(" ,[billID]");
                queryBuilder.Append(" ,[quantity]");
                queryBuilder.Append(" ,[status])");
                queryBuilder.Append(" VALUES");
                queryBuilder.Append(" (@serviceID");
                queryBuilder.Append(" ,@billID");
                queryBuilder.Append(" ,@quantity");
                queryBuilder.Append(" ,@status)");

                // Build command
                cmd.CommandText = queryBuilder.ToString(); //Load command.CommandText Attribute with value from query

                // Add command parameters
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@serviceID", SqlDbType.Int).Value = theServiceID;
                cmd.Parameters.Add("@billID", SqlDbType.Int).Value = theBillID;
                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = theQuantity;
                cmd.Parameters.Add("@status", SqlDbType.NVarChar, 4).Value = theStatus;

                //Runs a new extra test connection
                if (DBConnection.Instance.ConnectionStatus == true) { cmdCode = cmd.ExecuteNonQuery(); }
                if (cmdCode > 0) { trans.Commit(); retCode = RETURN_BILLING_TRACKER_SUCESS; }
                else { trans.Rollback(); }


                dBConnection.Conn.Close(); //Close connection
                dBConnection.Disconnect(); //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("Failed to Add ServiceList..."));
                retCode = ERR_ADD_BILL;
                throw ex;
            }
            finally
            {
                // Assures connection will be closed
                dBConnection.Conn.Close();
            }
            return retCode;

        }

        /// <summary>
        ///  Update Bill according to passed obj bill properties. If UPDATE sucessfull, returns 4500 code, otherwhise returns -4511 code
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int UpdateBill(Bill obj)
        {
            // Locals
            var retCode = RETURN_BILLING_TRACKER_SUCESS;
            var cmdCode = 0;
            var queryBuilder = new StringBuilder();
            var dt = new DataTable();

            // Check Objects Validity
            if (obj == null) { return ERR_UPDATE_BILL; }
            if (obj.Patient == null) { return ERR_UPDATE_BILL; }
            if (obj.Appointment == null) { return ERR_UPDATE_BILL; }

            // Bill object parameters
            var theBillID = obj.BillID;
            var thePatientID = obj.Patient.PatientID;
            var theAppointmentID = obj.Appointment.GetAppointmentID();
            var theFlagRecall = obj.FlagRecall;
            var theStatusDone = obj.StatusDone;

            try
            {
                // Updates Bill Table ========================================================================================================

                // Check if Bill exists
                dt = FindBill(thePatientID, theAppointmentID);
                if (dt.Rows.Count == 0) { return ERR_UPDATE_BILL; }

                // Setup Connection
                dBConnection.Connect(); //Call Connect method from DBConnection Class
                dBConnection.Conn.Open(); //Open connection
                trans = DBConnection.Instance.Conn.BeginTransaction(); //Start a transaction

                cmd.Connection = DBConnection.Instance.Conn; //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;

                // Build UPDATE Statement
                queryBuilder.Append("UPDATE[dbo].[Bill] ");
                queryBuilder.Append("  SET[patientID] = @patientID");
                queryBuilder.Append(" ,[appointmentID] = @appointmentID");
                queryBuilder.Append(" ,[flagRecall] = @flagRecall");
                queryBuilder.Append(" ,[statusDone] = @statusDone");
                queryBuilder.Append(" WHERE billID = @billID");

                // Add command parameters
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@billID", SqlDbType.Int).Value = theBillID;
                cmd.Parameters.Add("@patientID", SqlDbType.Int).Value = thePatientID;
                cmd.Parameters.Add("@appointmentID", SqlDbType.Int).Value = theAppointmentID;
                cmd.Parameters.Add("@flagRecall", SqlDbType.Int).Value = theFlagRecall;
                cmd.Parameters.Add("@statusDone", SqlDbType.Bit).Value = theStatusDone;

                // Build command
                cmd.CommandText = queryBuilder.ToString();

                // Runs a new extra test connection \\ NOT SURE ABOUT THE NECESSITY OF THIS
                if (DBConnection.Instance.ConnectionStatus == true) { cmdCode = cmd.ExecuteNonQuery(); }
                if (cmdCode > 0) { trans.Commit(); }
                else { trans.Rollback(); }

                dBConnection.Conn.Close(); //Close connection
                dBConnection.Disconnect(); //Call Disonnect method from DBConnection Class


                // Updates Bill obj Service List Table ========================================================================================================

                // Delete current Service Lines FROM THE DATABASE... NOT FROM THE CURRENT OBJ
                retCode = CleanBillServiceLine(obj);

                // Add new service Lines from the obj
                foreach (var serviceLine in obj.Services) { retCode = AddServiceList(obj.BillID, serviceLine); }
                
                if(retCode > 0) {  retCode = RETURN_BILLING_TRACKER_SUCESS;}
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("UpdateAddress - [Failure]."));
                throw ex;
            }
            finally
            {
                dBConnection.Conn.Close();
            }
            return retCode;
        }

        /// <summary>
        /// Update service List records on database based on the billID and the ServiceList objs info. If successfull, returns 4500 code, otherwise returns -4506 code
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int UpdateServiceList(int billID, ServiceList obj)
        {
            // Locals
            var retCode = RETURN_BILLING_TRACKER_SUCESS;
            var dt = new DataTable();
            var queryBuilder = new StringBuilder();

            // Check objs/properties validity
            if (billID <= 0) { return ERR_FAILED_UPDATE_SERVICE_LIST; }
            if (obj == null) { return ERR_FAILED_UPDATE_SERVICE_LIST; }
            if (obj.Service == null) { return ERR_FAILED_UPDATE_SERVICE_LIST; }

            // Create local parameters for UPDATE statement
            var theServiceID = obj.Service.ServiceID.ToString();
            var theBillID = billID.ToString();
            var theQuantity = obj.GetQuantity();
            var theStatus = obj.Status.ToString();

            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build UPDATE Statement
                queryBuilder.Append("UPDATE ServiceLine ");
                queryBuilder.AppendFormat("SET serviceID = {0}, billID = {1}, quantity = {2}, status = {3}  WHERE serviceID = {0}  AND billID = {1}",
                    theServiceID, theBillID, theQuantity, theStatus);
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                retCode = ERR_FAILED_UPDATE_SERVICE_LIST;
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return retCode;
        }

        /// <summary>
        /// Return bill information from table Bill in the database according to the given billID
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        public static DataTable FindBill(int billID)
        {
            // Locals
            var dt = new DataTable();
            var querybuilder = new StringBuilder();

            try
            {

                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query
                querybuilder.Append("SELECT billID, patientID, appointmentID, flagRecall, statusDone FROM Bill ");
                querybuilder.AppendFormat("WHERE billID = {0}", billID);

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return dt;
        }

        /// <summary>
        /// Find Total Encounters Billed
        /// </summary>
        /// <param name="billingMonth"></param>
        /// <returns></returns>
        public static int FindNumberOfEncounters(DateTime billingMonthYear)
        {
            // Locals
            var numEncounters = 0;
            var dt = new DataTable();
            var querybuilder = new StringBuilder();
            var billingMonth = billingMonthYear.Month;
            var billingYear = billingMonthYear.Year;


            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query // [REFERENCE] https://stackoverflow.com/questions/851236/where-clause-to-find-all-records-in-a-specific-month
                querybuilder.Append("SELECT COUNT(billID) ");
                querybuilder.Append("FROM Bill ");
                querybuilder.Append("JOIN Appointment ");
                querybuilder.Append("ON Bill.appointmentID = Appointment.appointmentID ");
                querybuilder.AppendFormat("WHERE MONTH(dateAndTime) = {0} AND YEAR(dateAndTime) = {1}", billingMonth,billingYear);    // @month should be represented by a number between 1 and 12

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                // check if dt has any row
                if (dt.Rows.Count == 0) { return RETURN_BILLING_TRACKER_FAILURE; }

                foreach (DataRow row in dt.Rows)
                {
                    numEncounters = Convert.ToInt32(row[0]);
                }

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return numEncounters;
        }

        /// <summary>
        /// Find Total Billed Procedures (in Dollars)
        /// </summary>
        /// <param name="billingMonth"></param>
        /// <returns></returns>
        public static double FindTotalBilledProcedures(DateTime billingDate)
        {
            // Locals
            var billedProcedures = 0.0;
            var dt = new DataTable();
            var querybuilder = new StringBuilder();
            var numBillingMonth = billingDate.Month;
            var numBillingYear = billingDate.Year;

            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query // [REFERENCE] https://stackoverflow.com/questions/851236/where-clause-to-find-all-records-in-a-specific-month
                querybuilder.Append("SELECT SUM(value) ");
                querybuilder.Append("FROM Service ");
                querybuilder.Append("JOIN ServiceLine ");
                querybuilder.Append("ON Service.serviceID = ServiceLine.serviceID ");
                querybuilder.Append("JOIN Bill ");
                querybuilder.Append("ON ServiceLine.billID = Bill.billID ");
                querybuilder.Append("JOIN Appointment ");
                querybuilder.Append("ON Bill.appointmentID = Appointment.appointmentID ");
                querybuilder.AppendFormat("WHERE MONTH(dateAndTime) = {0} AND YEAR(dateAndTime) = {1} ", numBillingMonth, numBillingYear);    // @month should be represented by a number between 1 and 12

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                // check if dt has any row
                if (dt.Rows.Count == 0) { return RETURN_BILLING_TRACKER_FAILURE; }

                foreach (DataRow row in dt.Rows)
                {
                    billedProcedures = Convert.ToDouble(row[0]);
                }

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return billedProcedures;
        }

        /// <summary>
        /// Find Total Received (in Dollars)
        /// </summary>
        /// <param name="billingMonth"></param>
        /// <returns></returns>
        public static double FindReceivedTotal(DateTime billingDate)
        {
            // Locals
            var receivedTotal = 0.0;
            var statusPaid = "PAID";
            var dt = new DataTable();
            var querybuilder = new StringBuilder();
            var numBillingMonth = billingDate.Month;
            var numBillingYear = billingDate.Year;

            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query // [REFERENCE] https://stackoverflow.com/questions/851236/where-clause-to-find-all-records-in-a-specific-month
                querybuilder.Append("SELECT SUM(value) ");
                querybuilder.Append("FROM Service ");
                querybuilder.Append("JOIN ServiceLine ");
                querybuilder.Append("ON Service.serviceID = ServiceLine.serviceID ");
                querybuilder.Append("JOIN Bill ");
                querybuilder.Append("ON ServiceLine.billID = Bill.billID ");
                querybuilder.Append("JOIN Appointment ");
                querybuilder.Append("ON Bill.appointmentID = Appointment.appointmentID ");
                querybuilder.AppendFormat("WHERE MONTH(dateAndTime) = {0} AND YEAR(dateAndTime) = {1} AND ServiceLine.status = '{2}' ", numBillingMonth, numBillingYear, statusPaid);    // @month should be represented by a number between 1 and 12

                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                // check if dt has any row
                if (dt.Rows.Count == 0) { return RETURN_BILLING_TRACKER_FAILURE; }

                foreach (DataRow row in dt.Rows)
                {
                    receivedTotal = Convert.ToDouble(row[0]);
                }

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return receivedTotal;
        }

        /// <summary>
        /// Find number of encounters to follow-up (Integer Sum of FHCV abd CMOH count)
        /// </summary>
        /// <param name="BillingMonth"></param>
        /// <returns></returns>
        public static int FindNumOfEncountersToFollowUp(DateTime billingMonth)
        {
            // Locals
            var encountersFollowUp = 0;
            var statusFHCV = "FHCV";
            var statusCMOH = "CMOH";

            var dt = new DataTable();
            var querybuilder = new StringBuilder();
            var numbillingMonth = billingMonth.Month;
            var numbillingYear = billingMonth.Year;

            

            try
            {
                // Establishes Connection
                DBConnection.Instance.Connect();                            //Call Connect method from DBConnection Class
                DBConnection.Instance.Conn.Open();                          //Open connection
                cmd.Connection = DBConnection.Instance.Conn;                //Load command.Connection Attribute with value from connection

                // Build Query // [REFERENCE] https://stackoverflow.com/questions/851236/where-clause-to-find-all-records-in-a-specific-month
                querybuilder.Append("SELECT COUNT(Appointment.appointmentID) ");
                querybuilder.Append("FROM Service ");
                querybuilder.Append("JOIN ServiceLine ");
                querybuilder.Append("ON Service.serviceID = ServiceLine.serviceID ");
                querybuilder.Append("JOIN Bill ");
                querybuilder.Append("ON ServiceLine.billID = Bill.billID ");
                querybuilder.Append("JOIN Appointment ");
                querybuilder.Append(" ON Bill.appointmentID = Appointment.appointmentID ");
                querybuilder.AppendFormat("WHERE (YEAR(dateAndTime) = {0} AND MONTH(dateAndTime) = {1}) AND (ServiceLine.status = '{2}'  OR ServiceLine.status = '{3}')", numbillingYear, numbillingMonth, statusFHCV, statusCMOH);    // @month should be represented by a number between 1 and 12
                // Execute query and fill datatable
                SqlDataAdapter da = new SqlDataAdapter(querybuilder.ToString(), DBConnection.Instance.Conn);
                da.Fill(dt);

                // check if dt has any row
                if (dt.Rows.Count == 0) { return RETURN_BILLING_TRACKER_FAILURE; }

                foreach (DataRow row in dt.Rows)
                {
                    encountersFollowUp = Convert.ToInt32(row[0]);
                }

                DBConnection.Instance.Conn.Close();                          //Close connection
                DBConnection.Instance.Disconnect();                          //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                DBConnection.Instance.Conn.Close();
                DBConnection.Instance.Disconnect();
                throw ex;
            }
            finally
            {
                DBConnection.Instance.Conn.Close();
            }

            return encountersFollowUp;
        }

        #endregion Unit Tested



        #region Support Methods

        public static int CleanBillServiceLine(Bill obj)
        {
            // Locals
            var retCode = RETURN_BILLING_TRACKER_SUCESS;
            var cmdCode = 0;
            var queryBuilder = new StringBuilder();

            try
            {
                // Check parameter validity
                if (obj == null) { retCode = ERR_UPDATE_BILL; }

                // Set connection
                dBConnection.Connect(); //Call Connect method from DBConnection Class
                dBConnection.Conn.Open(); //Open connection
                trans = DBConnection.Instance.Conn.BeginTransaction(); //Start a transaction

                cmd.Connection = DBConnection.Instance.Conn; //Load command.Connection Attribute with value from connection
                cmd.Transaction = trans;

                queryBuilder.Append("DELETE FROM[dbo].[ServiceLine] ");
                queryBuilder.Append("WHERE billID = @billID");

                // Build command
                cmd.CommandText = queryBuilder.ToString(); //Load command.CommandText Attribute with value from query

                // Add command parameters
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@billID", SqlDbType.Int).Value = obj.BillID;


                //Runs a new extra test connection
                if (DBConnection.Instance.ConnectionStatus == true) { cmdCode = cmd.ExecuteNonQuery(); }
                if (cmdCode > 0) { trans.Commit(); }
                else { trans.Rollback(); }

                dBConnection.Conn.Close(); //Close connection
                dBConnection.Disconnect(); //Call Disonnect method from DBConnection Class
            }
            catch (Exception ex)
            {
                trans.Rollback();
                dBConnection.Conn.Close();
                dBConnection.Disconnect();
                DBSupportException(new Exception("Failed to lean Bill's ServiceLine from database..."));
                retCode = ERR_UPDATE_BILL;
                throw ex;
            }
            finally
            {
                // Assures connection will be closed
                dBConnection.Conn.Close();
            }
            return retCode;
        }


        #endregion Support Methods





        #endregion Paulo's Methods
    }
}

